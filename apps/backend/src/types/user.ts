import { Request } from "express";
import { SessionData } from "../constants/user";

export type UserVerifyType = {
  service: string;
  user_data: {
    email: string; //“email của user”,
    full_name: string; //”Full name”,
    additional?: {
      localAccountId: string; //“user ID trên microsoft”,
      homeAccountId: string; //“ID của tổ chức, ví dụ VBI trên microsoft”,
      scopes: string; //“user profile scope”
    };
  };
};

export type UserRedirectType = {
  id_token: string; //"id token",
  user_id: string; //“user id",
  user_access_token: string; //"Access token",
  user_refresh_token: string; //"Refresh token"
};

export type CRequest = {
  session: SessionData;
} & Request;
