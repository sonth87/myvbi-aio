import { Router } from "express";
import { categoryCtrl } from "../../controllers";

export const router: Router = Router();

router.route("/get-all-category").get(categoryCtrl.getAllCategoriesExternal);
