import { Router } from "express";
import { router as ConfigRouter } from "./config";
import { router as NewsRouter } from "./news";
import { router as CategoriesRouter } from "./categories";

export const router: Router = Router();

router.use(ConfigRouter);
router.use(NewsRouter);
router.use(CategoriesRouter);
