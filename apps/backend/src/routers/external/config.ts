import { Router } from "express";
import { getConfiguration } from "../../controllers/configController";
import { validator } from "../../middlewares/validator";
import { configRule } from "../../constants/validator";

export const router: Router = Router();

router.post("/configuration", validator(configRule), getConfiguration);
