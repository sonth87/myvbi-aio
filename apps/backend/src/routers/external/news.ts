import { Router } from "express";
import { newsCtrl } from "../../controllers";

export const router: Router = Router();

router.get("/get-news-by-category", newsCtrl.getNewsByCategoryExternal);
router.post("/get-news-by-ids", newsCtrl.getNewsByIdsExternal);
router.get("/get-news-detail/:slug", newsCtrl.getNewsDetailExternal);
