import { UploadController } from "../../controllers";
import { Router } from "express";
import multer from "multer";
import { auth } from "../../middlewares/auth";
import { PERMISSION } from "../../constants/permission";
const upload = multer();

export const router: Router = Router();

router.post(
  "/upload",
  auth(PERMISSION.UPLOAD),
  upload.any(),
  UploadController.uploadFile
);
