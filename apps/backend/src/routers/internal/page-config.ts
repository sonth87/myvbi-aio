import { Router } from "express";
import { PageController } from "../../controllers";
import { auth } from "../../middlewares/auth";
import { PERMISSION } from "../../constants/permission";

export const router: Router = Router();

router.get(
  "/component",
  auth(PERMISSION.PAGE_CONFIG_LIST),
  PageController.getAllPageConfig
);

router
  .route("/component/:componentKey")
  .get(auth(PERMISSION.PAGE_CONFIG_VIEW), PageController.getPageConfigByAttrs)
  .put(
    auth(PERMISSION.PAGE_CONFIG_UPDATE),
    PageController.updatePageConfigByAttrs
  );
