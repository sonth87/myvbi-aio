import { Router } from "express";
import { auth } from "../../middlewares/auth";
import { PERMISSION } from "../../constants/permission";
import { categoryCtrl } from "../../controllers";

export const router: Router = Router();

router.post(
  "/category",
  auth(PERMISSION.CATEGORY_CREATE),
  categoryCtrl.createCategory
);
router.get(
  "/category/getAllCategories",
  auth(PERMISSION.CATEGORY_LIST),
  categoryCtrl.getAllCategories
);
router.get(
  "/category/getAllCategoriesByStatus",
  auth(PERMISSION.CATEGORY_LIST),
  categoryCtrl.getAllCategoriesByStatus
);
router
  .route("/category/:id")
  .get(auth(PERMISSION.CATEGORY_VIEW), categoryCtrl.getCategoryById)
  .put(auth(PERMISSION.CATEGORY_UPDATE), categoryCtrl.updateCategory)
  .delete(auth(PERMISSION.CATEGORY_DELETE), categoryCtrl.deleteCategory);
