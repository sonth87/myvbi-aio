import { Router } from "express";
import { router as GeneralConfigRouter } from "./general-config";
import { router as ProductRouter } from "./products";
import { router as UploadRouter } from "./upload";
import { router as NewsRouter } from "./newsRouter";
import { router as CategoriesRouter } from "./categoriesRouter";
import { router as PageConfigRouter } from "./page-config";
import { router as UserRouter } from "./user";
import { router as LeaderRouter } from "./leaders";

export const router: Router = Router();

router.use(GeneralConfigRouter);
router.use(ProductRouter);
router.use(UploadRouter);
router.use(NewsRouter);
router.use(CategoriesRouter);
router.use(PageConfigRouter);
router.use(UserRouter);
router.use(LeaderRouter);
