import { Router } from "express";
import { AuthController } from "../../controllers";

export const authRouter = Router();

authRouter
  .route("/signin")
  .get(AuthController.login)
  .post(AuthController.doLogin);
authRouter.get("/signout", AuthController.logout);
authRouter.post("/auth-verify", AuthController.auth_verify);
authRouter.post("/auth-redirect", AuthController.auth_redirect);
