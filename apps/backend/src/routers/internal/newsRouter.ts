import { Router } from "express";
import { newsCtrl } from "../../controllers";
import { auth } from "../../middlewares/auth";
import { PERMISSION } from "../../constants/permission";

export const router: Router = Router();

router.post("/news", auth(PERMISSION.NEWS_CREATE), newsCtrl.createNews);
router.get("/news/getAllNews", auth(PERMISSION.NEWS_LIST), newsCtrl.getAllNews);
router.get(
  "/news/getAllNewsConfig",
  auth(PERMISSION.NEWS_LIST),
  newsCtrl.getAllNewsConfig
);
router.post(
  "/news/getAllNewsByIds",
  auth(PERMISSION.NEWS_LIST),
  newsCtrl.getNewsByIds
);
router
  .route("/news/:id")
  .get(auth(PERMISSION.NEWS_VIEW), newsCtrl.getNewsById)
  .put(auth(PERMISSION.NEWS_UPDATE), newsCtrl.updateNewsById)
  .delete(auth(PERMISSION.NEWS_DELETE), newsCtrl.deleteNews);
