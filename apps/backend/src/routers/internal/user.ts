import { Router } from "express";
import { UserController } from "../../controllers";
import { auth } from "../../middlewares/auth";
import { PERMISSION } from "../../constants/permission";

export const router = Router();

router.get(
  "/user/profile",
  auth(PERMISSION.PROFILE),
  UserController.getUserProfile
);
router.get("/users", auth(PERMISSION.USER_LIST), UserController.getAllUser);
router.post(
  "/users/create",
  auth(PERMISSION.USER_CREATE),
  UserController.createUser
);
router
  .route("/users/:userId")
  .get(auth(PERMISSION.USER_VIEW), UserController.getUserInfo)
  .put(auth(PERMISSION.USER_UPDATE), UserController.updateUser)
  .delete(auth(PERMISSION.USER_DELETE), UserController.deleteUser);
