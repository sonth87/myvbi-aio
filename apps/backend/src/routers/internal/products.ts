import { ProductController } from "../../controllers";
import { Router } from "express";
import { auth } from "../../middlewares/auth";
import { PERMISSION } from "../../constants/permission";

export const router: Router = Router();

router.get(
  "/products",
  auth(PERMISSION.PRODUCT_LIST),
  ProductController.getProducts
);
router.post(
  "/products/create",
  auth(PERMISSION.PRODUCT_CREATE),
  ProductController.createProduct
);
router
  .route("/products/:productId")
  .get(auth(PERMISSION.PRODUCT_VIEW), ProductController.getProductById)
  .delete(auth(PERMISSION.PRODUCT_DELETE), ProductController.deleteProduct);

router
  .route("/products/:productCode/:productId")
  .put(auth(PERMISSION.PRODUCT_UPDATE), ProductController.updateProduct);
