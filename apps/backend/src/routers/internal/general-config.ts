import { GeneralController } from "../../controllers";
import { Router } from "express";
import { auth } from "../../middlewares/auth";
import { PERMISSION } from "../../constants/permission";

export const router: Router = Router();

router.get(
  "/general-config",
  auth(PERMISSION.GENERAL_CONFIG),
  GeneralController.getGeneralConfig
);
router.post(
  "/general-config/update",
  auth(PERMISSION.GENERAL_CONFIG_UPDATE),
  GeneralController.updateGeneralConfig
);
