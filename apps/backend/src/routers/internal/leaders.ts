import { LeaderController } from "../../controllers";
import { Router } from "express";
import { auth } from "../../middlewares/auth";
import { PERMISSION } from "../../constants/permission";

export const router: Router = Router();

router.get(
  "/leaders",
  auth(PERMISSION.LEADER_LIST),
  LeaderController.getLeaders
);
router.post(
  "/leaders/create",
  auth(PERMISSION.LEADER_CREATE),
  LeaderController.createLeader
);
router
  .route("/leaders/:leadersId")
  .get(auth(PERMISSION.LEADER_VIEW), LeaderController.getLeaderById)
  .put(auth(PERMISSION.LEADER_UPDATE), LeaderController.updateLeader)
  .delete(auth(PERMISSION.LEADER_DELETE), LeaderController.deleteLeader);
