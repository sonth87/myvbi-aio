import { Schema } from "mongoose";
import { ProductsType } from "@repo/types/products";
import { CMSDB } from "../utils/mongo";
import { toJSON } from "./plugins/json";

const productsSchema = new Schema<ProductsType>({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  code: {
    type: String,
    required: true,
    unique: false,
    trim: true,
  },
  icon: {
    type: String,
    required: true,
  },
  link: {
    type: String,
    required: false,
  },
  description: String,
  visible: Boolean,
  promotion: String,
  tags: String,
  type: String,
  group: String,
});

productsSchema.plugin(toJSON);
const Products = CMSDB.model("products", productsSchema);

export { Products };
