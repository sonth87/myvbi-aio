import { Schema } from "mongoose";
import {
  CompanyDataType,
  GeneralConfigType,
  MetaDataType,
} from "@repo/types/general";
import { CMSDB } from "../utils/mongo";
import { toJSON } from "./plugins/json";

const generalConfigSchema = new Schema<GeneralConfigType>({
  meta: new Schema<MetaDataType>({
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    tags: String,
    thumbnail: {
      type: String,
      required: true,
    },
  }),
  script: Array<String>,
  company: new Schema<CompanyDataType>({
    name: {
      type: String,
      required: true,
    },
    shortName: String,
    logo: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    email: String,
    address: String,
    description: String,
  }),
});

generalConfigSchema.plugin(toJSON);
const GeneralConfig = CMSDB.model("general_configs", generalConfigSchema);
export { GeneralConfig };
