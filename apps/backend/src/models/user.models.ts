import { user } from "@repo/types";
import { Schema } from "mongoose";
import { toJSON } from "./plugins/json";
import { CMSDB } from "../utils/mongo";

const userSchema = new Schema<user.UserType>(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      unique: true,
      required: true,
      trim: true,
    },
    phone: String,
    address: String,
    avatar: String,
    department: String,
    title: String,
    status: { type: Boolean, default: false },
    permission: [String],
    deleted: { type: Boolean, default: false },
  },
  {
    timestamps: { createdAt: "createdAt", updatedAt: "updatedAt" },
  }
);

userSchema.plugin(toJSON);
const User = CMSDB.model("user", userSchema);

export { User };
