import { Schema, model } from "mongoose";
import { CMSDB } from "../utils/mongo";

const categorySchema = new Schema(
  {
    ma_node: {
      type: String, //mỗi 1 danh mục đều có thể là 1 node cha
    },
    ma_goc: {
      type: Schema.Types.ObjectId,
      ref: "categories", // mỗi 1 danh mục đều có thể là con cua 1 ma_node nào đó
    },
    tieu_de: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    slug: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    mo_ta: {
      value: {
        type: String,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    trang_thai: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    banner_category: {
      value: {
        type: String,
      },
      key_content: String,
    },
    link_banner_category: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);
const CategoriesModel = CMSDB.model("categories", categorySchema);

export default CategoriesModel;
