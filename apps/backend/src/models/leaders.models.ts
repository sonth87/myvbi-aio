import { Schema } from "mongoose";
import { CMSDB } from "../utils/mongo";
import { toJSON } from "./plugins/json";
import { LeadersType } from "@repo/types/leaders";

const leadersSchema = new Schema<LeadersType>({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  title: {
    type: String,
    required: true,
    trim: true,
  },
  avatar: {
    type: String,
    required: true,
  },
  shortDescription: {
    type: String,
  },
  description: String,
});

leadersSchema.plugin(toJSON);
const Leaders = CMSDB.model("leaders", leadersSchema);

export { Leaders };
