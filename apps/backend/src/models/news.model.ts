import { Schema } from "mongoose";
import { CMSDB } from "../utils/mongo";

const newsSchema = new Schema(
  {
    anh: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    anh_banner: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    link_banner: { type: String },
    he_thong: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    ma_dm: {
      value: {
        type: Schema.Types.ObjectId,
        ref: "categories",
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    mo_ta: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    mo_ta_tu_khoa: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    ngay_tao: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    nguoi_tao: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    noi_dung: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    slug: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    tag: {
      value: {
        type: String,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    thu_tu: {
      value: {
        type: Number,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    tieu_de: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    tom_tat: {
      value: {
        type: String,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    trang_thai: {
      value: {
        type: String,
        require: true,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    tu_khoa: {
      value: {
        type: String,
        trim: true,
      },
      key_content: {
        type: String,
        trim: true,
      },
    },
    ma_goc: { type: String },
    deleted: { type: Boolean, default: false },
  },
  {
    timestamps: true,
  }
);
const NewsModel = CMSDB.model("news", newsSchema);

export default NewsModel;
