import { Schema } from "mongoose";
import { toJSON } from "./plugins/json";
import { pages } from "@repo/types";
import { CMSDB } from "../utils/mongo";

const pageConfigSchema = new Schema<pages.PageCompType>({
  title: String,
  description: String,
  visible: Boolean,
  updatedAt: String,
  key: Schema.Types.Mixed,
  content: Schema.Types.Mixed,
});

pageConfigSchema.plugin(toJSON);

const PageConfig = CMSDB.model("page_configs", pageConfigSchema);

export { PageConfig };
