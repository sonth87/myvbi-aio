export const PAGES = {
  home: "home",
  invester: "invester",
  structure: "structure",
  award: "award",
  cosoyte: "cosoyte",
};

export const COMPS = {
  banner: "banner",
  top_products: "top_products",
  partner: "partner",
  protect_family: "protect_family",
  protect_company: "protect_company",
  why_vbi: "why_vbi",
  customer_opinion: "customer_opinion",
  qna: "qna",
  invester: "invester",
  structure: "structure",
  award: "award",
  news1: "news1",
  news2: "news2",
  cosoyte_news: "cosoyte_news",
};

export const compsInPage = {
  [PAGES.home]: [
    COMPS.banner,
    COMPS.top_products,
    COMPS.protect_family,
    COMPS.protect_company,
    COMPS.why_vbi,
    COMPS.customer_opinion,
    COMPS.qna,
    COMPS.news1,
    COMPS.news2,
  ],
  [PAGES.structure]: [COMPS.structure],
  [PAGES.invester]: [],
  [PAGES.award]: [COMPS.award],
  [PAGES.cosoyte]: [COMPS.cosoyte_news],
};

export const RequestByRule = ["common", "page", "product", "leader"] as const;
