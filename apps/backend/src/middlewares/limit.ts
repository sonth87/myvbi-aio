import { Request, Response } from "express";
import rateLimit from "express-rate-limit"; // gioi han request

const apiLimiter = rateLimit({
  windowMs: 60 * 1000, // thoi gian theo chu ky
  max: 60, // request toi da theo chu ky
  handler: (req: Request, res: Response) => {
    res.status(429).send({
      status: 500,
      message: "Too many requests!",
    });
  },
  skip: (req: Request, res: Response) => {
    // bypass
    if (req.ip === "::ffff:127.0.0.1") return true;
    return false;
  },
});

export { apiLimiter };
