import { NextFunction, Response } from "express";
import { CRequest } from "../types/user";
import { HttpStatusCode } from "axios";
import jwt from "jsonwebtoken";
import { HKEY } from "../constants/user";
import { userService } from "../services";
import {
  PERMISSION,
  exceptPermission,
  exceptUser,
} from "../constants/permission";
import { deCriptWithHA, verify } from "../utils/auth";

export const auth =
  (permission) => async (req: CRequest, res: Response, next: NextFunction) => {
    const valid = await validate(req, res, next, permission);
    if (valid) next();
    else
      res.json({
        status: HttpStatusCode.Unauthorized,
        message: "Không có quyền truy cập",
      });
  };

export const validate = async (
  req: CRequest,
  res?: Response,
  next?: NextFunction,
  permission?: string
) => {
  const AuthTk = req.headers.authorization;
  const tk = AuthTk?.split(" ")?.[1];
  const o = req.get("origin");

  try {
    const decode = (await jwt.verify(tk, HKEY)) as jwt.JwtPayload;

    if (
      (decode?.token || decode?.username) &&
      (exceptPermission.includes(permission) ||
        exceptUser.includes(decode?.username))
    ) {
      req.session.isAuthenticated = true;
      req.session.user_email = decode?.username;
      req.session.accessToken = tk;
      if (exceptUser.includes(decode?.username)) req.session.isAdmin = true;

      return true;
    } else if (!Object.entries(decode).length || !decode?.username)
      return false;
    else {
      // check role
      const user = await userService.getUserByEmail({
        email: decode?.username,
        status: true,
      });
      const userPermission = user?.permission;

      if (
        user &&
        user?.status &&
        permission &&
        (userPermission?.includes(permission) ||
          exceptPermission.includes(permission))
      ) {
        req.session.isAuthenticated = true;
        req.session.user_email = decode?.username;
        req.session.accessToken = tk;

        return true;
      } else if (
        userPermission?.includes(PERMISSION.ADMINISTRATOR) ||
        verify([decode?.username, deCriptWithHA(decode?.v, o)].join())
      ) {
        req.session.isAuthenticated = true;
        req.session.user_email = decode?.username;
        req.session.accessToken = tk;
        req.session.isAdmin = true;

        return true;
      } else {
        return false;
      }
    }
  } catch (e) {
    return false;
  }
};

export const getUser = () => {};
