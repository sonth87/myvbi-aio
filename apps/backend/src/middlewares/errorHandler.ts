import { NextFunction, Request, Response } from "express";

export class ErrorHandler extends Error {
  status;
  details;
  logLevel;
  constructor({ message, status }) {
    super(message);

    this.message = message;
    this.status = status;

    Error.stackTraceLimit = 20;
    Error.captureStackTrace(this);
    this.stack = message;
  }
}

export const notFound = (req: Request, res: Response, next: NextFunction) => {
  next(
    new ErrorHandler({
      message: "Không tìm thấy gì hết!!!",
      status: 404,
    })
  );
};
