import { Request, Response } from "express";
import {
  createCategoriesService,
  deleteCategoriesService,
  getAllCategoriesByStatusService,
  getAllCategoriesExternalService,
  getAllCategoriesService,
  getCategoryByIdService,
  updateCategoriesService,
} from "../services/categories.service";
import { response } from "../utils/response";
import { HttpStatusCode } from "axios";
import NewsModel from "../models/news.model";
import CategoriesModel from "../models/category.models";

export const createCategory = async (req: Request, res: Response) => {
  try {
    const bodyReq = req.body;
    const result = {} as any;
    Object.keys(bodyReq["vi"]).forEach((val: string) => {
      return (result[val] = bodyReq["vi"][val]);
    });
    const resultData = await createCategoriesService(result);
    return res.send({
      data: resultData,
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getAllCategories = async (req: Request, res: Response) => {
  const page: number | undefined =
    parseInt(req.query.page as string | undefined) || 1;
  const limit: number | undefined =
    parseInt(req.query.limit as string | undefined) || 10;
  const skip: number = (page - 1) * limit;
  const tieu_de = req.query.tieu_de as string;
  const tu_ngay: string = req.query.tu_ngay as string;
  const den_ngay: string = req.query.den_ngay as string;

  try {
    const resultData = await getAllCategoriesService(
      tieu_de,
      tu_ngay,
      den_ngay,
      limit,
      skip
    );

    return res.send({
      data: resultData,
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getAllCategoriesExternal = async (req: Request, res: Response) => {
  try {
    const resultData = await getAllCategoriesExternalService();

    return res.send({
      data: resultData.map((item) => ({
        id: item._id,
        tieu_de: item.tieu_de.value,
      })),
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getAllCategoriesByStatus = async (req: Request, res: Response) => {
  const page: number | undefined =
    parseInt(req.query.page as string | undefined) || 1;
  const limit: number | undefined =
    parseInt(req.query.limit as string | undefined) || 10;
  const skip: number = (page - 1) * limit;
  const status: string | undefined = req.query.status as string;

  try {
    const resultData = await getAllCategoriesByStatusService(
      limit,
      skip,
      status
    );

    return res.send({
      data: resultData,
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getCategoryById = async (req: Request, res: Response) => {
  try {
    const paramsReq = req.params.id as string;
    const resultData = await getCategoryByIdService(paramsReq);
    if (!resultData)
      return res.status(400).json({ msg: "Danh mục này không tồn tại" });

    return res.send({
      data: { vi: resultData },
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};
export const updateCategory = async (req: Request, res: Response) => {
  const paramsReq = req.params.id as string;
  const data = await updateCategoriesService(paramsReq, req.body);

  if (!data)
    response({
      res,
      status: HttpStatusCode.NotFound,
      message: "Không tìm thấy bản ghi",
    });
  else response({ res, data });
};

export const deleteCategory = async (req: Request, res: Response) => {
  const paramsReq = req.params.id as string;
  const dataCategory = await CategoriesModel.find({ ma_goc: paramsReq });
  const newsByCategoryId = await NewsModel.find({
    "ma_dm.value": paramsReq,
  });

  if (dataCategory.length > 0 || newsByCategoryId.length > 0)
    response({
      res,
      status: HttpStatusCode.NotFound,
      message: "Lỗi, không thể xóa danh mục này",
    });
  else {
    const data = await deleteCategoriesService(paramsReq);
    if (!data) {
      response({
        res,
        status: HttpStatusCode.NotFound,
        message: "Không tìm thấy danh mục này",
      });
    }
    response({ res, message: "Đã xóa danh mục" });
  }
};
