import { Request, Response } from "express";
import {
  generalConfigService,
  leaderService,
  productService,
} from "../services";
import { response } from "../utils/response";
import { HttpStatusCode } from "axios";
import { getConfigByKeys } from "../services/pageConfig.service";
import { compsInPage } from "../constants/pagesComponents";

export const getConfiguration = async (req: Request, res: Response) => {
  const data = req.body;

  // type = "common" | "page" | "product" | "leader"
  const type = data?.type;

  // comps = "page_name" | ["comp_name"]
  const components = data?.comps;

  if (type === "common") {
    const commonConfig = await generalConfigService.getGeneralConfig();

    response({ res, data: commonConfig });
  } else if (type === "page" && components) {
    if (typeof components === "string") {
      const _comps = compsInPage?.[components];

      if (_comps) {
        const config = await getConfigByKeys(_comps, true);

        response({ res, data: config });
      }
    } else if (typeof components === "object") {
      const config = await getConfigByKeys(components, true);

      response({ res, data: config });
    } else
      response({
        res,
        status: HttpStatusCode.BadRequest,
        message: "Bad Request",
      });
  } else if (type === "product") {
    const data = await productService.getAllProductsActive();

    response({ res, data });
  } else if (type === "leader") {
    const data = await leaderService.getAllLeaders();

    response({ res, data });
  } else
    response({
      res,
      status: HttpStatusCode.BadRequest,
      message: "Bad Request",
    });
};
