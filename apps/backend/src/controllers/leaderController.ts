import { HttpStatusCode } from "axios";
import { Request, Response } from "express";
import { leaderService } from "../services";
import { response } from "../utils/response";

export const getLeaders = async (req: Request, res: Response) => {
  try {
    const rs = await leaderService.getAllLeaders();
    res.json({ status: "200", data: rs });
  } catch (e) {
    res.status(500).send({ status: "400", data: "error" });
  }
};

export const getLeaderById = async (req: Request, res: Response) => {
  const rs = await leaderService.getLeaderById(req.params.leadersId);

  if (!rs) res.status(404).json({ status: "404", data: "Data not found" });
  else res.json({ status: HttpStatusCode.Ok, data: rs });
};

export const createLeader = async (req: Request, res: Response) => {
  const data = await leaderService.createLeader(req.body);

  response({ res, data });
};

export const updateLeader = async (req: Request, res: Response) => {
  const data = await leaderService.updateLeaders(
    req.params.leadersId,
    req.body
  );

  if (!data)
    response({
      res,
      status: HttpStatusCode.NotFound,
      message: "Không tìm thấy bản ghi",
    });
  else response({ res, data });
};

export const deleteLeader = async (req: Request, res: Response) => {
  const data = await leaderService.deleteLeader(req.params.leadersId);

  if (!data)
    response({
      res,
      status: HttpStatusCode.NotFound,
      message: "Không tìm thấy bản ghi",
    });
  else response({ res, message: "Đã xóa" });
};
