import { Request, Response } from "express";
import {
  createNewsService,
  deleteNewsService,
  getAllsNewsService,
  getAllsNewsConfigService,
  getNewsByCategoryService,
  getNewsByIdExternalService,
  getNewsByIdService,
  getNewsByIdsService,
  updateNewsService,
} from "../services/news.service";
import { response } from "../utils/response";
import { HttpStatusCode } from "axios";

// Hàm giả lập một API endpoint để xử lý dữ liệu và trả về kết quả
function reqCreateKeyNode(data) {
  return new Promise((resolve, reject) => {
    // Xử lý dữ liệu và trả về kết quả giả
    const result = {
      success: true,
      data: {
        acknowledged: true,
        insertedCount: 1,
        insertedIds: {
          "0": "000000206cfc026313c1cfa9",
        },
      },
    };

    // Giả lập việc xử lý request bằng cách sử dụng setTimeout
    setTimeout(() => {
      resolve(result);
    }, 1000);
  });
}

function reqCreateKeyContent(data) {
  return new Promise((resolve, reject) => {
    // Xử lý dữ liệu và trả về kết quả giả
    const result = {
      success: true,
      data: {
        acknowledged: true,
        insertedCount: 2,
        insertedIds: {
          "0": "000000206cfc026313c1cfaa",
          "1": "000000206cfc026313c1cfab",
        },
      },
    };

    // Giả lập việc xử lý request bằng cách sử dụng setTimeout
    setTimeout(() => {
      resolve(result);
    }, 1000);
  });
}

// Gửi request giả lập và xử lý kết quả
async function sendDataAndProcessResult(dataNode) {
  try {
    // Gửi request giả lập và nhận kết quả
    const result: any = await reqCreateKeyNode(dataNode);
    return result?.data?.insertedIds[0]; // Trả về kết quả cho caller
  } catch (error) {
    console.error("Đã xảy ra lỗi:", error);
    throw error;
  }
}

async function sendDataAmdCreateKeyContentResult(dataNode) {
  try {
    // Gửi request giả lập và nhận kết quả
    const result = await reqCreateKeyContent(dataNode);
    return result; // Trả về kết quả cho caller
  } catch (error) {
    console.error("Đã xảy ra lỗi:", error);
    throw error;
  }
}

function createNewData(dataRaw, parent_node) {
  // Lấy danh sách các keys từ ngôn ngữ mặc định (ở đây là 'vi')
  const keys = Object.keys(dataRaw.vi);

  // Sử dụng map để tạo dữ liệu mới từ mỗi key
  const newData = keys.map((key) => ({
    parent_node: parent_node,
    key: key,
    vi: dataRaw.vi[key],
    en: dataRaw.en[key],
    kr: dataRaw.kr[key],
  }));

  return newData;
}

export const createNews = async (req: Request, res: Response) => {
  try {
    const bodyReq = req.body;
    const result = {} as any;
    Object.keys(bodyReq["vi"]).forEach((val: string) => {
      return (result[val] = bodyReq["vi"][val]);
    });
    // // data gửi đi tạo ra 1 key_node (tạo thành 1 bài viết)
    // const dataNode = {
    //   root_id: "647d90b0a7c25307a85792cb",
    //   parent_id: null,
    //   key_group: null,
    //   description: result.tieu_de.value,
    // };

    // //data nhận về khi tạo xong 1 key_node
    // const dataCreateKeyNode = await sendDataAndProcessResult(dataNode);

    // // Sử dụng hàm để tạo dữ liệu mới
    // const newData = createNewData(bodyReq, dataCreateKeyNode);

    // //data nhận về khi tạo xong key_content
    // await sendDataAmdCreateKeyContentResult(newData);

    // // Thêm trường dataCreateKeyNode vào result
    // result["parent_node"] = dataCreateKeyNode;

    const response = await createNewsService(result);
    return res.send({ data: response });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getAllNews = async (req: Request, res: Response) => {
  const page: number | undefined =
    parseInt(req.query.page as string | undefined) || 1;
  const limit: number | undefined =
    parseInt(req.query.limit as string | undefined) || 10;
  const skip: number = (page - 1) * limit;
  const tieu_de = req.query.tieu_de as string;
  const ma_dm: string = req.query.ma_dm as string;
  const tu_ngay: string = req.query.tu_ngay as string;
  const den_ngay: string = req.query.den_ngay as string;
  try {
    // Loại bỏ dấu gạch ngang trong chuỗi tìm kiếm
    // const sanitizedSearchQuery = req?.query?.tieu_de
    //   ? (req?.query?.tieu_de as string).replace(/-/g, " ")
    //   : "";
    // const match = {
    //   tieu_de: new RegExp(`.*${sanitizedSearchQuery}.*`, "i"),
    // } as any;
    // const resultData = await getAllsNewsService({
    //   match: match,
    // });
    const resultData = await getAllsNewsService(
      tieu_de,
      ma_dm,
      tu_ngay,
      den_ngay,
      limit,
      skip
    );
    return res.send({
      data: resultData,
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getAllNewsConfig = async (req: Request, res: Response) => {
  const page: number | undefined =
    parseInt(req.query.page as string | undefined) || 1;
  const limit: number | undefined =
    parseInt(req.query.limit as string | undefined) || 10;
  const skip: number = (page - 1) * limit;
  const tieu_de = req.query.tieu_de as string;
  const ma_dm: string = req.query.ma_dm as string;
  const tu_ngay: string = req.query.tu_ngay as string;
  const den_ngay: string = req.query.den_ngay as string;
  try {
    const resultData = await getAllsNewsConfigService(
      tieu_de,
      ma_dm,
      tu_ngay,
      den_ngay,
      limit,
      skip
    );
    return res.send({
      data: resultData,
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getNewsByIds = async (req: Request, res: Response) => {
  try {
    const listIds = req.body?.ids;
    const result = await getNewsByIdsService(listIds);

    res.json({
      data: {
        vi: result,
        en: {},
      },
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getNewsById = async (req: Request, res: Response) => {
  try {
    const paramReq = req?.params?.id;
    const result = await getNewsByIdService(paramReq);

    res.json({
      data: {
        vi: result,
        en: {},
      },
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getNewsDetailExternal = async (req: Request, res: Response) => {
  try {
    const slug = req.params?.slug as any;
    const result = await getNewsByIdExternalService(slug);

    res.json({
      data: result,
    });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

// chưa tích hợp language
export const updateNewsById = async (req: Request, res: Response) => {
  const data = await updateNewsService(
    req?.params?.id, //truyền ID
    req.body // truyền body
  );
  if (!data)
    response({
      res,
      status: HttpStatusCode.NotFound,
      message: "Không tìm thấy bản ghi",
    });
  else response({ res, data });
};

export const deleteNews = async (req: Request, res: Response) => {
  try {
    const deleteNews = await deleteNewsService(req);
    // Kiểm tra xem tài liệu đã được xóa thành công chưa
    if (!deleteNews) {
      return res
        .status(404)
        .json({ message: "Không tìm thấy bài đăng để cập nhật" });
    }
    // Trả về phản hồi thành công kèm theo tài liệu đã được cập nhật
    res.json({
      status: 200,
      message: "Bài đăng đã được xóa!",
    });
  } catch (err: any) {
    return res.status(500).json({ message: err.message });
  }
};

export const getNewsByCategoryExternal = async (
  req: Request,
  res: Response
) => {
  try {
    const page: number | undefined =
      parseInt(req.query.page as string | undefined) || 1;
    const limit: number | undefined =
      parseInt(req.query.limit as string | undefined) || 12;
    const skip: number = (page - 1) * limit;

    const resultData = await getNewsByCategoryService(
      req.query.id as string,
      limit,
      skip
    );
    return res.send({
      data: resultData,
    });
  } catch (err: any) {
    return res.status(500).json({ message: err.message });
  }
};

export const getNewsByIdsExternal = async (req: Request, res: Response) => {
  try {
    const data = req.body;

    const resultData = await getNewsByIdsService(data?.ids);
    return res.send({
      data: resultData,
    });
  } catch (err: any) {
    return res.status(500).json({ message: err.message });
  }
};
