import axios, { HttpStatusCode } from "axios";
import { NextFunction, Request, Response } from "express";
import { response } from "../utils/response";
import { CRequest, UserRedirectType, UserVerifyType } from "../types/user";
import { userService } from "../services";
import { deCriptWithHA, verify } from "../utils/auth";
import { HKEY } from "../constants/user";
import jwt from "jsonwebtoken";

export const login = async (
  req: Request,
  res: Response,
) => {
  try {
    const url = process.env.OPENAPI + "api/authentication/request-login-token";

    const rs = await axios.post(
      url,
      { service: "aad" },
      {
        headers: {
          "x-api-key": process.env.OPENAPI_KEY,
        },
      }
    );
    response({ res, data: rs?.data?.data });
  } catch (err) {
    console.log(err);
    response({ res, message: err });
  }
};

export const logout = async (
  req: CRequest,
  res: Response,
  next: NextFunction
) => {
  const url = process.env.OPENAPI + "api/authentication/logout";

  await axios.get(url);
  // clear data
  req.session = {
    accessToken: "",
    account: "",
    idToken: "",
    isAuthenticated: false,
    refreshToken: "",
    token: "",
    user_email: "",
    isAdmin: false,
  };

  response({ res, status: HttpStatusCode.Ok });
};

export const auth_verify = async (req: CRequest, res: Response) => {
  const data: UserVerifyType = req.body;

  if (!data?.user_data?.email) {
    res.json({ success: false });
    return false;
  }

  const checkUser = await userService.getUserByEmail({
    email: data.user_data.email,
  });

  // verify user
  const verify_state = {
    success: true,
    data: {
      USER_OPENID: data.user_data.email || "123",
      SERVICE_ACCESS_TOKEN: "",
    },
  };

  if (checkUser) verify_state.data.USER_OPENID = checkUser.email;
  else {
    const user = await userService.createUser({
      email: data?.user_data?.email,
      name: data?.user_data?.full_name || data?.user_data?.email,
      permission: [],
    });

    if (!user || !user?.status) verify_state.success = false;
    else verify_state.data.USER_OPENID = user.id;
  }

  res.json(verify_state);
};

export const auth_redirect = async (req: CRequest, res: Response) => {
  const data: UserRedirectType = req.body;
  const accessToken = jwt.sign(
    { username: data.user_id, token: data?.user_access_token },
    HKEY,
    {
      expiresIn: "1d",
    }
  );

  req.session.accessToken = accessToken;
  req.session.token = accessToken;
  req.session.refreshToken = data?.user_refresh_token;
  if (data?.user_access_token) req.session.isAuthenticated = true;

  res.redirect(
    process.env.REACT_APP_FE_BASEURL + "redirect?c=" + accessToken
    // "https://dev-cms.evbi.vn/redirect?c=" + accessToken
  );
};

export const doLogin = async (
  req: CRequest,
  res: Response,
  next: NextFunction
) => {
  const u = req.body?.user;
  const p = req.body?.password;
  const o = req.get("origin");
  const pair = [deCriptWithHA(u, o), deCriptWithHA(p, o)];

  const test = verify(pair.join());
  if (test) {
    const accessToken = jwt.sign({ username: pair[0], v: p }, HKEY);

    req.session.accessToken = accessToken;
    req.session.token = accessToken;
    req.session.refreshToken = "";
    req.session.isAuthenticated = true;
    req.session.isAdmin = true;

    response({ res, data: { username: pair[0], c: accessToken } });
  } else {
    response({
      status: HttpStatusCode.Unauthorized,
      res,
      message: "Thông tin không chính xác",
    });
  }
};
