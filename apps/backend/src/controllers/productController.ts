import { HttpStatusCode } from "axios";
import { Request, Response } from "express";
import { productService } from "../services";
import { response } from "../utils/response";

export const getProducts = async (req: Request, res: Response) => {
  try {
    const rs = await productService.getAllProducts();
    res.json({ status: "200", data: rs });
  } catch (e) {
    res.status(500).send({ status: "400", data: "error" });
  }
};

export const getProductById = async (req: Request, res: Response) => {
  const rs = await productService.getProductById(req.params.productId);

  if (!rs) res.status(404).json({ status: "404", data: "Data not found" });
  else res.json({ status: HttpStatusCode.Ok, data: rs });
};

export const createProduct = async (req: Request, res: Response) => {
  const data = await productService.createProduct(req.body);

  response({ res, data });
};

export const updateProduct = async (req: Request, res: Response) => {
  const data = await productService.updateProduct(
    req.params.productCode,
    req.params.productId,
    req.body
  );

  if (!data)
    response({
      res,
      status: HttpStatusCode.NotFound,
      message: "Không tìm thấy bản ghi",
    });
  else response({ res, data });
};

export const deleteProduct = async (req: Request, res: Response) => {
  const data = await productService.deleteProduct(req.params.productId);

  if (!data)
    response({
      res,
      status: HttpStatusCode.NotFound,
      message: "Không tìm thấy sản phẩm",
    });
  else response({ res, message: "Đã xóa sản phẩm" });
};
