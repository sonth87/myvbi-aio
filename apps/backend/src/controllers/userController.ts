import { HttpStatusCode } from "axios";
import { NextFunction, Response } from "express";
import { response } from "../utils/response";
import { CRequest } from "../types/user";
import { userService } from "../services";
import { validate } from "../middlewares/auth";
import { PERMISSION } from "../constants/permission";

export const getUserProfile = async (
  req: CRequest,
  res: Response,
  next: NextFunction
) => {
  if (
    req.session.isAuthenticated ||
    validate(req, res, next, PERMISSION.PROFILE)
  ) {
    const info = await userService.getUserByEmail({
      email: req.session.user_email,
    });

    const userInfo = {
      email: req.session.user_email,
      accessToken: req.session.accessToken,
      refreshToken: req.session.refreshToken,
      user: info || { permission: [] },
    };
    if (req.session.isAdmin)
      userInfo.user["permission"] = Object.values(PERMISSION);

    response({ res, data: userInfo });
  } else {
    response({
      status: HttpStatusCode.Unauthorized,
      res,
      message: "Chưa đăng nhập",
    });
  }
};

export const getAllUser = async (req: CRequest, res: Response) => {
  const rs = await userService.getAllUser();

  res.json({ status: HttpStatusCode.Ok, data: rs });
};

export const getUserInfo = async (req: CRequest, res: Response) => {
  const rs = await userService.getUserInfoById(req.params.userId);

  if (!rs) res.status(404).json({ status: "404", data: "Data not found" });
  else res.json({ status: HttpStatusCode.Ok, data: rs });
};

export const createUser = async (req: CRequest, res: Response) => {
  const data = await userService.createUser(req.body);

  response({
    res,
    data,
    status: !data ? HttpStatusCode.PreconditionFailed : HttpStatusCode.Ok,
    message: !data ? "Email đã tồn tại" : undefined,
  });
};

export const updateUser = async (req: CRequest, res: Response) => {
  const data = await userService.updateUser(req.params.userId, req.body);

  response({ res, data });
};

export const deleteUser = async (req: CRequest, res: Response) => {
  const data = await userService.deleteUser(req.params.userId);

  if (!data)
    response({
      res,
      status: HttpStatusCode.NotFound,
      message: "Không tìm thấy người dùng",
    });
  else response({ res, message: "Đã xóa người dùng" });
};
