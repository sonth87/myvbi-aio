import { Request, Response } from "express";
import {
  getAllConfigs,
  getConfigByKey,
  updateConfig,
} from "../services/pageConfig.service";
import { response } from "../utils/response";
import { COMPS } from "../constants/pagesComponents";
import { HttpStatusCode } from "axios";

export const getAllPageConfig = async (req: Request, res: Response) => {
  const data = await getAllConfigs();

  response({ res, data });
};

export const getPageConfigByAttrs = async (req: Request, res: Response) => {
  const componentKey = req.params.componentKey;
  const data = await getConfigByKey(componentKey);

  response({ res, data });
};

export const updatePageConfigByAttrs = async (req: Request, res: Response) => {
  const componentKey = req.params.componentKey;
  const payload = req.body;

  if (componentKey && Object.values(COMPS).includes(componentKey)) {
    const data = await updateConfig(componentKey, payload);
    response({ res, data });
  } else {
    response({
      res,
      status: HttpStatusCode.BadRequest,
      message: "Bad Request",
    });
  }
};
