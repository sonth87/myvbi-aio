import { Request, Response } from "express";
import { generalConfigService } from "../services";
import { response } from "../utils/response";

export const getGeneralConfig = async (req: Request, res: Response) => {
  const data = await generalConfigService.getGeneralConfig();

  response({ res, data });
};

export const updateGeneralConfig = async (req: Request, res: Response) => {
  const data = await generalConfigService.updateGeneralConfig(req.body);

  response({ res, data });
};
