import * as GeneralController from "./generalController";
import * as ProductController from "./productController";
import * as UploadController from "./uploadController";
import * as newsCtrl from "./newsController";
import * as categoryCtrl from "./categoriesController";
import * as PageController from "./pageConfigController";
import * as UserController from "./userController";
import * as AuthController from "./authController";
import * as LeaderController from "./leaderController";

export {
  GeneralController,
  ProductController,
  UploadController,
  newsCtrl,
  categoryCtrl,
  PageController,
  UserController,
  AuthController,
  LeaderController,
};
