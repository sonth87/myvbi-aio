import { HttpStatusCode } from "axios";
import { Response } from "express";
import { uploadService } from "../services";
import { response } from "../utils/response";
import FormData from "form-data";

type File = {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  buffer: any;
  size: number;
};

export const uploadFile = async (req: any, res: Response) => {
  const { files } = req;
  const formFile = new FormData();

  try {
    files?.map((file: File) => {
      const { buffer, originalname: filename } = file;

      formFile.append("files", buffer, filename);
    });

    const result = await uploadService.upload(formFile);

    response({ res, data: result?.data });
  } catch (err) {
    response({
      res,
      status: HttpStatusCode.BadRequest,
      message: "error appear",
    });
  }
};
