import mongoose from "mongoose";
import * as dotenv from "dotenv";
dotenv.config();

const dbConnection = (uri: string, dbName?: string, callback?: any) => {
  const db = mongoose.createConnection(uri, {
    // config
    connectTimeoutMS: 10000, // timeout 10s
    dbName,
  });

  db.on("error", (error) => {
    console.log(`Mongoose:: connection -- ${db.name} -- error : `, error);
    db.close().catch(() => console.log("Mongoose connection error"));
  });

  db.on("connected", () => {
    console.log(`Mongoose:: connected ${db.name} - ${db.host}`);
    mongoose.set("debug", (col, method, query, doc) => {
      console.log(
        `Mongoose Debug:: ${db.name}::${col}::${method}::${JSON.stringify(
          query
        )}`
      );
    });

    callback?.();
  });

  db.on("disconnected", (error) => {
    console.log(`Mongoose:: disconnected ${db.name}`, error);
  });

  // disconnect connect db khi stop (hoặc crash) node server
  process.on("SIGINT", async () => {
    await mongoose.connection.close();
    process.exit(0);
  });

  return db;
};

export default dbConnection;

const CMSDB = dbConnection(process.env.DB_CMS_URI, process.env.DB_CMS_NAME);

export { CMSDB };
