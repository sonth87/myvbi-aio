export default function findAllRelatedItems(data: any, ma_dm: string) {
  const result = [];

  function recursiveSearch(currentMaDm) {
    // Tìm các phần tử có ma_goc bằng currentMaDm
    const foundItems = data.filter(
      (item) => item?.ma_goc?.toString() === currentMaDm
    );

    // Thêm các phần tử tìm được vào kết quả
    result.push(...foundItems);

    // Tìm các phần tử tiếp theo dựa trên _id của các phần tử vừa tìm được
    foundItems.forEach((item) => {
      recursiveSearch(item._id.toString());
    });
  }

  // Bắt đầu tìm kiếm với ma_dm ban đầu
  recursiveSearch(ma_dm);

  return result;
}
