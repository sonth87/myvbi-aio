import { HttpStatusCode } from "axios";
import { NextFunction, Request, Response } from "express";

export const response = ({
  req,
  res,
  next,
  status,
  httpStatus = HttpStatusCode.Ok,
  data,
  message,
}: {
  req?: Request;
  res: Response;
  next?: NextFunction;
  status?: HttpStatusCode;
  httpStatus?: HttpStatusCode;
  data?: any;
  message?: string;
}) => {
  try {
    const statusCode = status ? status : HttpStatusCode.Ok;

    // if ([HttpStatusCode.Ok, HttpStatusCode.Accepted].includes(statusCode)) {
    // }

    res.status(httpStatus).json({
      status: statusCode,
      message: message,
      data: data,
    });
  } catch (e) {
    res.status(500).json({
      status: "error",
      message: "Loi roi",
    });
  }
};
