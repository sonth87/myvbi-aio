import CategoriesModel from "../models/category.models";
import NewsModel from "../models/news.model";
import findAllRelatedItems from "../utils/getAllCatByCondition";

const createNewsService = async (requestBody: any) => {
  try {
    return NewsModel.create(requestBody);
  } catch (error) {
    console.log(error);
  }
};

const getAllsNewsService = async (
  tieu_de: string,
  ma_dm: string,
  tu_ngay: string,
  den_ngay: string,
  limit: number,
  skip: number
) => {
  try {
    // sử dụng toán tử logic && để kiểm tra xem mỗi biến có khác (undefined, rỗng, null) không.
    // nếu biến không phải (undefined, rỗng, null), sử dụng một object có một dynamic key để thêm trường vào query.
    // khi biến là (undefined, rỗng, null), expression sẽ trả về false, không thêm gì vào query.
    // sử dụng toán tử spread ... để kết hợp tất cả các object thành một object lớn.
    const valueTieuDe = `\"${tieu_de}\"`;
    const query = {
      ...(tieu_de && {
        $text: { $search: `${valueTieuDe}` },
      }),
      ...(ma_dm && { "ma_dm.value": ma_dm }),
      ...(tu_ngay &&
        den_ngay && {
          createdAt: {
            $gte: new Date(tu_ngay),
            $lte: new Date(den_ngay),
          },
        }),
      deleted: false,
    };

    const res = await NewsModel.find(query)
      .sort({ "ngay_tao.value": -1 })
      .skip(skip)
      .limit(limit);

    const totalCount = await NewsModel.countDocuments(query);
    const data = {
      data: res,
      totalCount,
    };
    return data;
  } catch (er) {
    console.log(er);
  }
};

const getAllsNewsConfigService = async (
  tieu_de: string,
  ma_dm: string,
  tu_ngay: string,
  den_ngay: string,
  limit: number,
  skip: number
) => {
  try {
    // sử dụng toán tử logic && để kiểm tra xem mỗi biến có khác (undefined, rỗng, null) không.
    // nếu biến không phải (undefined, rỗng, null), sử dụng một object có một dynamic key để thêm trường vào query.
    // khi biến là (undefined, rỗng, null), expression sẽ trả về false, không thêm gì vào query.
    // sử dụng toán tử spread ... để kết hợp tất cả các object thành một object lớn.
    // eslint-disable-next-line no-useless-escape
    const valueTieuDe = `\"${tieu_de}\"`;
    const query = {
      ...(tieu_de && {
        $text: { $search: `${valueTieuDe}` },
      }),
      ...(ma_dm && { "ma_dm.value": ma_dm }),
      ...(tu_ngay &&
        den_ngay && {
          createdAt: {
            $gte: new Date(tu_ngay),
            $lte: new Date(den_ngay),
          },
        }),
      "trang_thai.value": "active",
      deleted: false,
    };

    const res = await NewsModel.find(query)
      .sort({ "ngay_tao.value": -1 })
      .skip(skip)
      .limit(limit);

    const totalCount = await NewsModel.countDocuments(query);
    const data = {
      data: res,
      totalCount,
    };
    return data;
  } catch (er) {
    console.log(er);
  }
};

const getNewsByCategoryService = async (
  ma_dm: string,
  limit: number,
  skip: number
) => {
  try {
    const resGetAllCat = await CategoriesModel.find({
      "trang_thai.value": "active",
    });
    const listCatByCondition = findAllRelatedItems(resGetAllCat, ma_dm).map(
      (item: any) => item._id.toString()
    );
    console.log(listCatByCondition);

    const query = {
      ...(ma_dm && {
        "ma_dm.value":
          listCatByCondition.length > 0
            ? {
                $in: [
                  "660b60843b49f82c60a25dab",
                  "660b60b33b49f82c60a25dad",
                  "6641c38fc16bd616b1afa3d4",
                ],
              }
            : ma_dm,
      }),
      deleted: false,
      "trang_thai.value": "active",
    };
    const res = await NewsModel.find(query)
      .sort({ "ngay_tao.value": -1 })
      .skip(skip)
      .limit(limit);

    const totalCount = await NewsModel.countDocuments(query);
    const data = {
      data: res,
      totalCount,
    };
    return data;
  } catch (error) {
    console.log(error);
  }
};

const getNewsByIdService = async (id: string) => {
  try {
    const res = await NewsModel.findById(id);
    return res;
  } catch (er) {
    console.log(er);
  }
};

const getNewsByIdExternalService = async (slug: string) => {
  try {
    // Tìm bài viết theo ID
    const res = await NewsModel.aggregate([
      {
        $match: {
          "slug.value": slug,
        },
      },
      {
        $lookup: {
          from: "categories",
          localField: "ma_dm.value",
          foreignField: "_id",
          as: "ma_dm",
        },
      },
    ]).exec();

    if (!res) {
      throw new Error("Không tìm thấy bài viết");
    }

    // Tìm 4 bài viết ngẫu nhiên không bao gồm bài viết với ID đã tìm thấy
    const randomNews = await NewsModel.aggregate([
      //@ts-ignore
      { $match: { _id: { $ne: res._id } } }, // Loại trừ bài viết có ID đã tìm thấy
      { $sample: { size: 4 } }, // Lấy ngẫu nhiên 4 bài viết
    ]);

    return { dataNewsDetail: res, randomNews };
  } catch (er) {
    console.log(er);
  }
};

const getNewsByIdsService = async (ids: string[]) => {
  try {
    const res = await NewsModel.find({
      _id: { $in: ids },
      deleted: false,
      "trang_thai.value": "active",
    });
    return res;
  } catch (er) {
    console.log(er);
  }
};

const updateNewsService = async (id: string, requestBody: any) => {
  try {
    const item = await getNewsByIdService(id);
    if (!item) return;
    Object.assign(item, requestBody.vi);
    item.save();
    return item;
  } catch (error) {
    console.log(error);
  }
};

const deleteNewsService = async (req: any) => {
  try {
    const item = await NewsModel.findByIdAndUpdate(
      req?.params?.id, //truyền ID
      {
        deleted: true,
        "trang_thai.value": "inactive",
      },
      { new: true }
    );
    return item;
  } catch (error) {
    console.log(error);
  }
};

export {
  createNewsService,
  updateNewsService,
  deleteNewsService,
  getAllsNewsConfigService,
  getAllsNewsService,
  getNewsByIdService,
  getNewsByIdsService,
  getNewsByIdExternalService,
  getNewsByCategoryService,
};
