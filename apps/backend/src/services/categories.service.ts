import CategoriesModel from "../models/category.models";

const createCategoriesService = async (requestBody: any) => {
  try {
    return CategoriesModel.create(requestBody);
  } catch (error) {
    console.log(error);
  }
};
const getAllCategoriesService = async (
  tieu_de: string,
  tu_ngay: string,
  den_ngay: string,
  limit: number,
  skip: number
) => {
  try {
    // sử dụng toán tử logic && để kiểm tra xem mỗi biến có khác (undefined, rỗng, null) không.
    // nếu biến không phải (undefined, rỗng, null), sử dụng một object có một dynamic key để thêm trường vào query.
    // khi biến là (undefined, rỗng, null), expression sẽ trả về false, không thêm gì vào query.
    // sử dụng toán tử spread ... để kết hợp tất cả các object thành một object lớn.
    const query = {
      ...(tieu_de && {
        "tieu_de.value": { $regex: tieu_de, $options: "i" },
      }),
      ...(tu_ngay &&
        den_ngay && {
          createdAt: {
            $gte: new Date(tu_ngay),
            $lte: new Date(den_ngay),
          },
        }),
    };
    const res = await CategoriesModel.find(query)
      .sort({ createdAt: -1, "tieu_de.value": 1 })
      .skip(skip)
      .limit(limit);
    const totalCount = await CategoriesModel.countDocuments(query);
    const data = {
      data: res,
      totalCount,
    };
    return data;
  } catch (er) {
    console.log(er);
  }
};

const getAllCategoriesExternalService = async () => {
  try {
    // const res = await CategoriesModel.find({
    //   ma_goc: { $ne: null },
    // });
    const res = await CategoriesModel.find({
      "trang_thai.value": "active",
    });

    return res;
  } catch (er) {
    console.log(er);
  }
};

const getAllCategoriesByStatusService = async (
  limit: number,
  skip: number,
  status: string
) => {
  try {
    const res = await CategoriesModel.find({
      "trang_thai.value": status,
    })
      .skip(skip)
      .limit(limit);
    const totalCount = await CategoriesModel.countDocuments({
      "trang_thai.value": status,
    });
    const data = {
      data: res,
      totalCount,
    };

    return data;
  } catch (er) {
    console.log(er);
  }
};

const getCategoryByIdService = async (param: string) => {
  try {
    const dataCategory = await CategoriesModel.findById(param);
    return dataCategory;
  } catch (er) {
    console.log(er);
  }
};

const updateCategoriesService = async (id: string, updateInfo) => {
  const item = await getCategoryByIdService(id);
  if (!item) return;

  Object.assign(item, updateInfo.vi);
  await item.save();
  return item;
};

const deleteCategoriesService = async (id: string) => {
  const result = await CategoriesModel.findById(id);

  if (!result) {
    return false;
  }
  await result.deleteOne();
  return true;
};

export {
  createCategoriesService,
  getAllCategoriesService,
  getAllCategoriesExternalService,
  getAllCategoriesByStatusService,
  getCategoryByIdService,
  updateCategoriesService,
  deleteCategoriesService,
};
