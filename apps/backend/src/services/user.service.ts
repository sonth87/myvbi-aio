import { user } from "@repo/types";
import { User } from "../models/user.models";

export const getUserInfoById = async (id: string) => {
  return await User.findById(id);
};

export const getAllUser = async () => {
  return await User.find({ deleted: { $in: ["false", false, undefined] } });
};

export const getUserByEmail = async ({
  email,
  status,
}: {
  email: string;
  status?: boolean;
}) => {
  const condition = { email };
  if (typeof status !== "undefined") condition["status"] = { $in: [status] };

  return await User.findOne(condition);
};

export const isEmailExist = async (email: string, curId: string) => {
  const user = await User.findOne({ email, _id: { $ne: curId } });
  return user;
};

export const createUser = async (data: user.UserType) => {
  const checkEx = await getUserByEmail({ email: data.email });

  if (checkEx || data?.id) return;
  else return await User.create(data);
};

export const updateUser = async (id: string, data: user.UserType) => {
  const cuser = await getUserInfoById(id);

  if (!cuser) return;

  const checkEx = await isEmailExist(data.email, id);

  if (!checkEx) {
    Object.assign(cuser, data);
    await cuser.save();
    return cuser;
  } else return;
};

export const deleteUser = async (id: string) => {
  const checkEx = await getUserInfoById(id);

  if (checkEx) {
    checkEx.deleted = true;
    await checkEx.save();
    return true;
  } else return;
};
