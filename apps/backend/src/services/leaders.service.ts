import { Types } from "mongoose";
import { Leaders } from "../models/leaders.models";

/**
 *
 * @returns {Promise<Leaders>}
 */
const getAllLeaders = async () => {
  return Leaders.find({});
};

const getLeaderById = async (id: string) => {
  return await Leaders.findById(id);
};

const createLeader = async (requestBody) => {
  return Leaders.create(requestBody);
};

const updateLeaders = async (id: string, updateInfo) => {
  const rs = await getLeaderById(id);
  if (!rs) return;

  Object.assign(rs, updateInfo);
  await rs.save();
  return rs;
};

const deleteLeader = async (id: string) => {
  const rs = await getLeaderById(id);

  if (!rs) return;
  else {
    await rs.deleteOne();
    return true;
  }
};

export {
  getAllLeaders,
  getLeaderById,
  createLeader,
  updateLeaders,
  deleteLeader,
};
