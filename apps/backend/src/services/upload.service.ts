import axios from "axios";

export const upload = async (formData: any) => {
  const url = process.env.OPENAPI + "sapi/upload-file-cms";
  const OK = process.env.OPENAPI_KEY;
  const result = await axios.post(url, formData, {
    headers: {
      "x-api-key": OK,
      "Content-Type": "multipart/form-data",
    },
  });

  return result?.data;
};
