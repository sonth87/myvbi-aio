import { GeneralConfig } from "../models/generalConfig.models";

/**
 *
 * @returns {Promise<GeneralConfig>}
 */
const getGeneralConfig = async () => {
  return await GeneralConfig.findOne();
};

const updateGeneralConfig = async (content) => {
  const exe = GeneralConfig.findOneAndUpdate({}, content, {
    upsert: true,
    new: true,
    setDefaultsOnInsert: true,
  });

  return exe;
};

export { getGeneralConfig, updateGeneralConfig };
