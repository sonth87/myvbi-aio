import { pages } from "@repo/types";
import { PageConfig } from "../models/pageConfig.models";

const getAllConfigs = async () => {
  return await PageConfig.find({});
};

const getConfigByKey = async (key: string) => {
  return await PageConfig.findOne({ key });
};

const getConfigByKeys = async (keys: string[], visible?: boolean) => {
  return await PageConfig.find({ key: { $in: keys }, visible: visible });
};

const updateConfig = async (key: string, data: pages.PageCompType) => {
  let rs = await getConfigByKey(key);

  if (!rs) {
    rs = await PageConfig.create(data);
  } else {
    Object.assign(rs, data);
    await rs.save();
  }

  return rs;
};

export { getAllConfigs, getConfigByKey, getConfigByKeys, updateConfig };
