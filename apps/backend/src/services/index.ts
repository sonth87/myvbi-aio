import * as generalConfigService from "./generalConfig.service";
import * as productService from "./products.service";
import * as uploadService from "./upload.service";
import * as categoriesSevive from "./categories.service";
import * as leaderService from "./leaders.service";
import * as userService from "./user.service";

export {
  generalConfigService,
  productService,
  uploadService,
  categoriesSevive,
  leaderService,
  userService,
};
