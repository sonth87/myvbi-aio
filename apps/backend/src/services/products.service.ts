import { Types } from "mongoose";
import { Products } from "../models/products.models";

/**
 *
 * @returns {Promise<Products>}
 */
const getAllProducts = async () => {
  return Products.find({});
};

const getAllProductsActive = async () => {
  return Products.find({ visible: true });
};

const getProductById = async (id) => {
  return await Products.findById(id);
};

const getProductByCode = async (code) => {
  return await Products.findById(code);
};

const isProductExist = async (
  code: string,
  currentId?: Types.ObjectId | string
) => {
  const prod = await Products.findOne({ id: code, _id: { $ne: currentId } });
  return prod;
};

const createProduct = async (requestBody) => {
  // if (await isProductExist(requestBody.code, requestBody.id)) {
  //   console.log("product exist");
  //   return false;
  // } else {
  return await Products.create(requestBody);
  // }
};

const updateProduct = async (code: string, id: string, updateInfo) => {
  const prod = await getProductById(id);
  if (!prod) return;

  if (updateInfo.code && (await isProductExist(updateInfo.code, prod._id)))
    return;

  Object.assign(prod, updateInfo);
  await prod.save();
  return prod;
};

const deleteProduct = async (id: string) => {
  const prod = await getProductById(id);

  if (!prod) return;
  else {
    await prod.deleteOne();
    return true;
  }
};

export {
  getAllProducts,
  getAllProductsActive,
  getProductById,
  getProductByCode,
  createProduct,
  updateProduct,
  deleteProduct,
};
