/** @type {import("eslint").Linter.Config} */
module.exports = {
  root: true,
  extends: ["eslint:recommended"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: true,
  },
  rules: {
    "no-console": "off",
    "no-unused-vars": "warn",
  },
  env: {
    browser: true,
    node: true,
  },
};
