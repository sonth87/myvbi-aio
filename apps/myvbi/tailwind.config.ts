const defaultSetting = require("mvi-ds-ui/tailwind.config.js");

module.exports = {
  ...defaultSetting,

  content: ["./src/**/*.{js,jsx,ts,tsx}", "./app/**/*.{js,jsx,ts,tsx}"],
};
