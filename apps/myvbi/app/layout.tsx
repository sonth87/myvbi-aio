import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
// import * as ServiceRegistration from "../src/serviceWorkerRegistration";

const inter = Inter({ subsets: ["latin"] });
// ServiceRegistration.unregister();

export const metadata: Metadata = {
  title: "VBI",
  description: "VBI",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
