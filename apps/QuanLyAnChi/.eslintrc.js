/** @type {import("eslint").Linter.Config} */
module.exports = {
  root: true,
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "@repo/eslint-config/react-internal.js"
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: true,
  },
  rules: {
    "no-console": "off",
    "no-unused-vars": "warn",
    "react/react-in-jsx-scope": "off",
  },
  env: {
    browser: true,
    node: true,
  },
};
