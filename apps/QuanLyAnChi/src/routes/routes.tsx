import React from "react";
import { ROUTES } from "../constants/path";
import { PrivateRouteProperties, PublicRouteProperties } from "../type/route";
import { MENU_LIST } from "../constants/const";

const LoginAsync = React.lazy(() => import("../pages/Authen/Login"));
const Err404Async = React.lazy(() => import("../pages/Err404Page"));
const LoginSuccessAsync = React.lazy(() => import("../pages/redirect"));

const publicRoutes: PublicRouteProperties[] = [
  {
    path: ROUTES.LOGIN,
    component: LoginAsync,
  },
  {
    path: ROUTES.LOGIN_SUCCESS,
    component: LoginSuccessAsync,
  },
];

const DashboardPage = React.lazy(() => import("../pages/Dashboard"));
const QuanLyAnChi = React.lazy(() => import("../pages/QuanLyAnChi"));
const DieuChuyenAnChi = React.lazy(() => import("../pages/DieuChuyenAnChi"));
const QuanLyMatHong = React.lazy(() => import("../pages/QuanLyMatHong"));
const QuanLyKho = React.lazy(() => import("../pages/QuanLyKho"));

// const listMenu: any = window.sessionStorage.getItem(MENU_LIST);
// const parsedListMenu = JSON.parse(listMenu);
// console.log("parsedListMenu", parsedListMenu);

const privateRoutes: PrivateRouteProperties[] = [
  {
    path: "/",
    component: DashboardPage,
  },
  {
    path: ROUTES.LOGIN_SUCCESS,
    component: LoginSuccessAsync,
  },
  {
    path: ROUTES.DKSD,
    component: QuanLyAnChi,
  },

  {
    path: ROUTES.DCAC,
    component: DieuChuyenAnChi,
  },
  {
    path: ROUTES.QLMH,
    component: QuanLyMatHong,
  },
];
export { publicRoutes, privateRoutes };
