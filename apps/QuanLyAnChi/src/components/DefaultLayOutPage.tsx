import React from "react";
import { Suspense } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
// import { ROUTES } from "../constants/path";
import { privateRoutes, publicRoutes } from "../routes/routes";
import AdminLayout from "./LayoutComponent/AdminLayout";
import { useAuth } from "../context/AuthContext";
import { ROUTES } from "../constants/path";
import Loading from "./Loading";
import { MainMenu } from "../constants/menu";

const DefaultLayOutPage = () => {
  const { isLoading, isMenu, isAuth } = useAuth();

  if (isAuth === undefined) {
    return <Loading />;
  }

  return (
    <div className="App">
      <Suspense fallback={<Loading />}>
        <Routes>
          {!isAuth ? (
            <>
              {publicRoutes.map((route, index) => {
                const RouterPage = route.component;
                if (RouterPage)
                  return (
                    <Route
                      key={index}
                      path={route.path}
                      element={<RouterPage {...route?.params} />}
                    />
                  );
                else return null;
              })}
            </>
          ) : (
            <>
              {privateRoutes.map((route, index) => {
                const RouterPage = route?.component;
                return (
                  <Route
                    key={index}
                    path={route.path}
                    element={
                      <AdminLayout menu={isMenu}>
                        <RouterPage params={route?.params} />
                      </AdminLayout>
                    }
                  />
                );
              })}
            </>
          )}

          {!isLoading && (
            <Route
              path="*"
              element={
                !isAuth ? (
                  <>
                    <Navigate replace to={ROUTES.LOGIN} />
                  </>
                ) : (
                  <>
                    <Navigate replace to={ROUTES.LOGIN_SUCCESS} />
                  </>
                )
              }
            />
          )}
        </Routes>
      </Suspense>
    </div>
  );
};

export default DefaultLayOutPage;
