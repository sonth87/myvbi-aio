import React, { useEffect, useState } from "react";
import { DsIcon, DsLayout, DsLogo, MenuItems } from "mvi-ds-ui";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";
import { UserType } from "@repo/types/user";

type AdminLayoutProps = {
  children?: any;
  hideTopMenu?: any;
  hideLeftMenu?: any;
  menu?: MenuItems[];
};
const AdminLayout = ({ menu, children }: AdminLayoutProps) => {
  const { isLoading, user } = useAuth();
  const [userData, setUserData] = useState<UserType | null>(null);
  const navigate = useNavigate();

  useEffect(() => {
    setUserData(user);
  }, [isLoading]);

  return (
    <div className="flex flex-col h-screen overflow-hidden">
      {userData ? (
        <DsLayout
          topMenu={{
            logo: <DsLogo />,
            onLogoClick: () => navigate("/"),
            fixedMenu: true,
          }}
          menu={{
            menuItems: menu,
            onItemClick: (item) => {
              navigate(item?.path || "/");
            },
            active: 0,
          }}
          user={{
            menu: [
              {
                label: (
                  <div
                    className="flex items-center py-1"
                    onClick={() => {
                      window.sessionStorage.clear();
                      window.location.assign("/signout");
                    }}
                  >
                    <DsIcon name="icon-bx-log-out-circle" className="mr-2" />
                    <span>Đăng xuất</span>
                  </div>
                ),
                key: "1",
              },
            ],
            name: userData?.name ? userData?.email : "",
            userName: userData?.name || userData?.email || "User",
          }}
        >
          <div className="p-4">{children}</div>
        </DsLayout>
      ) : null}
    </div>
  );
};

export default AdminLayout;
