import { DsButton, DsModal } from "mvi-ds-ui";
import React, { FC, ReactNode } from "react";

type Props = {
  open: boolean;
  onOK: () => void;
  onCancel?: () => void;
  children?: ReactNode;
};

const ConfirmRemove: FC<Props> = ({ open, onOK, onCancel, children }) => {
  return (
    <DsModal
      title="Modal"
      open={open}
      onOk={onOK}
      onCancel={onCancel}
      okText="Xóa"
      cancelText="Hủy bỏ"
      width={500}
      okType="danger"
      footer={[
        <DsButton
          label="Xóa"
          onClick={onOK}
          type="light"
          color="red"
          className="mr-4"
          key="delete"
        />,
        <DsButton
          label="Hủy bỏ"
          onClick={onCancel}
          type="light"
          key="cancel"
        />,
      ]}
    >
      {!children && <p>Bạn có chắc chắc muốn xóa?</p>}
      {children}
    </DsModal>
  );
};

export default ConfirmRemove;
