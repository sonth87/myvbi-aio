import { array, number, object, string } from "yup";

export const FormDeNghiCapACSchema = object({
  MA_AC: string().required("Không được để trống"),
  SL_DN: number().required("Không được để trống"),
  Q_DN: number().required("Không được để trống"),
});



export const formDNACSchema = object({
  // P_SO_DN: string().required("Không được để trống"),
  P_LOAI: string().required("Không được để trống"),
  P_DS_DN: array()
    .of(FormDeNghiCapACSchema)
    .min(1, "Phải có ít nhất một đối tượng trong mảng")
    .required("Không được để trống"),
});



export const FormDieuChuyenSchema = object({
  P_MA_LOAI: string().required("Không được để trống"),
  P_MA_CB: string().required("Không được để trống"),
  P_PHAN_BO_CT: array()
    // .of(FormDeNghiCapACSchema)
    .min(1, "Phải có ít nhất một đối tượng trong mảng")
    .required("Không được để trống"),
});

export const formMatHongSchema = object({
  P_MA_LOAI: string().required("Không được để trống"),
  P_PHAN_BO_CT: array()
    // .of(FormDeNghiCapACSchema)
    .min(1, "Phải có ít nhất một đối tượng trong mảng")
    .required("Không được để trống"),
});

