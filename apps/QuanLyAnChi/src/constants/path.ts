export const ROUTES = {
  LOGIN: "/login",
  LOGIN_SUCCESS: "/redirect",
  ERROR: "/404",
  DASHBOARD: "/",
  QLAC: "/quan-ly-an-chi",
  DKSD: "dang-ky-su-dung",
  DNCAC: "/de-nghi-cap-an-chi",
  DCAC: "/phan-bo-an-chi",
  QLMH: "/thong-bao-mat-hong",
  REDIRECT: "/redirect",
};
