export const GET_DM = "DN_LOAI_CAP";
export const GET_DV = "DN_DV_CN";
export const GET_TT = "DN_TTHAI_CAP";

export const COMMON_STATE_KEY = "commonState";
export const RESET_STATE_KEY = "resetState";

export const ALL = "ALL";
export const SUA = "SUA";
export const HUY = "HUY";
export const CAP_AC = "CAP_AC";
export const TU_CHOI = "TU_CHOI";
export const NHAN = "NHAN";
export const XUAT = "XUAT";
export const DUYET_NHAP = "DUYET_NHAP";

export const ENV = {
  file: process.env.REACT_APP_FILE_SERVER || "",
  be: process.env.REACT_APP_BE_AN_CHI_BASEURL || "",
};

export const SYSTEMCODE = "AN_CHI";
export const PRIVATEKEY = "5d2b0b946ae443c5a08a9905ffb29736";

export const typeLogin = {
  mcr: false,
  form: true,
};
export const MENU_LIST = "menu-list";
export const EMAIL_KEY = "email";
export const TOKEN_KEY = "code";
export const HA_TOKEN = "hacode";
export const KEY = "a375936a876de769923c507ce05f9e23";

export const TypeButonCommon = {
  TimKiemButton: "TimKiemButton",
  XuatExcelButton: "XuatExcelButton",
  TaoMoiButton: "TaoMoiButton",
};

export const Type = {
  ...TypeButonCommon,
  PheDuyetDeNghi: "PheDuyetDeNghi",
  NhanAnChi: "NhanAnChi",
  ChinhSuaButton: "ChinhSuaButton",
  CapAnChi: "CapAnChi",
  XoaDeNghi: "XoaDeNghi",
};
