import { SelectedAnChiType, SelectedType } from "../type/common";

export const convertDateTime = (dateTimeString: string) => {
  // Lấy các thành phần từ chuỗi
  const year = dateTimeString?.slice(0, 4);
  const month = dateTimeString?.slice(4, 6);
  const day = dateTimeString?.slice(6, 8);
  // Kết hợp các thành phần lại với nhau
  const formattedDateTime = `${day}/${month}/${year}`;
  return formattedDateTime;
};

// convert DD/MM/YYYY to YYYYMMDD
export const convertDate = (date: string) => {
  const parts = date.split("/"); // Tách chuỗi theo dấu "/"
  const formattedDate = `${parts[2]}${parts[1]}${parts[0]}`;
  return formattedDate;
};

export const convertToTimestamp = (dateTimeString: string) => {
  const [time, date] = dateTimeString.split(" ");
  const [hours, minutes] = time.split(":");
  const [day, month, year] = date.split("/");
  const isoDate = `${year}${month.padStart(2, "0")}${day.padStart(2, "0")}${hours.padStart(2, "0")}${minutes.padStart(2, "0")}00`; //yyyymmddhhmmss
  return isoDate;
};

//chuyển ngày tháng hiện tại
export function getCurrentDateFormatted(format?: string, firstDate?: boolean) {
  const currentDate = new Date();
  firstDate ? currentDate.setDate(1) : null;
  // Get the year, month, and day
  const year = currentDate.getFullYear();
  const month = (currentDate.getMonth() + 1).toString().padStart(2, "0"); // Months are zero-based
  const day = currentDate.getDate().toString().padStart(2, "0");
  const hours = currentDate.getHours().toString().padStart(2, "0");
  const minutes = currentDate.getMinutes().toString().padStart(2, "0");
  const seconds = currentDate.getSeconds().toString().padStart(2, "0");

  switch (format) {
    case "YYYYMMDD":
      return `${year}${month}${day}`;
    case "DD/MM/YYYY":
      return `${day}/${month}/${year}`;
    case "HHMMDDMMYYYY":
      return `${hours}:${minutes} ${day}/${month}/${year}`;
    case "YYYYMMDDHHMMSS":
      return `${year}${month}${day}${hours}${minutes}${seconds}`;
    default:
      break;
  }
}

export const filterMenu = (data: any) => {
  const MainMenu = data.map((item: any) => ({
    label: item?.TEN_NHOM,
    icon: "icon-bx-user-circle",
    path: item.children[0].URL, // Assuming the path is the URL of the first child
    group: item?.MA_NHOM?.toString(),
    childs: item.children.map((child: any) => ({
      id_cn: child.ID,
      label: child.TEN, // Correcting the typo from 'lable' to 'label'
      path: child.URL,
    })),
  }));
  return MainMenu;
};

export const converDataSelected = (data: SelectedType[]) => {
  const result = data?.map((item: SelectedType) => ({
    label: item.TEN,
    value: item.MA,
  }));
  return result;
};

export const converDataSelectedAnChi = (data: SelectedAnChiType[]) => {
  const result = data?.map((item: SelectedAnChiType) => ({
    label: `${item?.MA_AC} - ${item?.TEN_AC}`,
    value: item?.MA_AC,
  }));
  return result;
};

export const addCommas = (num: string) => {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const removeNonNumeric = (num: string) =>
  num.toString().replace(/[^0-9]/g, "");


export const checkValueObject  = (keyCheck: string[], dataCheck: any[] ) =>{
  let rs = !dataCheck.some(
    (obj: any) => !keyCheck.every((key: any) => obj[key] !== null && obj[key] !== "")
  )
  return rs;
}