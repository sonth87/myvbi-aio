import { MenuItems } from "mvi-ds-ui";
import { ROUTES } from "./path";

export const MainMenu: any = [
  {
    label: "Dashboard",
    icon: "icon-bxs-dashboard",
    path: ROUTES.DASHBOARD,
    group: "1",
  },
  {
    label: "Quản lý ấn chỉ",
    icon: "icon-bx-user-circle",
    path: ROUTES.QLAC,
    group: "2",
  },
];
