import { atom } from "recoil";
import { COMMON_STATE_KEY, RESET_STATE_KEY } from "../constants/const";
export const comomState = atom({
  key: COMMON_STATE_KEY,
  default: {
    danhMuc: null,
    donVi: null,
    trangThai: null,
  },
});

export const isResetState = atom({
  key: RESET_STATE_KEY,
  default: false,
});

export const statePhanBo = atom({
  key: "statesPhanBo",
  default: {
    loai_dieu_chuyen: null,
    nguoi_nhan: null,
  } as any,
})

export const stateHMH = atom({
  key: "statesHMH",
  default: {
    loai_hmh: null,
    don_vi: null,
  } as any,
})

