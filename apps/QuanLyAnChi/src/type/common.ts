export type FileUpload = {
  file_upload: string;
  original_name: string;
  mimetype: string;
  file_size: number;
};

export type NotifyType = { type: "success" | "error"; content?: string };

export type UserType = {
  username: string;
  email: string;
  name: string;
};

export type SelectedType = {
  MA: string;
  TEN: string;
};

export type SelectedAnChiType = {
  MA_AC: string;
  TEN_AC: string;
  NV: string;
  STATUS: number;
  GHI_CHU: string;
  SO_TO: null;
  SO_LIEN: null;
  NGUOI_TAO: string;
  NGAY_TAO: string;
  NGAY_SUA: string;
  NGUOI_SUA: string;
};

export type ValuesSearchProps = {
  P_HASH: string;
  P_TAI_KHOAN: string;
  P_ID_CN: string;
  P_LOAI_XUAT: string;
  P_MA_DVI_XUAT: string;
  P_MA_DVI_HOAN: string;
  P_SO_DN: string;
  P_TRANG_THAI: string;
  P_DU_PHONG: string;
  P_ID_DN: string;
  P_TU_NGAY: string | undefined;
  P_DEN_NGAY: string | undefined;
};

export type ValuesFormikDNAC = {
  P_SO_DN: string;
  P_LOAI: string;
  P_DS_DN: any;
};

export type FormDeNghiCapACType = {
  MA_AC: string;
  SL_DN: number;
  Q_DN: number;
};

export type ValuesFormikPhanBoAnChi = {
  P_SO_CT: string;
  P_NGAY_CT: string;
  P_DON_VI_CAP: string;
  P_DON_VI_NHAN: string;
  P_NOI_DUNG: string;
  P_MA_LOAI: string, // LOại điều chuyển
  P_MA_CB: string, // NGười nhận 
  P_PHAN_BO_CT: any
};

export type ValuesFormikMT = {
  P_SO_CT: string,
  P_NGAY_CT: string,
  P_MA_LOAI: string,
  P_NOI_DUNG: string,
  P_PHAN_BO_CT: any,
}