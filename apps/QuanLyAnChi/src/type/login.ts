export type LoginType = {
  email: string;
  password: string;
};

export type GetUserType = {
  P_EMAIL: string;
  P_TAI_KHOAN: string;
  P_MAT_KHAU: string;
  P_HASH: string;
};

export type GetListMenuType = {
  SystemCode: string;
  Email: string;
  Signature: string;
};
