import CryptoJS from "crypto-js";
import { PRIVATEKEY } from "../constants/const";

export const HmacKey256 = (objKey: any) => {
  // chuyển đổi các biến thành 1 chuỗi sẵn sàng để mã hóa
  const stringPrepared = Object.keys(objKey)
    .map((key) => `${key}=${objKey[key]}`)
    .join("&");

  // Khóa bí mật (secret key) sử dụng cho HMAC
  const secretKey = PRIVATEKEY || "";

  // Tạo HMAC-SHA256 hash
  const hash = CryptoJS.HmacSHA256(stringPrepared, secretKey);

  // Chuyển đổi hash thành chuỗi hex
  const hashInHex = hash.toString(CryptoJS.enc.Hex);

  return hashInHex;
};
