import { getMenuList } from "../apis/user";
import { SYSTEMCODE } from "../constants/const";
import { HmacKey256 } from "./hmackey";

export const functionGetMenuList = async (email: string) => {
  const SIGNATURE = HmacKey256({
    systemcode: SYSTEMCODE,
    email: email,
  });
  const bodyReq = {
    SystemCode: SYSTEMCODE,
    Email: email,
    Signature: SIGNATURE,
  };
  const resultMenuList = await getMenuList(bodyReq);
  return resultMenuList;
};
