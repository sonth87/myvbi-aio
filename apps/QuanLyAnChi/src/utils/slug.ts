export const slugConvert = (text: string) => {
  let slug = replaceUtf8Char(text);
  slug = replaceSpecialChar(slug);

  //Đổi khoảng trắng thành ký tự gạch ngang
  slug = slug.replace(/ /gi, "-");
  //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
  //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
  slug = slug.replace(/\-\-\-\-\-/gi, "-");
  slug = slug.replace(/\-\-\-\-/gi, "-");
  slug = slug.replace(/\-\-\-/gi, "-");
  slug = slug.replace(/\-\-/gi, "-");
  //Xóa các ký tự gạch ngang ở đầu và cuối
  slug = "@" + slug + "@";
  slug = slug.replace(/\@\-|\-\@|\@/gi, "");
  return slug;
};

export const replaceUtf8Char = (txt: string) => {
  //Đổi chữ hoa thành chữ thường
  let text = txt.toLowerCase();

  //Đổi ký tự có dấu thành không dấu
  text = txt.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, "a");
  text = text.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, "e");
  text = text.replace(/i|í|ì|ỉ|ĩ|ị/gi, "i");
  text = text.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, "o");
  text = text.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, "u");
  text = text.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, "y");
  text = text.replace(/đ/gi, "d");

  return text;
};

export const replaceSpecialChar = (txt: string) => {
  //Xóa các ký tự đặt biệt
  let text = txt.replace(
    /\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi,
    ""
  );

  return text;
};

export const productCodeConvert = (text: string) => {
  let code = replaceUtf8Char(text);
  code = code.replace(
    /\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\s|\/|\?|\>|\<|\'|\"|\:|\;|_/gi,
    ""
  );

  return code;
};
