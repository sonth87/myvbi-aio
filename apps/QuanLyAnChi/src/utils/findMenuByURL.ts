const findMenuByUrl: any = (menuArray: any, targetUrl: string) => {
  for (let menu of menuArray) {
    if (menu.path === targetUrl) {
      return menu;
    }

    if (menu.childs && menu.childs.length > 0) {
      const found = findMenuByUrl(menu.childs, targetUrl);
      if (found) {
        return found;
      }
    }
  }
  return null;
};

export const MenuButtonAfterFilter = (listMenu: any, locationPath: string) => {
  //tìm menu con theo URL
  const foundMenu = findMenuByUrl(JSON.parse(listMenu), locationPath);
  //tìm các button trong menu con đó
  const buttonByMenuList = findMenuByUrl(
    foundMenu?.childs,
    locationPath
  )?.childs;
  return buttonByMenuList;
};

export function hasButton(list: any, value: string) {
  return list.some((item: any) => item.authorized === value);
}
