import { message } from "mvi-ds-ui";
import React, {
  FC,
  Dispatch,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";
import { NotifyType } from "../type/common";
import { LoginType, UserTypeAC } from "@repo/types/user";
import { getMenuList, getUserInfo, getUserInfoWith365 } from "../apis/user";
import { filterMenu } from "../constants/genaralFunction";
import {
  MENU_LIST,
  PRIVATEKEY,
  SYSTEMCODE,
  typeLogin,
} from "../constants/const";
import { HmacKey256 } from "../utils/hmackey";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../constants/path";
import { functionGetMenuList } from "../utils/getMenuList";

const AuthContext = createContext<{
  isLoading: boolean;
  setIsLoading: Dispatch<any>;
  isAuth?: boolean;
  setIsAuth: Dispatch<any>;
  notify?: (n: NotifyType) => void;
  user: any;
  setUser: Dispatch<any>;
  isMenu: any;
  setIsMenu: Dispatch<any>;
  setLogin: Dispatch<any>;
}>({
  isLoading: false,
  setIsLoading: () => {},
  isAuth: false,
  setIsAuth: () => {},
  notify: () => {},
  user: null,
  setUser: () => {},
  isMenu: null,
  setIsMenu: () => {},
  setLogin: () => {},
});

const AuthenticationProvider: FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const navigate = useNavigate();
  const [messageApi, contextHolder] = message.useMessage();
  const [isLoading, setIsLoading] = useState(false);
  const [user, setUser] = useState<UserTypeAC | null>(null);
  const [isAuth, setIsAuth] = useState<boolean>();
  const [isMenu, setIsMenu] = useState([]);
  const [login, setLogin] = useState<LoginType>(typeLogin);
  const emailSession = window.sessionStorage.getItem("email");
  const codeSession = window.sessionStorage.getItem("code");

  const getUser = async () => {
    const reqBody = {
      P_EMAIL: emailSession || "",
      P_TAI_KHOAN: "",
      P_MAT_KHAU: "",
      P_HASH: "",
    };

    const userInfo = await getUserInfo(reqBody);
    setIsLoading(true);
    if (userInfo?.data?.Success) {
      const rsMenuList = await functionGetMenuList(userInfo?.data?.data?.EMAIL);
      if (rsMenuList.Data.Data[0].Items) {
        const menuList = rsMenuList.Data.Data[0].Items.map((item: any) => ({
          label: item.MenuName,
          icon: item.MenuIcon || "icon-bxs-dashboard",
          path: item.MenuUrl,
          group: item.MenuType,
          authorized: item.Authorize,
          childs:
            item.Items.length > 0
              ? item.Items.map((itemChild: any) => ({
                  label: itemChild.MenuName,
                  path: itemChild.MenuUrl,
                  childs: [],
                  authorized: item.Authorize,
                }))
              : null,
        })).sort((a: any, b: any) => a.sort - b.sort);

        const transformMenuData = (menuArray: any, group = "1") => {
          return menuArray
            .map((item: any) => {
              const transformedItem: any = {
                label: item.MenuName,
                icon: item.MenuIcon || "icon-bxs-dashboard",
                path: item.MenuUrl,
                group: item.MenuType,
                authorized: item.Authorize,
                sort: item.MenuSort,
              };

              // Nếu phần tử có Items, chuyển đổi chúng thành children
              if (item.Items && item.Items.length > 0) {
                transformedItem.childs = transformMenuData(item.Items, group);
              }

              return transformedItem;
            })
            .sort((a: any, b: any) => a.sort - b.sort);
        };

        // Chuyển đổi dữ liệu
        const transformedData = transformMenuData(
          rsMenuList.Data.Data[0].Items
        );

        window.sessionStorage.setItem(
          MENU_LIST,
          JSON.stringify(transformedData)
        );
        setIsMenu(menuList);
      }
      setIsLoading(false);
      setIsAuth(true);
      setUser({
        email: userInfo?.data?.data?.EMAIL,
        hash: userInfo?.data?.data?.HASH,
        loai: userInfo?.data?.data?.LOAI,
        name: userInfo?.data?.data?.NAME,
        phong: userInfo?.data?.data?.PHONG,
        tai_khoan: userInfo?.data?.data?.TAI_KHOAN,
      });
    } else {
      setIsLoading(false);
      setUser(null);
      setIsAuth(false);
    }
  };
  const getUserWith365 = async () => {
    const userInfo = await getUserInfoWith365();
    if (userInfo?.email) {
      //sau khi gọi 365 thành công sẽ gọi API của anh Khoa
      const result = await getUserInfo({
        P_EMAIL: userInfo?.email,
        P_TAI_KHOAN: "",
        P_MAT_KHAU: "",
        P_HASH: "",
      });
      // nếu không pass qua thì dừng lại
      if (!result?.data?.Success) {
        return false;
      } else {
        const rsMenuList = await functionGetMenuList(userInfo?.email);
        if (rsMenuList.Data.Data[0].Items) {
          const menuList = rsMenuList.Data.Data[0].Items.map((item: any) => ({
            label: item.MenuName,
            icon: item.MenuIcon || "icon-bxs-dashboard",
            path: item.MenuUrl,
            group: item.MenuType,
            authorized: item.Authorize,
            childs:
              item.Items.length > 0
                ? item.Items.map((itemChild: any) => ({
                    label: itemChild.MenuName,
                    path: itemChild.MenuUrl,
                    childs: [],
                    authorized: item.Authorize,
                  }))
                : null,
          })).sort((a: any, b: any) => a.sort - b.sort);

          const transformMenuData = (menuArray: any, group = "1") => {
            return menuArray
              .map((item: any) => {
                const transformedItem: any = {
                  label: item.MenuName,
                  icon: item.MenuIcon || "icon-bxs-dashboard",
                  path: item.MenuUrl,
                  group: item.MenuType,
                  authorized: item.Authorize,
                  sort: item.MenuSort,
                };

                // Nếu phần tử có Items, chuyển đổi chúng thành children
                if (item.Items && item.Items.length > 0) {
                  transformedItem.childs = transformMenuData(item.Items, group);
                }

                return transformedItem;
              })
              .sort((a: any, b: any) => a.sort - b.sort);
          };

          // Chuyển đổi dữ liệu
          const transformedData = transformMenuData(
            rsMenuList.Data.Data[0].Items
          );

          window.sessionStorage.setItem(
            MENU_LIST,
            JSON.stringify(transformedData)
          );
          setIsMenu(menuList);
        }
      }
      //nếu pass qua thì gọi API của anh Quý
      setIsLoading(false);
      setIsAuth(true);
      setUser({
        email: userInfo?.data?.data?.EMAIL,
        hash: userInfo?.data?.data?.HASH,
        loai: userInfo?.data?.data?.LOAI,
        name: userInfo?.data?.data?.NAME,
        phong: userInfo?.data?.data?.PHONG,
        tai_khoan: userInfo?.data?.data?.TAI_KHOAN,
      });
      navigate(ROUTES.DASHBOARD);
    } else {
      setIsLoading(false);
      setUser(null);
      setIsAuth(false);
    }
  };

  useEffect(() => {
    setIsAuth(false);
    setIsLoading(true);
    if (codeSession) {
      getUserWith365();
    } else {
      getUser();
    }
  }, []);

  const notify = ({ type, content }: NotifyType) => {
    messageApi.open({
      type: type,
      content:
        content || type === "success"
          ? content || "Cập nhật thành công"
          : content || "Đã xảy ra lỗi, vui lòng thử lại sau.",
      duration: 3,
      className: "center translate-y-16",
    });
  };

  return (
    <AuthContext.Provider
      value={{
        isLoading,
        setIsLoading,
        isAuth,
        setIsAuth,
        notify,
        user,
        setUser,
        isMenu,
        setIsMenu,
        setLogin,
      }}
    >
      {contextHolder}
      {children}
    </AuthContext.Provider>
  );
};

const useAuth = () => useContext(AuthContext);
export { AuthContext, useAuth };
export default AuthenticationProvider;
