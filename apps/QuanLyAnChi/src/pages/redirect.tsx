import React, { useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { EMAIL_KEY, MENU_LIST } from "../constants/const";
import { DsIcon } from "mvi-ds-ui";

const Redirect = () => {
  const listMenu: any = window.sessionStorage.getItem(MENU_LIST);
  const [searchParams] = useSearchParams();
  const email = searchParams.get("email");
  const hash = window.sessionStorage.getItem("hash");

  const parsedListMenu = JSON.parse(listMenu);

  useEffect(() => {
    if (email) {
      window.sessionStorage.setItem(EMAIL_KEY, email);
    }
    parsedListMenu
      ? window.location.replace(`/${parsedListMenu[0]?.path}`)
      : window.location.replace("/");
  }, [email]);

  useEffect(() => {
    if (hash && parsedListMenu) {
      window.location.replace(`/${parsedListMenu[0]?.path}`);
    }
  }, [hash]);

  return (
    <div className="absolute top-0 left-0 w-full h-full bg-transparent flex justify-center items-center z-50">
      <div className="absolute top-0 left-0 w-full h-full bg-black opacity-50" />
      <div className="flex justify-center items-center relative text-2xl text-white font-bold">
        <DsIcon
          name="icon-bx-loader-alt"
          className="mr-4 animate-spin !text-3xl"
        />
        Đang kiểm tra...
      </div>
    </div>
  );
};

export default Redirect;
