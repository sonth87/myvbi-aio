import { useMutation, useQueryClient } from "@tanstack/react-query";
import { DsButton, DsModal, message } from "mvi-ds-ui";
import { FC } from "react";
import { delDeNghiNhap } from "../../apis/AnChiApi";
import { useAuth } from "../../context/AuthContext";
import { ALL, HUY } from "../../constants/const";

type Props = {
  isOpen: boolean;
  pId?: string | undefined;
  handleClose: (val: boolean, record?: any) => void;
};

const HuyAnChi: FC<Props> = ({ isOpen, pId, handleClose }) => {
  const { user } = useAuth();

  const listMenu: any = window.sessionStorage.getItem("list-menu");
  const parsedListMenu = JSON.parse(listMenu);

  const [messageApi, contextHolder] = message.useMessage();
  const queryCache = useQueryClient();

  const { mutateAsync: delACAsync, isPending: pendingDelete } = useMutation({
    mutationFn: delDeNghiNhap,
  });

  const handleRemoveProduct = async () => {
    if (pId && isOpen) {
      const rs: any = await delACAsync({
        P_HASH: user.hash,
        P_TAI_KHOAN: user.email,
        P_ID_CN: parsedListMenu && parsedListMenu[1].group,
        P_LOAI: HUY,
        P_MA_DVI: ALL,
        P_PHONG: ALL,
        P_SO_DN: ALL,
        P_GHI_CHU: ALL,
        P_NOI_DUNG: ALL,
        P_ID_DN: `${pId}`,
        P_DANG_KY_CT: "",
      });
      messageApi.open({
        type: rs?.data?.data?.Success ? "success" : "error",
        content: rs?.data?.data?.Success
          ? rs?.data?.data?.mess
          : "Đã xảy ra lỗi, vui lòng thử lại sau.",
        duration: 5,
      });
      handleClose(false);
      queryCache.invalidateQueries({ queryKey: ["list-an-chi"] });
    }
  };

  return (
    <div>
      {contextHolder}
      <DsModal
        centered
        open={isOpen}
        onCancel={() => handleClose(false)}
        footer={[
          <DsButton
            key="confirm-del-prod"
            isLoading={pendingDelete}
            onClick={handleRemoveProduct}
            color="red"
            disabled={pendingDelete}
          >
            {pendingDelete ? "Đang xóa đề nghị" : "Xác nhận xóa"}
          </DsButton>,
          <DsButton
            key="cancel-del-prod"
            onClick={() => handleClose(false)}
            className="ds-ml-4"
            disabled={pendingDelete}
          >
            Thoát
          </DsButton>,
        ]}
      >
        Bạn có chắc chắn muốn xóa đề nghị này không?
      </DsModal>
    </div>
  );
};

export default HuyAnChi;
