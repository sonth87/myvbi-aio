import { useFormik } from "formik";
import {
  DsButton,
  DsDatePicker,
  DsInput,
  DsModal,
  DsTable,
  message,
} from "mvi-ds-ui";
import { FC, useState } from "react";
import { cols } from "./colNhanAnChi";
import { acDeNghiXuatHoan, getAnChiChiTiet } from "../../apis/AnChiApi";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useAuth } from "../../context/AuthContext";
import { ALL, CAP_AC, NHAN, TU_CHOI } from "../../constants/const";

type Props = {
  isOpen: boolean;
  handleClose: (value: boolean, record?: any) => void;
  pId: number;
};

const NhanAnChi: FC<Props> = ({ isOpen, handleClose, pId }) => {
  const { user } = useAuth();
  const [messageApi, contextHolder] = message.useMessage();
  const queryCache = useQueryClient();

  const listMenu: any = window.sessionStorage.getItem("list-menu");
  const parsedListMenu = JSON.parse(listMenu);

  const { data: dataDeNghiAnChiChiTiet } = useQuery({
    queryKey: ["an-chi-chi-tiet", pId, isOpen],
    queryFn: () =>
      getAnChiChiTiet({
        P_HASH: user.hash,
        P_TAI_KHOAN: user.email,
        P_ID_CN: parsedListMenu[1]?.group,
        P_LOAI_XUAT: CAP_AC,
        P_MA_DVI_XUAT: ALL,
        P_MA_DVI_HOAN: ALL,
        P_SO_DN: ALL,
        P_TRANG_THAI: ALL,
        P_DU_PHONG: ALL,
        P_ID_DN: `${pId}`,
        P_TU_NGAY: "",
        P_DEN_NGAY: "",
      }),
    enabled: !!pId && isOpen,
    refetchOnWindowFocus: false,
    select: (data) => {
      return data?.data?.data;
    },
  });
  const { mutateAsync } = useMutation({
    mutationFn: (data: any) => acDeNghiXuatHoan(data),
  });

  const [optSubmit, setOptSubmit] = useState(false);

  const handleSubmitReject = () => {
    setOptSubmit(false);
    form.submitForm();
  };
  const handleSubmitApprove = () => {
    setOptSubmit(true);
    form.submitForm();
  };

  const form = useFormik({
    initialValues: {
      P_ID_DN: dataDeNghiAnChiChiTiet?.data?.SO_DN || "",
      NGAY_NHAN: dataDeNghiAnChiChiTiet?.data?.NGAY_NHAN || "",
      CHI_TIET: dataDeNghiAnChiChiTiet?.data?.CHI_TIET || [],
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      let P_DANG_KY_CT_string = JSON.stringify(values.CHI_TIET).replace(
        /"/g,
        '\\"'
      );
      const dataBody = {
        P_HASH: user.hash,
        P_TAI_KHOAN: user.tai_khoan,
        P_ID_CN: parsedListMenu && parsedListMenu[1].group,
        P_LOAI: optSubmit ? NHAN : TU_CHOI,
        P_ID_DN: `${pId}`,
        P_NOI_DUNG: "ALL",
        P_AN_CHI_CT: P_DANG_KY_CT_string,
      };

      const rs = await mutateAsync(dataBody);
      messageApi.open({
        type: rs?.data?.data?.Success ? "success" : "error",
        content: rs?.data?.data?.mess,
        duration: 5,
      });
      handleClose(false);
      queryCache.invalidateQueries({ queryKey: ["list-an-chi"] });
    },
  });

  return (
    <div>
      {contextHolder}
      <DsModal
        open={isOpen}
        width={"80%"}
        centered
        title="Nhận ấn chỉ"
        closable={true}
        onCancel={() => handleClose(false)}
        footer={[
          <DsButton
            color="red"
            key="cancel-del-prod"
            onClick={() => {
              // handleClose(false);z
              // form.submitForm();
              handleSubmitReject();
            }}
            className="ds-mr-4"
          >
            Từ chối
          </DsButton>,
          <DsButton
            key="confirm-del-prod"
            //@ts-ignore
            onClick={() => handleSubmitApprove()}
          >
            Xác nhận
          </DsButton>,
        ]}
      >
        <form onSubmit={form.handleSubmit}>
          <div className="grid gap-4">
            <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
              <DsInput
                name="P_ID_DN"
                label="Số đề nghị"
                type="text"
                disabled={true}
                value={form?.values?.P_ID_DN}
                onChange={(e) => form.setFieldValue("P_SO_DN", e || "")}
              />
              <DsDatePicker
                label="Ngày đề nghị"
                size="large"
                name="NGAY_NHAN"
                format="DD/MM/YYYY"
                value={form?.values?.NGAY_NHAN}
                disabled={true}
                picker="date"
              />
            </div>
          </div>
          <div className="mt-4">
            <DsTable
              bordered
              columns={cols()}
              dataSource={form?.values?.CHI_TIET || []}
              className="!contents"
            />
          </div>
        </form>
      </DsModal>
    </div>
  );
};

export default NhanAnChi;
