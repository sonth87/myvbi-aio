import { useQuery } from "@tanstack/react-query";
import {
  DsButton,
  DsIcon,
  DsInput,
  DsModal,
  DsSelect,
  Form,
  DsTable,
  DsPopover,
} from "mvi-ds-ui";
import { FC, useEffect, useState } from "react";
import { useAuth } from "../../context/AuthContext";
import { getDSAnChi } from "../../apis/AnChiApi";
import {
  addCommas,
  converDataSelectedAnChi,
  removeNonNumeric,
} from "../../constants/genaralFunction";
import { useRecoilValue } from "recoil";
import { isResetState } from "../../recoil/atom";

type Props = {
  handleDataTable: (value: any) => void;
  dataList?: any;
  isResetTableList?: boolean;
};

interface Item {
  ID: string;
  MA_AC: string;
  SL_DN: string;
  Q_DN: string;
  GHI_CHU: string;
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  editing: boolean;
  dataIndex: string;
  title: any;
  inputType: "number" | "text" | "select" | "input";
  record: Item;
  index: number;
}

const TableCapAC: FC<Props> = ({
  handleDataTable,
  dataList,
  isResetTableList,
}) => {
  const [form] = Form.useForm();
  const [data, setData] = useState<any>(dataList);
  const [editingID, setEditingID] = useState<any>();
  const [statusAdd, setStatusAdd] = useState(false);
  const { user } = useAuth();
  const isReset = useRecoilValue(isResetState);

  useEffect(() => {
    const dataObject: any =
      dataList &&
      dataList.flatMap((item: any) => {
        return (
          Array(item.Q_DN)
            //@ts-ignore
            .fill()
            .map(() => ({
              STT: null,
              MA_AC: item.MA_AC,
              SO_SO: "",
              TU_SO: "",
              DEN_SO: "",
              SO_LUONG: "",
              KY_HIEU: "",
              DON_GIA: "",
              THANH_TIEN: "",
              DA_DUNG: "0",
            }))
        );
      });
    // Fill in the STT sequentially
    dataObject.forEach((item: any, index: any) => {
      item.STT = `${index + 1}`;
    });
    setData(dataObject);
  }, [dataList]);

  const dataBody = {
    P_HASH: user.hash,
    P_TAI_KHOAN: user.tai_khoan,
    P_ID_CN: "",
    P_LOAI: "DULIEU",
    P_MA_AC: "",
    P_TEN: "",
    P_NV: "",
    P_GHI_CHU: "",
  };

  const { data: dataDSAnChi } = useQuery({
    queryKey: ["ma-an-chi-list", dataBody],
    queryFn: () => getDSAnChi(dataBody),
    refetchOnWindowFocus: false,
    select: (data) => {
      return converDataSelectedAnChi(data);
    },
  });

  const EditableCell: React.FC<React.PropsWithChildren<EditableCellProps>> = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
  }) => {
    const handleChange = (e: any, dataindex: any) => {
      console.log(e);
      const value = e ? e : 0;
      if (dataindex === "DON_GIA" || dataindex === "SO_LUONG") {
        const valueInput = removeNonNumeric(`${value}`);
        form.setFieldsValue({ [dataindex]: addCommas(`${valueInput}`) });
      } else {
        form.setFieldsValue({ [dataindex]: `${value}` });
      }

      //bắt đầu tính số lượng
      if (
        form.getFieldValue("TU_SO") &&
        form.getFieldValue("DEN_SO") &&
        Number(form.getFieldValue("TU_SO")) > 0 &&
        Number(form.getFieldValue("DEN_SO")) > 0
      ) {
        let soLuong =
          Number(form.getFieldValue("DEN_SO")) -
          Number(form.getFieldValue("TU_SO")) +
          1;
        form.setFieldsValue({ SO_LUONG: `${soLuong}` });
      }
      //kết thúc tính số lượng

      //Bắt đầu tính Thành tiền
      if (form.getFieldValue("DON_GIA") && form.getFieldValue("SO_LUONG")) {
        let thanhTien =
          Number(removeNonNumeric(form.getFieldValue("DON_GIA"))) *
          Number(removeNonNumeric(form.getFieldValue("SO_LUONG")));
        form.setFieldsValue({ THANH_TIEN: addCommas(`${thanhTien}`) });
      }
    };

    let inputNode;
    if (inputType === "number") {
      inputNode = (
        <DsInput
          type="number"
          size="small"
          min={0}
          onChange={(e) => handleChange(e, dataIndex)}
          disabled={dataIndex === "SO_LUONG" ? true : false}
        />
      );
    } else if (inputType === "select") {
      inputNode = <DsSelect size="small" options={dataDSAnChi || []} />;
    } else if (inputType === "text") {
      inputNode = (
        <DsInput
          size="small"
          type="text"
          onChange={(e) => handleChange(e, dataIndex)}
          disabled={dataIndex === "THANH_TIEN" ? true : false}
        />
      );
    }

    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item
            name={dataIndex}
            style={{ margin: 0 }}
            rules={[
              {
                required: true,
                message: `Không bỏ trống ô ${title}!`,
              },
            ]}
          >
            {inputNode}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  const isEditing = (record: Item, index: number) => index === editingID;
  const edit = (record: Partial<Item> & { ID: React.Key }, index: number) => {
    form.setFieldsValue({
      MA_AC: "",
      SO_SO: "",
      TU_SO: "",
      DEN_SO: "",
      SO_LUONG: "",
      ...record,
    });
    setEditingID(index);
  };

  const cancel = (ID: string) => {
    setEditingID("");
    if (statusAdd) {
      const newData = data.filter((item: any) => item.ID !== ID);
      setData(newData);
      setStatusAdd(false);
    }
  };
  useEffect(() => {
    handleDataTable(data);
  }, [data]);

  const save = async (indexRecord: React.Key) => {
    try {
      const row = (await form.validateFields()) as any;
      const newData = [...data];
      const index = newData.findIndex((item, index) => indexRecord === index);
      console.log(index);
      if (index > -1) {
        setStatusAdd(false);
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setData(newData);
        setEditingID("");
      } else {
        setStatusAdd(false);
        newData.push(row);
        setData(newData);
        setEditingID("");
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };

  const handleDelete = (ID: string) => {
    const newData = data.filter((item: any) => item.ID !== ID);
    setData(newData);
  };

  const columns = [
    {
      title: "Mã ấn chỉ",
      dataIndex: "MA_AC",
      width: 300,
      fixed: "left",
      editable: true,
      render: (_: any, record: Item) => {
        const item =
          dataDSAnChi &&
          dataDSAnChi.find((element: any) => element?.value === record?.MA_AC);
        const labelCol = item?.label || null;
        return labelCol;
      },
    },
    {
      title: "Mã quyển",
      dataIndex: "SO_SO",
      width: 150,
      align: "center",
      editable: true,
    },
    {
      title: "Số đầu",
      dataIndex: "TU_SO",
      width: 150,
      align: "center",
      editable: true,
    },
    {
      title: "Số cuối",
      dataIndex: "DEN_SO",
      width: 150,
      align: "center",
      editable: true,
    },
    {
      title: "Số lượng",
      dataIndex: "SO_LUONG",
      width: 150,
      align: "center",
      editable: true,
    },
    {
      title: "Đơn giá",
      dataIndex: "DON_GIA",
      width: 150,
      align: "center",
      editable: true,
    },
    {
      title: "Thành tiền",
      dataIndex: "THANH_TIEN",
      width: 150,
      align: "center",
      editable: true,
    },
    {
      title: "Ký hiệu",
      dataIndex: "KY_HIEU",
      width: 150,
      align: "center",
      editable: true,
    },
    {
      title: "Thao tác",
      dataIndex: "ThaoTac",
      fixed: "right",
      width: 80,
      align: "center",
      render: (_: any, record: Item, index: any) => {
        const editable = isEditing(record, index);
        return editable ? (
          <div className="flex justify-center">
            <DsIcon
              name="icon-bxs-check-circle"
              className="md:text-md pt-1 text-[#37b652]"
              onClick={() => save(index)}
            />

            <DsPopover
              closeOnClick={true}
              trigger={["click"]}
              content={
                <>
                  <div className="flex item-center mb-1">
                    <DsIcon
                      name="icon-bxs-error-circle"
                      className="md:text-md text-[#FAAD14] mr-1"
                    />
                    <p>Bạn có chắc chắn hủy bản này không?</p>
                  </div>

                  <div className="flex justify-around">
                    <DsButton
                      onClick={(e) => {}}
                      color="gray"
                      type="outline"
                      size="small"
                    >
                      Thoát
                    </DsButton>
                    <DsButton
                      onClick={() => cancel(record.ID)}
                      size="small"
                      color="red"
                    >
                      Hủy
                    </DsButton>
                  </div>
                </>
              }
              position="bottomLeft"
            >
              <DsIcon
                name="icon-bx-x"
                className="md:text-md pt-1 text-[#b43c3c]"
              />
            </DsPopover>
          </div>
        ) : (
          <div className="flex justify-center">
            <DsIcon
              name="icon-bx-edit"
              className="md:text-md pt-1 text-[#1779e2]"
              onClick={() => edit(record, index)}
            />
            <DsPopover
              closeOnClick={true}
              trigger={["click"]}
              content={
                <>
                  <div className="flex item-center mb-1">
                    <DsIcon
                      name="icon-bxs-error-circle"
                      className="md:text-md text-[#FAAD14] mr-1"
                    />
                    <p>Bạn có chắc chắn xóa không?</p>
                  </div>

                  <div className="flex justify-around">
                    <DsButton
                      // className="p-1 cursor-pointer hover:bg-[#FAFAFA]"
                      onClick={(e) => {}}
                      color="gray"
                      type="outline"
                      size="small"
                    >
                      Hủy
                    </DsButton>
                    <DsButton
                      // className="p-1 cursor-pointer  hover:bg-[#FAFAFA]"
                      onClick={() => handleDelete(record.ID)}
                      size="small"
                      color="red"
                    >
                      Xóa
                    </DsButton>
                  </div>
                </>
              }
              onOpenChange={() => {}}
              position="bottomLeft"
            >
              {/* <DsIcon
                name="icon-bx-trash"
                className="md:text-md pt-1 text-[#F4175A]"
              /> */}
            </DsPopover>
          </div>
        );
      },
    },
  ];

  const mergedColumns: any = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record: Item, index: number) => ({
        record,
        inputType:
          col.dataIndex === "TU_SO" ||
          col.dataIndex === "DEN_SO" ||
          col.dataIndex === "SO_LUONG"
            ? "number"
            : col.dataIndex === "MA_AC"
              ? "select"
              : "text",
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record, index),
      }),
    };
  });

  // Hàm xử lý thêm dòng mới
  const handleAddRow = () => {
    setStatusAdd(true);
    const newSTT = (data.length + 1).toString();
    const newData: any = {
      STT: newSTT,
      MA_AC: "",
      SO_SO: "",
      TU_SO: "",
      DEN_SO: "",
      SO_LUONG: "",
    };
    setData([newData, ...data]);
    setEditingID(newSTT); // Đánh dấu dòng mới để chỉnh sửa ngay lập tức
    form.resetFields();
  };

  useEffect(() => {
    if (!isReset) {
      setEditingID("");
    }
  }, [isReset]);

  return (
    <div>
      <p className="my-1 font-medium">Danh sách đề nghị cấp</p>
      {/* <DsButton
        onClick={handleAddRow}
        disabled={statusAdd}
        className="mb-2"
        buttonType="button"
      >
        + Thêm
      </DsButton> */}

      <Form form={form} component={false}>
        <DsTable
          scroll={{ y: 300 }}
          virtual
          className="w-full min-h-[300px] !inline"
          components={{
            body: {
              cell: EditableCell,
            },
          }}
          bordered
          dataSource={data}
          columns={mergedColumns}
          rowClassName="editable-row"
        />
      </Form>
    </div>
  );
};

export default TableCapAC;
