import { useFormik } from "formik";
import { DsButton, DsDatePicker, DsInput, DsModal, message } from "mvi-ds-ui";
import { FC, useEffect, useState } from "react";
import { formDNACSchema } from "../../constants/validation";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useAuth } from "../../context/AuthContext";
import { ALL, XUAT } from "../../constants/const";
import { acDeNghiXuatHoan, getAnChiChiTiet } from "../../apis/AnChiApi";
import TableCapAC from "./TableCapAC";
import { useRecoilState } from "recoil";
import { isResetState } from "../../recoil/atom";
import { checkValueObject, removeNonNumeric } from "../../constants/genaralFunction";

type Props = {
  isOpen: boolean;
  handleClose: (value: boolean, record?: any) => void;
  pId: number;
};

const CapAnChi: FC<Props> = ({ isOpen, handleClose, pId }) => {
  const { user, notify } = useAuth();
  const queryCache = useQueryClient();
  const [isResetTableState, setIsResetTableState] = useRecoilState(isResetState);
  const [messageApi, contextHolder] = message.useMessage();
  const listMenu: any = window.sessionStorage.getItem("list-menu");
  const parsedListMenu = JSON.parse(listMenu);

  const { data: dataDeNghiAnChiChiTiet } = useQuery({
    queryKey: ["an-chi-chi-tiet", pId, isOpen],
    queryFn: () =>
      getAnChiChiTiet({
        P_HASH: user.hash,
        P_TAI_KHOAN: user.email,
        P_ID_CN: parsedListMenu[1]?.group,
        P_LOAI_XUAT: ALL,
        P_MA_DVI_XUAT: ALL,
        P_MA_DVI_HOAN: ALL,
        P_SO_DN: ALL,
        P_TRANG_THAI: ALL,
        P_DU_PHONG: ALL,
        P_ID_DN: `${pId}`,
        P_TU_NGAY: "",
        P_DEN_NGAY: "",
      }),
    enabled: !!pId && isOpen,
    refetchOnWindowFocus: false,
    select: (data) => {
      return data?.data?.data;
    },
  });
  const { mutateAsync } = useMutation({
    mutationFn: (data: any) => acDeNghiXuatHoan(data),
  });

  const form = useFormik({
    initialValues: {
      P_ID_DN: dataDeNghiAnChiChiTiet?.data?.SO_DN || "",
      NGAY_DN: dataDeNghiAnChiChiTiet?.data?.NGAY_DN || "",
      CHI_TIET: dataDeNghiAnChiChiTiet?.data?.CHI_TIET || [],
      P_DS_PB_AC: dataDeNghiAnChiChiTiet?.data?.P_AN_CHI_CT || [],
    },
    enableReinitialize: true,
    onSubmit: async () => {
      const data_P_DS_PB_AC = form.values?.P_DS_PB_AC?.map((item: any) => ({
        ...item,
        DON_GIA: removeNonNumeric(item.DON_GIA),
        THANH_TIEN: removeNonNumeric(item.THANH_TIEN),
      }));

      let resultCheck = checkValueObject(["SO_SO", "TU_SO","DEN_SO","SO_LUONG","KY_HIEU","DON_GIA","THANH_TIEN"], data_P_DS_PB_AC);
    
      if(!resultCheck){
        messageApi.open({
          type: "error",
          content: "Nhập đủ thông tin cấp phát theo số lượng đã phê duyệt!",
          duration: 5,
          className: "right",
        });
        return false
      }
      const dataBody = {
        P_HASH: user.hash,
        P_TAI_KHOAN: user.email,
        P_ID_CN: parsedListMenu[1]?.group,
        P_LOAI: XUAT,
        P_ID_DN: `${pId}`,
        P_NOI_DUNG: ALL,
        P_AN_CHI_CT: JSON.stringify(data_P_DS_PB_AC),
      };
      const rs = await mutateAsync(dataBody);
      console.log(rs);
      messageApi.open({
        type: rs?.data?.data?.Success ? "success" : "error",
        content: rs?.data?.data?.mess,
        duration: 5,
      });
      handleClose(false);
      queryCache.invalidateQueries({ queryKey: ["list-an-chi"] });
    },
  });

  const handleDataTable = (value: any) => {
    form.setFieldValue("P_DS_PB_AC", value);
  };

  // useEffect(() => {
  //   if (!isResetTableList) {
  //     form.resetForm();
  //     handleClose(false);
  //   }
  //   setIsResetTableList(false);
  // }, [isResetTableList]);

  return (
    <div>
      {contextHolder}
      <DsModal
        open={isOpen}
        width={"95%"}
        centered
        title="Cấp ấn chỉ"
        closable={false}
        footer={[
          <DsButton
            // disabled={Object.keys(form.errors).length !== 0}
            key="confirm-del-prod"
            //@ts-ignore
            onClick={form.handleSubmit}
          >
            Lưu
          </DsButton>,
          <DsButton
            color="red"
            key="cancel-del-prod"
            onClick={() => {
              handleClose(false);
              setIsResetTableState(true);
              form.resetForm();
            }}
            className="ds-ml-4"
          >
            Thoát
          </DsButton>,
        ]}
      >
        <form onSubmit={form.handleSubmit}>
          <div className="grid gap-4">
            <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
              <DsInput
                name="P_SO_CT"
                label="Số CT"
                type="text"
                disabled={true}
                value={form?.values?.P_ID_DN}
              />
              <DsDatePicker
                label="Ngày cấp"
                size="large"
                name="NGAY_DN"
                format="DD/MM/YYYY"
                disabled={true}
                value={form.values?.NGAY_DN}
                picker="date"
              />
            </div>
          </div>
          <div className="mt-4">
            <TableCapAC
              handleDataTable={handleDataTable}
              dataList={form.values.CHI_TIET}
              isResetTableList={isOpen}
            />
          </div>
        </form>
      </DsModal>
    </div>
  );
};

export default CapAnChi;
