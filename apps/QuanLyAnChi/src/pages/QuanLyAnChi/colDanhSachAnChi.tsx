import { DsIcon, DsPopover } from "mvi-ds-ui";
import { MENU_LIST, Type, TypeButonCommon } from "../../constants/const";
import { MenuButtonAfterFilter } from "../../utils/findMenuByURL";

export const cols = (
  handleOpenModalEditAC: (value: boolean, record?: any) => void,
  handleOpenModalHuyAC: (value: boolean, record?: any) => void,
  handleOpenModalCapAC: (value: boolean, record?: any) => void,
  handleOpenModalNhanAC: (value: boolean, record?: any) => void,
  handleOpenModalPheDuyetAC: (value: boolean, record?: any) => void
) => [
  {
    title: "Số đề nghị",
    dataIndex: "SO_DN",
    key: "SO_DN",
    align: "center",
    fixed: "left",
    render: (_: any, record: any) => record.SO_DN,
  },
  {
    title: "Loại đề nghị",
    dataIndex: "tieu_de",
    key: "tieu_de",
    render: (_: any, record: any) => record.LOAI_DX,
  },
  {
    title: "Đơn vị đề xuất",
    dataIndex: "TEN_DVI_DN",
    key: "TEN_DVI_DN",
    render: (_: any, record: any) => record.TEN_DVI_DN,
  },
  {
    title: "Đơn vị cấp phát",
    dataIndex: "TEN_DVI_CAP",
    key: "TEN_DVI_CAP",
    render: (_: any, record: any) => record.TEN_DVI_CAP,
  },
  {
    title: "Trạng thái",
    dataIndex: "LABLE_STATUS",
    key: "LABLE_STATUS",
    render: (_: any, record: any) => <div>{record.LABLE_STATUS}</div>,
  },
  {
    title: "Ngày tạo",
    dataIndex: "NGAY_DN",
    key: "NGAY_DN",
    render: (_: any, record: any) => record.NGAY_DN,
  },
  {
    title: "Người tạo",
    dataIndex: "NGUOI_DN",
    key: "NGUOI_DN",
    render: (_: any, record: any) => record.NGUOI_DN,
  },
  {
    dataIndex: "thao-tac",
    title: `Thao tác`,
    width: 100,
    align: "center",
    fixed: "left",
    render: (_: any, record: any) => {
      const listMenu: any = window.sessionStorage.getItem(MENU_LIST);
      const buttonByMenuList = MenuButtonAfterFilter(
        listMenu,
        location.pathname.replace(/^\//, "")
      )
        //bỏ các button common
        .filter(
          (item: any) =>
            !Object.values(TypeButonCommon).includes(item.authorized)
        );
      const handleButtonClick = (authorized: string, record: any) => {
        switch (authorized) {
          case Type.PheDuyetDeNghi:
            handleOpenModalPheDuyetAC(true, record);
            break;
          case Type.NhanAnChi:
            handleOpenModalNhanAC(true, record);
            break;
          case Type.ChinhSuaButton:
            handleOpenModalEditAC(true, record);
            break;
          case Type.CapAnChi:
            handleOpenModalCapAC(true, record);
            break;
          case Type.XoaDeNghi:
            handleOpenModalHuyAC(true, record);
            break;
          default:
            console.log("Không có hành động phù hợp", record);
            break;
        }
      };

      const renderOptions = buttonByMenuList.map((item: any, index: number) => (
        <div
          key={index}
          className="p-1 cursor-pointer hover:bg-[#FAFAFA]"
          onClick={() => handleButtonClick(item.authorized, item)}
        >
          {item.label}
        </div>
      ));
      return (
        <div className="flex justify-center">
          <DsPopover
            closeOnClick={true}
            trigger={["click"]}
            content={
              <div className="flex flex-col divide-y">{renderOptions}</div>
            }
            onOpenChange={() => {}}
            position="bottomLeft"
          >
            <DsIcon name="icon-bx-dots-horizontal-rounded" />
          </DsPopover>
        </div>
      );
    },
  },
];
