import { useQuery } from "@tanstack/react-query";
import {
  DsButton,
  DsIcon,
  DsInput,
  DsModal,
  DsSelect,
  Form,
  DsTable,
  DsPopover,
} from "mvi-ds-ui";
import { FC, useEffect, useState } from "react";
import { useAuth } from "../../context/AuthContext";
import { getDSAnChi } from "../../apis/AnChiApi";
import { converDataSelectedAnChi } from "../../constants/genaralFunction";
import { useRecoilValue } from "recoil";
import { isResetState } from "../../recoil/atom";

type Props = {
  handleDataTable: (value: any) => void;
  dataList?: any;
  isResetTableList?: boolean;
};

interface Item {
  STT: string;
  MA_AC: string;
  SL_DN: string;
  Q_DN: string;
  GHI_CHU: string;
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  editing: boolean;
  dataIndex: string;
  title: any;
  inputType: "number" | "text" | "select" | "input";
  record: Item;
  index: number;
}

const TableDSCapAC: FC<Props> = ({
  handleDataTable,
  dataList,
  isResetTableList,
}) => {
  const [form] = Form.useForm();
  const [data, setData] = useState(dataList);
  const [editingSTT, setEditingSTT] = useState("");
  const [statusAdd, setStatusAdd] = useState(false);
  const { user } = useAuth();
  const isReset = useRecoilValue(isResetState);

  useEffect(() => {
    setData(dataList);
  }, [dataList]);
  const dataBody = {
    P_HASH: user.hash,
    P_TAI_KHOAN: user.tai_khoan,
    P_ID_CN: "",
    P_LOAI: "DULIEU",
    P_MA_AC: "",
    P_TEN: "",
    P_NV: "",
    P_GHI_CHU: "",
  };
  const { data: dataDSAnChi } = useQuery({
    queryKey: ["ma-an-chi-list", dataBody],
    queryFn: () => getDSAnChi(dataBody),
    refetchOnWindowFocus: false,
    select: (data) => {
      return converDataSelectedAnChi(data);
    },
  });

  const EditableCell: React.FC<React.PropsWithChildren<EditableCellProps>> = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
  }) => {
    let inputNode;
    const handleChangeInput = async (value: any, dataIndex: any, record: any) => {
      if (dataIndex === "SL_DN") {
        let so_q = Math.ceil(value / 10);
        form.setFieldsValue({ "Q_DN": so_q});
      }
    }
    if (inputType === "number") {
      inputNode = 
      <DsInput type="number" size="small" min={0} 
      disabled={dataIndex === "Q_DN" ? true : false}
      onChange={(value) => handleChangeInput(value, dataIndex, record)}
      />;
    } else if (inputType === "select") {
      inputNode = <DsSelect size="small" options={dataDSAnChi || []} />;
    } else if (inputType === "text") {
      inputNode = <DsInput size="small" type="text" />;
    }
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item
            name={dataIndex}
            style={{ margin: 0 }}
            rules={[
              {
                required: true,
                message: `Không bỏ trống ô ${title}!`,
              },
            ]}
          >
            {inputNode}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  const isEditing = (record: Item) => record.STT === editingSTT;
  const edit = (record: Partial<Item> & { STT: React.Key }) => {
    form.setFieldsValue({
      MA_AC: "",
      SL_DN: "",
      Q_DN: "",
      GHI_CHU: "",
      ...record,
    });
    setEditingSTT(record.STT);
  };

  const cancel = (STT: string) => {
    setEditingSTT("");
    if (statusAdd) {
      const newData = data.filter((item: any) => item.STT !== STT);
      setData(newData);
      setStatusAdd(false);
    }
  };  
  useEffect(() => {
    handleDataTable(data);
  }, [data]);

  const save = async (STT: React.Key) => {
    try {
      const row = (await form.validateFields()) as any;
      const newData = [...data];
      const index = newData.findIndex((item) => STT === item.STT);
      if (index > -1) {
        setStatusAdd(false);
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setData(newData);
        setEditingSTT("");
      } else {
        setStatusAdd(false);
        newData.push(row);
        setData(newData);
        setEditingSTT("");
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };

  const handleDelete = (STT: string) => {
    const newData = data.filter((item: any) => item.STT !== STT);
    setData(newData);
  };

  const columns = [
    {
      title: "Mã ấn chỉ",
      dataIndex: "MA_AC",
      width: "40%",
      editable: true,
      render: (_: any, record: Item) => {
        const item =
          dataDSAnChi &&
          dataDSAnChi.find((element: any) => element?.value === record?.MA_AC);
        const labelCol = item?.label || null;
        return labelCol;
      },
    },
    {
      title: "Số lượng đề nghị",
      dataIndex: "SL_DN",
      width: "25%",
      editable: true,
    },
    {
      title: "Số quyển",
      dataIndex: "Q_DN",
      width: "25%",
      editable: true,
    },
    {
      title: "Ghi chú",
      dataIndex: "GHI_CHU",
      width: "25%",
      editable: false,
      hidden: true,
    },
    {
      title: "Thao tác",
      width: "10%",
      dataIndex: "ThaoTac",
      render: (_: any, record: Item) => {
        const editable = isEditing(record);
        return editable ? (
          <div className="flex justify-center">
            <DsIcon
              name="icon-bxs-check-circle"
              className="md:text-md pt-1 text-[#37b652]"
              onClick={() => save(record.STT)}
            />

            <DsPopover
              closeOnClick={true}
              trigger={["click"]}
              content={
                <>
                  <div className="flex item-center mb-1">
                    <DsIcon
                      name="icon-bxs-error-circle"
                      className="md:text-md text-[#FAAD14] mr-1"
                    />
                    <p>Bạn có chắc chắn hủy bản này không?</p>
                  </div>

                  <div className="flex justify-around">
                    <DsButton
                      onClick={(e) => {}}
                      color="gray"
                      type="outline"
                      size="small"
                    >
                      Thoát
                    </DsButton>
                    <DsButton
                      onClick={() => cancel(record.STT)}
                      size="small"
                      color="red"
                    >
                      Hủy
                    </DsButton>
                  </div>
                </>
              }
              position="bottomLeft"
            >
              <DsIcon
                name="icon-bx-x"
                className="md:text-md pt-1 text-[#b43c3c]"
              />
            </DsPopover>
          </div>
        ) : (
          <div className="flex justify-around">
            <DsIcon
              name="icon-bx-edit"
              className="md:text-md pt-1 text-[#1779e2]"
              onClick={() => edit(record)}
            />
            <DsPopover
              closeOnClick={true}
              trigger={["click"]}
              content={
                <>
                  <div className="flex item-center mb-1">
                    <DsIcon
                      name="icon-bxs-error-circle"
                      className="md:text-md text-[#FAAD14] mr-1"
                    />
                    <p>Bạn có chắc chắn xóa không?</p>
                  </div>

                  <div className="flex justify-around">
                    <DsButton
                      // className="p-1 cursor-pointer hover:bg-[#FAFAFA]"
                      onClick={(e) => {}}
                      color="gray"
                      type="outline"
                      size="small"
                    >
                      Hủy
                    </DsButton>
                    <DsButton
                      // className="p-1 cursor-pointer  hover:bg-[#FAFAFA]"
                      onClick={() => handleDelete(record.STT)}
                      size="small"
                      color="red"
                    >
                      Xóa
                    </DsButton>
                  </div>
                </>
              }
              onOpenChange={() => {}}
              position="bottomLeft"
            >
              {/* <DsIcon name="icon-bx-trash" onClick={() => {}} /> */}
              <DsIcon
                name="icon-bx-trash"
                className="md:text-md pt-1 text-[#F4175A]"
              />
            </DsPopover>
          </div>
        );
      },
    },
  ];

  const mergedColumns: any = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record: Item) => ({
        record,
        inputType:
          col.dataIndex === "SL_DN" || col.dataIndex === "Q_DN"
            ? "number"
            : col.dataIndex === "MA_AC"
              ? "select"
              : "text",
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  // Hàm xử lý thêm dòng mới
  const handleAddRow = () => {
    setStatusAdd(true);
    const newSTT = (data.length + 1).toString();
    const newData: any = {
      STT: newSTT,
      MA_AC: "",
      SL_DN: "",
      Q_DN: "",
      GHI_CHU: "",
    };
    setData([...data, newData]);
    setEditingSTT(newSTT); // Đánh dấu dòng mới để chỉnh sửa ngay lập tức
    form.resetFields();
  };

  useEffect(() => {
    if (!isReset) {
      setEditingSTT("");
    }
    setStatusAdd(false);
  }, [isReset]);

  return (
    <div>
      <p className="my-1 font-medium">Danh sách đề nghị cấp</p>
      <DsButton
        onClick={handleAddRow}
        disabled={statusAdd}
        className="mb-2"
        buttonType="button"
      >
        + Thêm
      </DsButton>

      <Form form={form} component={false}>
        <DsTable
          className="w-full"
          components={{
            body: {
              cell: EditableCell,
            },
          }}
          bordered
          dataSource={data}
          columns={mergedColumns}
          rowClassName="editable-row"
        />
      </Form>
    </div>
  );
};

export default TableDSCapAC;
