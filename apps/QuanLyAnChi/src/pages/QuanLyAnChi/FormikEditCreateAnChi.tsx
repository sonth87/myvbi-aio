import { useFormik } from "formik";
import { DsButton, DsInput, DsModal, DsSelect, Form } from "mvi-ds-ui";
import { FC, useCallback, useEffect, useState } from "react";
import { ValuesFormikDNAC } from "../../type/common";
import { formDNACSchema } from "../../constants/validation";
import TableDSDNCapAC from "./TableDSDNCapAC";
import { useRecoilValue } from "recoil";
import { comomState } from "../../recoil/atom";
import { converDataSelected } from "../../constants/genaralFunction";
import { useAuth } from "../../context/AuthContext";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { createDeNghiNhap, getAnChiChiTiet } from "../../apis/AnChiApi";
import { ALL, SUA } from "../../constants/const";

type Props = {
  isOpen: boolean;
  handleClose: (value: boolean, record?: any) => void;
  pId: number;
};

const FormikEditCreateAnChi: FC<Props> = ({ isOpen, handleClose, pId }) => {
  const queryCache = useQueryClient();
  const { user, notify } = useAuth();
  const [isResetTableList, setIsResetTableList] = useState<boolean>(true);

  // Lấy giá trị của comomState
  const commonState = useRecoilValue(comomState);

  const listMenu: any = window.sessionStorage.getItem("list-menu");
  const parsedListMenu = JSON.parse(listMenu);

  const { data: dataDeNghiAnChiChiTiet } = useQuery({
    queryKey: ["an-chi-chi-tiet", pId, isOpen],
    queryFn: () =>
      getAnChiChiTiet({
        P_HASH: user.hash,
        P_TAI_KHOAN: user.email,
        P_ID_CN: parsedListMenu[1]?.group,
        P_LOAI_XUAT: ALL,
        P_MA_DVI_XUAT: ALL,
        P_MA_DVI_HOAN: ALL,
        P_SO_DN: ALL,
        P_TRANG_THAI: ALL,
        P_DU_PHONG: ALL,
        P_ID_DN: `${pId}`,
        P_TU_NGAY: "",
        P_DEN_NGAY: "",
      }),
    enabled: !!pId && isOpen,
    refetchOnWindowFocus: false,
    select: (data) => {
      return data?.data?.data;
    },
  });

  const { mutateAsync } = useMutation({
    mutationFn: (data: any) => createDeNghiNhap(data),
  });

  const form = useFormik<ValuesFormikDNAC>({
    initialValues: {
      P_SO_DN: dataDeNghiAnChiChiTiet?.data?.SO_DN || "",
      P_LOAI: dataDeNghiAnChiChiTiet?.data?.LOAI_DX || "",
      P_DS_DN: dataDeNghiAnChiChiTiet?.data?.CHI_TIET || [],
    },
    enableReinitialize: true,
    validationSchema: formDNACSchema,
    onSubmit: async (values) => {
      let P_DANG_KY_CT_string = JSON.stringify(values.P_DS_DN).replace(
        /"/g,
        '\\"'
      );

      const dataBody = {
        P_HASH: user.hash,
        P_TAI_KHOAN: user.tai_khoan,
        P_ID_CN: parsedListMenu && parsedListMenu[1].group,
        P_LOAI: pId ? SUA : values.P_LOAI,
        P_MA_DVI: ALL,
        P_PHONG: ALL,
        P_SO_DN: ALL,
        P_GHI_CHU: ALL,
        P_NOI_DUNG: ALL,
        P_ID_DN: pId ? `${pId}` : "",
        P_DANG_KY_CT: P_DANG_KY_CT_string,
      };
      const resutl = await mutateAsync(dataBody);
      notify?.({
        type: resutl?.data?.data?.Success ? "success" : "error",
        content: resutl?.data?.data?.mess,
      });
      if (resutl?.data?.data?.Success) {
        form.resetForm();
        handleClose(false);
        setIsResetTableList(true);
        queryCache.invalidateQueries({ queryKey: ["list-an-chi"] });
      }
    },
  });
  const getFieldErr = useCallback(
    (field: keyof ValuesFormikDNAC) => {
      const fErr = form.errors;
      return form.touched?.[field] && fErr?.[field]
        ? fErr?.[field]?.toString()
        : null;
    },
    [form.errors, form.touched]
  );

  const handleDataTable = (value: any) => {
    form.setFieldValue("P_DS_DN", value);
  };

  useEffect(() => {
    if (!isResetTableList) {
      form.resetForm();
      handleClose(false);
    }
    setIsResetTableList(true);
  }, [isResetTableList]);

  return (
    <div>
      <DsModal
        open={isOpen}
        width={"80%"}
        centered
        title="Đề nghị cấp ấn chỉ"
        closable={false}
        footer={[
          <DsButton
            disabled={Object.keys(form.errors).length !== 0}
            key="confirm-del-prod"
            //@ts-ignore
            onClick={form.handleSubmit}
          >
            Lưu
          </DsButton>,
          <DsButton
            color="red"
            key="cancel-del-prod"
            onClick={() => {
              setIsResetTableList(false);
            }}
            className="ds-ml-4"
          >
            Thoát
          </DsButton>,
        ]}
      >
        <form onSubmit={form.handleSubmit}>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <DsInput
              name="P_SO_DN"
              label="Số đề nghị"
              type="text"
              disabled={true}
              value={form?.values?.P_SO_DN}
              onChange={(e) => form.setFieldValue("P_SO_DN", e || "")}
              state={getFieldErr("P_SO_DN") ? "error" : "normal"}
              message={getFieldErr("P_SO_DN")}
            />
            <DsSelect
              name="P_LOAI"
              label="Loại đề xuất"
              options={
                commonState?.danhMuc
                  ? converDataSelected(commonState?.danhMuc)
                  : []
              }
              value={form?.values?.P_LOAI || null}
              onChange={(item) => {
                form.setFieldValue("P_LOAI", item);
              }}
              state={getFieldErr("P_LOAI") ? "error" : "normal"}
              message={getFieldErr("P_LOAI")}
              disabled={!!pId}
              required
            />
          </div>
          {/* 
            Bên trong TableDSCapAC dùng form của antd 
            ===>>> chưa chuyển sang formik, sẽ nghiên cứu và chuyển sang sau 
          */}
          <TableDSDNCapAC
            handleDataTable={handleDataTable}
            dataList={form?.values?.P_DS_DN}
            isResetTableList={isResetTableList}
          />
        </form>
      </DsModal>
    </div>
  );
};

export default FormikEditCreateAnChi;
