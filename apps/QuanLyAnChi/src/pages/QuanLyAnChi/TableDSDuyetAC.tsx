import { useQuery } from "@tanstack/react-query";
import {
  DsButton,
  DsIcon,
  DsInput,
  DsModal,
  DsSelect,
  Form,
  DsTable,
  DsPopover,
} from "mvi-ds-ui";
import { FC, useEffect, useState } from "react";
import { useAuth } from "../../context/AuthContext";
import { getDSAnChi } from "../../apis/AnChiApi";
import { converDataSelectedAnChi } from "../../constants/genaralFunction";
import { useRecoilValue } from "recoil";
import { isResetState } from "../../recoil/atom";

type Props = {
  handleDataTable: (value: any) => void;
  dataList?: any;
  isResetTableList?: boolean;
};

interface Item {
  ID: string;
  MA_AC: string;
  SL_DN: string;
  Q_DN: string;
  GHI_CHU: string;
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  editing: boolean;
  dataIndex: string;
  title: any;
  inputType: "number" | "text" | "select" | "input";
  record: Item;
  index: number;
}

const TableDSDuyetAC: FC<Props> = ({
  handleDataTable,
  dataList,
  isResetTableList,
}) => {
  const [form] = Form.useForm();
  const [data, setData] = useState(dataList);
  const [editingID, setEditingID] = useState("");
  const [statusAdd, setStatusAdd] = useState(false);
  const { user } = useAuth();
  const isReset = useRecoilValue(isResetState);

  useEffect(() => {
    if (dataList) {
      setData(dataList);
    }
  }, [dataList]);

  const dataBody = {
    P_HASH: user.hash,
    P_TAI_KHOAN: user.tai_khoan,
    P_ID_CN: "",
    P_LOAI: "DULIEU",
    P_MA_AC: "",
    P_TEN: "",
    P_NV: "",
    P_GHI_CHU: "",
  };
  const { data: dataDSAnChi } = useQuery({
    queryKey: ["ma-an-chi-list", dataBody],
    queryFn: () => getDSAnChi(dataBody),
    refetchOnWindowFocus: false,
    select: (data) => {
      return converDataSelectedAnChi(data);
    },
  });

  const EditableCell: React.FC<React.PropsWithChildren<EditableCellProps>> = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
  }) => {
    let inputNode;
    if (inputType === "number") {
      inputNode = <DsInput type="number" size="small" min={0} />;
    } else if (inputType === "select") {
      inputNode = <DsSelect size="small" options={dataDSAnChi || []} />;
    } else if (inputType === "text") {
      inputNode = <DsInput size="small" type="text" />;
    }
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item
            name={dataIndex}
            style={{ margin: 0 }}
            rules={[
              {
                required: true,
                message: `Không bỏ trống ô ${title}!`,
              },
            ]}
          >
            {inputNode}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  const isEditing = (record: Item) => record.ID === editingID;
  const edit = (record: Partial<Item> & { ID: React.Key }) => {
    form.setFieldsValue({
      MA_AC: "",
      SL_DN: "",
      Q_DN: "",
      SL_THUC_TE: "",
      GHI_CHU: "",
      ...record,
    });
    setEditingID(record.ID);
  };

  const cancel = (ID: string) => {
    setEditingID("");
    if (statusAdd) {
      const newData = data.filter((item: any) => item.ID !== ID);
      setData(newData);
      setStatusAdd(false);
    }
  };
  useEffect(() => {
    handleDataTable(data);
  }, [data]);

  const save = async (ID: React.Key) => {
    try {
      const row = (await form.validateFields()) as any;
      const newData = [...data];
      const index = newData.findIndex((item) => ID === item.ID);
      if (index > -1) {
        setStatusAdd(false);
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setData(newData);
        setEditingID("");
      } else {
        setStatusAdd(false);
        newData.push(row);
        setData(newData);
        setEditingID("");
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };

  const columns = [
    {
      title: "Mã ấn chỉ",
      dataIndex: "MA_AC",
      width: 250,
      fixed: "left",
      editable: false,
      render: (_: any, record: Item) => {
        const item =
          dataDSAnChi &&
          dataDSAnChi.find((element: any) => element?.value === record?.MA_AC);
        const labelCol = item?.label || null;
        return labelCol;
      },
    },
    {
      title: "Số lượng đề nghị",
      dataIndex: "SL_DN",
      width: 100,
      align: "center",
      editable: false,
    },
    {
      title: "Số quyển",
      dataIndex: "Q_DN",
      width: 100,
      align: "center",
      editable: false,
    },
    {
      title: "Số lượng phê duyệt",
      dataIndex: "SL_THUC_TE",
      width: 100,
      align: "center",
      editable: true,
    },
    {
      title: "Ghi chú",
      dataIndex: "GHI_CHU",
      width: 0,
      align: "center",
      editable: false,
      hidden: true,
    },
    {
      title: "Thao tác",
      width: 100,
      align: "center",
      dataIndex: "ThaoTac",
      render: (_: any, record: Item) => {
        const editable = isEditing(record);
        return editable ? (
          <div className="flex justify-center">
            <DsIcon
              name="icon-bxs-check-circle"
              className="md:text-md pt-1 text-[#37b652]"
              onClick={() => save(record.ID)}
            />

            <DsPopover
              closeOnClick={true}
              trigger={["click"]}
              content={
                <>
                  <div className="flex item-center mb-1">
                    <DsIcon
                      name="icon-bxs-error-circle"
                      className="md:text-md text-[#FAAD14] mr-1"
                    />
                    <p>Bạn có chắc chắn hủy bản này không?</p>
                  </div>

                  <div className="flex justify-around">
                    <DsButton
                      onClick={(e) => {}}
                      color="gray"
                      type="outline"
                      size="small"
                    >
                      Thoát
                    </DsButton>
                    <DsButton
                      onClick={() => cancel(record.ID)}
                      size="small"
                      color="red"
                    >
                      Hủy
                    </DsButton>
                  </div>
                </>
              }
              position="bottomLeft"
            >
              <DsIcon
                name="icon-bx-x"
                className="md:text-md pt-1 text-[#b43c3c]"
              />
            </DsPopover>
          </div>
        ) : (
          <div className="flex justify-around">
            <DsIcon
              name="icon-bx-edit"
              className="md:text-md pt-1 text-[#1779e2]"
              onClick={() => edit(record)}
            />
          </div>
        );
      },
    },
  ];

  const mergedColumns: any = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record: Item) => ({
        record,
        inputType:
          col.dataIndex === "SL_DN" ||
          col.dataIndex === "Q_DN" ||
          col.dataIndex === "SL_THUC_TE"
            ? "number"
            : col.dataIndex === "MA_AC"
              ? "select"
              : "text",
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  useEffect(() => {
    if (!isReset) {
      setEditingID("");
    }
  }, [isReset]);

  return (
    <div>
      <Form form={form} component={false}>
        <DsTable
          scroll={{ y: 300 }}
          virtual
          className="w-full min-h-[300px] !inline"
          components={{
            body: {
              cell: EditableCell,
            },
          }}
          bordered
          dataSource={data}
          columns={mergedColumns}
          rowClassName="editable-row"
        />
      </Form>
    </div>
  );
};

export default TableDSDuyetAC;
