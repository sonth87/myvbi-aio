export const cols = () => [
  {
    title: "STT",
    dataIndex: "STT",
    key: "STT",
  },
  {
    title: "Mã ấn chỉ",
    dataIndex: "MA_AC",
    key: "MA_AC",
  },
  {
    title: "Mã quyển",
    dataIndex: "SO_SO",
    key: "SO_SO",
  },
  {
    title: "Số đầu",
    dataIndex: "TU_SO",
    key: "TU_SO",
  },
  {
    title: "Số cuối",
    dataIndex: "DEN_SO",
    key: "DEN_SO",
  },
  {
    title: "Số lượng",
    dataIndex: "SO_LUONG",
    key: "SO_LUONG",
  },
];
