import { useFormik } from "formik";
import { DsButton, DsDatePicker, DsInput, DsModal, DsSelect, message } from "mvi-ds-ui";
import { FC, useEffect, useState } from "react";
import { formDNACSchema } from "../../constants/validation";
import TableDSCapAC from "./TableDSDNCapAC";
import TableDSDuyetAC from "./TableDSDuyetAC";
import { useRecoilValue } from "recoil";
import { comomState } from "../../recoil/atom";
import { checkValueObject, converDataSelected } from "../../constants/genaralFunction";
import { duyetAnChi, getAnChiChiTiet } from "../../apis/AnChiApi";
import { useAuth } from "../../context/AuthContext";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { ALL, DUYET_NHAP } from "../../constants/const";

type Props = {
  isOpen: boolean;
  handleClose: (value: boolean, record?: any) => void;
  pId: number;
};

const PheDuyetDeNghi: FC<Props> = ({ isOpen, handleClose, pId }) => {
  const { user, notify } = useAuth();
  const queryCache = useQueryClient();
  const [messageApi, contextHolder] = message.useMessage();
  const [isResetTableList, setIsResetTableList] = useState<boolean>(true);

  // Lấy giá trị của comomState
  const commonState = useRecoilValue(comomState);
  const listMenu: any = window.sessionStorage.getItem("list-menu");
  const parsedListMenu = JSON.parse(listMenu);

  const { data: dataDeNghiAnChiChiTiet } = useQuery({
    queryKey: ["an-chi-chi-tiet", pId, isOpen],
    queryFn: () =>
      getAnChiChiTiet({
        P_HASH: user.hash,
        P_TAI_KHOAN: user.email,
        P_ID_CN: parsedListMenu[1]?.group,
        P_LOAI_XUAT: ALL,
        P_MA_DVI_XUAT: ALL,
        P_MA_DVI_HOAN: ALL,
        P_SO_DN: ALL,
        P_TRANG_THAI: ALL,
        P_DU_PHONG: ALL,
        P_ID_DN: `${pId}`,
        P_TU_NGAY: "",
        P_DEN_NGAY: "",
      }),
    enabled: !!pId && isOpen,
    refetchOnWindowFocus: false,
    select: (data) => {
      return data?.data?.data;
    },
  });

  const { mutateAsync } = useMutation({
    mutationFn: (data: any) => duyetAnChi(data),
  });

  const form = useFormik({
    initialValues: {
      P_SO_DN: dataDeNghiAnChiChiTiet?.data?.SO_DN || "",
      P_LOAI: dataDeNghiAnChiChiTiet?.data?.LOAI_DX || "",
      P_NGAY_DN: dataDeNghiAnChiChiTiet?.data?.NGAY_DN || "",
      P_DS_DN: dataDeNghiAnChiChiTiet?.data?.CHI_TIET || [],
    },
    enableReinitialize: true,
    validationSchema: formDNACSchema,
    onSubmit: async (values) => {
      let resultCheck = checkValueObject(["SL_THUC_TE"], values.P_DS_DN);
      if(!resultCheck){
        messageApi.open({
          type: "error",
          content: "Phải nhập đủ Số lượng phê duyệt trước khi Lưu!",
          duration: 4,
          className: "right",
        });
        return false
      }
      const dataBody = {
        P_HASH: user.hash,
        P_TAI_KHOAN: user.tai_khoan,
        P_ID_CN: parsedListMenu && parsedListMenu[1].group,
        P_LOAI: DUYET_NHAP,
        P_MA_DVI: ALL,
        P_PHONG: ALL,
        P_SO_DN: ALL,
        P_GHI_CHU: ALL,
        P_NOI_DUNG: ALL,
        P_ID_DN: `${pId}`,
        P_DANG_KY_CT: JSON.stringify(values.P_DS_DN),
      };
      const result = await mutateAsync(dataBody);

      notify?.({
        type: result?.data?.data?.Success ? "success" : "error",
        content: result?.data?.data?.mess,
      });
      if (result?.data?.data?.Success) {
        form.resetForm();
        handleClose(false);
        setIsResetTableList(true);
        queryCache.invalidateQueries({ queryKey: ["list-an-chi"] });
      }
    },
  });

  const handleDataTable = (value: any) => {
    form.setFieldValue("P_DS_DN", value);
  };

  const onDateChangeStartDate = (date: string, str: string) => {
    form.setFieldValue("P_NGAY_CT", str);
  };

  useEffect(() => {
    if (!isResetTableList) {
      handleClose(false);
    }
    setIsResetTableList(true);
  }, [isResetTableList]);

  return (
    <div>
      {contextHolder}
      <DsModal
        open={isOpen}
        width={"80%"}
        centered
        title="Phê duyệt đề nghị"
        closable={false}
        footer={[
          <DsButton
            disabled={Object.keys(form.errors).length !== 0}
            key="confirm-del-prod"
            //@ts-ignore
            onClick={form.handleSubmit}
          >
            Lưu
          </DsButton>,
          <DsButton
            color="red"
            key="cancel-del-prod"
            onClick={() => {
              setIsResetTableList(false);
              // handleClose(false);
              // form.resetForm();
            }}
            className="ds-ml-4"
          >
            Thoát
          </DsButton>,
        ]}
      >
        <form onSubmit={form.handleSubmit}>
          <div className="grid gap-4">
            <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
              <DsInput
                name="P_SO_DN"
                label="Số đề nghị"
                type="text"
                value={form?.values?.P_SO_DN}
                onChange={(e) => form.setFieldValue("P_SO_DN", e || "")}
                disabled={true}
              />
              <DsSelect
                name="P_LOAI"
                label="Loại đề xuất"
                options={
                  commonState?.danhMuc
                    ? converDataSelected(commonState?.danhMuc)
                    : []
                }
                value={form?.values?.P_LOAI || null}
                onChange={(item) => {
                  form.setFieldValue("P_LOAI", item);
                }}
                disabled={true}
              />
              <DsDatePicker
                label="Ngày đề nghị"
                size="large"
                name="tu_ngay"
                format="DD/MM/YYYY"
                value={form.values?.P_NGAY_DN}
                onChange={onDateChangeStartDate}
                picker="date"
                disabled={true}
              />
            </div>
          </div>
          <div className="mt-4">
            <TableDSDuyetAC
              handleDataTable={handleDataTable}
              dataList={form?.values?.P_DS_DN}
              isResetTableList={isResetTableList}
            />
          </div>
        </form>
      </DsModal>
    </div>
  );
};

export default PheDuyetDeNghi;
