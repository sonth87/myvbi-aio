import { useFormik } from "formik";
import { DsButton, DsDatePicker, DsInput, DsSelect } from "mvi-ds-ui";
import { useAuth } from "../../context/AuthContext";
import { useQueries } from "@tanstack/react-query";
import { getThamSo } from "../../apis/AnChiApi";
import { GET_DM, GET_DV, GET_TT, MENU_LIST, Type } from "../../constants/const";
import {
  converDataSelected,
  convertDate,
  convertDateTime,
} from "../../constants/genaralFunction";
import { FC, useEffect } from "react";
import { ValuesSearchProps } from "../../type/common";
import { useRecoilState } from "recoil";
import { comomState } from "../../recoil/atom";
import { hasButton, MenuButtonAfterFilter } from "../../utils/findMenuByURL";

type initValueSearchProps = {
  initValueSearch: ValuesSearchProps;
  handleChangeValuesSearch: (values: ValuesSearchProps) => void;
};

const TimKiemAnChi: FC<initValueSearchProps> = ({
  initValueSearch,
  handleChangeValuesSearch,
}) => {
  const listMenu: any = window.sessionStorage.getItem(MENU_LIST);
  const { user } = useAuth();
  const [commonState, setCommonState] = useRecoilState(comomState);
  const bodyGetDanhMuc = {
    P_TAI_KHOAN: user.email || " ",
    P_HASH: user.hash || "",
    P_LOAI: GET_DM,
    P_VALUE: "",
  };
  const bodyGetDonVi = {
    P_TAI_KHOAN: user.email || "",
    P_HASH: user.hash || "",
    P_LOAI: GET_DV,
    P_VALUE: "",
  };
  const bodyGetTrangThai = {
    P_TAI_KHOAN: user.email || "",
    P_HASH: user.hash || "",
    P_LOAI: GET_TT,
    P_VALUE: "",
  };
  const results = useQueries({
    queries: [
      {
        queryKey: ["get-danh-muc"],
        queryFn: () => getThamSo(bodyGetDanhMuc),
        refetchOnWindowFocus: false,
      },
      {
        queryKey: ["get-don-vi"],
        queryFn: () => getThamSo(bodyGetDonVi),
        refetchOnWindowFocus: false,
      },
      {
        queryKey: ["get-trang-thai"],
        queryFn: () => getThamSo(bodyGetTrangThai),
        refetchOnWindowFocus: false,
      },
    ],
    combine: (results) => {
      return {
        data: results.map((result) => result.data),
        pending: results.some((result) => result.isPending),
      };
    },
  });

  useEffect(() => {
    if (!results.pending) {
      setCommonState({
        danhMuc: results.data[0]?.data?.data,
        donVi: results.data[1]?.data?.data?.DON_VI,
        trangThai: results.data[2]?.data?.data,
      });
    }
  }, [results, setCommonState]);

  const form = useFormik({
    initialValues: {
      P_HASH: initValueSearch.P_HASH,
      P_TAI_KHOAN: initValueSearch.P_TAI_KHOAN,
      P_ID_CN: initValueSearch.P_ID_CN,
      P_LOAI_XUAT: initValueSearch.P_LOAI_XUAT,
      P_MA_DVI_XUAT: initValueSearch.P_MA_DVI_XUAT,
      P_MA_DVI_HOAN: initValueSearch.P_MA_DVI_HOAN,
      P_SO_DN: initValueSearch.P_SO_DN,
      P_TRANG_THAI: initValueSearch.P_TRANG_THAI,
      P_DU_PHONG: initValueSearch.P_DU_PHONG,
      P_ID_DN: initValueSearch.P_ID_DN,
      P_TU_NGAY: initValueSearch.P_TU_NGAY,
      P_DEN_NGAY: initValueSearch.P_DEN_NGAY,
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      console.log(values);
      handleChangeValuesSearch(values);
    },
  });

  const onDateChangeStartDate = (date: string, str: string) => {
    form.setFieldValue("P_TU_NGAY", convertDate(str));
  };
  const onDateChangeEndDate = (date: string, str: string) => {
    form.setFieldValue("P_DEN_NGAY", convertDate(str));
  };

  //tìm menu con theo URL
  const buttonByMenuList = MenuButtonAfterFilter(
    listMenu,
    location.pathname.replace(/^\//, "")
  );

  return (
    <div>
      {results.pending ? null : (
        <form onSubmit={form.handleSubmit}>
          <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:ds-grid-cols-4 gap-4">
            <DsInput
              label="Số đề nghị"
              required
              size="large"
              type="text"
              onChange={(e) => form.setFieldValue("P_SO_DN", e)}
              // disabled={isLoading}
            />
            <DsSelect
              label="Loại đề xuất"
              size="large"
              options={converDataSelected(results?.data[0]?.data?.data)}
              required
              onChange={(e) => form.setFieldValue("P_LOAI_XUAT", e)}
            />
            <DsSelect
              label="Đơn vị đề xuất"
              size="large"
              options={converDataSelected(results?.data[1]?.data?.data?.DON_VI)}
              required
              onChange={(e) => form.setFieldValue("P_MA_DVI_XUAT", e)}
            />
            <DsSelect
              label="Đơn vị cấp phát"
              size="large"
              options={converDataSelected(results?.data[1]?.data?.data?.DON_VI)}
              required
              onChange={(e) => form.setFieldValue("P_MA_DVI_HOAN", e)}
            />
            <DsSelect
              label="Trạng thái"
              size="large"
              required
              options={converDataSelected(results.data[2].data.data)}
              onChange={(e) => form.setFieldValue("P_TRANG_THAI", e)}
            />
            <DsDatePicker
              label="Ngày tạo từ"
              allowClear={false}
              size="large"
              name="tu_ngay"
              format="DD/MM/YYYY"
              value={
                form?.values?.P_TU_NGAY &&
                convertDateTime(form?.values?.P_TU_NGAY.toString())
              }
              onChange={onDateChangeStartDate}
              picker="date"
            />

            <DsDatePicker
              label="đến ngày"
              allowClear={false}
              size="large"
              name="den_ngay"
              format="DD/MM/YYYY"
              value={
                form?.values?.P_DEN_NGAY &&
                convertDateTime(form?.values?.P_DEN_NGAY.toString())
              }
              onChange={onDateChangeEndDate}
              picker="date"
            />
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4 items-center">
              <DsButton size="large" className="ds-w-full">
                Tìm kiếm
              </DsButton>
              {hasButton(buttonByMenuList, Type.XuatExcelButton) && (
                <DsButton size="large" className="ds-w-full">
                  Xuất Excel
                </DsButton>
              )}
            </div>
          </div>
        </form>
      )}
    </div>
  );
};

export default TimKiemAnChi;
