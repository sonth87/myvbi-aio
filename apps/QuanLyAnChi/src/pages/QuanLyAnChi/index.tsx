import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import usePageInfo from "../../hooks/usePageInfo";
import TimKiemAnChi from "./TimKiemAnChi";
import { DsButton, DsTable } from "mvi-ds-ui";
import { cols } from "./colDanhSachAnChi";
import { useAuth } from "../../context/AuthContext";
import { useQuery } from "@tanstack/react-query";
import { ValuesSearchProps } from "../../type/common";
import { getDSDeNghiAnChi } from "../../apis/AnChiApi";
import FormikEditCreateAnChi from "./FormikEditCreateAnChi";
import NhanAnChi from "./NhanAnChi";
import CapAnChi from "./CapAnChi";
import PheDuyetDeNghi from "./PheDuyetDeNghi";
import HuyAnChi from "./HuyAnChi";
import { ALL, MENU_LIST, Type } from "../../constants/const";
import { useRecoilState } from "recoil";
import { isResetState } from "../../recoil/atom";
import { useLocation } from "react-router-dom";
import { hasButton, MenuButtonAfterFilter } from "../../utils/findMenuByURL";

function TaxManage() {
  const listMenu: any = window.sessionStorage.getItem(MENU_LIST);
  const { user } = useAuth();
  const location = useLocation();

  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const [isOpenEditAC, setIsOpenEditAC] = useState(false);
  const [isOpenHuyAC, setIsOpenHuyAC] = useState(false);
  const [isOpenCapAC, setIsOpenCapAC] = useState(false);
  const [isOpenNhanAC, setIsOpenNhanAC] = useState(false);
  const [isOpenPheDuyetDN, setIsOpenPheDuyetDN] = useState(false);
  const [idRecord, setIdRecord] = useState<any>();

  const [isResetTableState, setIsResetTableState] =
    useRecoilState(isResetState);

  useEffect(() => {
    setModuleInfo({
      module: {
        icon: "icon-bx-package",
        title: "Quản lý danh sách ấn chỉ",
      },
      path: [
        {
          label: "Quản lý danh sách ấn chỉ",
        },
      ],
    });
    return () => cleanModuleInfo();
  }, []);

  const [initValueSearch, setInitValueSearch] = useState<ValuesSearchProps>({
    P_HASH: user.hash,
    P_TAI_KHOAN: user.email,
    P_ID_CN: "3",
    P_LOAI_XUAT: ALL,
    P_MA_DVI_XUAT: ALL,
    P_MA_DVI_HOAN: ALL,
    P_SO_DN: ALL,
    P_TRANG_THAI: ALL,
    P_DU_PHONG: ALL,
    P_ID_DN: ALL,
    // P_TU_NGAY: getCurrentDateFormatted("YYYYMMDD"),
    // P_DEN_NGAY: getCurrentDateFormatted("YYYYMMDD"),
    P_TU_NGAY: "20240101",
    P_DEN_NGAY: "20241230",
  });

  const handleChangeValuesSearch = (values: ValuesSearchProps) => {
    setInitValueSearch(values);
  };

  const { data: dataDSDeNghiAnChi } = useQuery({
    queryKey: ["list-an-chi", initValueSearch],
    queryFn: () => getDSDeNghiAnChi(initValueSearch),
    enabled: !!initValueSearch,
    refetchOnWindowFocus: false,
    select: (data) => {
      return data?.data?.data;
    },
  });

  const handleOpenModalEditAC = (value: boolean, record?: any) => {
    setIdRecord(record?.ID_DN);
    setIsOpenEditAC(value);
    value ? setIsResetTableState(false) : setIsResetTableState(true);
  };

  const handleOpenModalHuyAC = (value: boolean, record: any) => {
    setIdRecord(record?.ID_DN);
    setIsOpenHuyAC(value);
  };
  const handleOpenModalCapAC = (value: boolean, record: any) => {
    setIdRecord(record?.ID_DN);
    setIsOpenCapAC(value);
    value ? setIsResetTableState(false) : setIsResetTableState(true);
  };
  const handleOpenModalNhanAC = (value: boolean, record: any) => {
    setIdRecord(record?.ID_DN);
    setIsOpenNhanAC(value);
  };
  const handleOpenModalPheDuyetDN = (value: boolean, record: any) => {
    setIdRecord(record?.ID_DN);
    setIsOpenPheDuyetDN(value);
    value ? setIsResetTableState(false) : setIsResetTableState(true);
  };

  //tìm menu con theo URL
  const buttonByMenuList = MenuButtonAfterFilter(
    listMenu,
    location.pathname.replace(/^\//, "")
  );

  return (
    <div>
      <Helmet>
        <title>Quản lý danh sách ấn chỉ</title>
      </Helmet>
      {hasButton(buttonByMenuList, Type.TimKiemButton) && (
        <TimKiemAnChi
          initValueSearch={initValueSearch}
          handleChangeValuesSearch={handleChangeValuesSearch}
        />
      )}

      <div className="flex my-1 ">
        {hasButton(buttonByMenuList, Type.TaoMoiButton) && (
          <DsButton
            className="my-3"
            label="Tạo mới"
            onClick={() => {
              setIsOpenEditAC(true);
            }}
          />
        )}
      </div>
      <div>
        <DsTable
          bordered
          columns={cols(
            handleOpenModalEditAC,
            handleOpenModalHuyAC,
            handleOpenModalCapAC,
            handleOpenModalNhanAC,
            handleOpenModalPheDuyetDN
          )}
          dataSource={dataDSDeNghiAnChi || []}
          className="!contents"
          pagination={{
            showSizeChanger: true,
            total: dataDSDeNghiAnChi?.length,
          }}
        />
      </div>

      <FormikEditCreateAnChi
        isOpen={isOpenEditAC}
        handleClose={handleOpenModalEditAC}
        pId={idRecord}
      />

      <HuyAnChi
        isOpen={isOpenHuyAC}
        handleClose={handleOpenModalHuyAC}
        pId={idRecord}
      />

      <NhanAnChi
        isOpen={isOpenNhanAC}
        handleClose={handleOpenModalNhanAC}
        pId={idRecord}
      />
      <CapAnChi
        isOpen={isOpenCapAC}
        handleClose={handleOpenModalCapAC}
        pId={idRecord}
      />
      <PheDuyetDeNghi
        isOpen={isOpenPheDuyetDN}
        handleClose={handleOpenModalPheDuyetDN}
        pId={idRecord}
      />
    </div>
  );
}

export default TaxManage;
