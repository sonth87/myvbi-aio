import { DsIcon } from "mvi-ds-ui";

export const cols = (
  handleOpenModalEditPhanBoAC: (value: boolean, record?: any) => void,
) => [
    {
      title: "Số Chứng từ",
      dataIndex: "SO_PHIEU",
      key: "SO_PHIEU",
    },
    {
      title: "Loại điều chuyển",
      dataIndex: "LOAI_DC",
      key: "LOAI_DC",
    },
    {
      title: "Đơn vị cấp",
      dataIndex: "TEN_DVI_DN",
      key: "TEN_DVI_DN",
    },
    {
      title: "Người cấp",
      dataIndex: "NGUOI_XUAT",
      key: "NGUOI_XUAT",
    },
    {
      title: "Đơn vị nhận",
      dataIndex: "TEN_DVI_NHAN",
      key: "TEN_DVI_NHAN",
    },
    {
      title: "Người nhận",
      dataIndex: "NGUOI_NHAN",
      key: "NGUOI_NHAN",
    },
    {
      title: "Ngày tạo",
      dataIndex: "NGAY_PB",
      key: "NGAY_PB",
    },
    {
      title: "Người tạo",
      dataIndex: "NGUOI_TAO",
      key: "NGUOI_TAO",
    },
    {
      dataIndex: "thao-tac",
      title: `Thao tác`,
      align: "center",
      render: (_: any, record: any) => {
        return <div className="flex justify-around">
          <div onClick={() => handleOpenModalEditPhanBoAC(true, record)}>
            <DsIcon className="" name="icon-bx-edit text-blue-600 cursor-pointer" />
          </div>
          {/* <div>
            <DsIcon className="" name="icon-bx-trash text-red-600 cursor-pointer" />
          </div> */}
        </div>;
      },
    },
  ];
