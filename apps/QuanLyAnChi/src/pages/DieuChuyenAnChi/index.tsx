import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import usePageInfo from "../../hooks/usePageInfo";
import TimKiemDieuChuyenAnChi from "./TimKiemDieuChuyenAnChi";
import { DsButton, DsIcon, DsTable } from "mvi-ds-ui";
import { cols } from "./colDieuChuyenAnChi";
import PhanBoAnChiDetail from "./PhanBoAnChiDetail";
import { useAuth } from "../../context/AuthContext";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { getDanhSachDieuChuyenAnChi } from "../../apis/DieuChuyenAnChi";
import TablePhanBoAnChi from "./TablePhanBoAnChi";

function DieuChuyenAnChi() {
const queryClient = useQueryClient();

  const { user } = useAuth();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const [isOpenEditPhanBoAC, setIsOpenEditPhanBoAC] = useState(false);
  const [idRecord, setIdRecord] = useState<any>();

  useEffect(() => {
    setModuleInfo({
      module: {
        icon: "icon-bx-package",
        title: "Điều chuyển ấn chỉ",
      },
      path: [
        {
          label: "Điều chuyển ẩn chỉ",
        },
      ],
    });
    return () => cleanModuleInfo();
  }, []);

  
  const [initValueSearch, setInitValueSearch] = useState<any>({
    "P_HASH":  user.hash,
    "P_TAI_KHOAN": user.email,
    "P_ID_CN":	"3",
    "P_SO_PB": "ALL",
    "P_LOAI_PB": "ALL",
    "P_NGUOI_NHAN": "ALL",
    "P_NOI_DUNG": "ALL",
    "P_ID_PB": "ALL",
    "P_TU_NGAY": "20240101",
    "P_DEN_NGAY": "20241230"
  });
  

  const { data: dataSource } = useQuery({
    queryKey: ["list-dieu-chuyen", initValueSearch],
    queryFn: () => getDanhSachDieuChuyenAnChi(initValueSearch),
    enabled: !!initValueSearch,
    refetchOnWindowFocus: false,
    select: (data) => {
      return data?.data?.data;
    },
  });

 const handleChangeValuesSearch = (values: any) => {
    setInitValueSearch(values);
  };

  const handleOpenModalEditPhanBoAC = (value: boolean,  record?: any) => {
    setIdRecord(record?.ID_PB);
    console.log('edit', value)
    setIsOpenEditPhanBoAC(value);
  };

  return (
    <div>
      <Helmet>
        <title>Quản lý điều chuyển ấn chỉ</title>
      </Helmet>

      <TimKiemDieuChuyenAnChi
       initValueSearch={initValueSearch}
       handleChangeValuesSearch={handleChangeValuesSearch}
       />

      <div className="flex my-1 ">
        <DsButton
          className="my-3"
          label="Tạo mới"
          onClick={() => {
            setIsOpenEditPhanBoAC(true);
          }}
        />
      </div>
     
      <div>
        <DsTable
          bordered
          columns={cols(handleOpenModalEditPhanBoAC)}
          className="!inline"
          dataSource={dataSource || []}
          pagination={{
            showSizeChanger: true,
            total: dataSource?.length,
          }}
        />
      </div>
      <PhanBoAnChiDetail
        isOpen={isOpenEditPhanBoAC}
        handleClose={handleOpenModalEditPhanBoAC}
        pId={idRecord}
      />
    </div>
  );
}

export default DieuChuyenAnChi;
