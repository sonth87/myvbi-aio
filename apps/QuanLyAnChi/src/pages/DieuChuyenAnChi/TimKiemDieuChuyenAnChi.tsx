import { useFormik } from "formik";
import { DsButton, DsDatePicker, DsInput, DsSelect } from "mvi-ds-ui";

import { useSearchParams } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { useAuth } from "../../context/AuthContext";
import { FC, useEffect } from "react";
import { GET_DV } from "../../constants/const";
import { useQueries } from "@tanstack/react-query";
import { getThamSo } from "../../apis/AnChiApi";
import { useRecoilState } from "recoil";
import { statePhanBo } from "../../recoil/atom";
import { converDataSelected, convertDate, convertDateTime } from "../../constants/genaralFunction";

type initValueSearchProps = {
  initValueSearch: any;
  handleChangeValuesSearch: (values: any) => void;
};

const TimKiemDieuChuyenAnChi: FC<initValueSearchProps> = ({
  initValueSearch,
  handleChangeValuesSearch,
}) => {
  const { notify, user } = useAuth();
  const [searchParams, setSearchParams] = useSearchParams();
  const [statePB, setStatePB] = useRecoilState(statePhanBo);

  const typeDieuChuyen = {
    P_TAI_KHOAN: user.email || " ",
    P_HASH: user.hash || "",
    P_LOAI: "DN_LOAI_PB",
    P_VALUE: "",
  };
  const listUser = {
    P_TAI_KHOAN: user.email || "",
    P_HASH: user.hash || "",
    P_LOAI: "DM_NGUOI_NHAN",
    P_VALUE: "",
  };
  const results = useQueries({
    queries: [
      {
        queryKey: ["get-danh-muc"],
        queryFn: () => getThamSo(typeDieuChuyen),
        refetchOnWindowFocus: false,
      },
      {
        queryKey: ["get-nguoi-nhan"],
        queryFn: () => getThamSo(listUser),
        refetchOnWindowFocus: false,
      },
      
      // {
      //   queryKey: ["get-don-vi"],
      //   queryFn: () => getThamSo(listUnit),
      //   refetchOnWindowFocus: false,
      // },
    ],
    combine: (results) => {
      return {
        data: results.map((result) => result.data),
        pending: results.some((result) => result.isPending),
      };
    },
  });

  useEffect(() => {
    if (!results.pending) {
      setStatePB({
        loai_dieu_chuyen: results.data[0]?.data?.data,
        nguoi_nhan: results.data[1]?.data?.data?.DM_NGUOI_NHAN
      });
    }
  }, [results, setStatePB]);
  const form = useFormik({
    initialValues: {
      P_HASH: initValueSearch.P_HASH,
      P_TAI_KHOAN: initValueSearch.P_TAI_KHOAN,
      P_ID_CN: initValueSearch.P_ID_CN,
      P_SO_PB: initValueSearch.P_SO_PB,
      P_LOAI_PB: initValueSearch.P_LOAI_PB,
      P_NGUOI_NHAN: initValueSearch.P_NGUOI_NHAN,
      P_NOI_DUNG: initValueSearch.P_NOI_DUNG,
      P_ID_PB: initValueSearch.P_ID_PB,
      P_TU_NGAY: initValueSearch.P_TU_NGAY,
      P_DEN_NGAY: initValueSearch.P_DEN_NGAY,
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      console.log(values);
      handleChangeValuesSearch(values);
    },
  });

  const onDateChangeStartDate = (date: string, str: string) => {
    form.setFieldValue("P_TU_NGAY", convertDate(str));
  };
  const onDateChangeEndDate = (date: string, str: string) => {
    form.setFieldValue("P_DEN_NGAY", convertDate(str));
  };
  return (
    <div>
      <form onSubmit={form.handleSubmit}>
        <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:ds-grid-cols-4 gap-4">
          <DsInput
            label="Số chứng từ"
            size="large"
            type="text"
            // value={form.values.tieu_de}
            onChange={(e) => form.setFieldValue("P_SO_PB", e)}
            // disabled={isLoading}
          />
          <DsSelect
            label="Loại điều chuyển"
            size="large"
            options={converDataSelected(results?.data[0]?.data?.data)}
            onChange={(e) => form.setFieldValue("P_LOAI_PB", e)}
            required
          />
           <DsSelect
            label="Người nhận"
            size="large"
            // options={converDataSelected(results?.data[1]?.data?.data?.DM_NGUOI_NHAN)}
            options={results?.data[1]?.data?.data?.DM_NGUOI_NHAN.map((item: any)=>{
              return{
                value: item?.TAI_KHOAN,
                label: item?.TEN
              }
            })}
            onChange={(e) => form.setFieldValue("P_NGUOI_NHAN", e)}
            required
          />
          {/* <DsSelect
            label="Đơn vị nhận"
            size="large"
            options={converDataSelected(results?.data[1]?.data?.data?.DON_VI)}
            required
          />  */}
          <DsDatePicker
            label="Ngày tạo từ"
            size="large"
            name="tu_ngay"
            format="DD/MM/YYYY"
            // value={form.values?.tu_ngay}
            onChange={onDateChangeStartDate}
            value={
              form?.values?.P_TU_NGAY &&
              convertDateTime(form?.values?.P_TU_NGAY.toString())
            }
            picker="date"
          />

          <DsDatePicker
            label="Đến ngày"
            size="large"
            name="den_ngay"
            format="DD/MM/YYYY"
            // value={form.values.den_ngay}
            value={
              form?.values?.P_DEN_NGAY &&
              convertDateTime(form?.values?.P_DEN_NGAY.toString())
            }
            onChange={onDateChangeEndDate}
            picker="date"
          />
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4 items-center">
            <DsButton size="large" className="ds-w-full">
              Tìm kiếm
            </DsButton>
            <DsButton size="large" className="ds-w-full">
              Xuất Excel
            </DsButton>
          </div>
        </div>
      </form>
    </div>
  );
};

export default TimKiemDieuChuyenAnChi;
