import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import {
  DsButton,
  DsIcon,
  DsInput,
  DsModal,
  DsSelect,
  Form,
  DsTable,
  DsPopover,
} from "mvi-ds-ui";
import { FC, useEffect, useState } from "react";
import { useAuth } from "../../context/AuthContext";
import { getDSAnChi, getThamSo } from "../../apis/AnChiApi";
import { converDataSelectedAnChi } from "../../constants/genaralFunction";

type Props = {
  handleDataTable: (value: any) => void;
  listMaQuyen: any;
  isResetTableList: boolean;
  dataTable: any;
  pId?: number;
};

interface Item {
  STT: string;
  MA_AC: string;
  SO_SO: string;
  KY_HIEU: string;
  TU_SO: number,
  DEN_SO: number,
  SO_LUONG: string,
  DON_GIA: string,
  THANH_TIEN: string,
}

const originData: any = [];
interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  editing: boolean;
  dataIndex: string;
  title: any;
  inputType: "number" | "text" | "select" | "input";
  record: Item;
  index: number;
}

const TablePhanBoAnChi: FC<Props> = ({ handleDataTable, listMaQuyen, isResetTableList, dataTable, pId }) => {
  const [form] = Form.useForm();
  const [data, setData] = useState(dataTable);
  const [editingSTT, setEditingSTT] = useState("");
  const [statusAdd, setStatusAdd] = useState(false);
  const { user, notify } = useAuth();
  const [itemDefault, setItemDefault] = useState<any>(null);
  const [listQuyen, setListQuyen] = useState<any>([]);
  const [isDisable, setIsDisable] = useState(true);

  const dataBody = {
    P_HASH: user.hash,
    P_TAI_KHOAN: user.tai_khoan,
    P_ID_CN: "",
    P_LOAI: "DULIEU",
    P_MA_AC: "",
    P_TEN: "",
    P_NV: "",
    P_GHI_CHU: "",
  };

  useEffect(() => {
    setData(dataTable);
  }, [dataTable]);

  const { data: dataDSAnChi } = useQuery({
    queryKey: ["ma-an-chi-list", dataBody],
    queryFn: () => getDSAnChi(dataBody),
    refetchOnWindowFocus: false,
    select: (data) => {
      return converDataSelectedAnChi(data);
    },
  });

  const EditableCell: React.FC<React.PropsWithChildren<EditableCellProps>> = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
  }) => {
    const save_handle = async (dataIndex: any) => {
      try {
        const values = await form.validateFields();
        if( dataIndex === "MA_AC") {
          form.setFieldsValue({"SO_SO": "", TU_SO: "", DEN_SO: "", SO_LUONG: "", DON_GIA: "", THANH_TIEN: ""});
          let rs = listMaQuyen?.filter((item: any) => item?.MA_AC === values?.MA_AC);
          setListQuyen(rs);
        }

        if (dataIndex === "SO_SO") {
          const item_2 = listMaQuyen.find((element: any) => element?.value === values?.SO_SO);
          // const rsp = { ...record, ...values, TU_SO: item_2?.TU_SO, DEN_SO: item_2?.DEN_SO}
          const itemForm = {
            "TU_SO": item_2?.TU_SO,
            "DEN_SO": item_2?.DEN_SO,
            "DON_GIA": item_2?.DON_GIA,
            "SO_LUONG": (item_2?.DEN_SO - item_2?.TU_SO) + 1,
            "THANH_TIEN": ((item_2?.DEN_SO - item_2?.TU_SO) + 1) * item_2?.DON_GIA
          }
          setItemDefault(itemForm);
          form.setFieldsValue(itemForm);
        }
      } catch (errInfo) {
        console.log('Save failed:', errInfo);
      }
    };
    const handleChangeInput = async (value: any, dataIndex: any, record: any) => {
      const rs = await form.validateFields();
      if (dataIndex === "TU_SO") {
        let so_luong = (rs.DEN_SO - value) + 1;
        form.setFieldsValue({ "TU_SO": value, "SO_LUONG": so_luong, "THANH_TIEN": (so_luong * rs?.DON_GIA) });
      } else if (dataIndex === "DEN_SO") {
        let so_luong = (value - rs.TU_SO) + 1;
        form.setFieldsValue({ "DEN_SO": value, "SO_LUONG": so_luong, "THANH_TIEN": (so_luong * rs?.DON_GIA) });
      }
    }
    let inputNode;
    if (inputType === "number") {
      inputNode =
        <DsInput name={dataIndex} type="number" size="small" min={0}
          disabled={(dataIndex === "SO_LUONG" || dataIndex === "DON_GIA" || dataIndex === "THANH_TIEN") ? true : false}
          onChange={(value) => handleChangeInput(value, dataIndex, record)}
        />;
    } else if (inputType === "select") {
      inputNode =
        <DsSelect size="small"
          name={dataIndex}
          options={dataIndex === 'MA_AC' ? dataDSAnChi : listQuyen || []}
          placeholder={`Chọn ${title}`}
          onChange={() => save_handle(dataIndex)}
        />;
    } else if (inputType === "text") {
      inputNode = <DsInput name={dataIndex} size="small" type="text" />;
    }
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item
            name={dataIndex}
            style={{ margin: 0 }}
            className="[&>*>input]:bg-red-600"
          >
            {inputNode}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  const isEditing = (record: Item) => record.STT === editingSTT;
  const edit = (record: Partial<Item> & { STT: React.Key }) => {
    form.setFieldsValue({
      MA_AC: "",
      SO_SO: "",
      KY_HIEU: "",
      TU_SO: "",
      DEN_SO: "",
      SO_LUONG: "",
      DON_GIA: "",
      THANH_TIEN: "",
      ...record,
    });
    setEditingSTT(record.STT);
  };

  const cancel = (STT: string) => {
    setEditingSTT("");
    if (statusAdd) {
      const newData = data.filter((item: any) => item.STT !== STT);
      setData(newData);
      setStatusAdd(false);
    }
  };
  useEffect(() => {
    handleDataTable(data);
  }, [data]);

  const save = async (STT: React.Key) => {
    try {
      const row = (await form.validateFields()) as any;
      const newData = [...data];
      const index = newData.findIndex((item) => STT === item.STT);
      if (index > -1) {
        setStatusAdd(false);
        const item = newData[index];
        if(row.TU_SO < itemDefault.TU_SO || row.DEN_SO > itemDefault.DEN_SO || row.DEN_SO < itemDefault.TU_SO) {
          notify?.({type: "error",
            content: `Số ấn chỉ không thuộc trong khoảng từ  ${itemDefault.TU_SO} đến ${itemDefault.DEN_SO}.`
          });
          return false;
        }
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setData(newData);
        setEditingSTT("");
      } else {
        setStatusAdd(false);
        newData.push(row);
        setData(newData);
        setEditingSTT("");
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };

  const handleDelete = (STT: string) => {
    const newData = data.filter((item: any) => item.STT !== STT);
    setData(newData);
  };

  const columns = [
    {
      title: "Mã ấn chỉ",
      dataIndex: "MA_AC",
      editable: true,
      render: (_: any, record: Item) => {
        const item =
          dataDSAnChi &&
          dataDSAnChi.find((element: any) => element?.value === record?.MA_AC);
        const labelCol = item?.label || null;
        return labelCol;
      },
    },
    {
      title: "Mã quyển",
      dataIndex: "SO_SO",
      editable: true,
      render: (_: any, record: Item) => {
        const item =
          listMaQuyen &&
          listMaQuyen.find((element: any) => element?.SO_SO === record?.SO_SO);
        console.log('record', record);
        console.log('listMaQuyen', listMaQuyen);
        console.log('item', item);

        const labelCol = item?.label || null;
        return labelCol;
      },
    },
    {
      title: "Số đầu",
      dataIndex: "TU_SO",
      width: "10%",
      editable: true,
    },
    {
      title: "Số cuối",
      dataIndex: "DEN_SO",
      editable: true,
      width: "10%",
    },
    {
      title: "Số lượng",
      dataIndex: "SO_LUONG",
      editable: true,
      width: "10%",
    },
    {
      title: "Đơn giá",
      dataIndex: "DON_GIA",
      editable: true,
      width: "10%",
    },
    {
      title: "Thành tiền",
      dataIndex: "THANH_TIEN",
      editable: true,
      width: "10%",
      // hidden: true,
    },
    {
      title: "Thao tác",
      width: "8%",
      dataIndex: "ThaoTac",
      hidden: pId ? true : false,
      render: (_: any, record: Item) => {
        const editable = isEditing(record);
        return editable ? (
          <div className="flex justify-around">
            <DsIcon
              name="icon-bxs-check-circle"
              className="md:text-md pt-1 text-[#37b652]"
              onClick={() => save(record.STT)}
            />
            <DsPopover
              closeOnClick={true}
              trigger={["click"]}
              content={
                <>
                  <div className="flex item-center mb-1">
                    <DsIcon
                      name="icon-bxs-error-circle"
                      className="md:text-md text-[#FAAD14] mr-1"
                    />
                    <p>Bạn có chắc chắn hủy bản này không?</p>
                  </div>

                  <div className="flex justify-around">
                    <DsButton
                      onClick={(e) => { }}
                      color="gray"
                      type="outline"
                      size="small"
                    >
                      Thoát
                    </DsButton>
                    <DsButton
                      onClick={() => cancel(record.STT)}
                      size="small"
                      color="red"
                    >
                      Hủy
                    </DsButton>
                  </div>
                </>
              }
              position="bottomLeft"
            >
              <DsIcon
                name="icon-bx-x"
                className="md:text-md pt-1 text-[#b43c3c]"
              />
            </DsPopover>
          </div>
        ) : (
          <div className="flex justify-around">
            <DsIcon
              name="icon-bx-edit"
              className="md:text-md pt-1 text-[#1779e2]"
              onClick={() => edit(record)}
            />
            <DsPopover
              closeOnClick={true}
              trigger={["click"]}
              content={
                <>
                  <div className="flex item-center mb-1">
                    <DsIcon
                      name="icon-bxs-error-circle"
                      className="md:text-md text-[#FAAD14] mr-1"
                    />
                    <p>Bạn có chắc chắn xóa không?</p>
                  </div>

                  <div className="flex justify-around">
                    <DsButton
                      // className="p-1 cursor-pointer hover:bg-[#FAFAFA]"
                      onClick={(e) => { }}
                      color="gray"
                      type="outline"
                      size="small"
                    >
                      Hủy
                    </DsButton>
                    <DsButton
                      // className="p-1 cursor-pointer  hover:bg-[#FAFAFA]"
                      onClick={() => handleDelete(record.STT)}
                      size="small"
                      color="red"
                    >
                      Xóa
                    </DsButton>
                  </div>
                </>
              }
              onOpenChange={() => { }}
              position="bottomLeft"
            >
              {/* <DsIcon name="icon-bx-trash" onClick={() => {}} /> */}
              <DsIcon
                name="icon-bx-trash"
                className="md:text-md pt-1 text-[#F4175A]"
              />
            </DsPopover>
          </div>
        );
      },
    },
  ];

  const mergedColumns: any = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record: Item) => ({
        record,
        inputType:
          col.dataIndex === "TU_SO" || col.dataIndex === "DEN_SO" || col.dataIndex === "SO_LUONG" || col.dataIndex === "DON_GIA" || col.dataIndex === "THANH_TIEN"
            ? "number"
            : col.dataIndex === "MA_AC" || col.dataIndex === "SO_SO"
              ? "select"     
              : "text",
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  // Hàm xử lý thêm dòng mới
  const handleAddRow = () => {
    setStatusAdd(true);
    const newSTT = (data.length + 1).toString();
    const newData: any = {
      STT: newSTT,
      MA_AC: "",
      KY_HIEU: "",
      TU_SO: "",
      DEN_SO: "",
      SO_LUONG: "",
      DON_GIA: "",
      THANH_TIEN: "",
      SO_SO: "",
    };
    setData([...data, newData]);
    setEditingSTT(newSTT); // Đánh dấu dòng mới để chỉnh sửa ngay lập tức
    form.resetFields();
  };

  useEffect(() => {
    if (!isResetTableList) {
      setStatusAdd(false);
      setListQuyen([]);
    }
  }, [isResetTableList]);

  return (
    <div>
      {!pId && (
        <>
          <p className="my-1 font-medium">Danh sách phân bổ</p>
          <DsButton
            onClick={handleAddRow}
            disabled={statusAdd}
            className="mb-2"
            buttonType="button"
          >
            + Thêm
          </DsButton>
        </>
      )}


      <Form form={form} component={false}>
        <DsTable
          className="w-full"
          components={{
            body: {
              cell: EditableCell,
            },
          }}
          bordered
          dataSource={data}
          columns={mergedColumns}
          rowClassName="editable-row"
        />
      </Form>
    </div>
  );
};

export default TablePhanBoAnChi;

