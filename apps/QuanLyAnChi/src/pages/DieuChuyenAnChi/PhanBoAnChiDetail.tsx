import { useFormik } from "formik";
import {
  DsButton,
  DsDatePicker,
  DsInput,
  DsModal,
  DsSelect,
} from "mvi-ds-ui";
import { FC, useCallback, useEffect, useState } from "react";
import { ValuesFormikPhanBoAnChi } from "../../type/common";
import { FormDieuChuyenSchema, formDNACSchema } from "../../constants/validation";
import TablePhanBoAnChi from "./TablePhanBoAnChi";
import { cols } from "./colDSPhanBoAnChi";
import { statePhanBo } from "../../recoil/atom";
import { converDataSelected, convertDateTime, getCurrentDateFormatted } from "../../constants/genaralFunction";
import { useRecoilValue } from "recoil";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { getThamSo } from "../../apis/AnChiApi";
import { useAuth } from "../../context/AuthContext";
import { createDieuChuyenAnChi, getDanhSachDieuChuyenAnChi } from "../../apis/DieuChuyenAnChi";

type Props = {
  isOpen: boolean;
  handleClose: (value: boolean) => void;
  pId?: number;
};

const PhanBoAnChiDetail: FC<Props> = ({ isOpen, handleClose, pId }) => {
  const queryCache = useQueryClient();
  const { user, notify } = useAuth();
  const statesPhanBo = useRecoilValue(statePhanBo);
  const listMenu: any = window.sessionStorage.getItem("list-menu");
  const parsedListMenu = JSON.parse(listMenu);
  const [isResetTableList, setIsResetTableList] = useState<boolean>(true);
  const [initialValues, setInitialValues] = useState<any>({
    P_SO_CT: "",
    P_NGAY_CT: "",
    P_DON_VI_CAP: "",
    P_DON_VI_NHAN: "",
    P_NOI_DUNG: "",
    P_MA_LOAI: "",
    P_MA_CB: "",
    P_PHAN_BO_CT: [],
  });

  const { data: listMaQuyen } = useQuery({
    queryKey: ["get-ma-quyen"],
    queryFn: () => getThamSo({
      P_TAI_KHOAN: user.email || " ",
      P_HASH: user.hash || "",
      P_LOAI: "DM_SO",
      P_VALUE: "",
    }),
    refetchOnWindowFocus: false,
    select: (data) => {
      let rs = data?.data?.data?.DM_SO;
      return rs.map((item: any, index: any) => {
        return {
          value: item?.SO_SO,
          // value: index,
          label: item?.SO_SO,
          ...item,
        }
      });
    },
  });


  const { data: rsDetail } = useQuery({
    queryKey: ["an-chi-chi-tiet", pId, isOpen],
    // queryKey: ["an-chi-chi-tiet", isOpen],
    queryFn: () =>
      getDanhSachDieuChuyenAnChi({
        P_HASH: user.hash,
        P_TAI_KHOAN: user.email,
        P_ID_PB: `${pId}`,
      }),
    enabled: !!pId && isOpen,
    refetchOnWindowFocus: false,
    select: (data) => {
      return data?.data?.data;
    },
  });

  const { mutateAsync } = useMutation({
    mutationFn: (data: any) => createDieuChuyenAnChi(data),
  });

  const { mutateAsync: mutateAsyncDetail } = useMutation({
    mutationFn: (data: any) => getDanhSachDieuChuyenAnChi(data),
  });

  const form = useFormik<ValuesFormikPhanBoAnChi>({
    initialValues: {
      P_SO_CT: rsDetail?.SO_PHIEU || "",
      P_NGAY_CT: rsDetail?.NGAY_PB || getCurrentDateFormatted("YYYYMMDD"),
      P_DON_VI_CAP: rsDetail?.TEN_DVI_DN || "",
      P_DON_VI_NHAN: rsDetail?.TEN_DVI_NHAN || "",
      P_NOI_DUNG: rsDetail?.NOI_DUNG || "",
      P_MA_LOAI: rsDetail?.LOAI || "",
      P_MA_CB: rsDetail?.NGUOI_NHAN || "",
      P_PHAN_BO_CT: rsDetail?.CHI_TIET || [],
    },
    enableReinitialize: true,
    validationSchema: FormDieuChuyenSchema,
    onSubmit: async (values) => {
      values.P_PHAN_BO_CT.map((data: any) => {
        return listMaQuyen.map((item: any) => {
          if (item?.value === data?.SO_SO) data.SO_SO = item?.SO_SO
        })
      });
      const params = {
        "P_HASH": user.hash,
        "P_TAI_KHOAN": user.tai_khoan,
        "P_ID_CN": parsedListMenu && parsedListMenu[1].group,
        "P_LOAI": "THEM",
        "P_MA_LOAI": values.P_MA_LOAI,
        "P_MA_CB": values.P_MA_CB,
        "P_NOI_DUNG": values.P_NOI_DUNG,
        "P_PHAN_BO_CT": JSON.stringify(values.P_PHAN_BO_CT)
      };
      const resutl = await mutateAsync(params);
      notify?.({
        type: resutl?.data?.data?.Success ? "success" : "error",
        content: resutl?.data?.data?.mess,
      });
      if (resutl?.data?.data?.Success) {
        form.resetForm();
        handleClose(false);
        setIsResetTableList(true);
        queryCache.invalidateQueries({ queryKey: ["list-dieu-chuyen"] });

      }
      console.log("values", values);
    },
  });

  const getFieldErr = useCallback(
    (field: keyof ValuesFormikPhanBoAnChi) => {
      const fErr = form.errors;
      return form.touched?.[field] && fErr?.[field] ? fErr?.[field]?.toString() : null;
    },
    [form.errors, form.touched]
  );

  const handleDataTable = (value: any) => {
    form.setFieldValue("P_PHAN_BO_CT", value);
  };

  const onDateChangeStartDate = (date: string, str: string) => {
    form.setFieldValue("P_NGAY_CT", str);
  };

  useEffect(() => {
    if (!isResetTableList) {
      form.resetForm();
      handleClose(false);
    }
    setIsResetTableList(true);
  }, [isResetTableList]);

  return (
    <div>
      <DsModal
        open={isOpen}
        width={"95%"}
        centered
        title="Phân bổ ấn chỉ"
        closable={false}
        footer={[
          <DsButton
            disabled={Object.keys(form.errors).length !== 0}
            key="confirm-del-prod"
            //@ts-ignore
            onClick={form.handleSubmit}
            style={{display: pId ? 'none': ""}}
          >
            Lưu
          </DsButton>,
          <DsButton
            color="red"
            key="cancel-del-prod"
            onClick={() => {
              setIsResetTableList(false);
            }}
            className="ds-ml-4"
          >
            Thoát
          </DsButton>
        ]}
      >
        <form onSubmit={form.handleSubmit}>
          <div className="grid gap-4">
            <div className="grid grid-cols-1 md:grid-cols-4 gap-4">
              <DsInput
                name="P_SO_CT"
                label="Số CT"
                type="text"
                disabled={true}
                value={form?.values?.P_SO_CT}
                onChange={(e) => form.setFieldValue("P_SO_CT", e || "")}
                state={getFieldErr("P_SO_CT") ? "error" : "normal"}
                message={getFieldErr("P_SO_CT")}
              />
              <DsDatePicker
                label="Ngày chứng từ"
                size="large"
                name="P_NGAY_CT"
                format="DD/MM/YYYY"
                onChange={onDateChangeStartDate}
                value={
                  form?.values?.P_NGAY_CT &&
                  convertDateTime(form?.values?.P_NGAY_CT.toString())
                }
                picker="date"
                disabled
              />
              <DsSelect
                name="P_MA_LOAI"
                label="Loại điều chuyển"
                options={
                  statesPhanBo?.loai_dieu_chuyen
                    ? converDataSelected(statesPhanBo?.loai_dieu_chuyen)
                    : []
                }
                value={form?.values?.P_MA_LOAI || null}
                onChange={(item) => {
                  form.setFieldValue("P_MA_LOAI", item);
                }}
                state={getFieldErr("P_MA_LOAI") ? "error" : "normal"}
                message={getFieldErr("P_MA_LOAI")}
              />
              <DsSelect
                name="P_MA_CB"
                label="Người nhận"
                options={
                  statesPhanBo?.nguoi_nhan ? statesPhanBo?.nguoi_nhan.map((item: any) => {
                    return {
                      value: item?.TAI_KHOAN,
                      label: item?.TEN
                    }
                  }) : []
                }
                value={form?.values?.P_MA_CB || null}
                onChange={(item) => {
                  form.setFieldValue("P_MA_CB", item);
                }}
                state={getFieldErr("P_MA_CB") ? "error" : "normal"}
                message={getFieldErr("P_MA_CB")}
              />
            </div>
            <DsInput
              name="P_NOI_DUNG"
              label="Nội dung"
              type="textarea"
              value={form?.values?.P_NOI_DUNG}
              onChange={(e) => form.setFieldValue("P_NOI_DUNG", e || "")}
              state={getFieldErr("P_NOI_DUNG") ? "error" : "normal"}
              message={getFieldErr("P_NOI_DUNG")}
            />
          </div>
          <div className="mt-4">
              <TablePhanBoAnChi
                handleDataTable={handleDataTable}
                listMaQuyen={listMaQuyen}
                isResetTableList={isResetTableList}
                dataTable={form?.values?.P_PHAN_BO_CT}
                pId={pId}
              />
          </div>
        </form>
      </DsModal>
    </div>
  );
};

export default PhanBoAnChiDetail;
