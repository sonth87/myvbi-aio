import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import usePageInfo from "../../hooks/usePageInfo";
import { DsButton, DsTable } from "mvi-ds-ui";
import TimKiemMatHong from "./TimKiemMatHong";
import { cols } from "./colDanhSachMatHong";
import FormHuyMatHong from "./FormHuyMatHong";
import { useAuth } from "../../context/AuthContext";
import { getDsMatHuy } from "../../apis/AnChiApi";
import { useQuery } from "@tanstack/react-query";

function TaxManage() {
  const { user } = useAuth();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [idRecord, setIdRecord] = useState<any>();

  useEffect(() => {
    setModuleInfo({
      module: {
        icon: "icon-bx-package",
        title: "Quản lý mất hỏng",
      },
      path: [
        {
          label: "Quản lý mất hỏng",
        },
      ],
    });
    return () => cleanModuleInfo();
  }, []);

  const [initValueSearch, setInitValueSearch] = useState<any>({
    "P_HASH": user.hash,
    "P_TAI_KHOAN": user.email,
    "P_ID_CN": "3",
    "P_SO_PB": "ALL",
    "P_LOAI_PB": "ALL",
    "P_NOI_DUNG": "ALL",
    "P_ID_PB": "ALL",
    "P_TU_NGAY": "20240101",
    "P_DEN_NGAY": "20241230"
    // getCurrentDateFormatted("YYYYMMDD")
  });

  const { data: dataSource } = useQuery({
    queryKey: ["list-tb-mat-huy", initValueSearch],
    queryFn: () => getDsMatHuy(initValueSearch),
    enabled: !!initValueSearch,
    refetchOnWindowFocus: false,
  });

  const handleOpenModal = (value: boolean,  record?: any) => {
    console.log('edit', value)
    setIdRecord(record?.ID_PB);
    setIsOpenModal(value);
  };

  const handleChangeValuesSearch = (values: any) => {
    setInitValueSearch(values);
  };

  return (
    <div>
      <Helmet>
        <title>Mất Hỏng</title>
      </Helmet>

      <TimKiemMatHong
        initValueSearch={initValueSearch}
        handleChangeValuesSearch={handleChangeValuesSearch}
       />

      <div className="flex my-1 ">
        <DsButton
          className="my-3"
          label="Tạo mới"
          onClick={() => {
            setIsOpenModal(true);
            setIdRecord(null)
          }}
        />
      </div>
      <div>
        <DsTable
          bordered
          columns={cols(handleOpenModal)}
          dataSource={dataSource || []}
          className="!inline"
          pagination={{
            showSizeChanger: true,
            total: dataSource?.length,
          }}
        />
      </div>
      <FormHuyMatHong
        isOpen={isOpenModal}
        handleClose={() => setIsOpenModal(false)}
        pId={idRecord}
      />
    </div>
  );
}

export default TaxManage;
