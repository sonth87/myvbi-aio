import { DsIcon } from "mvi-ds-ui";

export const cols = (handleOpenModal: (value: boolean,  record?: any) => void) => [
  
  {
    title: "Số sổ Chứng từ",
    dataIndex: "SO_PHIEU",
    key: "SO_PHIEU",
  },
  {
    title: "Đơn vị",
    dataIndex: "MA_DV_X",
    key: "MA_DV_X",
  },
  {
    title: "Loại",
    dataIndex: "TEN_LOAI",
    key: "TEN_LOAI",
  },
  {
    title: "Người tạo",
    dataIndex: "NGUOI_XUAT",
    key: "NGUOI_XUAT",
  },
  {
    title: "Ngày tạo",
    dataIndex: "NGAY_PB",
    key: "NGAY_PB",
  },
  {
    dataIndex: "thao-tac",
    title: `Thao tác`,
    width: '8%',
    align: "center",
    render: (_: any, record: any) => {
      return <div className="flex justify-around">
        <div
         onClick={() => handleOpenModal(true, record)}
         >
          <DsIcon className="" name="icon-bx-edit text-blue-600 cursor-pointer" />
        </div>
        {/* <div>
          <DsIcon className="" name="icon-bx-trash text-red-600 cursor-pointer" />
        </div> */}
      </div>;
    },
  },
];
