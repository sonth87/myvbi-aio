export const cols = () => [
  {
    title: "STT",
    dataIndex: "index",
    key: "tieu_de",
  },
  // {
  //   dataIndex: "thao-tac",
  //   title: `Thao tác`,
  //   align: "center",
  //   render: (_: any, record: any) => {
  //     return <div className="flex justify-center"></div>;
  //   },
  // },
  {
    title: "LHNV",
    dataIndex: "tieu_de",
    key: "tieu_de",
  },
  {
    title: "Mã quyền",
    dataIndex: "tieu_de",
    key: "tieu_de",
    render: (_: any, record: any) => record.tieu_de && record.tieu_de.value,
  },
  {
    title: "Số đầu",
    dataIndex: "tieu_de",
    key: "tieu_de",
    render: (_: any, record: any) => record.tieu_de && record.tieu_de.value,
  },
  {
    title: "Số cuối",
    dataIndex: "tieu_de",
    key: "tieu_de",
    render: (_: any, record: any) => record.tieu_de && record.tieu_de.value,
  },
  {
    title: "Số lượng",
    dataIndex: "tieu_de",
    key: "tieu_de",
    render: (_: any, record: any) => record.tieu_de && record.tieu_de.value,
  },
  {
    title: "Đơn giá",
    dataIndex: "tieu_de",
    key: "tieu_de",
    render: (_: any, record: any) => record.tieu_de && record.tieu_de.value,
  },
  {
    title: "Thành tiền",
    dataIndex: "tieu_de",
    key: "tieu_de",
    render: (_: any, record: any) => record.tieu_de && record.tieu_de.value,
  },
];
