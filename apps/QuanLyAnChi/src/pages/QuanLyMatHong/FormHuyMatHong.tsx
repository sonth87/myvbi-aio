import { useFormik } from "formik";
import {
  DsButton,
  DsDatePicker,
  DsInput,
  DsModal,
  DsSelect,
  DsTable,
  Form,
} from "mvi-ds-ui";
import { FC, useCallback, useEffect, useState } from "react";
import { ValuesFormikMT, ValuesFormikPhanBoAnChi } from "../../type/common";
import { formDNACSchema, formMatHongSchema } from "../../constants/validation";
import { cols } from "./colDSAnChiHuyMatHong";
import TableCreateMatHuy from "./TableCreateMatHuy";
import { converDataSelected, convertDateTime, getCurrentDateFormatted } from "../../constants/genaralFunction";
import { useRecoilValue } from "recoil";
import { stateHMH } from "../../recoil/atom";
import { useMutation, useQuery } from "@tanstack/react-query";
import { getThamSo } from "../../apis/AnChiApi";
import { useAuth } from "../../context/AuthContext";
import { createDieuChuyenAnChi, getDanhSachDieuChuyenAnChi } from "../../apis/DieuChuyenAnChi";

type Props = {
  isOpen: boolean;
  handleClose: (value: boolean) => void;
  pId: any;
};

const FormHuyMatHong: FC<Props> = ({ isOpen, handleClose, pId }) => {
  const { user, notify } = useAuth();
  const statesHMH = useRecoilValue(stateHMH);
  const listMenu: any = window.sessionStorage.getItem("list-menu");
  const parsedListMenu = JSON.parse(listMenu);
  const [isResetTableList, setIsResetTableList] = useState<boolean>(true);

  const { data: listMaQuyen } = useQuery({
    queryKey: ["get-ma-quyen"],
    queryFn: () => getThamSo({
      P_TAI_KHOAN: user.email || " ",
      P_HASH: user.hash || "",
      P_LOAI: "DM_SO",
      P_VALUE: "",
    }),
    refetchOnWindowFocus: false,
    select: (data) => {
      let rs = data?.data?.data?.DM_SO;
      return rs.map((item: any, index: any) => {
        return {
          value: index,
          label: item?.SO_SO,
          ...item,
        }
      });
    },
  });


  const { data: rsDetail } = useQuery({
    queryKey: ["an-chi-chi-tiet", pId, isOpen],
    // queryKey: ["an-chi-chi-tiet", isOpen],
    queryFn: () =>
      getDanhSachDieuChuyenAnChi({
        P_HASH: user.hash,
        P_TAI_KHOAN: user.email,
        P_ID_PB: `${pId}`,
      }),
    enabled: !!pId && isOpen,
    refetchOnWindowFocus: false,
    select: (data) => {
      return data?.data?.data;
    },
  });

  const { mutateAsync } = useMutation({
    mutationFn: (data: any) => createDieuChuyenAnChi(data),
  });
  
  const form = useFormik({
    initialValues: {
      P_SO_CT: rsDetail?.SO_PHIEU || "",
      P_NGAY_CT: rsDetail?.NGAY_PB || getCurrentDateFormatted("YYYYMMDD"),
      P_MA_LOAI: rsDetail?.LOAI || "",
      P_NOI_DUNG: rsDetail?.NOI_DUNG || "",
      P_PHAN_BO_CT: rsDetail?.CHI_TIET || [] ,
    },
    enableReinitialize: true,
    validationSchema: formMatHongSchema,
    onSubmit: async (values) => {
      console.log(values);
      values.P_PHAN_BO_CT.map((data: any) => {
        return listMaQuyen.map((item: any) => {
          if (item?.value === data?.SO_SO) data.SO_SO = item?.SO_SO
        })
      });

      const params = {
        "P_HASH": user.hash,
        "P_TAI_KHOAN": user.tai_khoan,
        "P_ID_CN": parsedListMenu && parsedListMenu[1].group,
        "P_LOAI": "THEM_MH",
        "P_MA_LOAI": values.P_MA_LOAI,
        // "P_MA_CB": values.P_MA_CB,
        "P_NOI_DUNG": values.P_NOI_DUNG,
        "P_PHAN_BO_CT": JSON.stringify(values.P_PHAN_BO_CT)
      };
      console.log('params', params);
      const resutl = await mutateAsync(params);
      notify?.({
        type: resutl?.data?.data?.Success ? "success" : "error",
        content: resutl?.data?.data?.mess,
      });
      if (resutl?.data?.data?.Success) {
        form.resetForm();
        handleClose(false);
        setIsResetTableList(true);
      }
    },
  });

  const getFieldErr = useCallback(
    (field: keyof ValuesFormikMT) => {
      const fErr = form.errors;
      return form.touched?.[field] && fErr?.[field] ? fErr?.[field]?.toString() : null;
    },
    [form.errors, form.touched]
  );

  const handleDataTable = (value: any) => {
    form.setFieldValue("P_PHAN_BO_CT", value);
  };

  const onDateChangeStartDate = (date: string, str: string) => {
    form.setFieldValue("P_NGAY_CT", str);
  };

  useEffect(() => {
    if (!isResetTableList) {
      form.resetForm();
      handleClose(false);
    }
    setIsResetTableList(true);
  }, [isResetTableList]);

  return (
    <div>
      <DsModal
        open={isOpen}
        width={"90%"}
        centered
        title="Khai báo ấn chỉ hủy mất hỏng"
        closable={false}
        footer={[
          <DsButton
            disabled={Object.keys(form.errors).length !== 0}
            key="confirm-del-prod"
            //@ts-ignore
            onClick={form.handleSubmit} 
            style={{display: pId ? 'none': ""}}
          >
            Lưu
          </DsButton>,
          <DsButton
            color="red"
            key="cancel-del-prod"
            onClick={() => {
              setIsResetTableList(false);
            }}
            className="ds-ml-4"
          >
            Thoát
          </DsButton>,
        ]}
      >
        <form onSubmit={form.handleSubmit}>
          <div className="grid gap-4">
            <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
              <DsInput
                name="P_SO_CT"
                label="Số CT"
                type="text"
                value={form?.values?.P_SO_CT}
                onChange={(e) => form.setFieldValue("P_SO_CT", e || "")}
                state={getFieldErr("P_SO_CT") ? "error" : "normal"}
                message={getFieldErr("P_SO_CT")}
                disabled
              />
              <DsDatePicker
                label="Ngày chứng từ"
                size="large"
                name="P_NGAY_CT"
                format="DD/MM/YYYY"
                value={
                  form?.values?.P_NGAY_CT &&
                  convertDateTime(form?.values?.P_NGAY_CT.toString())
                }
                onChange={onDateChangeStartDate}
                picker="date"
                disabled
              />
              <DsSelect
                name="P_MA_LOAI"
                label="Loại"
                options={
                  statesHMH?.loai_hmh
                    ? converDataSelected(statesHMH?.loai_hmh)
                    : []
                }
                value={form?.values?.P_MA_LOAI || null}
                onChange={(item) => {
                  console.log('item -------->', item);
                  form.setFieldValue("P_MA_LOAI", item);
                }}
                // state={getFieldErr("P_DON_VI_NHAN") ? "error" : "normal"}
                // message={getFieldErr("P_DON_VI_NHAN")}
              />
            </div>
            <DsInput
              name="P_NOI_DUNG"
              label="Lý do mất"
              type="textarea"
              value={form?.values?.P_NOI_DUNG}
              onChange={(e) => form.setFieldValue("P_NOI_DUNG", e || "")}
              // state={getFieldErr("P_NOI_DUNG") ? "error" : "normal"}
              // message={getFieldErr("P_NOI_DUNG")}
            />
          </div>
          <div className="mt-4">
             <TableCreateMatHuy
                handleDataTable={handleDataTable}
                listMaQuyen={listMaQuyen}
                isResetTableList={isResetTableList}
                dataTable={form?.values?.P_PHAN_BO_CT  || []}
                pId={pId}
              />
          </div>
        </form>
      </DsModal>
    </div>
  );
};

export default FormHuyMatHong;

