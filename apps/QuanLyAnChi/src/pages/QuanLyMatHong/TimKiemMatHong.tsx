import { useFormik } from "formik";
import { DsButton, DsDatePicker, DsInput, DsSelect } from "mvi-ds-ui";

import { useSearchParams } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { useAuth } from "../../context/AuthContext";
import { FC, useEffect } from "react";
import { getThamSo } from "../../apis/AnChiApi";
import { useQueries } from "@tanstack/react-query";
import { stateHMH } from "../../recoil/atom";
import { useRecoilState } from "recoil";
import { converDataSelected, convertDate, convertDateTime } from "../../constants/genaralFunction";

type initValueSearchProps = {
  initValueSearch: any;
  handleChangeValuesSearch: (values: any) => void;
};

const TimKiemMatHong: FC<initValueSearchProps> = ({
  initValueSearch,
  handleChangeValuesSearch,
}) => { 
  const { notify, user } = useAuth();
  const [searchParams, setSearchParams] = useSearchParams();
  const [statesHMH, setStatesHMH]= useRecoilState(stateHMH);

  const typeMH = {
    P_TAI_KHOAN: user.email || " ",
    P_HASH: user.hash || "",
    P_LOAI: "HMH",
    P_VALUE: "",
  };
  const listUser = {
    P_TAI_KHOAN: user.email || "",
    P_HASH: user.hash || "",
    P_LOAI: "DON_VI",
    P_VALUE: "",
  };

  const results = useQueries({
    queries: [
      {
        queryKey: ["get-danh-muc-hmh"],
        queryFn: () => getThamSo(typeMH),
        refetchOnWindowFocus: false,
      },
      {
        queryKey: ["get-don-vi"],
        queryFn: () => getThamSo(listUser),
        refetchOnWindowFocus: false,
      },
    ],
    combine: (results) => {
      return {
        data: results.map((result) => result.data),
        pending: results.some((result) => result.isPending),
      };
    },
  });


  useEffect(() => {
    if (!results.pending) {
      setStatesHMH({
        loai_hmh: results.data[0]?.data?.data,
        don_vi: results.data[1]?.data?.data?.DON_VI
      });
    }
  }, [results, setStatesHMH]);


  const form = useFormik({
    initialValues: {
      P_HASH: initValueSearch.P_HASH,
      P_TAI_KHOAN: initValueSearch.P_TAI_KHOAN,
      P_ID_CN: initValueSearch.P_ID_CN,
      P_SO_PB: initValueSearch.P_SO_PB,
      P_LOAI_PB: initValueSearch.P_LOAI_PB,
      P_DVI_BAO: initValueSearch.P_DVI_BAO,
      P_ID_PB: initValueSearch.P_ID_PB,
      P_TU_NGAY: initValueSearch.P_TU_NGAY,
      P_DEN_NGAY: initValueSearch.P_DEN_NGAY,
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      console.log(values);
      handleChangeValuesSearch(values);
    },
  });

  const onDateChangeStartDate = (date: string, str: string) => {
    form.setFieldValue("P_TU_NGAY", convertDate(str));
  };

  const onDateChangeEndDate = (date: string, str: string) => {
    form.setFieldValue("P_DEN_NGAY", convertDate(str));
  };

  return (
    <div>
      <form onSubmit={form.handleSubmit}>
        <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
        <DsInput
            label="Số chứng từ"
            size="large"
            type="text"
            // value={form.values.tieu_de}
            onChange={(e) => form.setFieldValue("P_SO_PB", e)}
            // disabled={isLoading}
          />

          <DsSelect
            label="Loại"
            size="large"
            options={converDataSelected(results?.data[0]?.data?.data)}
            required
            onChange={(e) => form.setFieldValue("P_LOAI_PB", e)}
          />

          <DsSelect
            label="Đơn vị"
            size="large"
            options={converDataSelected(results.data[1]?.data?.data?.DON_VI)}
            required
            onChange={(e) => form.setFieldValue("P_DVI_BAO", e)}
          />
          <DsDatePicker
            label="Ngày tạo từ"
            size="large"
            name="P_TU_NGAY"
            format="DD/MM/YYYY"
            // value={form.values?.tu_ngay}
            onChange={onDateChangeStartDate}
            value={
              form?.values?.P_TU_NGAY &&
              convertDateTime(form?.values?.P_TU_NGAY.toString())
            }
            picker="date"
          />

          <DsDatePicker
            label="Đến ngày"
            size="large"
            name="P_DEN_NGAY"
            format="DD/MM/YYYY"
            // value={form.values.den_ngay}
            value={
              form?.values?.P_DEN_NGAY &&
              convertDateTime(form?.values?.P_DEN_NGAY.toString())
            }
            onChange={onDateChangeEndDate}
            picker="date"
          />
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4 items-center">
            <DsButton size="large" className="ds-w-full">
              Tìm kiếm
            </DsButton>
            <DsButton size="large" className="ds-w-full">
              Xuất Excel
            </DsButton>
          </div>
        </div>
      </form>
    </div>
  );
};

export default TimKiemMatHong;
