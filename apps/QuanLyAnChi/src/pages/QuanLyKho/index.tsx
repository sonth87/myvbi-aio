import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import usePageInfo from "../../hooks/usePageInfo";
import { DsButton, DsTable } from "mvi-ds-ui";
import TimKiemMatHong from "../QuanLyMatHong/TimKiemMatHong";
import { cols } from "../QuanLyMatHong/colDanhSachMatHong";
import FormHuyMatHong from "../QuanLyMatHong/FormHuyMatHong";

function TaxManage() {
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const [isOpenModal, setIsOpenModal] = useState(false);
  useEffect(() => {
    setModuleInfo({
      module: {
        icon: "icon-bx-package",
        title: "Quản lý kho",
      },
      path: [
        {
          label: "Quản lý kho",
        },
      ],
    });
    return () => cleanModuleInfo();
  }, []);
  const handleOpenModal = (value: boolean) => {
    setIsOpenModal(value);
  };
  return (
    <div>
      <Helmet>
        <title>Kho</title>
      </Helmet>
      {/* <TimKiemMatHong /> */}
      <div className="flex my-1 ">
        <DsButton
          className="my-3"
          label="Tạo mới"
          onClick={() => {
            setIsOpenModal(true);
          }}
        />
      </div>
      <div>
        <DsTable
          bordered
          columns={cols(handleOpenModal)}
          dataSource={[]}
          className="!inline"
          pagination={{}}
        />
      </div>
      {/* <FormHuyMatHong isOpen={isOpenModal} handleClose={handleOpenModal} /> */}
    </div>
  );
}

export default TaxManage;
