import { useFormik } from "formik";
import { DsButton, DsDatePicker, DsIcon, DsInput, DsSelect } from "mvi-ds-ui";
import React, { useState } from "react";
import { array, boolean, number, object, string } from "yup";

export const formDNACSchema = object({
  name: string().required("Không được để trống"),
  birthday: string().required("Không được để trống"),
  age: string().required("Không được để trống"),
  sex: string().required("Không được để trống"),
  phone: string().required("Không được để trống"),
  email: string().required("Không được để trống"),
  ci_card: string().required("Không được để trống"),
  address: string().required("Không được để trống"),
});

function calculateAge(birthdate: string) {
  console.log("birthdate", birthdate);
  if (birthdate) {
    // Chuyển đổi chuỗi ngày sinh từ định dạng dd/mm/yyyy sang yyyy-mm-dd
    let parts: any = birthdate.split("/");
    let birthDate = new Date(parts[2], parts[1] - 1, parts[0]); // Tháng trong JavaScript bắt đầu từ 0

    // Lấy ngày hiện tại
    let today = new Date();
    // Tính tuổi
    let age = today.getFullYear() - birthDate.getFullYear();
    // Điều chỉnh nếu chưa đến ngày sinh nhật năm nay
    let monthDiff = today.getMonth() - birthDate.getMonth();
    if (
      monthDiff < 0 ||
      (monthDiff === 0 && today.getDate() < birthDate.getDate())
    ) {
      age--;
    }
    return age;
  } else {
    return "";
  }
}

function currentDate() {
  let today = new Date();

  let day: any = today.getDate();
  let month: any = today.getMonth() + 1; // Tháng trong JavaScript bắt đầu từ 0, nên cần cộng thêm 1
  let year: any = today.getFullYear();

  // Đảm bảo ngày và tháng luôn có hai chữ số
  if (day < 10) {
    day = "0" + day;
  }
  if (month < 10) {
    month = "0" + month;
  }

  let formattedDate = day + "/" + month + "/" + year;
  return formattedDate;
}

const Dashboard = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [isShowInput, setIsShowInput] = useState<boolean>(false);
  const handleCheckOldContract = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  };
  const showInputOldContract = () => {
    setIsShowInput(!isShowInput);
  };

  const form = useFormik({
    initialValues: {
      name: "",
      birthday: "",
      age: "",
      sex: "",
      phone: "",
      email: "",
      ci_card: "",
      address: "",
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      console.log(values);
    },
    validationSchema: formDNACSchema,
  });

  const handleChangeBirthday = (date: string, str: string) => {
    form.setFieldValue("birthday", str);
    form.setFieldValue("age", calculateAge(str));
  };

  return (
    <>

      Đây là Dashboard
      {/* Thông tin hợp đồng cũ */}
      {/* <div
        className={`border border-[#efefef] rounded-lg p-4 ${
          isShowInput ? "flex justify-between" : "block"
        }`}
      >
        <b onClick={showInputOldContract} className="cursor-pointer">
          1. Thông tin hợp đồng cũ (nếu có)
        </b>
        {isShowInput ? (
          <p
            className="font-medium text-sky-600 cursor-pointer"
            onClick={showInputOldContract}
          >
            Chỉnh sửa
          </p>
        ) : (
          <div>
            <div className="mt-4 flex">
              <DsInput
                placeholder="Số hợp đồng cũ"
                size="small"
                disabled={loading}
              />
              <DsButton
                label={loading ? "Đang kiểm tra" : "Kiểm tra"}
                shape="rounded"
                type="light"
                className="ml-4"
                disabled={loading}
                prefixIcon={
                  loading ? <DsIcon name="icon-bx-at !text-sm mr-1" /> : null
                }
                onClick={handleCheckOldContract}
              />
            </div>
            <p className="text-sm font-medium mt-2">
              Giảm 5% tới 15% hoặc tăng phí bảo hiểm tái tục căn cứ trên tỷ lệ
              bồi thường của người được bảo hiểm tại thời điểm tái tục
            </p>
          </div>
        )}
      </div> */}

      {/* Thông tin người mua */}
      {/* <div
        className={`border border-[#efefef] rounded-lg p-4 mt-4 ${
          isShowInput ? "flex justify-between" : "block"
        }`}
      >
        <b className="cursor-pointer">2. Thông tin người mua</b>
        <form onSubmit={form.handleSubmit}>
          <div className="grid grid-cols-4 gap-4 mt-4">
            <DsInput
              label="Tên người mua"
              size="large"
              type="text"
              name="name"
              onChange={(e) => form.setFieldValue("name", e)}
              // disabled={isLoading}
            />
            <DsDatePicker
              label="Ngày sinh"
              size="large"
              name="birthday"
              // maxDate={"18/06/2024"}
              format="DD/MM/YYYY"
              onChange={handleChangeBirthday}
              picker="date"
            />
            <DsInput
              label="Tuổi"
              name="age"
              size="large"
              value={form.values.age.toString() || ""}
              type="text"
              disabled={true}
            />
            <DsSelect
              label="Giới tính"
              name="sex"
              size="large"
              options={[
                { label: "Nam", value: "0" },
                { label: "Nữ", value: "1" },
              ]}
              onChange={(e) => form.setFieldValue("sex", e)}
            />
            <DsInput
              label="Số diện thoại"
              size="large"
              type="text"
              name="phone"
              onChange={(e) => form.setFieldValue("phone", e)}
              // disabled={isLoading}
            />
            <DsInput
              label="Email"
              name="email"
              size="large"
              type="text"
              onChange={(e) => form.setFieldValue("email", e)}
              // disabled={isLoading}
            />
            <DsInput
              label="Số CCCD/ Hộ chiếu"
              name="ci_card"
              size="large"
              type="text"
              onChange={(e) => form.setFieldValue("ci_card", e)}
              // disabled={isLoading}
            />
          </div>
          <DsInput
            className="mt-4"
            label="Địa chỉ"
            required
            size="large"
            type="text"
            name="address"
            onChange={(e) => form.setFieldValue("address", e)}
            // disabled={isLoading}
          />
          <DsButton
            size="normal"
            shape="rounded"
            className="px-2"
            // disabled={Object.keys(form.errors).length === 0}
          >
            Lưu
          </DsButton>
        </form>
      </div> */}
    </>
  );
};

export default Dashboard;
