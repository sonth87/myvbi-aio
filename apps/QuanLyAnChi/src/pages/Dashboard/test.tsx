import React, { useState } from "react";


const  MyComponent = () => {
    return (
      <div className="flex flex-col justify-center bg-white">
        <div className="flex flex-col w-full max-md:max-w-full">
          <div className="flex flex-col w-full max-md:max-w-full">
            <div className="flex flex-col w-full max-md:max-w-full">
              <div className="flex flex-col justify-center px-16 py-2 w-full bg-slate-50 max-md:px-5 max-md:max-w-full">
                <div className="flex gap-5 pl-20 mx-3 max-md:flex-wrap max-md:pl-5 max-md:mr-2.5">
                  <div className="flex gap-2 justify-center text-sm font-semibold leading-5 text-rose-600 whitespace-nowrap">
                    <img
                      loading="lazy"
                      src="https://cdn.builder.io/api/v1/image/assets/TEMP/9aeb9f54646f7c9b791631af246341605c060f3252879d2a743fbd1b451ffef4?apiKey=88fb82266c4d44249e007971612bb358&"
                      className="shrink-0 w-5 aspect-square"
                    />
                    <div>1900.1566</div>
                  </div>
                  <div className="flex justify-center items-center">
                    <img
                      loading="lazy"
                      src="https://cdn.builder.io/api/v1/image/assets/TEMP/bb263f6276f0a1d3efd327b8c9fd683e23ef2fcca996027f1312f096f964b7e6?apiKey=88fb82266c4d44249e007971612bb358&"
                      className="w-5 aspect-square"
                    />
                  </div>
                </div>
              </div>
              <div className="flex flex-col justify-center px-16 w-full bg-white max-md:px-5 max-md:max-w-full">
                <div className="flex gap-5 py-3 mx-3 max-md:flex-wrap max-md:mr-2.5 max-md:max-w-full">
                  <div className="flex flex-col flex-1 justify-center items-start max-md:max-w-full">
                    <img
                      loading="lazy"
                      src="https://cdn.builder.io/api/v1/image/assets/TEMP/3ffc5b7579848cb8f069beed10574e49ff02a6ee6595d048073ec416bda4ee55?apiKey=88fb82266c4d44249e007971612bb358&"
                      className="max-w-full aspect-[5.26] w-[207px]"
                    />
                  </div>
                  <div className="flex gap-2 justify-center py-1 text-base font-medium leading-6 text-center text-slate-500">
                    <div className="flex gap-2 px-3 py-1 rounded-[1000px]">
                      <div>Sản phẩm bạn & gia đình</div>
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/08fbcead94df4617d914c90aad06c13d2633397189ca861e3bb3f5ce8967192a?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="shrink-0 w-6 aspect-square"
                      />
                    </div>
                    <div className="justify-center px-2 py-1 rounded-[1000px]">
                      Trang chủ
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex flex-col px-16 mt-3 w-full max-md:px-5 max-md:max-w-full">
              <img
                loading="lazy"
                srcSet="https://cdn.builder.io/api/v1/image/assets/TEMP/57aa56273f8c4bc949eef548e163010df755df7eb5f54e7f6a21c7fae86de017?apiKey=88fb82266c4d44249e007971612bb358&width=100 100w, https://cdn.builder.io/api/v1/image/assets/TEMP/57aa56273f8c4bc949eef548e163010df755df7eb5f54e7f6a21c7fae86de017?apiKey=88fb82266c4d44249e007971612bb358&width=200 200w, https://cdn.builder.io/api/v1/image/assets/TEMP/57aa56273f8c4bc949eef548e163010df755df7eb5f54e7f6a21c7fae86de017?apiKey=88fb82266c4d44249e007971612bb358&width=400 400w, https://cdn.builder.io/api/v1/image/assets/TEMP/57aa56273f8c4bc949eef548e163010df755df7eb5f54e7f6a21c7fae86de017?apiKey=88fb82266c4d44249e007971612bb358&width=800 800w, https://cdn.builder.io/api/v1/image/assets/TEMP/57aa56273f8c4bc949eef548e163010df755df7eb5f54e7f6a21c7fae86de017?apiKey=88fb82266c4d44249e007971612bb358&width=1200 1200w, https://cdn.builder.io/api/v1/image/assets/TEMP/57aa56273f8c4bc949eef548e163010df755df7eb5f54e7f6a21c7fae86de017?apiKey=88fb82266c4d44249e007971612bb358&width=1600 1600w, https://cdn.builder.io/api/v1/image/assets/TEMP/57aa56273f8c4bc949eef548e163010df755df7eb5f54e7f6a21c7fae86de017?apiKey=88fb82266c4d44249e007971612bb358&width=2000 2000w, https://cdn.builder.io/api/v1/image/assets/TEMP/57aa56273f8c4bc949eef548e163010df755df7eb5f54e7f6a21c7fae86de017?apiKey=88fb82266c4d44249e007971612bb358&"
                className="w-full aspect-[3.03] max-md:max-w-full"
              />
              <div className="flex flex-col px-3 mt-10 max-md:max-w-full">
                <div className="flex gap-2.5 justify-between w-full max-md:flex-wrap max-md:max-w-full">
                  <div className="flex flex-col max-md:max-w-full">
                    <div className="flex gap-3 text-4xl leading-[60px] max-md:flex-wrap">
                      <div className="font-medium text-slate-700">
                        Bảo hiểm sức khỏe
                      </div>
                      <div className="flex-1 font-bold text-rose-600">
                        VBI Care
                      </div>
                    </div>
                    <div className="mt-2 text-lg leading-7 text-slate-500 max-md:max-w-full">
                      Bảo vệ gia đình bạn chủ động hoàn toàn trước các rủi ro bất
                      ngờ về sức khỏe. Giải pháp tài chính tối ưu giúp bạn và gia
                      đình an tâm tận hưởng cuộc sống.
                    </div>
                  </div>
                  <div className="flex gap-3 self-start text-base font-semibold leading-6">
                    <div className="flex flex-col justify-center px-6 py-3 text-rose-600 bg-white border border-rose-600 border-solid rounded-[1000px] max-md:px-5">
                      <div className="justify-center">Tính phí nhanh</div>
                    </div>
                    <div className="flex flex-col justify-center px-6 py-3 text-white bg-[linear-gradient(270deg,#0872B7_0%,#00538E_100%)] rounded-[1000px] max-md:px-5">
                      <div className="justify-center">Mua ngay</div>
                    </div>
                  </div>
                </div>
                <div className="flex flex-col justify-center mt-10 text-base font-medium leading-6 text-slate-400 max-md:max-w-full">
                  <div className="flex gap-5 pr-20 border-b border-solid border-zinc-200 max-md:flex-wrap max-md:pr-5">
                    <div className="flex flex-col justify-center text-slate-700">
                      <div className="pt-3 pb-2.5">Quyền lợi ưu việt</div>
                      <div className="shrink-0 h-0.5 bg-slate-700" />
                    </div>
                    <div className="flex flex-col justify-center">
                      <div className="justify-center py-3">
                        Đối tượng bảo hiểm
                      </div>
                    </div>
                    <div className="flex flex-col justify-center">
                      <div className="justify-center py-3">Thời gian chờ</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col justify-center p-16 w-full bg-white max-md:px-5 max-md:max-w-full">
            <div className="flex flex-col mx-3 bg-white rounded-3xl max-md:mr-2.5 max-md:max-w-full">
              <div className="flex flex-col justify-center text-4xl font-medium leading-[56.16px] text-slate-700 max-md:max-w-full">
                <div className="flex flex-col justify-center max-md:max-w-full">
                  <div className="flex gap-5 pr-20 max-md:flex-wrap max-md:pr-5">
                    <div>Quyền lợi ưu việt</div>
                    <div className="shrink-0 my-auto w-12 h-1 bg-rose-600" />
                  </div>
                </div>
              </div>
              <div className="flex flex-col flex-wrap content-start mt-10 max-md:max-w-full">
                <div className="max-md:max-w-full">
                  <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                    <div className="flex flex-col w-3/12 max-md:ml-0 max-md:w-full">
                      <div className="flex flex-col grow self-stretch p-5 rounded-2xl border border-solid border-zinc-200 max-md:mt-6">
                        <div className="flex justify-center items-center px-3 w-14 h-14 bg-pink-50 rounded-[1000px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/acdf955c953fb0a8e41b25dcc4871990db1057bc448029029da3b1d78c218f59?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="w-full aspect-square"
                          />
                        </div>
                        <div className="flex flex-col mt-4">
                          <div className="text-xl font-semibold leading-8 text-slate-700">
                            Điều trị nội trú, phẫu thuật do bệnh
                          </div>
                          <div className="mt-1 text-sm leading-5 text-slate-500">
                            Lên đến 250 triệu đồng
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                      <div className="flex flex-col grow self-stretch p-5 rounded-2xl border border-solid border-zinc-200 max-md:mt-6">
                        <div className="flex justify-center items-center px-3 w-14 h-14 bg-pink-50 rounded-[1000px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/f93f03dd50d53622938106dacfe69d2bf7818f8e8a957cdd94006d77dcbb65ec?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="w-full aspect-square"
                          />
                        </div>
                        <div className="flex flex-col mt-4">
                          <div className="text-xl font-semibold leading-8 text-slate-700">
                            Điều trị ngoại trú
                          </div>
                          <div className="mt-1 text-sm leading-5 text-slate-500">
                            Lên đến 20 triệu đồng
                            <br />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                      <div className="flex flex-col grow self-stretch p-5 rounded-2xl border border-solid border-zinc-200 max-md:mt-6">
                        <div className="flex justify-center items-center px-3 w-14 h-14 bg-pink-50 rounded-[1000px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/b06887214f4c28bfa58dbb6f85db2b1455d4d94908f30dd07423ad490fd1af1b?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="w-full aspect-square"
                          />
                        </div>
                        <div className="flex flex-col mt-4">
                          <div className="text-xl font-semibold leading-8 text-slate-700">
                            Chăm sóc Thai sản
                          </div>
                          <div className="mt-1 text-sm leading-5 text-slate-500">
                            Lên đến 40 triệu đồng/năm
                            <br />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                      <div className="flex flex-col grow self-stretch p-5 rounded-2xl border border-solid border-zinc-200 max-md:mt-6">
                        <div className="flex justify-center items-center px-3 w-14 h-14 bg-pink-50 rounded-[1000px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/b78a13036c9443c41362de882c5197df85b79ee5041a5d4829b10bdd70048740?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="w-full aspect-square"
                          />
                        </div>
                        <div className="flex flex-col mt-4">
                          <div className="text-xl font-semibold leading-8 text-slate-700">
                            Sinh mạng
                          </div>
                          <div className="mt-1 text-sm leading-5 text-slate-500">
                            Lên đến 1 tỷ VNĐ
                            <br />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mt-6 max-md:max-w-full">
                  <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                    <div className="flex flex-col w-3/12 max-md:ml-0 max-md:w-full">
                      <div className="flex flex-col grow self-stretch p-5 rounded-2xl border border-solid border-zinc-200 max-md:mt-6">
                        <div className="flex justify-center items-center px-3 w-14 h-14 bg-pink-50 rounded-[1000px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/42c448ae86fba6cde1bba8bf1bed312654b72924303bdfc2f40fcf682a73f5ff?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="w-full aspect-square"
                          />
                        </div>
                        <div className="flex flex-col mt-4">
                          <div className="text-xl font-semibold leading-8 text-slate-700">
                            Điều trị nha khoa
                          </div>
                          <div className="mt-1 text-sm leading-5 text-slate-500">
                            Lên đến 4 triệu đồng
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                      <div className="flex flex-col grow self-stretch p-5 rounded-2xl border border-solid border-zinc-200 max-md:mt-6">
                        <div className="flex justify-center items-center px-3 w-14 h-14 bg-pink-50 rounded-[1000px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/f3e441b2bceb3c4bfd26f2889bb5028917b87304a69b71a7d55ce8e84ea90015?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="w-full aspect-square"
                          />
                        </div>
                        <div className="flex flex-col mt-4">
                          <div className="text-xl font-semibold leading-8 text-slate-700">
                            Trợ cấp nằm viện do tai nạn
                          </div>
                          <div className="mt-1 text-sm leading-5 text-slate-500">
                            Lên đến 18 triệu đồng
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                      <div className="flex flex-col grow self-stretch p-5 rounded-2xl border border-solid border-zinc-200 max-md:mt-6">
                        <div className="flex justify-center items-center px-3 w-14 h-14 bg-pink-50 rounded-[1000px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/6ac515e60a53a8aa54eecd7f3c39dad1bd5eea459f94c53c7dc3c56289d46e1b?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="w-full aspect-square"
                          />
                        </div>
                        <div className="flex flex-col mt-4">
                          <div className="text-xl font-semibold leading-8 text-slate-700">
                            Bảo lãnh viện phí 24/7
                          </div>
                          <div className="mt-1 text-sm leading-5 text-slate-500">
                            Trên 200 cơ sở y tế
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                      <div className="flex flex-col grow self-stretch p-5 rounded-2xl border border-solid border-zinc-200 max-md:mt-6">
                        <div className="flex justify-center items-center px-3 w-14 h-14 bg-pink-50 rounded-[1000px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/b46c7f4d572ae38cac6f4436386748262ba31b3c2f0cc336175ca5fe07e8d2cc?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="w-full aspect-square"
                          />
                        </div>
                        <div className="flex flex-col mt-4">
                          <div className="text-xl font-semibold leading-8 text-slate-700">
                            Bồi thường online 100%
                          </div>
                          <div className="mt-1 text-sm leading-5 text-slate-500">
                            Chỉ 5+ ngày
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="flex flex-col justify-center px-4 py-3 mt-10 max-w-full text-base font-semibold leading-6 text-rose-600 bg-pink-100 rounded-[1000px] w-[189px]">
                <div className="flex gap-2 justify-center">
                  <div>Chi tiết quyền lợi</div>
                  <img
                    loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/c083596c7cc71ff4026fb860080752a08ff044458701f89b31d3562a0cfd4a80?apiKey=88fb82266c4d44249e007971612bb358&"
                    className="shrink-0 w-6 aspect-square"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col justify-center p-16 w-full bg-slate-50 max-md:px-5 max-md:max-w-full">
            <div className="flex flex-col mx-3 max-md:mr-2.5 max-md:max-w-full">
              <div className="flex gap-5 px-3 text-4xl font-medium leading-[56.16px] text-slate-700 max-md:flex-wrap max-md:pr-5">
                <div>Tính phí nhanh</div>
                <div className="shrink-0 my-auto w-12 h-1 bg-rose-600" />
              </div>
              <div className="flex flex-col p-6 mt-10 bg-white rounded-2xl border border-solid border-zinc-200 max-md:px-5 max-md:max-w-full">
                <div className="flex gap-4 pr-20 max-md:flex-wrap max-md:pr-5">
                  <div className="flex flex-col justify-center">
                    <div className="flex gap-2 py-2 pr-2 pl-4 bg-white rounded-lg border border-solid border-zinc-200">
                      <div className="flex flex-col flex-1">
                        <div className="text-xs leading-4 text-slate-400">
                          Ngày sinh{" "}
                        </div>
                        <div className="text-base leading-6 text-slate-700">
                          01/01/2018 (6 tuổi)
                        </div>
                      </div>
                      <div className="flex justify-center items-center p-2 rounded-lg">
                        <img
                          loading="lazy"
                          src="https://cdn.builder.io/api/v1/image/assets/TEMP/f8538467b09c3abf9b1b41ce495874b7aeaf2ba85974ef9a9bcd68b6a8ec2315?apiKey=88fb82266c4d44249e007971612bb358&"
                          className="w-6 aspect-square"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="flex flex-col justify-center px-6 py-3 my-auto text-base font-semibold leading-6 bg-gray-100 rounded-[1000px] text-slate-400 max-md:px-5">
                    <div className="justify-center">Tính phí</div>
                  </div>
                </div>
                <div className="flex flex-col justify-center py-3 pr-3 pl-4 mt-4 text-sm leading-5 bg-orange-50 rounded-lg text-slate-700 max-md:max-w-full">
                  <div className="flex gap-2 max-md:flex-wrap">
                    <img
                      loading="lazy"
                      src="https://cdn.builder.io/api/v1/image/assets/TEMP/100afcc12f3f36de286380cc4db2150aee4a4d8a62e8f7afd9bb304bc0299e55?apiKey=88fb82266c4d44249e007971612bb358&"
                      className="shrink-0 self-start w-5 aspect-square"
                    />
                    <div className="flex-1 justify-center max-md:max-w-full">
                      Để hưởng mức phí tối ưu, Quý khách có thể mua bảo hiểm cho
                      con kèm cha/mẹ trên cùng một hợp đồng (chương trình của
                      cha/mẹ có quyền lợi tương đương hoặc cao hơn của con) để phí
                      bảo hiểm của con không bị tăng 30% như khi tham gia độc lập.
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-10 max-md:max-w-full">
                <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                  <div className="flex flex-col w-[67%] max-md:ml-0 max-md:w-full">
                    <div className="flex flex-col grow max-md:mt-6 max-md:max-w-full">
                      <div className="flex flex-col justify-center text-base font-medium leading-6 text-slate-400 max-md:max-w-full">
                        <div className="flex gap-3 pr-20 max-md:flex-wrap max-md:pr-5">
                          <div className="flex flex-col justify-center border border-solid border-zinc-200 rounded-[37px]">
                            <div className="justify-center px-4 py-3">
                              VBI Care Đồng
                            </div>
                          </div>
                          <div className="flex flex-col justify-center border border-solid border-zinc-200 rounded-[37px]">
                            <div className="justify-center px-4 py-3">
                              VBI Care Bạc
                            </div>
                          </div>
                          <div className="flex flex-col justify-center text-white bg-slate-700 rounded-[37px]">
                            <div className="justify-center px-4 py-3">
                              VBI Care Titan
                            </div>
                          </div>
                          <div className="flex flex-col justify-center border border-solid border-zinc-200 rounded-[37px]">
                            <div className="justify-center px-4 py-3">
                              VBI Care Vàng
                            </div>
                          </div>
                          <div className="flex flex-col justify-center border border-solid border-zinc-200 rounded-[37px]">
                            <div className="justify-center px-4 py-3">
                              VBI Care Kim Cương
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="flex flex-col p-6 mt-6 bg-white rounded-2xl max-md:px-5 max-md:max-w-full">
                        <div className="text-xl font-semibold leading-8 text-slate-700 max-md:max-w-full">
                          Quyền lợi bảo hiểm chính
                        </div>
                        <div className="flex flex-col mt-6 max-md:max-w-full">
                          <div className="flex flex-col justify-center p-3 bg-white rounded-lg border border-solid border-zinc-100 max-md:max-w-full">
                            <div className="flex gap-3 max-md:flex-wrap">
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/ff3df81eae040efb98cedf91076825f85cba73271b0e1186d9884b913326a1e4?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex-1 text-base font-medium leading-6 text-slate-700 max-md:max-w-full">
                                1.1. Quyền lợi bảo hiểm chính - Chi phí y tế nội
                                trú, phẫu thuật do bệnh
                              </div>
                              <div className="text-base font-medium leading-6 text-right text-rose-600">
                                50 triệu VNĐ/ năm
                              </div>
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c67a675a883162543341fb3bfd6095f7a3e7d5e77d37d0aaea666b154f20bc98?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-col justify-center p-3 mt-3 bg-white rounded-lg border border-solid border-zinc-100 max-md:max-w-full">
                            <div className="flex gap-3 max-md:flex-wrap">
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/ff3df81eae040efb98cedf91076825f85cba73271b0e1186d9884b913326a1e4?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex-1 text-base font-medium leading-6 text-slate-700 max-md:max-w-full">
                                1.2. Tử vong, thương tật vĩnh viễn do bệnh *
                              </div>
                              <div className="text-base font-medium leading-6 text-right text-rose-600">
                                50 triệu VNĐ/ năm
                              </div>
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c67a675a883162543341fb3bfd6095f7a3e7d5e77d37d0aaea666b154f20bc98?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-col justify-center p-3 mt-3 bg-white rounded-lg border border-solid border-zinc-100 max-md:max-w-full">
                            <div className="flex gap-3 max-md:flex-wrap">
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/ff3df81eae040efb98cedf91076825f85cba73271b0e1186d9884b913326a1e4?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex-1 text-base font-medium leading-6 text-slate-700 max-md:max-w-full">
                                1.3. Tử vong, thương tật vĩnh viễn do tai nạn
                              </div>
                              <div className="text-base font-medium leading-6 text-right text-rose-600">
                                50 triệu VNĐ/ năm
                              </div>
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c67a675a883162543341fb3bfd6095f7a3e7d5e77d37d0aaea666b154f20bc98?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-col justify-center p-3 mt-3 bg-white rounded-lg border border-solid border-zinc-100 max-md:max-w-full">
                            <div className="flex gap-3 max-md:flex-wrap">
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/ff3df81eae040efb98cedf91076825f85cba73271b0e1186d9884b913326a1e4?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex-1 text-base font-medium leading-6 text-slate-700 max-md:max-w-full">
                                1.4. Chi phí y tế điều trị tai nạn
                              </div>
                              <div className="text-base font-medium leading-6 text-right text-rose-600">
                                50 triệu VNĐ/ năm
                              </div>
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c67a675a883162543341fb3bfd6095f7a3e7d5e77d37d0aaea666b154f20bc98?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-col justify-center p-3 mt-3 bg-white rounded-lg border border-solid border-zinc-100 max-md:max-w-full">
                            <div className="flex gap-3 max-md:flex-wrap">
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/ff3df81eae040efb98cedf91076825f85cba73271b0e1186d9884b913326a1e4?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex-1 text-base font-medium leading-6 text-slate-700 max-md:max-w-full">
                                1.5. Dịch vụ bảo lãnh viện phí
                              </div>
                              <div className="text-base font-medium leading-6 text-right text-rose-600">
                                50 triệu VNĐ/ năm
                              </div>
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c67a675a883162543341fb3bfd6095f7a3e7d5e77d37d0aaea666b154f20bc98?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="flex flex-col p-6 mt-6 bg-white rounded-2xl max-md:px-5 max-md:max-w-full">
                        <div className="text-xl font-semibold leading-8 text-slate-700 max-md:max-w-full">
                          Quyền lợi bổ sung
                        </div>
                        <div className="flex flex-col mt-6 max-md:max-w-full">
                          <div className="flex flex-col justify-center p-3 bg-white rounded-lg border border-solid border-zinc-100 max-md:max-w-full">
                            <div className="flex gap-3 max-md:flex-wrap">
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/164df4703bd7f58d9adce7fa273cee70989549bb8bcf3996ece6cd4439b10234?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex-1 text-base font-medium leading-6 text-slate-700 max-md:max-w-full">
                                2.1. Điều trị ngoại trú do bệnh
                              </div>
                              <div className="text-base font-medium leading-6 text-right text-rose-600">
                                50 triệu VNĐ/ năm
                              </div>
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c67a675a883162543341fb3bfd6095f7a3e7d5e77d37d0aaea666b154f20bc98?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-col justify-center p-3 mt-3 bg-white rounded-lg border border-solid border-zinc-100 max-md:max-w-full">
                            <div className="flex gap-3 max-md:flex-wrap">
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/164df4703bd7f58d9adce7fa273cee70989549bb8bcf3996ece6cd4439b10234?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex-1 text-base font-medium leading-6 text-slate-700 max-md:max-w-full">
                                2.2. Bảo hiểm thai sản
                              </div>
                              <div className="text-base font-medium leading-6 text-right text-rose-600">
                                50 triệu VNĐ/ năm
                              </div>
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c67a675a883162543341fb3bfd6095f7a3e7d5e77d37d0aaea666b154f20bc98?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-col justify-center p-3 mt-3 bg-white rounded-lg border border-solid border-zinc-100 max-md:max-w-full">
                            <div className="flex gap-3 max-md:flex-wrap">
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/164df4703bd7f58d9adce7fa273cee70989549bb8bcf3996ece6cd4439b10234?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex-1 text-base font-medium leading-6 text-slate-700 max-md:max-w-full">
                                2.3. Điều trị Nha khoa
                              </div>
                              <div className="text-base font-medium leading-6 text-right text-rose-600">
                                50 triệu VNĐ/ năm
                              </div>
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c67a675a883162543341fb3bfd6095f7a3e7d5e77d37d0aaea666b154f20bc98?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-col justify-center p-3 mt-3 bg-white rounded-lg border border-solid border-zinc-100 max-md:max-w-full">
                            <div className="flex gap-3 max-md:flex-wrap">
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/164df4703bd7f58d9adce7fa273cee70989549bb8bcf3996ece6cd4439b10234?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex-1 text-base font-medium leading-6 text-slate-700 max-md:max-w-full">
                                2.4. Trợ cấp nằm viện do tai nạn
                              </div>
                              <div className="text-base font-medium leading-6 text-right text-rose-600">
                                50 triệu VNĐ/ năm
                              </div>
                              <div className="flex justify-center items-center">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/c67a675a883162543341fb3bfd6095f7a3e7d5e77d37d0aaea666b154f20bc98?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="mt-6 text-sm leading-5 text-slate-500 max-md:max-w-full">
                        <span className="font-medium text-slate-700">
                          (*) Danh sách bệnh/tình trạng áp dụng đồng chi trả
                        </span>
                        :{" "}
                        <span className="">
                          Viêm xoang mãn tính/Viêm họng mãn tính, Hen, Phổi tắc
                          nghẽn mãn tính (COPD), Suy thận/Sỏi thận, Đái tháo
                          đường, Bệnh lý về huyết áp, Bệnh về khớp, Ung thư,
                          U/Bướu/Nang/Polyp các loại, Viêm dạ dày/đại tràng/trực
                          tràng, Viêm gan virus, Rối loạn tiền đình, Rối loạn
                          tuyến giáp, Bệnh tim.
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="flex flex-col ml-5 w-[33%] max-md:ml-0 max-md:w-full">
                    <div className="flex flex-col justify-center max-md:mt-6">
                      <div className="flex flex-col p-6 w-full rounded-2xl bg-slate-700 max-md:px-5">
                        <div className="flex flex-col">
                          <div className="flex gap-0 justify-between text-base leading-6 text-white">
                            <div className="flex-1">VBI care Titan ( 1 năm )</div>
                            <div className="flex-1 font-semibold text-right">
                              1.204.000 VNĐ
                            </div>
                          </div>
                          <div className="flex flex-col mt-4">
                            <div className="flex gap-3 text-base leading-6 text-white">
                              <div className="flex-1">Quyền lợi bổ sung</div>
                              <div className="flex-1 font-semibold text-right">
                                1.204.000 VNĐ
                              </div>
                            </div>
                            <div className="flex flex-col mt-4 text-sm leading-5 text-white text-opacity-80">
                              <div className="flex gap-3">
                                <div className="flex-1">Thai sản</div>
                                <div className="flex-1 text-right">
                                  1.000.000 VNĐ
                                </div>
                              </div>
                              <div className="flex gap-3 mt-2">
                                <div className="flex-1">Bảo hiểm ngoại trú</div>
                                <div className="flex-1 text-right">
                                  200.000 VNĐ
                                </div>
                              </div>
                              <div className="flex gap-3 mt-2">
                                <div className="flex-1">Nha khoa</div>
                                <div className="flex-1 text-right">0 VNĐ</div>
                              </div>
                              <div className="flex gap-3 mt-2">
                                <div className="flex-1">Trợ cấp nằm viện</div>
                                <div className="flex-1 text-right">0 VNĐ</div>
                              </div>
                            </div>
                          </div>
                          <div className="flex gap-3 mt-4 text-base leading-6">
                            <div className="flex-1 text-white">Giảm phí</div>
                            <div className="flex-1 font-semibold text-right text-green-400">
                              -142.000 VNĐ
                            </div>
                          </div>
                        </div>
                        <img
                          loading="lazy"
                          src="https://cdn.builder.io/api/v1/image/assets/TEMP/e361aa50e1dc3512984aa07aec9d5de87cfbea3d25751ccca29164b4efc6672c?apiKey=88fb82266c4d44249e007971612bb358&"
                          className="mt-5 w-full border border-solid border-slate-500 stroke-[1px] stroke-slate-500"
                        />
                        <div className="flex gap-3 mt-5">
                          <div className="flex-1 text-lg leading-7 text-white">
                            Phí tạm tính
                          </div>
                          <div className="flex-1 text-xl font-semibold leading-8 text-right text-white">
                            2.338.000. VNĐ
                          </div>
                        </div>
                        <div className="flex justify-center items-center px-4 py-3 mt-5 text-base font-semibold leading-6 text-rose-600 bg-white rounded-lg max-md:px-5">
                          <div className="justify-center">Mua ngay</div>
                        </div>
                        <div className="mt-5 text-sm leading-5 text-white">
                          Ưu đãi đặc quyền: Giảm 5% tới 15% phí bảo hiểm tái tục
                          căn cứ trên tỷ lệ bồi thường của người được bảo hiểm tại
                          thời điểm tái tục.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col justify-center p-16 w-full bg-white max-md:px-5 max-md:max-w-full">
            <div className="flex flex-col mx-3 max-md:mr-2.5 max-md:max-w-full">
              <div className="flex gap-5 pr-20 text-4xl font-medium leading-[56.16px] text-slate-700 max-md:flex-wrap max-md:pr-5">
                <div>Tiện ích</div>
                <div className="shrink-0 my-auto w-12 h-1 bg-rose-600" />
              </div>
              <div className="justify-center mt-10 max-md:pr-5 max-md:max-w-full">
                <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                  <div className="flex flex-col w-6/12 max-md:ml-0 max-md:w-full">
                    <div className="flex overflow-hidden relative flex-col grow justify-center p-6 rounded-2xl aspect-[1.58] max-md:px-5 max-md:mt-6">
                      <img
                        loading="lazy"
                        srcSet="https://cdn.builder.io/api/v1/image/assets/TEMP/f9845fdabe9fd9b0f823648b9dd6398bf0fc91e9954452aa8c79fb01375f68a4?apiKey=88fb82266c4d44249e007971612bb358&width=100 100w, https://cdn.builder.io/api/v1/image/assets/TEMP/f9845fdabe9fd9b0f823648b9dd6398bf0fc91e9954452aa8c79fb01375f68a4?apiKey=88fb82266c4d44249e007971612bb358&width=200 200w, https://cdn.builder.io/api/v1/image/assets/TEMP/f9845fdabe9fd9b0f823648b9dd6398bf0fc91e9954452aa8c79fb01375f68a4?apiKey=88fb82266c4d44249e007971612bb358&width=400 400w, https://cdn.builder.io/api/v1/image/assets/TEMP/f9845fdabe9fd9b0f823648b9dd6398bf0fc91e9954452aa8c79fb01375f68a4?apiKey=88fb82266c4d44249e007971612bb358&width=800 800w, https://cdn.builder.io/api/v1/image/assets/TEMP/f9845fdabe9fd9b0f823648b9dd6398bf0fc91e9954452aa8c79fb01375f68a4?apiKey=88fb82266c4d44249e007971612bb358&width=1200 1200w, https://cdn.builder.io/api/v1/image/assets/TEMP/f9845fdabe9fd9b0f823648b9dd6398bf0fc91e9954452aa8c79fb01375f68a4?apiKey=88fb82266c4d44249e007971612bb358&width=1600 1600w, https://cdn.builder.io/api/v1/image/assets/TEMP/f9845fdabe9fd9b0f823648b9dd6398bf0fc91e9954452aa8c79fb01375f68a4?apiKey=88fb82266c4d44249e007971612bb358&width=2000 2000w, https://cdn.builder.io/api/v1/image/assets/TEMP/f9845fdabe9fd9b0f823648b9dd6398bf0fc91e9954452aa8c79fb01375f68a4?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="object-cover absolute inset-0 size-full"
                      />
                      <div className="flex relative gap-5 justify-between mt-24 max-md:mt-10">
                        <div className="flex flex-col text-white leading-[150%]">
                          <div className="text-2xl font-semibold">
                            Tra cứu bệnh viện
                          </div>
                          <div className="mt-2 text-base">
                            Tra cứu các cơ sở ý tế được bảo lãnh
                          </div>
                        </div>
                        <div className="flex justify-center items-center self-start p-2.5 bg-gray-100 rounded-3xl h-[54px] w-[54px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/831204277e98bfe8f9773cccd82df50dcfcea48b05d5a2ac2cd4a0b1a1d46001?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="aspect-square w-[34px]"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="flex flex-col ml-5 w-6/12 max-md:ml-0 max-md:w-full">
                    <div className="flex overflow-hidden relative flex-col grow justify-center p-6 rounded-2xl aspect-[1.58] max-md:px-5 max-md:mt-6">
                      <img
                        loading="lazy"
                        srcSet="https://cdn.builder.io/api/v1/image/assets/TEMP/3d1a8255f82d4b4e5ba4935719ef497b6eed4450a0c1fa7551c211ac9e1e811b?apiKey=88fb82266c4d44249e007971612bb358&width=100 100w, https://cdn.builder.io/api/v1/image/assets/TEMP/3d1a8255f82d4b4e5ba4935719ef497b6eed4450a0c1fa7551c211ac9e1e811b?apiKey=88fb82266c4d44249e007971612bb358&width=200 200w, https://cdn.builder.io/api/v1/image/assets/TEMP/3d1a8255f82d4b4e5ba4935719ef497b6eed4450a0c1fa7551c211ac9e1e811b?apiKey=88fb82266c4d44249e007971612bb358&width=400 400w, https://cdn.builder.io/api/v1/image/assets/TEMP/3d1a8255f82d4b4e5ba4935719ef497b6eed4450a0c1fa7551c211ac9e1e811b?apiKey=88fb82266c4d44249e007971612bb358&width=800 800w, https://cdn.builder.io/api/v1/image/assets/TEMP/3d1a8255f82d4b4e5ba4935719ef497b6eed4450a0c1fa7551c211ac9e1e811b?apiKey=88fb82266c4d44249e007971612bb358&width=1200 1200w, https://cdn.builder.io/api/v1/image/assets/TEMP/3d1a8255f82d4b4e5ba4935719ef497b6eed4450a0c1fa7551c211ac9e1e811b?apiKey=88fb82266c4d44249e007971612bb358&width=1600 1600w, https://cdn.builder.io/api/v1/image/assets/TEMP/3d1a8255f82d4b4e5ba4935719ef497b6eed4450a0c1fa7551c211ac9e1e811b?apiKey=88fb82266c4d44249e007971612bb358&width=2000 2000w, https://cdn.builder.io/api/v1/image/assets/TEMP/3d1a8255f82d4b4e5ba4935719ef497b6eed4450a0c1fa7551c211ac9e1e811b?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="object-cover absolute inset-0 size-full"
                      />
                      <div className="flex relative gap-5 justify-between mt-24 max-md:mt-10">
                        <div className="flex flex-col text-white leading-[150%]">
                          <div className="text-2xl font-semibold">
                            Hướng dẫn bồi thường
                          </div>
                          <div className="mt-2 text-base">
                            Hướng dẫn bồi thường đơn bảo hiểm
                          </div>
                        </div>
                        <div className="flex justify-center items-center self-start p-2.5 bg-gray-100 rounded-3xl h-[54px] w-[54px]">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/4c316791ff4ae5e6ec4d5f5c26dc402eef5f001329881d0e070e4b33e451702a?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="aspect-square w-[34px]"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col px-20 py-16 w-full bg-slate-50 max-md:px-5 max-md:max-w-full">
            <div className="flex gap-5 pr-20 text-4xl font-medium leading-[56.16px] text-slate-700 max-md:flex-wrap max-md:pr-5">
              <div>Câu hỏi thường gặp</div>
              <div className="shrink-0 my-auto w-12 h-1 bg-rose-600" />
            </div>
            <div className="mt-10 max-md:max-w-full">
              <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                <div className="flex flex-col w-[33%] max-md:ml-0 max-md:w-full">
                  <div className="flex flex-col self-stretch text-lg font-semibold leading-7 text-slate-400 max-md:mt-6">
                    <div className="flex gap-2.5 justify-between py-4 text-rose-600">
                      <div>Về bảo hiểm sức khỏe</div>
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/6b4c1543864dfb1abb1879f93d1122867f1cfae3da9acf4fb2e223c87cede1c7?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="shrink-0 self-start w-6 aspect-square"
                      />
                    </div>
                    <div className="shrink-0 h-px bg-zinc-200" />
                    <div className="flex gap-2.5 justify-between py-4 rounded-lg">
                      <div>Về bồi thường</div>
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/6b4c1543864dfb1abb1879f93d1122867f1cfae3da9acf4fb2e223c87cede1c7?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="shrink-0 self-start w-6 aspect-square"
                      />
                    </div>
                    <div className="shrink-0 h-px bg-zinc-200" />
                    <div className="flex gap-2.5 justify-between py-4 rounded-lg">
                      <div>Cần hỗ trợ</div>
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/6b4c1543864dfb1abb1879f93d1122867f1cfae3da9acf4fb2e223c87cede1c7?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="shrink-0 self-start w-6 aspect-square"
                      />
                    </div>
                  </div>
                </div>
                <div className="flex flex-col ml-5 w-[67%] max-md:ml-0 max-md:w-full">
                  <div className="flex flex-col grow p-6 rounded-2xl max-md:px-5 max-md:mt-6 max-md:max-w-full">
                    <div className="flex flex-col max-md:max-w-full">
                      <div className="flex gap-3 text-base font-semibold leading-6 text-rose-600 max-md:flex-wrap">
                        <div className="flex-1 max-md:max-w-full">
                          1. Hợp đồng bảo hiểm sẽ được gửi đến Khách Hàng như thế
                          nào?
                        </div>
                        <img
                          loading="lazy"
                          src="https://cdn.builder.io/api/v1/image/assets/TEMP/b012d18ec1c822fc54d4070af81984e5cd57c084ae393a02708096d0dab443c0?apiKey=88fb82266c4d44249e007971612bb358&"
                          className="shrink-0 w-6 aspect-square"
                        />
                      </div>
                      <div className="mt-2.5 text-sm leading-5 text-slate-700 max-md:max-w-full">
                        Khi mua và thanh toán thành công sản phẩm bảo hiểm sức
                        khỏe, khách hàng sẽ được cấp giấy chứng nhận bảo hiểm điện
                        tử do VBI phát hành qua email đã đăng ký.Giấy chứng nhận
                        điện tử có đầy đủ giá trị pháp lý theo quy định tại Nghị
                        định 52/2013/NĐ-CP ngày 16/05/2013 của Chính phủ về Thương
                        mại điện tử. Khách hàng cũng có thể truy cập về thông tin
                        giấy chứng nhận và quyền lợi bảo hiểm trên App My VBI
                      </div>
                    </div>
                    <div className="shrink-0 mt-6 h-px bg-zinc-100 max-md:max-w-full" />
                    <div className="flex gap-3 mt-6 text-base font-semibold leading-6 text-slate-700 max-md:flex-wrap">
                      <div className="flex-1 max-md:max-w-full">
                        2. Ưu điểm của bảo hiểm sức khỏe VBI Care là gì?
                      </div>
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/6a082ac5b0e8b19eb3e3a2fc934dc61fdaa9bc328c7c8967692b0be560854dcd?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="shrink-0 w-6 aspect-square"
                      />
                    </div>
                    <div className="shrink-0 mt-6 h-px bg-zinc-100 max-md:max-w-full" />
                    <div className="flex gap-3 mt-6 text-base font-semibold leading-6 text-slate-700 max-md:flex-wrap">
                      <div className="flex-1 max-md:max-w-full">
                        3. Tại sao nên mua bảo hiểm online?
                      </div>
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/6a082ac5b0e8b19eb3e3a2fc934dc61fdaa9bc328c7c8967692b0be560854dcd?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="shrink-0 w-6 aspect-square"
                      />
                    </div>
                    <div className="shrink-0 mt-6 h-px bg-zinc-100 max-md:max-w-full" />
                    <div className="flex gap-3 mt-6 text-base font-semibold leading-6 text-slate-700 max-md:flex-wrap">
                      <div className="flex-1 max-md:max-w-full">
                        4. Quy trình mua trả góp bảo hiểm sức khỏe trên Website
                        như thế nào?
                      </div>
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/6a082ac5b0e8b19eb3e3a2fc934dc61fdaa9bc328c7c8967692b0be560854dcd?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="shrink-0 w-6 aspect-square"
                      />
                    </div>
                    <div className="shrink-0 mt-6 h-px bg-zinc-100 max-md:max-w-full" />
                    <div className="flex gap-3 mt-6 text-base font-semibold leading-6 text-slate-700 max-md:flex-wrap">
                      <div className="flex-1 max-md:max-w-full">
                        5. Bảo hiểm sức khỏe VBI Care cho phép khám/ chữa bệnh ở
                        những bệnh viện nào?
                      </div>
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/6a082ac5b0e8b19eb3e3a2fc934dc61fdaa9bc328c7c8967692b0be560854dcd?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="shrink-0 w-6 aspect-square"
                      />
                    </div>
                    <div className="shrink-0 mt-6 h-px bg-zinc-100 max-md:max-w-full" />
                    <div className="flex gap-3 mt-6 text-base font-semibold leading-6 text-slate-700 max-md:flex-wrap">
                      <div className="flex-1 max-md:max-w-full">
                        6. Bảo hiểm sức khỏe VBI Care thanh toán bao nhiêu % bảo
                        hiểm?
                      </div>
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/6a082ac5b0e8b19eb3e3a2fc934dc61fdaa9bc328c7c8967692b0be560854dcd?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="shrink-0 w-6 aspect-square"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex z-10 flex-col justify-center pb-12 w-full rounded-3xl max-md:max-w-full">
            <div className="flex flex-col px-16 pt-12 mb-48 w-full bg-slate-50 max-md:px-5 max-md:mb-10 max-md:max-w-full">
              <div className="flex z-10 flex-col justify-center py-0.5 mx-3 mb-0 max-md:mr-2.5 max-md:mb-2.5 max-md:max-w-full">
                <div className="flex flex-col justify-center rounded-3xl max-md:max-w-full">
                  <div className="flex flex-col justify-center rounded-3xl max-md:max-w-full">
                    <div className="max-md:max-w-full">
                      <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                        <div className="flex flex-col w-[33%] max-md:ml-0 max-md:w-full">
                          <div className="flex flex-col grow justify-center items-start py-20 pr-5 pl-16 max-md:pl-5 max-md:max-w-full">
                            <div className="flex flex-col justify-center mt-5">
                              <img
                                loading="lazy"
                                src="https://cdn.builder.io/api/v1/image/assets/TEMP/8312ce3c8401468d41afb74aff9f948e794e79fff358bfd35d5c83b07f8c8c07?apiKey=88fb82266c4d44249e007971612bb358&"
                                className="max-w-full aspect-square fill-[linear-gradient(270deg,#D71249_0%,#00538E_100%)] w-[110px]"
                              />
                              <div className="flex flex-col mt-9">
                                <div className="justify-center text-3xl font-semibold leading-10 text-rose-600">
                                  Tải{" "}
                                  <span className="font-bold text-rose-600">
                                    App MyVBI
                                  </span>{" "}
                                  để được hỗ trợ mọi lúc & mọi nơi
                                </div>
                                <div className="flex gap-5 justify-between mt-10">
                                  <div className="flex flex-col justify-center items-start">
                                    <img
                                      loading="lazy"
                                      srcSet="https://cdn.builder.io/api/v1/image/assets/TEMP/75c9ba0299f1f2eb2007d02d3984de547a35a1f21b9d551bb28e9d065ceb8b2e?apiKey=88fb82266c4d44249e007971612bb358&width=100 100w, https://cdn.builder.io/api/v1/image/assets/TEMP/75c9ba0299f1f2eb2007d02d3984de547a35a1f21b9d551bb28e9d065ceb8b2e?apiKey=88fb82266c4d44249e007971612bb358&width=200 200w, https://cdn.builder.io/api/v1/image/assets/TEMP/75c9ba0299f1f2eb2007d02d3984de547a35a1f21b9d551bb28e9d065ceb8b2e?apiKey=88fb82266c4d44249e007971612bb358&width=400 400w, https://cdn.builder.io/api/v1/image/assets/TEMP/75c9ba0299f1f2eb2007d02d3984de547a35a1f21b9d551bb28e9d065ceb8b2e?apiKey=88fb82266c4d44249e007971612bb358&width=800 800w, https://cdn.builder.io/api/v1/image/assets/TEMP/75c9ba0299f1f2eb2007d02d3984de547a35a1f21b9d551bb28e9d065ceb8b2e?apiKey=88fb82266c4d44249e007971612bb358&width=1200 1200w, https://cdn.builder.io/api/v1/image/assets/TEMP/75c9ba0299f1f2eb2007d02d3984de547a35a1f21b9d551bb28e9d065ceb8b2e?apiKey=88fb82266c4d44249e007971612bb358&width=1600 1600w, https://cdn.builder.io/api/v1/image/assets/TEMP/75c9ba0299f1f2eb2007d02d3984de547a35a1f21b9d551bb28e9d065ceb8b2e?apiKey=88fb82266c4d44249e007971612bb358&width=2000 2000w, https://cdn.builder.io/api/v1/image/assets/TEMP/75c9ba0299f1f2eb2007d02d3984de547a35a1f21b9d551bb28e9d065ceb8b2e?apiKey=88fb82266c4d44249e007971612bb358&"
                                      className="aspect-square w-[92px]"
                                    />
                                  </div>
                                  <div className="flex flex-col items-center">
                                    <img
                                      loading="lazy"
                                      src="https://cdn.builder.io/api/v1/image/assets/TEMP/f0d95276cd936b3e49966fc500fcf5d544f354049f7f49552b1eeda28d0ea8e6?apiKey=88fb82266c4d44249e007971612bb358&"
                                      className="aspect-[5] w-[196px]"
                                    />
                                    <img
                                      loading="lazy"
                                      src="https://cdn.builder.io/api/v1/image/assets/TEMP/79309348f568be4aad83a70bcac613b550435e414b537be892608c40866bbefd?apiKey=88fb82266c4d44249e007971612bb358&"
                                      className="mt-3 aspect-[5] w-[196px]"
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="flex flex-col ml-5 w-[67%] max-md:ml-0 max-md:w-full">
                          <div className="flex flex-col grow items-end px-20 pt-7 pb-16 max-md:px-5 max-md:max-w-full">
                            <div className="flex gap-5 justify-between items-start max-w-full w-[628px] max-md:flex-wrap max-md:mr-2.5">
                              <div className="flex gap-4 px-3 py-2.5 mt-1 bg-white rounded-xl">
                                <div className="flex justify-center items-center self-start p-1.5 w-9 h-9 bg-pink-50 rounded-lg">
                                  <img
                                    loading="lazy"
                                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/70191a0adc631680a4343514e9ebdc97c8867fbdd65ca92f1a544667d518434e?apiKey=88fb82266c4d44249e007971612bb358&"
                                    className="w-6 aspect-square"
                                  />
                                </div>
                                <div className="flex flex-col flex-1 justify-center">
                                  <div className="text-sm font-semibold leading-5 bg-clip-text bg-[linear-gradient(270deg,#D71249_0%,#00538E_100%)]">
                                    Khai báo online
                                  </div>
                                  <div className="mt-1 text-xs font-medium leading-4 text-slate-500">
                                    Chủ động khai báo và theo dõi tiến trình xử lý
                                    online
                                  </div>
                                </div>
                              </div>
                              <div className="flex gap-4 px-3 py-2.5 bg-white rounded-xl">
                                <div className="flex justify-center items-center self-start p-1.5 w-9 h-9 bg-pink-50 rounded-lg">
                                  <img
                                    loading="lazy"
                                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/5d55f5d2c0196b10916fbf7efddff34a2e9d5bc8455f19bd9bad226d56a33be5?apiKey=88fb82266c4d44249e007971612bb358&"
                                    className="w-6 aspect-square"
                                  />
                                </div>
                                <div className="flex flex-col flex-1 justify-center">
                                  <div className="text-sm font-semibold leading-5 bg-clip-text bg-[linear-gradient(270deg,#D71249_0%,#00538E_100%)]">
                                    Chính sách bảo hiểm
                                  </div>
                                  <div className="mt-1 text-xs font-medium leading-4 text-slate-500">
                                    Lưu trữ tất cả chính sách bảo hiểm
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="flex flex-col justify-center self-start px-3 py-2.5 mt-44 ml-3 max-w-full bg-white rounded-xl w-[249px] max-md:mt-10 max-md:ml-2.5">
                              <div className="flex flex-col justify-center">
                                <div className="flex flex-col justify-center">
                                  <div className="text-sm font-semibold leading-5 bg-clip-text bg-[linear-gradient(270deg,#D71249_0%,#00538E_100%)]">
                                    Tiện ích cuộc sống
                                  </div>
                                  <div className="mt-1 text-xs font-medium leading-4 text-slate-500">
                                    Các tiện ích giúp bạn trong cuộc sống
                                  </div>
                                </div>
                                <div className="flex gap-1.5 mt-4 text-xs font-semibold leading-3 text-slate-700">
                                  <img
                                    loading="lazy"
                                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/063f24643fee32f1359ad7eb66d0cef32e7c239d713f2db75953d21060612ffc?apiKey=88fb82266c4d44249e007971612bb358&"
                                    className="shrink-0 w-6 border border-white border-solid aspect-square"
                                  />
                                  <img
                                    loading="lazy"
                                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/2beb0d4342de7e1d294810750b59c6e34d71d3b5d1e1e84ab29e43b067414c7b?apiKey=88fb82266c4d44249e007971612bb358&"
                                    className="shrink-0 w-6 border border-white border-solid aspect-square"
                                  />
                                  <img
                                    loading="lazy"
                                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/1fd166af7e388d8ecf2fedc68f6f304eaafdf77feceeb903b46d258e24127669?apiKey=88fb82266c4d44249e007971612bb358&"
                                    className="shrink-0 w-6 border border-white border-solid aspect-square"
                                  />
                                  <img
                                    loading="lazy"
                                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/02824f52f659dcfca4f08d0ece35f16e25c8185b92c959d311c8acb4ca8465ab?apiKey=88fb82266c4d44249e007971612bb358&"
                                    className="shrink-0 w-6 border border-white border-solid aspect-square"
                                  />
                                  <div className="my-auto">+10 tiện ích khác</div>
                                </div>
                              </div>
                            </div>
                            <div className="flex gap-4 px-3 py-2.5 mt-14 bg-white rounded-xl max-md:mt-10 max-md:mr-2.5">
                              <div className="flex justify-center items-center self-start p-1.5 w-9 h-9 bg-pink-50 rounded-lg">
                                <img
                                  loading="lazy"
                                  src="https://cdn.builder.io/api/v1/image/assets/TEMP/d5096b0eaed6b3d3d587052f527d18e442f7d9e1296891a0cacb63e7ca410d7b?apiKey=88fb82266c4d44249e007971612bb358&"
                                  className="w-6 aspect-square"
                                />
                              </div>
                              <div className="flex flex-col justify-center">
                                <div className="text-sm font-semibold leading-5 bg-clip-text bg-[linear-gradient(270deg,#D71249_0%,#00538E_100%)]">
                                  Hỗ trợ online
                                </div>
                                <div className="mt-1 text-xs font-medium leading-4 text-slate-500">
                                  Hỗ trợ bạn mọi nơi và mọi lúc
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="w-full bg-zinc-800 min-h-[340px] max-md:max-w-full" />
          <div className="flex flex-col justify-center w-full max-md:max-w-full">
            <div className="flex flex-col justify-center p-16 w-full bg-zinc-800 max-md:px-5 max-md:max-w-full">
              <div className="mx-3 max-md:mr-2.5 max-md:max-w-full">
                <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                  <div className="flex flex-col w-[33%] max-md:ml-0 max-md:w-full">
                    <div className="flex flex-col max-md:mt-6">
                      <img
                        loading="lazy"
                        src="https://cdn.builder.io/api/v1/image/assets/TEMP/cada37d4ca64cf9cf0ea342a127da0330b738efaab506d249388bb2bb99998fd?apiKey=88fb82266c4d44249e007971612bb358&"
                        className="max-w-full aspect-[5.26] w-[238px]"
                      />
                      <div className="flex flex-col mt-9">
                        <div className="flex flex-col text-base leading-6 text-white">
                          <div className="flex gap-1.5">
                            <img
                              loading="lazy"
                              src="https://cdn.builder.io/api/v1/image/assets/TEMP/71815355588f59773090f476efa8787560b81df1d9ff912a482b29d7d77abf67?apiKey=88fb82266c4d44249e007971612bb358&"
                              className="shrink-0 self-start w-6 aspect-square"
                            />
                            <div className="flex-1">
                              Tầng 10 -11, tòa nhà 126 Đội Cấn Quận Ba Đình,
                              <br />
                              Hà Nội
                            </div>
                          </div>
                          <div className="flex gap-1.5 mt-5">
                            <img
                              loading="lazy"
                              src="https://cdn.builder.io/api/v1/image/assets/TEMP/71815355588f59773090f476efa8787560b81df1d9ff912a482b29d7d77abf67?apiKey=88fb82266c4d44249e007971612bb358&"
                              className="shrink-0 self-start w-6 aspect-square"
                            />
                            <div className="flex-1">
                              Số 243A Đê La Thành, Láng Thượng, Đống Đa, <br />
                              Hà Nội
                            </div>
                          </div>
                          <div className="flex gap-1.5 mt-5 font-medium whitespace-nowrap leading-[150%] text-neutral-200">
                            <img
                              loading="lazy"
                              src="https://cdn.builder.io/api/v1/image/assets/TEMP/6243a8f829c90ae049a1aa27e55f82925062695880a5c6c79c773dd7c9ed7864?apiKey=88fb82266c4d44249e007971612bb358&"
                              className="shrink-0 w-6 aspect-square"
                            />
                            <div className="flex-1">1900.1566</div>
                          </div>
                        </div>
                        <div className="mt-14 text-base leading-6 text-white max-md:mt-10">
                          Tải xuống ứng dụng của VBI
                        </div>
                        <div className="mt-5">
                          <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                            <div className="flex flex-col w-[30%] max-md:ml-0 max-md:w-full">
                              <img
                                loading="lazy"
                                srcSet="https://cdn.builder.io/api/v1/image/assets/TEMP/cc276ad3c67c08bb791d6aaea426fbeb2c8d07bea5ed9d3e40e79fd933f48e32?apiKey=88fb82266c4d44249e007971612bb358&width=100 100w, https://cdn.builder.io/api/v1/image/assets/TEMP/cc276ad3c67c08bb791d6aaea426fbeb2c8d07bea5ed9d3e40e79fd933f48e32?apiKey=88fb82266c4d44249e007971612bb358&width=200 200w, https://cdn.builder.io/api/v1/image/assets/TEMP/cc276ad3c67c08bb791d6aaea426fbeb2c8d07bea5ed9d3e40e79fd933f48e32?apiKey=88fb82266c4d44249e007971612bb358&width=400 400w, https://cdn.builder.io/api/v1/image/assets/TEMP/cc276ad3c67c08bb791d6aaea426fbeb2c8d07bea5ed9d3e40e79fd933f48e32?apiKey=88fb82266c4d44249e007971612bb358&width=800 800w, https://cdn.builder.io/api/v1/image/assets/TEMP/cc276ad3c67c08bb791d6aaea426fbeb2c8d07bea5ed9d3e40e79fd933f48e32?apiKey=88fb82266c4d44249e007971612bb358&width=1200 1200w, https://cdn.builder.io/api/v1/image/assets/TEMP/cc276ad3c67c08bb791d6aaea426fbeb2c8d07bea5ed9d3e40e79fd933f48e32?apiKey=88fb82266c4d44249e007971612bb358&width=1600 1600w, https://cdn.builder.io/api/v1/image/assets/TEMP/cc276ad3c67c08bb791d6aaea426fbeb2c8d07bea5ed9d3e40e79fd933f48e32?apiKey=88fb82266c4d44249e007971612bb358&width=2000 2000w, https://cdn.builder.io/api/v1/image/assets/TEMP/cc276ad3c67c08bb791d6aaea426fbeb2c8d07bea5ed9d3e40e79fd933f48e32?apiKey=88fb82266c4d44249e007971612bb358&"
                                className="shrink-0 max-w-full aspect-square w-[116px] max-md:mt-8"
                              />
                            </div>
                            <div className="flex flex-col ml-5 w-[70%] max-md:ml-0 max-md:w-full">
                              <div className="flex flex-col self-stretch my-auto max-md:mt-9">
                                <div className="flex flex-col justify-center items-start px-6 py-2.5 bg-gray-100 rounded-[60px] max-md:px-5">
                                  <img
                                    loading="lazy"
                                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/f5d733bbaf19e566699b4bc0cc4adb93bf8135b434b91a6c594b6f2220099958?apiKey=88fb82266c4d44249e007971612bb358&"
                                    className="max-w-full aspect-[4.17] w-[119px]"
                                  />
                                </div>
                                <div className="flex flex-col justify-center items-start px-7 py-3 mt-3 bg-gray-100 rounded-[60px] max-md:px-5">
                                  <img
                                    loading="lazy"
                                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/ce409a873e2bd1a0d9412f0fe576e83ee638b682f5c48db2d3f8ebdb48c3ae48?apiKey=88fb82266c4d44249e007971612bb358&"
                                    className="max-w-full aspect-[4] w-[105px]"
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="self-start mt-14 text-base leading-6 text-white text-opacity-80 max-md:mt-10">
                          Theo dõi VBI qua mạng xã hội
                        </div>
                        <div className="flex gap-4 self-start mt-4 max-md:pr-5">
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/a74f46ee4aafbaf8884a349884564e1931682ba5b59c3a5ba3ac39588c7e77bd?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="shrink-0 w-6 aspect-square"
                          />
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/981f31a3b29511ac8bc300917d7e1ba070c8d486de70639b36e95efa5bdc7777?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="shrink-0 w-6 aspect-square"
                          />
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/ec9cf6b23e08cc9512cab2bc5568f482d6c5712ff2c9f6d0f2e754054ed72a0a?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="shrink-0 w-6 aspect-square"
                          />
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/7c4f62f0927ea99c01d8782e0771cb84896bade849723fdfc5b5b0a2fff37729?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="shrink-0 w-6 aspect-square"
                          />
                          <img
                            loading="lazy"
                            src="https://cdn.builder.io/api/v1/image/assets/TEMP/c04379e0047125cd04157cc687bb898daa78d4b7809835f219e526e44e000d86?apiKey=88fb82266c4d44249e007971612bb358&"
                            className="shrink-0 w-6 aspect-square"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="flex flex-col ml-5 w-[67%] max-md:ml-0 max-md:w-full">
                    <div className="flex flex-col grow max-md:mt-6 max-md:max-w-full">
                      <div className="max-md:pr-5 max-md:max-w-full">
                        <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                          <div className="flex flex-col w-[33%] max-md:ml-0 max-md:w-full">
                            <div className="flex flex-col max-md:mt-6">
                              <div className="text-xl font-medium leading-7 text-white">
                                Công ty
                              </div>
                              <div className="flex flex-col mt-3 text-base leading-6 text-white text-opacity-80">
                                <div>Về chúng tôi</div>
                                <div className="mt-4">Ban giám đốc</div>
                                <div className="mt-4">Cơ hội nghề nghiệp</div>
                                <div className="mt-4">Quan hệ đối tác</div>
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-col ml-5 w-[33%] max-md:ml-0 max-md:w-full">
                            <div className="flex flex-col justify-center max-md:mt-6">
                              <div className="flex flex-col">
                                <div className="text-xl font-medium leading-7 text-white">
                                  Hỗ trợ
                                </div>
                                <div className="flex flex-col mt-3 text-base leading-6 text-white text-opacity-80">
                                  <div>Dịch vụ khách hàng</div>
                                  <div className="mt-4">Yêu cầu bồi thường</div>
                                  <div className="mt-4">Câu hỏi thường gặp</div>
                                  <div className="mt-4">Quy tắc bảo hiểm</div>
                                  <div className="mt-4 leading-6">
                                    Thoả thuận sử dụng website & ứng dụng
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-col ml-5 w-[33%] max-md:ml-0 max-md:w-full">
                            <div className="flex flex-col grow max-md:mt-6">
                              <div className="text-xl font-medium leading-7 text-white">
                                Tiện ích
                              </div>
                              <div className="flex flex-col mt-3 text-base leading-6 text-white text-opacity-80">
                                <div>Mã thanh toán</div>
                                <div className="mt-4">Garage gần bạn</div>
                                <div className="mt-4">Bệnh viện bảo lãnh</div>
                                <div className="mt-4">Liên hệ CSGT</div>
                                <div className="mt-4">Trung tâm đăng kiểm</div>
                                <div className="mt-4">Trung tâm cứu hộ</div>
                                <div className="mt-4">VBI Gần bạn</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="flex flex-col mt-10 max-md:max-w-full">
                        <div className="flex flex-col max-md:max-w-full">
                          <div className="text-xl font-medium leading-7 text-white max-md:max-w-full">
                            Sản phẩm bảo hiểm
                          </div>
                          <div className="mt-4 text-base font-medium leading-6 text-white max-md:max-w-full">
                            Sản phẩm cho bạn và gia đình
                          </div>
                          <div className="mt-4 max-md:max-w-full">
                            <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                              <div className="flex flex-col w-3/12 max-md:ml-0 max-md:w-full">
                                <div className="flex flex-col grow text-base leading-6 text-white text-opacity-80 max-md:mt-6">
                                  <div>Bảo hiểm Sức khỏe</div>
                                  <div className="mt-4">Bảo hiểm ung thư</div>
                                  <div className="mt-4">Bảo hiểm ung thư vú</div>
                                  <div className="mt-4">
                                    Bảo hiểm vì cộng đồng
                                  </div>
                                </div>
                              </div>
                              <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                                <div className="flex flex-col text-base leading-6 text-white text-opacity-80 max-md:mt-6">
                                  <div className="leading-6">
                                    Bảo hiểm Tai nạn <br />
                                    con người
                                  </div>
                                  <div className="mt-4">
                                    Bảo hiểm sốt xuất huyết
                                  </div>
                                  <div className="mt-4">
                                    Bảo hiểm vật chất ô tô
                                  </div>
                                </div>
                              </div>
                              <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                                <div className="flex flex-col text-base leading-6 text-white text-opacity-80 max-md:mt-6">
                                  <div>Bảo hiểm nhà tư nhân</div>
                                  <div className="mt-4">
                                    Bảo hiểm du lịch nội địa
                                  </div>
                                  <div className="mt-4">
                                    Bảo hiểm du lịch quốc tế
                                  </div>
                                </div>
                              </div>
                              <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                                <div className="flex flex-col text-base leading-6 text-white text-opacity-80 max-md:mt-6">
                                  <div>Bảo hiểm vật chất ô tô</div>
                                  <div className="mt-4">Bảo hiểm TNDS ô tô</div>
                                  <div className="mt-4">Bảo hiểm TNDS xe máy</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="flex flex-col mt-10 max-md:mt-10 max-md:max-w-full">
                          <div className="text-base font-medium leading-6 text-white max-md:max-w-full">
                            Sản phẩm doanh nghiệp
                          </div>
                          <div className="mt-4 max-md:max-w-full">
                            <div className="flex gap-5 max-md:flex-col max-md:gap-0">
                              <div className="flex flex-col w-3/12 max-md:ml-0 max-md:w-full">
                                <div className="flex flex-col grow text-base leading-6 text-white text-opacity-80 max-md:mt-6">
                                  <div className="leading-[150%]">
                                    Bảo hiểm xe cơ giới
                                  </div>
                                  <div className="mt-4">
                                    Trách nhiệm dân sự bắt buộc
                                  </div>
                                  <div className="mt-4">
                                    Tai nạn lái phụ xe và người ngồi trên xe
                                  </div>
                                  <div className="mt-4">
                                    Bảo hiểm TNDS tự nguyện của chủ xe
                                  </div>
                                  <div className="mt-4 leading-[150%]">
                                    Bảo hiểm vật chất xe
                                  </div>
                                  <div className="mt-4">
                                    TNDS với hàng hóa vận chuyển trên xe
                                  </div>
                                </div>
                              </div>
                              <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                                <div className="flex flex-col grow text-base leading-6 text-white text-opacity-80 max-md:mt-6">
                                  <div className="leading-[150%]">
                                    Bảo hiểm mọi rủi ro tài sản
                                  </div>
                                  <div className="mt-4">
                                    Hỏa hoạn và các rủi ro đặc biệt
                                  </div>
                                  <div className="mt-4">
                                    Bảo hiểm gián đoạn kinh doanh
                                  </div>
                                  <div className="mt-4">
                                    Bảo hiểm mọi rủi ro xây dựng và lắp đặt
                                  </div>
                                  <div className="mt-4 leading-[150%]">
                                    Bảo hiểm máy móc thiết bị
                                  </div>
                                  <div className="mt-4">
                                    Công trình xây dựng dân dụng
                                  </div>
                                </div>
                              </div>
                              <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                                <div className="flex flex-col text-base leading-6 text-white text-opacity-80 max-md:mt-6">
                                  <div>Vận chuyển hàng hóa xuất nhập khẩu</div>
                                  <div className="mt-4 leading-[150%]">
                                    Vận chuyển nội địa
                                  </div>
                                  <div className="mt-4 leading-[150%]">
                                    Bảo hiểm thân tàu
                                  </div>
                                  <div className="mt-4">
                                    Bảo hiểm rủi ro người đóng tàu
                                  </div>
                                  <div className="mt-4">
                                    Bảo hiểm sức khoẻ <br />
                                    An Phát
                                  </div>
                                  <div className="mt-4 leading-[150%]">
                                    Bảo hiểm Thành công
                                  </div>
                                </div>
                              </div>
                              <div className="flex flex-col ml-5 w-3/12 max-md:ml-0 max-md:w-full">
                                <div className="flex flex-col grow text-base leading-6 text-white text-opacity-80 max-md:mt-6">
                                  <div>Bảo hiểm xây dựng lắp đặt</div>
                                  <div className="mt-4">
                                    Bảo hiểm sụt giảm sản lượng
                                  </div>
                                  <div className="mt-4 leading-[150%]">
                                    Bảo hiểm mọi rủi ro tài sản
                                  </div>
                                  <div className="mt-4 leading-[150%]">
                                    Trách nhiệm sản phẩm
                                  </div>
                                  <div className="mt-4">
                                    Trách nhiệm bên thứ 3/công cộng
                                  </div>
                                  <div className="mt-4">
                                    Trách nhiệm <br />
                                    nghề nghiệp
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
    }

    export default MyComponent;