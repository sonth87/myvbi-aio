import { DsButton, DsIcon, DsInput } from "mvi-ds-ui";
import { useCallback, useEffect } from "react";
import { useMutation } from "@tanstack/react-query";
import { object, string } from "yup";
import { useFormik } from "formik";
import { LoginType } from "../../type/login";
import { getMenuList, getUserInfoByForm, signin } from "../../apis/user";
import { useAuth } from "../../context/AuthContext";
import { ROUTES } from "../../constants/path";
import { useNavigate } from "react-router-dom";
import { filterMenu } from "../../constants/genaralFunction";
import { MENU_LIST, SYSTEMCODE } from "../../constants/const";
import { HmacKey256 } from "../../utils/hmackey";
import { functionGetMenuList } from "../../utils/getMenuList";

const Login = () => {
  const { notify, setIsLoading, setIsMenu, setIsAuth, setUser, setLogin } =
    useAuth();
  const navigate = useNavigate();

  const { mutateAsync } = useMutation({
    mutationFn: () => signin(),
  });

  const { mutateAsync: loginForm, isPending: isLoginFormPendding } =
    useMutation({
      mutationFn: getUserInfoByForm,
    });

  const signIn = async () => {
    try {
      setLogin({
        mcr: true,
        form: false,
      });
      const data = await mutateAsync();
      const login_url = data?.data?.login_url;
      if (login_url) window.location.href = login_url;
    } catch (error) {
      console.error("Error during sign-in:", error);
    }
  };

  const LoginValidation = object({
    email: string()
      // .email("Vui lòng nhập đúng định dạng email")
      .required("Vui lòng nhập email"),
    password: string().required("Vui lòng nhập mật khẩu"),
  });

  const form = useFormik<LoginType>({
    initialValues: {
      email: "",
      password: "",
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      setLogin({
        mcr: false,
        form: true,
      });
      setIsLoading(true);
      const rs = await loginForm(values);
      if (rs?.data?.Success) {
        setIsAuth(true);
        setIsLoading(false);
        setUser({
          email: rs?.data?.data?.EMAIL,
          hash: rs?.data?.data?.HASH,
          loai: rs?.data?.data?.LOAI,
          name: rs?.data?.data?.NAME,
          phong: rs?.data?.data?.PHONG,
          tai_khoan: rs?.data?.data?.TAI_KHOAN,
        });
        sessionStorage.setItem("hash", rs?.data?.data?.HASH);
        sessionStorage.setItem("email", rs?.data?.data?.EMAIL);
        const rsMenuList = await functionGetMenuList(rs?.data?.data?.EMAIL);
        if (rsMenuList.Data.Data[0].Items) {
          const menuList = rsMenuList.Data.Data[0].Items.map((item: any) => ({
            label: item.MenuName,
            icon: item.MenuIcon || "icon-bxs-dashboard",
            path: item.MenuUrl,
            group: item.MenuType,
            authorized: item.Authorize,
            childs:
              item.Items.length > 0
                ? item.Items.map((itemChild: any) => ({
                    label: itemChild.MenuName,
                    path: itemChild.MenuUrl,
                    childs: [],
                    authorized: item.Authorize,
                  }))
                : null,
          })).sort((a: any, b: any) => a.sort - b.sort);

          const transformMenuData = (menuArray: any, group = "1") => {
            return menuArray
              .map((item: any) => {
                const transformedItem: any = {
                  label: item.MenuName,
                  icon: item.MenuIcon || "icon-bxs-dashboard",
                  path: item.MenuUrl,
                  group: item.MenuType,
                  authorized: item.Authorize,
                  sort: item.MenuSort,
                };

                // Nếu phần tử có Items, chuyển đổi chúng thành children
                if (item.Items && item.Items.length > 0) {
                  transformedItem.childs = transformMenuData(item.Items, group);
                }

                return transformedItem;
              })
              .sort((a: any, b: any) => a.sort - b.sort);
          };

          // Chuyển đổi dữ liệu
          const transformedData = transformMenuData(
            rsMenuList.Data.Data[0].Items
          );

          window.sessionStorage.setItem(
            MENU_LIST,
            JSON.stringify(transformedData)
          );
          setIsMenu(menuList);
        }

        notify?.({ type: "success", content: rs?.data?.mess });
      } else {
        notify?.({ type: "error", content: rs?.data?.mess });
      }
    },
    validationSchema: LoginValidation,
  });

  const getFieldErr = useCallback(
    (field: keyof LoginType) => {
      const fErr = form.errors;
      return form.touched?.[field] && fErr?.[field] ? fErr?.[field] : null;
    },
    [form.errors, form.touched]
  );

  return (
    <div>
      <section className="bg-gray-50 dark:bg-gray-900 relative overflow-hidden">
        <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
          <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
              <a
                href="#"
                className="flex justify-center items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
              >
                <img
                  className="w-16 h-16 mr-2"
                  src="/logo-vbi.png"
                  alt="logo"
                />
              </a>
              <h1 className="flex justify-center text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                Đăng nhập với tài khoản
              </h1>
              <form onSubmit={form.handleSubmit}>
                <div className="grid grid-cols-1 gap-4">
                  <DsInput
                    label="Email"
                    name="email"
                    size="large"
                    type="text"
                    onChange={(e) => form.setFieldValue("email", e)}
                    state={getFieldErr("email") ? "error" : "normal"}
                    message={getFieldErr("email")}
                    // disabled={isLoading}
                  />
                  <DsInput
                    label="Mật khẩu"
                    name="password"
                    size="large"
                    type="password"
                    onChange={(e) => form.setFieldValue("password", e)}
                    state={getFieldErr("password") ? "error" : "normal"}
                    message={getFieldErr("password")}
                    // disabled={isLoading}
                  />

                  <DsButton
                    label={isLoginFormPendding ? "Loading ..." : "Đăng nhập"}
                    buttonType="submit"
                    className="!w-full"
                    size="large"
                  />
                </div>
              </form>
              <DsButton
                // disabled={isPending}
                // isLoading={isPending}
                size="large"
                className="!w-full"
                color="gray"
                type="outline-gray"
                label="Đăng nhập với Office 365"
                prefixIcon={
                  <DsIcon
                    className="icon-bxl-microsoft-teams mr-2"
                    name="icon-bx-at"
                  />
                }
                onClick={() => signIn()}
              />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Login;
