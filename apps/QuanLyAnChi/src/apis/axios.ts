import axios, { HttpStatusCode } from "axios";

const baseUrl = process.env.REACT_APP_BE_AN_CHI_BASEURL;

console.log("baseUrl AC ---->", baseUrl);

const axiosInstance = axios.create({
  baseURL: baseUrl,
  timeout: 30000,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  },
});

axiosInstance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    if (response.status === HttpStatusCode.Ok) return response?.data;
    else return response?.data; // response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axiosInstance;

