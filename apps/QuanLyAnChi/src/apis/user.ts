import axios from "axios";
import { GetListMenuType, GetUserType } from "../type/login";
import { ENV, TOKEN_KEY } from "../constants/const";
import axiosInstance from "./axios";

export const signin = async () => {
  const data = await axios.get(ENV.be + "auth/signin");

  return data?.data;
};

export const getUserInfo = async (reqBody: GetUserType) => {
  const response = await axiosInstance.post("/auth/an-chi-login", reqBody);
  return response?.data;
};

export const getUserInfoWith365 = async () => {
  const tokenSession = window.sessionStorage.getItem(TOKEN_KEY);
  const response = await axiosInstance.get("/auth/user/profile", {
    headers: {
      Authorization: `Bearer ${tokenSession}`,
    },
  });
  return response?.data;
};

export const getUserInfoByForm = async (form?: {
  email: string;
  password: string;
}) => {
  const response = await axiosInstance.post("/auth/an-chi-login", {
    // P_EMAIL: "",
    // P_TAI_KHOAN: form?.email,
    // P_MAT_KHAU: form?.password,
    // P_HASH: "",

    P_EMAIL: form?.email,
    P_TAI_KHOAN: "",
    P_MAT_KHAU: "",
    P_HASH: "",
  });
  return response?.data;
};

export const getMenuList = async (reqBody: GetListMenuType) => {
  const response = await axiosInstance.post("auth/get-menu-list", reqBody);
  return response?.data;
};
