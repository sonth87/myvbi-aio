import axiosInstance from "./axios";


export const getDanhSachDieuChuyenAnChi = async (reqBody: any) => {
  const response = await axiosInstance.post(
    "/api/get-danh-sach-dieu-chuyen-ac",
    reqBody
  );
  return response?.data;
};

export const createDieuChuyenAnChi = async (reqBody: any) => {
  const response = await axiosInstance.post("/api/create-dieu-chuyen-an-chi", reqBody);
  return response;
};