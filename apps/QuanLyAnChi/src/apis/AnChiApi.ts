import { ValuesSearchProps } from "../type/common";
import axiosInstance from "./axios";

export const getThamSo = async (reqBody: any) => {
  const response = await axiosInstance.post("/api/an-chi-danh-muc", reqBody);
  return response?.data;
};

export const getDSDeNghiAnChi = async (reqBody: ValuesSearchProps) => {
  const response = await axiosInstance.post(
    "/api/get-danh-sach-de-nghi-ac",
    reqBody
  );
  return response?.data;
};

export const getDSAnChi = async (reqBody: any) => {
  const response = await axiosInstance.post("/api/get-danh-sach-ac", reqBody);
  return response?.data?.data?.data;
};

export const getAnChiChiTiet = async (reqBody: any) => {
  const response = await axiosInstance.post("/api/get-ac-chi-tiet", reqBody);
  return response;
};

export const createDeNghiNhap = async (reqBody: any) => {
  const response = await axiosInstance.post("/api/tao-de-nghi-nhap", reqBody);
  return response;
};

export const delDeNghiNhap = async (reqBody: any) => {
  const response = await axiosInstance.post("/api/huy-de-nghi", reqBody);
  return response;
};

export const duyetAnChi = async (reqBody: any) => {
  const response = await axiosInstance.post("/api/tao-de-nghi-nhap", reqBody);
  return response;
};

export const acDeNghiXuatHoan = async (reqBody: any) => {
  const response = await axiosInstance.post(
    "/api/ac-de-nghi-xuat-hoan",
    reqBody
  );
  return response;
};

export const getDsMatHuy = async (reqBody: any) => {
  const response = await axiosInstance.post("/api/get-danh-sach-mat-huy", reqBody);
  return response?.data?.data?.data;
};