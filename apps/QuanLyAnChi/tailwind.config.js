const defaultSetting = require("mvi-ds-ui/tailwind.config.js");

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],

  ...defaultSetting,
};
