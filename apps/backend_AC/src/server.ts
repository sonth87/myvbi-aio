import express from "express";
import * as path from "path";
import cors from "cors";
import bodyParser from "body-parser";
import { router as AnChiRouter } from "./routers";
import session from "express-session";

import * as dotenv from "dotenv";
import { apiLimiter } from "./middlewares/limit";
import helmet from "helmet";
import { authRouter } from "./routers/auth";
import { mySession } from "./constants/user";
import { notFound } from "./middlewares/errorHandler";

dotenv.config({ path: path.join(__dirname, "..", "/.env") });

const PORT = process.env.BE_PORT || 2527;

const app = express();

// user session
app.use(session(mySession));

// security HTTP headers
app.use(helmet());

// parse urlencoded request body
app.use(bodyParser.urlencoded({ extended: true }));

// parse json request body
app.use(bodyParser.json());

// enable cors
app.use(cors());

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

// routes
app.use("/api", apiLimiter, AnChiRouter);
app.use("/auth", apiLimiter, authRouter);

app.use(notFound);

export default app;
export { PORT };
