import { z } from "zod";
import { COMPS, PAGES, RequestByRule } from "./pagesComponents";

export const configRule = {
  type: z.enum(RequestByRule),
  comps: z
    .union([z.nativeEnum(PAGES), z.array(z.nativeEnum(COMPS))])
    .optional(),
};
