import axios from "axios";
import * as path from "path";
import * as dotenv from "dotenv";
dotenv.config({ path: path.join(__dirname, "../../.env") });

const baseURL = process.env.OPENAPI;
const timeout = 30000;
const NODE_API_KEY = process.env.OPENAPI_KEY_AC;
export const axiosInstance = axios.create({ baseURL, timeout });

axiosInstance.interceptors.request.use(
  (config: any) => {
    return {
      ...config,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        "x-api-key": NODE_API_KEY,
      },
    };
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  function (response: any) {
    return response;
  },
  function (error: any) {
    return Promise.reject(error);
  }
);
