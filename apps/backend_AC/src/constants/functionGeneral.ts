export const convertDateTime = (dateTimeString) => {
  // Lấy các thành phần từ chuỗi
  const year = dateTimeString?.slice(0, 4);
  const month = dateTimeString?.slice(4, 6);
  const day = dateTimeString?.slice(6, 8);

  // Kết hợp các thành phần lại với nhau
  const formattedDateTime = `${year}-${month}-${day}`;

  return formattedDateTime;
};
