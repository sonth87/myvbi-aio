export const mySession = {
  secret: "sonth",
  cookie: {
    maxAge: 1000 * 60 * 60 * 24, // 1 week
  },
  resave: true,
  saveUninitialized: true,
  authCodeUrlRequest: {},
  isAuthenticated: false,
  token: "",
  user_email: "",
  accessToken: "",
  refreshToken: "",
  idToken: "",
  account: {},
};

export type SessionData = {
  token: string
  isAuthenticated: boolean;
  user_email: any;
  accessToken: string;
  refreshToken: string;
  idToken: string;
  account: any;
  isAdmin: boolean;
};

// declare module "express-session" {
//   export interface SessionData {
//     pkceCodes: any;
//     authCodeUrlRequest: any;
//     authCodeRequest: any;
//     csrfToken: any;
//     isAuthenticated: boolean;
//     user_email: any;
//     accessToken: string;
//     idToken: string;
//     account: any;
//   }
// }

export const HKEY = "a375936a876de769923c507ce05f9e23";