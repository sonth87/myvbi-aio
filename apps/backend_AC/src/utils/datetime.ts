export const getCurrentDateTime = () => {
  const d = getDateObj();
  return `${d.year}-${d.month}-${d.day} ${d.hour}:${d.minutes}:${d.second}`;
};

export const getDateObj = () => {
  const _date = new Date();
  const year = _date.getFullYear();
  const month = _date.getMonth() + 1;
  const day = _date.getDate();
  const hour = _date.getHours();
  const minutes = _date.getMinutes();
  const second = _date.getSeconds();

  return {
    year,
    month: month < 10 ? `0${month}` : month,
    day,
    hour: hour < 10 ? `0${hour}` : hour,
    minutes: minutes < 10 ? `0${minutes}` : month,
    second: second < 10 ? `0${second}` : second,
  };
};
