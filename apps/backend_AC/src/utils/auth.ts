import { hacc } from "../constants/permission";
import { HKEY } from "../constants/user";
const CryptoJS = require("crypto-js");
const { createHash } = require("crypto");

export const keygen = (ha: string) => {
  const key = CryptoJS.enc.Utf8.parse(ha + HKEY);
  return key.toString();
};

export const deCriptWithHA = (txt: string, ha: string) => {
  if (txt && ha) {
    return CryptoJS.AES.decrypt(txt, keygen(ha)).toString(CryptoJS.enc.Utf8);
  }
  return;
};

export const criptWithHA = (txt: string, ha: string) => {
  if (txt && ha) {
    return CryptoJS.AES.decrypt(txt, ha + HKEY).toString();
  }
  return;
};

export const verify = (txt: string) => {
  return createHash("md5").update(txt).digest("hex") === hacc;
};
