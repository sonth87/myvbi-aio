import { Router } from "express";
import { DieuChuyenAcController } from "../controllers";

export const router: Router = Router();

router.post("/get-danh-sach-dieu-chuyen-ac", DieuChuyenAcController.getDSDieuChuyenAnChi);
router.post("/create-dieu-chuyen-an-chi", DieuChuyenAcController.createDieuChuyenAnChi);

