import { Router } from "express";
import { router as AnChiRouter } from "./AnChiRouter";
import {router as DieuChuyenAcRouter} from "./DieuChuyenAcRouter";

export const router: Router = Router();

router.use(AnChiRouter);
router.use(DieuChuyenAcRouter);