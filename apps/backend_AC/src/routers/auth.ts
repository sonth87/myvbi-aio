import { Router } from "express";
import { AuthController } from "../controllers";
import { auth } from "../middlewares/auth";

export const authRouter = Router();

authRouter
  .route("/signin")
  .get(AuthController.login)
  .post(AuthController.doLogin);
authRouter.get("/signout", AuthController.logout);

authRouter.post("/auth-redirect", AuthController.auth_redirect);

authRouter.post("/auth-verify", AuthController.auth_verify);

authRouter.post("/an-chi-login", AuthController.anChiNewLogin);

authRouter.get("/user/profile", auth(), AuthController.getUserProfile);

authRouter.post("/get-menu-list", AuthController.getMenuList);
