import { Router } from "express";
import { AnChiController } from "../controllers";

export const router: Router = Router();

router.post("/an-chi-danh-muc", AnChiController.getThamSo);
router.post("/get-danh-sach-de-nghi-ac", AnChiController.getDSDeNghiAnChi);
router.post("/get-danh-sach-ac", AnChiController.getDSAnChi);
router.post("/get-ac-chi-tiet", AnChiController.getAnChiChiTiet);
router.post("/tao-de-nghi-nhap", AnChiController.createDeNghiNhap);
router.post("/huy-de-nghi", AnChiController.huyDeNghiNhap);
router.post("/ac-de-nghi-xuat-hoan", AnChiController.deNghiXuatHoan);
router.post("/get-danh-sach-mat-huy", AnChiController.getDsMatHuy);
