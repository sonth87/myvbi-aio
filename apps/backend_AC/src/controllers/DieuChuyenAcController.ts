import { Request, Response } from "express";
import { response } from "../utils/response";
import { axiosInstance } from "../constants/axios";

export const getDSDieuChuyenAnChi = async (req: Request, res: Response) => {
    try {
      const bodyValue = req.body;
      const result = await axiosInstance.post("/sapi/ac-phan-bo-tim", bodyValue);
      response({ res, data: result?.data });
    } catch (err: any) {
      return res.status(500).json({ msg: err.message });
    }
  };

  export const createDieuChuyenAnChi = async (req: Request, res: Response) => {
    try {
      const bodyValue = req.body;
      const result = await axiosInstance.post(
        "/sapi/ac-phan-bo-action",
        bodyValue
      );
      response({ res, data: result?.data });
    } catch (err: any) {
      return res.status(500).json({ msg: err.message });
    }
  };