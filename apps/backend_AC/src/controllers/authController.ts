import axios, { HttpStatusCode } from "axios";
import { NextFunction, Request, Response } from "express";
import { response } from "../utils/response";
import { CRequest, UserRedirectType, UserVerifyType } from "../types/user";
// import { userService } from "../services";
import { deCriptWithHA, verify } from "../utils/auth";
import { HKEY } from "../constants/user";
import jwt from "jsonwebtoken";
import { axiosInstance } from "../constants/axios";

export const login = async (req: Request, res: Response) => {
  try {
    const url = process.env.OPENAPI + "api/authentication/request-login-token";
    const rs = await axios.post(
      url,
      { service: "aad" },
      {
        headers: {
          "x-api-key": process.env.OPENAPI_KEY_AC,
        },
      }
    );
    response({ res, data: rs?.data?.data });
  } catch (err) {
    console.log(err);
    response({ res, message: err });
  }
};

export const getUserProfile = async (req: CRequest, res: Response) => {
  if (req.session.isAuthenticated) {
    const userInfo = {
      email: req.session.user_email,
      accessToken: req.session.accessToken,
      refreshToken: req.session.refreshToken,
      isAuthenticated: req.session.isAuthenticated,
    };

    response({ res, data: userInfo });
  } else {
    response({
      status: HttpStatusCode.Unauthorized,
      res,
      message: "Chưa đăng nhập",
    });
  }
};

export const anChiNewLogin = async (req: Request, res: Response) => {
  try {
    const bodyValue = req.body;
    const result = await axiosInstance.post("/sapi/ac-new-login", bodyValue);
    response({ res, data: result?.data });
  } catch (err) {
    console.log(err);
    response({ res, message: err });
  }
};

export const auth_verify = async (req: CRequest, res: Response) => {
  const data: UserVerifyType = req.body;

  if (!data?.user_data?.email) {
    res.json({ success: false });
    return false;
  }

  // verify user
  const verify_state = {
    success: true,
    data: {
      USER_OPENID: data.user_data.email || "123",
      SERVICE_ACCESS_TOKEN: "",
    },
  };

  res.json(verify_state);
};

export const auth_redirect = async (req: CRequest, res: Response) => {
  const data: UserRedirectType = req.body;
  const accessToken = jwt.sign(
    { username: data.user_id, token: data?.user_access_token },
    HKEY,
    {
      expiresIn: "1d",
    }
  );

  req.session.accessToken = accessToken;
  req.session.token = accessToken;
  req.session.refreshToken = data?.user_refresh_token;
  req.session.user_email = data?.user_id;
  if (data?.user_access_token) req.session.isAuthenticated = true;

  res.redirect(
    process.env.REACT_APP_FE_BASEURL + "redirect?email=" + data.user_id
  );
};

export const doLogin = async (
  req: CRequest,
  res: Response,
  next: NextFunction
) => {
  const u = req.body?.user;
  const p = req.body?.password;
  const o = req.get("origin");
  const pair = [deCriptWithHA(u, o), deCriptWithHA(p, o)];

  const test = verify(pair.join());
  if (test) {
    const accessToken = jwt.sign({ username: pair[0], v: p }, HKEY);

    req.session.accessToken = accessToken;
    req.session.token = accessToken;
    req.session.refreshToken = "";
    req.session.isAuthenticated = true;
    req.session.isAdmin = true;

    response({ res, data: { username: pair[0], c: accessToken } });
  } else {
    response({
      status: HttpStatusCode.Unauthorized,
      res,
      message: "Thông tin không chính xác",
    });
  }
};

export const logout = async (
  req: CRequest,
  res: Response,
  next: NextFunction
) => {
  const url = process.env.OPENAPI + "api/authentication/logout";

  await axios.get(url);
  // clear data
  req.session = {
    accessToken: "",
    account: "",
    idToken: "",
    isAuthenticated: false,
    refreshToken: "",
    token: "",
    user_email: "",
    isAdmin: false,
  };

  response({ res, status: HttpStatusCode.Ok });
};

export const getMenuList = async (req: Request, res: Response) => {
  try {
    const bodyReq = req.body;
    const rs = await axios.post(
      process.env.AUTHOZIED_ADMIN + "/api/platforms/authorize/get-list",
      bodyReq
    );

    response({ res, data: rs.data });
  } catch (err) {
    console.log(err);
    response({ res, message: err });
  }
};
