import { Request, Response } from "express";
import { response } from "../utils/response";
import { axiosInstance } from "../constants/axios";

export const getThamSo = async (req: Request, res: Response) => {
  try {
    const bodyValue = req.body;
    const result = await axiosInstance.post("/sapi/ac-new-danh-muc", bodyValue);

    response({ res, data: result?.data });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getDSDeNghiAnChi = async (req: Request, res: Response) => {
  try {
    const bodyValue = req.body;
    const result = await axiosInstance.post("/sapi/ac-de-nghi-tim", bodyValue);
    response({ res, data: result?.data });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getDSAnChi = async (req: Request, res: Response) => {
  try {
    const bodyValue = req.body;
    const result = await axiosInstance.post("/sapi/ac-new-ma-ac", bodyValue);
    response({ res, data: result?.data });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const getAnChiChiTiet = async (req: Request, res: Response) => {
  try {
    const bodyValue = req.body;
    const result = await axiosInstance.post("/sapi/ac-de-nghi-tim", bodyValue);
    response({ res, data: result?.data });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const createDeNghiNhap = async (req: Request, res: Response) => {
  try {
    const bodyValue = req.body;
    const result = await axiosInstance.post(
      "/sapi/ac-de-de-nghi-action",
      bodyValue
    );
    response({ res, data: result?.data });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const huyDeNghiNhap = async (req: Request, res: Response) => {
  try {
    const bodyValue = req.body;
    const result = await axiosInstance.post(
      "/sapi/ac-de-de-nghi-action",
      bodyValue
    );
    response({ res, data: result?.data });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

export const deNghiXuatHoan = async (req: Request, res: Response) => {
  try {
    const bodyValue = req.body;
    console.log(bodyValue);
    const result = await axiosInstance.post(
      "/sapi/ac-de-nghi-xuat-hoan",
      bodyValue
    );
    response({ res, data: result?.data });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};



export const getDsMatHuy = async (req: Request, res: Response) => {
  try {
    const bodyValue = req.body;
    const result = await axiosInstance.post("/sapi/ac-mat-huy-tim", bodyValue);
    response({ res, data: result?.data });
  } catch (err: any) {
    return res.status(500).json({ msg: err.message });
  }
};

