import { NextFunction, Response } from "express";
import { CRequest } from "../types/user";
import { HttpStatusCode } from "axios";
import jwt from "jsonwebtoken";
import { HKEY } from "../constants/user";

export const auth =
  () => async (req: CRequest, res: Response, next: NextFunction) => {
    const valid = await validate(req);
    if (valid) next();
    else
      res.json({
        status: HttpStatusCode.Unauthorized,
        message: "Không có quyền truy cập",
      });
  };

export const validate = async (req: CRequest) => {
  const AuthTk = req.headers.authorization;
  const tk = AuthTk?.split(" ")?.[1];

  try {
    const decode = (await jwt.verify(tk, HKEY)) as jwt.JwtPayload;

    if (decode?.token || decode?.username) {
      req.session.isAuthenticated = true;
      req.session.user_email = decode?.username;
      req.session.accessToken = tk;

      return true;
    } else {
      return false;
    }
  } catch (e) {
    return false;
  }
};

export const getUser = () => {};
