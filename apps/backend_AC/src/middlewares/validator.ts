import { NextFunction, Request, Response } from "express";
import { response } from "../utils/response";
import { HttpStatusCode } from "axios";
import { z } from "zod";

export const validator =
  (rule) => (req: Request, res: Response, next: NextFunction) => {
    const ValidateSchema = z.object(rule);

    try {
      ValidateSchema.parse(req.body);

      next();
    } catch (e) {
      response({
        res,
        status: HttpStatusCode.BadRequest,
        message: "Bad Request",
      });
    }
  };
