import { useEffect, useState } from "react";
import { DsButton, DsTabs, useLocation } from "mvi-ds-ui";
import LoadingComponent from "../../components/LoadingComponent";
import {
  CREATE,
  CREATE_CATEGORY,
  CREATE_FORM,
  EDIT,
  EN,
  HELMET_NEWS,
  LANG_EN,
  LANG_VI,
  MANAGER_CATEGORY,
  UPDATE,
  UPDATE_CATEGORY,
  UPDATE_FORM,
  VI,
  VIEW_CATEGORY,
  VIEW_FORM,
} from "../../constants/const";
import { Helmet } from "react-helmet";
import FormCategoriesComponent from "../../components/FormComponent/FormCategoriesComponent";
import usePageInfo from "../../hooks/usePageInfo";
import { ROUTES } from "../../constants/path";
import BackBtn from "../../components/BackBtn";
import { extractOperation } from "../../constants/genaralFunction";
import { categorySchema } from "../../constants/validation";
import { useFormik } from "formik";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useAuth } from "../../context/AuthContext";
import {
  createCategory,
  getCategoryById,
  updateCategory,
} from "../../apis/category";
import { CategoryQueryType } from "../../type/newsType";
import { useNavigate, useSearchParams } from "react-router-dom";
import { RequestFormType } from "../../type/product";
import { PERMISSION } from "../../constants/user";
import DeleteNews from "../News/deleteNews";
import CategoryDelete from "./CategoryDelete";

const CreateCategories = () => {
  const navigate = useNavigate();
  const { setIsLoading, notify, checkPermission } = useAuth();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();

  const location = useLocation();
  const typeForm = extractOperation(location.pathname);

  const [searchParams] = useSearchParams();
  const pId = searchParams.get("id");

  const [isOpenModalDel, setIsOpenModalDel] = useState(false);

  const handleCloseModalDelete = () => {
    setIsOpenModalDel(false);
  };

  const { mutateAsync } = useMutation({
    mutationFn: (data: RequestFormType) => {
      return pId ? updateCategory(data) : createCategory(data);
    },
  });

  const { data, isLoading } = useQuery({
    queryKey: ["get-category-by-id", pId],
    queryFn: () => getCategoryById(pId || ""),
    enabled: !!pId,
    refetchOnWindowFocus: false,
    staleTime: 0, // Data is considered stale immediately
    refetchOnMount: true, // Forces a refetch on every mount
  });

  const form = useFormik<CategoryQueryType>({
    initialValues: {
      vi: {
        tieu_de: {
          value: data?.vi?.tieu_de?.value || "",
          key_content: "",
        },
        slug: {
          value: data?.vi?.slug?.value || "",
          key_content: "",
        },
        ma_goc: data?.vi?.ma_goc || null,
        mo_ta: {
          value: data?.vi?.mo_ta?.value || "",
          key_content: "",
        },
        trang_thai: {
          value: data?.vi?.trang_thai?.value || "active",
          key_content: "",
        },
        banner_category: {
          value: data?.vi?.banner_category?.value || "",
          key_content: "",
        },
        link_banner_category: data?.vi?.link_banner_category || "",
      },
      en: {
        tieu_de: {
          value: data?.en?.tieu_de?.value || "",
          key_content: "",
        },
        slug: {
          value: data?.en?.slug?.value || "",
          key_content: "",
        },
        ma_goc: data?.en?.ma_goc || null,
        mo_ta: {
          value: data?.en?.mo_ta?.value || "",
          key_content: "",
        },
        trang_thai: {
          value: data?.en?.trang_thai?.value || "active",
          key_content: "",
        },
        banner_category: {
          value: data?.en?.banner_category?.value || "",
          key_content: "",
        },
        link_banner_category: data?.en?.link_banner_category || "",
      },
    },
    enableReinitialize: true,
    onSubmit: async (values: any) => {
      const rs = await mutateAsync({ code: pId || "", form: values });
      notify?.({ type: rs ? "success" : "error" });
      navigate(ROUTES.CATEGORY);
    },
    validationSchema: categorySchema,
  });

  const handleButtonClick = () => {
    // Modify the path as needed
    const newPath = location.pathname.replace("view", "update");
    navigate(newPath + location.search);
  };

  const renderDsButton = () => {
    if (typeForm === CREATE_FORM) {
      return (
        <div className="">
          <DsButton
            label={CREATE}
            buttonType="submit"
            //@ts-ignore
            onClick={form.handleSubmit}
          />
        </div>
      );
    } else if (typeForm == UPDATE_FORM) {
      return (
        <div className="flex">
          <div className="mr-4">
            <DsButton
              label={UPDATE}
              buttonType="submit"
              //@ts-ignore
              onClick={form.handleSubmit}
            />
          </div>
          {checkPermission(PERMISSION.CATEGORY_DELETE) && (
            <>
              <DsButton
                prefixIcon="icon-bx-trash"
                color="red"
                onClick={() => {
                  setIsOpenModalDel(true);
                }}
              />
              <CategoryDelete
                categoryID={pId}
                isOpen={isOpenModalDel}
                handleClose={handleCloseModalDelete}
              />
            </>
          )}
        </div>
      );
    } else if (typeForm == VIEW_FORM) {
      return (
        <div className="flex">
          <div className="mr-4">
            {checkPermission(PERMISSION.CATEGORY_CREATE) && (
              <DsButton
                label={EDIT}
                buttonType="submit"
                //@ts-ignore
                onClick={handleButtonClick}
              />
            )}
          </div>

          {checkPermission(PERMISSION.CATEGORY_DELETE) && (
            <>
              <DsButton
                prefixIcon="icon-bx-trash"
                color="red"
                onClick={() => {
                  setIsOpenModalDel(true);
                }}
              />
              <CategoryDelete
                categoryID={pId}
                isOpen={isOpenModalDel}
                handleClose={handleCloseModalDelete}
              />
            </>
          )}
        </div>
      );
    }
  };

  useEffect(() => {
    setModuleInfo({
      module: {
        icon: "icon-bx-package",
        title: `${
          typeForm === CREATE_FORM
            ? CREATE_CATEGORY
            : typeForm === UPDATE_FORM
              ? UPDATE_CATEGORY
              : VIEW_CATEGORY
        }`,
      },
      path: [
        {
          label: `${MANAGER_CATEGORY}`,
          link: ROUTES.CATEGORY,
        },
        {
          label: `${
            typeForm === CREATE_FORM
              ? CREATE_CATEGORY
              : typeForm === UPDATE_FORM
                ? UPDATE_CATEGORY
                : VIEW_CATEGORY
          }`,
        },
      ],
    });
    return () => cleanModuleInfo();
  }, []);

  useEffect(() => {
    setIsLoading(isLoading);
  }, [isLoading]);

  return isLoading ? (
    <LoadingComponent />
  ) : (
    <div>
      <Helmet>
        <title>
          {HELMET_NEWS} -{" "}
          {typeForm === CREATE_FORM
            ? CREATE_CATEGORY
            : typeForm === UPDATE_FORM
              ? UPDATE_CATEGORY
              : VIEW_CATEGORY}
        </title>
      </Helmet>
      <div className="flex justify-between">
        <div className="">
          <BackBtn />
        </div>
        {renderDsButton()}
      </div>
      <form onSubmit={form.handleSubmit}>
        <DsTabs
          items={[
            {
              children: (
                <FormCategoriesComponent
                  form={form}
                  typeForm={typeForm}
                  lang={LANG_VI}
                  data={data}
                />
              ),
              key: "1",
              label: VI,
              badge: !!form?.errors?.vi,
              badgeColorCustom: "red",
            },
            {
              children: (
                <FormCategoriesComponent
                  form={form}
                  typeForm={typeForm}
                  lang={LANG_EN}
                  data={data}
                />
              ),
              key: "2",
              label: EN,
              badge: !!form.errors?.en,
              badgeColorCustom: "red",
            },
          ]}
        />
      </form>
    </div>
  );
};

export default CreateCategories;
