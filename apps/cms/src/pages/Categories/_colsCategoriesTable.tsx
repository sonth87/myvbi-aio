import { DsIcon, DsTooltip } from "mvi-ds-ui";
import { CategoryType } from "../../type/newsType";
import { TrangThaiHoatDong } from "../../constants/genaralFunction";
import CategoryDelete from "./CategoryDelete";
import { PERMISSION } from "../../constants/user";
import { useAuth } from "../../context/AuthContext";

export const cols = (
  handleEdit: (id: string, slug: string) => void,
  handleView: (id: string, slug: string) => void,
  handleDelete: (_id: string) => void
) => [
  {
    title: "Tên danh mục",
    dataIndex: "tieu_de",
    key: "tieu_de",
    render: (_: any, record: CategoryType) =>
      record.tieu_de && record.tieu_de.value,
  },
  {
    title: "Mô tả",
    dataIndex: "mo_ta",
    key: "mo_ta",
    render: (_: any, record: CategoryType) =>
      record.mo_ta && record.mo_ta.value,
  },
  {
    dataIndex: "trang_thai",
    title: "Trạng thái",
    render: (_: any, record: CategoryType) =>
      TrangThaiHoatDong(record.trang_thai && record.trang_thai.value),
  },
  {
    dataIndex: "thao-tac",
    title: `Thao tác`,
    align: "center",
    render: (_: any, record: CategoryType) => {
      const { checkPermission } = useAuth();
      return (
        <div className="flex justify-center">
          {checkPermission(PERMISSION.NEWS_VIEW) && (
            <DsIcon
              onClick={() => {
                handleView(record?._id && record._id, record.slug.value);
              }}
              className="mx-1 text-[#0064D3]"
              name="icon-bxs-show"
            />
          )}
          {checkPermission(PERMISSION.CATEGORY_UPDATE) && (
            <DsIcon
              onClick={() => {
                handleEdit(record?._id && record._id, record.slug.value);
              }}
              className="mx-1 text-[#0064D3]"
              name="icon-bx-edit-alt"
            />
          )}

          {checkPermission(PERMISSION.NEWS_DELETE) && (
            <DsIcon
              className="mx-1 text-[#F4175A]"
              name="icon-bx-trash"
              onClick={() => handleDelete(record?._id)}
            />
          )}
        </div>
      );
    },
  },
];
