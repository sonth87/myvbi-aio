import { useFormik } from "formik";
import { DsButton, DsDatePicker, DsInput } from "mvi-ds-ui";

import { useSearchParams } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { useAuth } from "../../context/AuthContext";

const SearchCategories = () => {
  const { notify } = useAuth();
  const [searchParams, setSearchParams] = useSearchParams();
  const searchData = {
    tieu_de: searchParams.get("tieu_de"),
    tu_ngay: searchParams.get("tu_ngay"),
    den_ngay: searchParams.get("den_ngay"),
  };

  const form = useFormik({
    initialValues: {
      tieu_de: searchData.tieu_de || "",
      tu_ngay: searchData.tu_ngay || "",
      den_ngay: searchData.den_ngay || "",
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      if (
        (values?.tu_ngay && !values?.den_ngay) ||
        (!values?.tu_ngay && values?.den_ngay)
      ) {
        notify?.({
          type: "error",
          content: "Chọn đủ ngày Từ ngày và Đến ngày",
        });
      } else {
        setSearchParams({
          tieu_de: values?.tieu_de?.trim() ?? "",
          tu_ngay: values?.tu_ngay?.trim() ?? "",
          den_ngay: values?.den_ngay?.trim() ?? "",
        });
      }
    },
  });

  const onDateChangeStartDate = (date: string, str: string) => {
    form.setFieldValue("tu_ngay", str);
  };
  const onDateChangeEndDate = (date: string, str: string) => {
    form.setFieldValue("den_ngay", str);
  };

  const handleResetForm = async () => {
    location.href = `${ROUTES.CATEGORY}`;
  };

  return (
    <div>
      <form onSubmit={form.handleSubmit}>
        <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5 xl:ds-grid-cols-5 gap-4">
          <DsInput
            label="Tên danh mục"
            name="tieu_de"
            size="large"
            type="text"
            value={form.values.tieu_de}
            onChange={(e) => form.setFieldValue("tieu_de", e)}
            // disabled={isLoading}
          />
          <DsDatePicker
            label="Ngày tạo từ"
            size="large"
            name="tu_ngay"
            format="DD/MM/YYYY"
            value={form.values?.tu_ngay}
            onChange={onDateChangeStartDate}
            picker="date"
          />

          <DsDatePicker
            label="đến ngày"
            size="large"
            name="den_ngay"
            format="DD/MM/YYYY"
            value={form.values.den_ngay}
            onChange={onDateChangeEndDate}
            picker="date"
          />
          <DsButton size="large" className="ds-w-full">
            Tìm kiếm
          </DsButton>
          <DsButton
            prefixIcon="icon-bx-revision"
            className="!bg-black100"
            size="large"
            label="Đặt lại"
            color="black"
            type="light"
            buttonType="reset"
            onClick={handleResetForm}
          />
        </div>
      </form>
    </div>
  );
};

export default SearchCategories;
