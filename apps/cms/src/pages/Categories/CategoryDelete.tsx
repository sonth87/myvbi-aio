import { useMutation, useQueryClient } from "@tanstack/react-query";
import { DsButton, DsIcon, DsModal, message } from "mvi-ds-ui";
import { FC, useState } from "react";
import { deleteCategory } from "../../apis/category";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { extractOperation } from "../../constants/genaralFunction";
import { UPDATE_FORM, VIEW_FORM } from "../../constants/const";

type Props = {
  categoryID: string | undefined | null;
  isOpen: boolean;
  handleClose: (val: boolean) => void;
};

const CategoryDelete: FC<Props> = ({ categoryID, isOpen, handleClose }) => {
  // const [isOpen, setIsOpen] = useState(false);
  const [messageApi, contextHolder] = message.useMessage();
  const queryCache = useQueryClient();

  const { mutateAsync: deleteCategoryAsync, isPending: pendingDelete } =
    useMutation({
      mutationFn: deleteCategory,
    });

  const handleRemoveProduct = async () => {
    if (categoryID) {
      const rs = await deleteCategoryAsync(categoryID);
      messageApi.open({
        type: rs.status !== 200 ? "error" : "success",
        content:
          rs.status === 200
            ? "Đã xóa danh mục"
            : "Đã xảy ra lỗi, vui lòng thử lại sau",
        duration: 5,
        className: "center",
      });
      handleClose(false);
      queryCache.invalidateQueries({ queryKey: ["category-list"] });
    }
  };

  return (
    <div>
      {contextHolder}

      <DsModal
        open={isOpen}
        onCancel={() => handleClose(false)}
        footer={[
          <DsButton
            key="confirm-del-prod"
            isLoading={pendingDelete}
            onClick={handleRemoveProduct}
            color="red"
            disabled={pendingDelete}
          >
            {pendingDelete ? "Đang xóa danh mục" : "Xác nhận xóa"}
          </DsButton>,
          <DsButton
            key="cancel-del-prod"
            onClick={() => handleClose(false)}
            className="ds-ml-4"
            disabled={pendingDelete}
          >
            Thoát
          </DsButton>,
        ]}
      >
        <p>Điều kiện để xóa danh mục này là </p>
        {/* <ul className="mr-2"> */}
        <p className="text-[red] block ml-2">
          - Danh mục không có bài viết đang sử dụng
        </p>
        <p className="text-[red] block ml-2">- Không có các danh mục con</p>
        {/* </ul> */}
        Bạn có chắc chắn với các điều kiện trên không?
      </DsModal>
    </div>
  );
};

export default CategoryDelete;
