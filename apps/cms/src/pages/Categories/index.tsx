import { useNavigate, useSearchParams } from "react-router-dom";
import { DsButton, DsTable } from "mvi-ds-ui";
import { useEffect, useState } from "react";
import { cols } from "./_colsCategoriesTable";
import { Helmet } from "react-helmet";
import { ROUTES } from "../../constants/path";
import usePageInfo from "../../hooks/usePageInfo";
import {
  CREATE_CATEGORY,
  CREATE,
  UPDATE_CATEGORY,
  MANAGER_CATEGORY,
  HELMET_CATEGORY,
} from "../../constants/const";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { getListCategories } from "../../apis/category";
import { convertToCategoryTree } from "../../constants/genaralFunction";
import SearchCategories from "./SearchCategory";
import { PERMISSION } from "../../constants/user";
import { useAuth } from "../../context/AuthContext";
import CategoryDelete from "./CategoryDelete";

const DashboardNews = () => {
  const { checkPermission } = useAuth();
  const queryCache = useQueryClient();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const [page, setPage] = useState<number>(1);
  const [pageSize, setPageSize] = useState<number>(10);
  const [isOpenModalDel, setIsOpenModalDel] = useState(false);
  const [pId, setPId] = useState("");

  const handleCloseModalDelete = () => {
    setIsOpenModalDel(false);
    setPId("");
  };

  const handleDelete = (recordId: string) => {
    setIsOpenModalDel(true);
    setPId(recordId);
  };

  useEffect(() => {
    setModuleInfo({
      module: {
        icon: "icon-bx-package",
        title: `${MANAGER_CATEGORY}`,
      },
      path: [
        {
          label: `${MANAGER_CATEGORY}`,
        },
      ],
    });
    return () => cleanModuleInfo();
  }, []);

  // Lấy dữ liệu từ searchParams
  const searchData = {
    tieu_de: searchParams.get("tieu_de"),
    ma_dm: searchParams.get("ma_dm"),
    tu_ngay: searchParams.get("tu_ngay"),
    den_ngay: searchParams.get("den_ngay"),
  };
  const handleEdit = (id: string, slug: string) => {
    navigate(`${ROUTES.CATEGORY_UPDATE}?id=${id}&slug=${slug}`);
  };

  const handleView = (id: string, slug: string) => {
    navigate(`${ROUTES.CATEGORY_VIEW}?id=${id}&slug=${slug}`);
  };

  const handlePaginationChange = (page: number, pageSize: number) => {
    setPage(page);
    setPageSize(pageSize);
  };

  const { data: dataTableCategory } = useQuery({
    queryKey: ["category-list", searchData, pageSize, page],
    queryFn: () => getListCategories(pageSize, page, searchData),
    refetchOnWindowFocus: false,
  });

  useEffect(() => {
    queryCache.invalidateQueries({
      queryKey: ["category-list", page, setPageSize],
    });
  }, [page, setPageSize]);

  return (
    <div>
      <Helmet>
        <title>{HELMET_CATEGORY}</title>
      </Helmet>

      <div className="bg-[#FFFFFF] rounded-lg">
        <SearchCategories />
        <div className="flex my-1 ">
          {checkPermission(PERMISSION.CATEGORY_CREATE) && (
            <DsButton
              className="my-3"
              label={CREATE_CATEGORY}
              onClick={() => {
                navigate(`${ROUTES.CATEGORY_CREATE}`);
              }}
            />
          )}
        </div>
        <div>
          <DsTable
            bordered
            columns={cols(handleEdit, handleView, handleDelete)}
            dataSource={
              dataTableCategory
                ? convertToCategoryTree(dataTableCategory.data)
                : []
            }
            className="!inline"
            pagination={{
              pageSize: pageSize,
              total: dataTableCategory?.totalCount,
              onChange: handlePaginationChange,
            }}
          />
          <CategoryDelete
            categoryID={pId}
            isOpen={isOpenModalDel}
            handleClose={handleCloseModalDelete}
          />
        </div>
      </div>
    </div>
  );
};

export default DashboardNews;
