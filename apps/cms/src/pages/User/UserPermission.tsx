import React, { FC } from "react";
import { permissionGroup, permissionLabelMapping } from "../../constants/user";
import { DsCheckBox } from "mvi-ds-ui";

type Props = {
  permission?: string[];
  onChange?: (permission: string[], state: boolean) => void;
};

const UserPermission: FC<Props> = ({ permission, onChange }) => {
  console.log(permission);
  return (
    <div className="grid mb-8">
      <div className="grid grid-cols-3 p-3 text-center font-semibold">
        <div>Module</div>
        <div className="col-span-2">Chức năng</div>
      </div>
      {Object.entries(permissionGroup).map((p) => {
        const moduleName = p[0];
        const perList = p[1];
        return (
          <div className="grid grid-cols-3 border-t" key={"row-" + moduleName}>
            <div className="p-3">
              {
                permissionLabelMapping?.[
                  moduleName as keyof typeof permissionLabelMapping
                ]
              }
            </div>
            <div className="grid grid-cols-3 col-span-2 p-3">
              {Object.entries(perList).map((k) => {
                const perGroup = k[0];
                const per = k[1];

                const hasThisPer = per.find((cp) => permission?.includes(cp));
                return (
                  <div key={"sub-" + perGroup}>
                    <DsCheckBox
                      label={perGroup}
                      checked={!!hasThisPer}
                      onChange={(e) => {
                        onChange?.(per, e.target.checked);
                      }}
                    />
                  </div>
                );
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default UserPermission;
