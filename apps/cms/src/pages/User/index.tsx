import React, { useEffect } from "react";
import usePageInfo from "../../hooks/usePageInfo";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { DsButton, DsTable } from "mvi-ds-ui";
import { PERMISSION, columnsUserList } from "../../constants/user";
import { useQuery } from "@tanstack/react-query";
import { getListUsers } from "../../apis/user";
import { useAuth } from "../../context/AuthContext";

const Users = () => {
  const { checkPermission } = useAuth();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const navigate = useNavigate();
  const { data, isLoading } = useQuery({
    queryKey: ["user-list"],
    queryFn: getListUsers,
  });

  useEffect(() => {
    setModuleInfo({
      module: { icon: "icon-bx-user-circle", title: "Người dùng" },
      path: [{ label: "Users" }],
    });

    return () => cleanModuleInfo();
  }, []);

  const handleCreate = () => {
    navigate(ROUTES.USERS_CREATE);
  };

  return (
    <div>
      <div className="w-full text-right">
        {checkPermission(PERMISSION.USER_CREATE) && (
          <DsButton
            label="Tạo mới"
            prefixIcon="icon-bx-plus"
            onClick={handleCreate}
          />
        )}
      </div>

      <DsTable
        dataSource={data}
        columns={columnsUserList({
          editable: checkPermission(PERMISSION.USER_UPDATE),
        })}
        className="w-full mt-4"
        loading={isLoading}
        rowKey={"id"}
      />
    </div>
  );
};

export default Users;
