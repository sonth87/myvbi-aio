import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import React, { useCallback, useEffect } from "react";
import usePageInfo from "../../hooks/usePageInfo";
import { ROUTES } from "../../constants/path";
import BackBtn from "../../components/BackBtn";
import { useFormik } from "formik";
import { userSchema } from "../../constants/validation";
import { DsButton, DsImageView, DsInput, DsSwitch, DsUpload } from "mvi-ds-ui";
import { useUpload } from "../../hooks/useUpload";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";
import { UserType } from "@repo/types/user";
import { createUser, getUserById, updateUser } from "../../apis/user";
import UserDelete from "./UserDelete";
import UserPermission from "./UserPermission";
import { PERMISSION } from "../../constants/user";

const UserForm = () => {
  const { notify, checkPermission } = useAuth();
  const navigate = useNavigate();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const { image, uploadFile } = useUpload();
  const queryCache = useQueryClient();
  const [searchParams] = useSearchParams();
  const uId = searchParams.get("id");

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (data: any) => (uId ? updateUser(data) : createUser(data)),
  });

  useEffect(() => {
    setModuleInfo({
      module: { icon: "icon-bx-user-circle", title: "Người dùng" },
      path: [{ label: "Users", link: ROUTES.USERS }, { label: "Update" }],
    });

    return () => cleanModuleInfo();
  }, []);

  const { data: uData, isLoading } = useQuery({
    queryKey: ["user", uId],
    queryFn: () => getUserById(uId || ""),
    refetchOnWindowFocus: false,
    enabled: !!uId,
    retry: 1,
  });

  const form = useFormik<UserType>({
    initialValues: {
      name: uData?.name || "",
      email: uData?.email || "",
      address: uData?.address || "",
      department: uData?.department || "",
      phone: uData?.phone || "",
      title: uData?.title || "",
      permission: uData?.permission || [],
      avatar: uData?.avatar || "",
      status: uData?.status || false,
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      const rs = await mutateAsync({ id: uId, form: values });

      if (rs.data) {
        notify?.({ type: "success" });
        navigate(ROUTES.USERS);
        queryCache.invalidateQueries({ queryKey: ["user-list"] });
      } else {
        notify?.({ type: "error", content: "Email đã tồn tại" });
        form.setFieldError("email", "Email đã tồn tại");
      }
    },
    validationSchema: userSchema,
  });

  const handleUploadFile = async (file: any) => {
    const img = await uploadFile(file.file);
    if (img) form.setFieldValue("avatar", img);
  };

  const getFieldErr = useCallback(
    (field: keyof UserType) => {
      const fErr = form.errors;
      return form.touched?.[field] && fErr?.[field]
        ? fErr?.[field]?.toString()
        : null;
    },
    [form.errors, form.touched]
  );

  const handlePermissionChange = useCallback(
    (permission: string[], state: boolean) => {
      let _permission = form?.values?.permission || [];

      if (state) _permission = [..._permission, ...permission];
      else {
        permission.forEach((p) => {
          const idx = _permission.findIndex((_p) => _p === p);
          if (idx >= 0) _permission.splice(idx, 1);
        });
      }

      form.setFieldValue(
        "permission",
        _permission.filter(function (item, pos) {
          return _permission.indexOf(item) == pos;
        })
      );
    },
    [form]
  );

  const isUpdate = uData?.id ? true : false;
  const loading = isPending || isLoading;

  if (uId && !loading && !uData) {
    return (
      <div>
        <div className="">
          <BackBtn />
        </div>
        <div className="w-full p-8 flex items-center justify-center text-xl">
          404 Không tìm thấy user
        </div>
      </div>
    );
  }

  return (
    <div>
      <div className="flex justify-between">
        <BackBtn />

        {isUpdate && uData?.id && checkPermission(PERMISSION.USER_DELETE) && (
          <UserDelete id={uData?.id} />
        )}
      </div>
      <form onSubmit={form.handleSubmit}>
        <div className="grid grid-cols-12 gap-8 w-full mt-4">
          <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
            <div>Thông tin người dùng</div>
            <div>
              <DsInput
                label="Họ và tên"
                required
                disabled={loading}
                name="name"
                onChangeEvent={form.handleChange}
                value={form.values?.name}
                state={getFieldErr("name") ? "error" : "normal"}
                message={getFieldErr("name")}
              />
            </div>
            <div>
              <DsInput
                label="Email"
                required
                disabled={loading || isUpdate}
                name="email"
                onChangeEvent={form.handleChange}
                value={form.values?.email}
                state={getFieldErr("email") ? "error" : "normal"}
                message={getFieldErr("email")}
              />
            </div>
            <div>
              <DsInput
                label="Phone"
                disabled={loading}
                name="phone"
                onChangeEvent={form.handleChange}
                value={form.values?.phone}
                state={getFieldErr("phone") ? "error" : "normal"}
                message={getFieldErr("phone")}
              />
            </div>
            <div>
              <DsInput
                label="Địa chỉ"
                disabled={loading}
                name="address"
                onChangeEvent={form.handleChange}
                value={form.values?.address}
                state={getFieldErr("address") ? "error" : "normal"}
                message={getFieldErr("address")}
              />
            </div>
            <div>
              <DsInput
                label="Chức danh"
                disabled={loading}
                name="title"
                onChangeEvent={form.handleChange}
                value={form.values?.title}
                state={getFieldErr("title") ? "error" : "normal"}
                message={getFieldErr("title")}
              />
            </div>
            <div>
              <DsInput
                label="Phòng ban"
                disabled={loading}
                name="department"
                onChangeEvent={form.handleChange}
                value={form.values?.department}
                state={getFieldErr("department") ? "error" : "normal"}
                message={getFieldErr("department")}
              />
            </div>
            {/* <div>
              <DsSelect
                label="Phòng ban"
                disabled={loading}
                name="department"
                onChange={(val) => {
                  form.setFieldValue("department", val);
                }}
                defaultValue={PRD_GROUP.K}
                value={form.values?.department}
                state={getFieldErr("department") ? "error" : "normal"}
                message={getFieldErr("department")}
                options={Object.entries(ProductGroup).map((p) => ({
                  label: p[1],
                  value: p[0],
                }))}
              />
            </div> */}

            <div>
              <DsSwitch
                disabled={loading}
                checkedChildren="On"
                unCheckedChildren="Off"
                onChange={(checked: any) => {
                  form.setFieldValue("status", checked);
                }}
                value={form.values?.status}
                label="Trạng thái"
                bgColor="blue"
                size="default"
              />
            </div>
          </div>
          <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
            <div>Avatar</div>
            <div className="text-center">
              <div className="relative text-center w-60 mx-auto p-4">
                {!form.values?.status && (
                  <div className="absolute top-0 left-0 w-full h-full flex justify-center items-center z-10 bg-blackOpacity200 text-white font-bold">
                    INACTIVE
                  </div>
                )}
                <div className="overflow-hidden flex items-center justify-center p-4">
                  {form.values?.avatar && (
                    <DsImageView src={form.values?.avatar} />
                  )}
                </div>
              </div>

              <div className="mt-4">
                <DsUpload
                  type="button"
                  customRequest={handleUploadFile}
                  showUploadList={false}
                  label="Upload"
                  disabled={loading}
                  accept="image/png, image/jpeg"
                />
              </div>
            </div>
          </div>
        </div>

        <div className="mt-8">
          <div>Permission</div>
          <UserPermission
            onChange={handlePermissionChange}
            permission={form?.values?.permission}
          />
        </div>

        <div className="sticky bottom-0 py-4 bg-white w-full border-t z-10">
          <DsButton
            label={isUpdate ? "Cập nhật" : "Tạo mới"}
            prefixIcon="icon-bxs-save"
            buttonType="submit"
          />
        </div>
      </form>
    </div>
  );
};

export default UserForm;
