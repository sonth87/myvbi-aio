import { useMutation, useQueryClient } from "@tanstack/react-query";
import { DsButton, DsModal, message } from "mvi-ds-ui";
import React, { FC, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { deleteUser } from "../../apis/user";

type Props = {
  id: string;
};

const UserDelete: FC<Props> = ({ id }) => {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const [messageApi, contextHolder] = message.useMessage();
  const queryCache = useQueryClient();

  const { mutateAsync: deleteUserAsync, isPending: pendingDelete } =
    useMutation({
      mutationFn: deleteUser,
    });

  const handleRemoveUser = async () => {
    if (id) {
      const rs = await deleteUserAsync(id);

      messageApi.open({
        type: rs ? "success" : "error",
        content: rs
          ? "Đã xóa nguời dùng"
          : "Đã xảy ra lỗi, vui lòng thử lại sau.",
        duration: 5,
        className: "right",
      });

      navigate(ROUTES.USERS);
      queryCache.invalidateQueries({ queryKey: ["user-list"] });
    }
  };

  return (
    <div>
      {contextHolder}
      <DsButton
        prefixIcon="icon-bx-trash"
        color="red"
        onClick={() => {
          setIsOpen(true);
        }}
      />
      <DsModal
        open={isOpen}
        onCancel={() => (pendingDelete ? null : setIsOpen(false))}
        footer={[
          <DsButton
            key="confirm-del-user"
            isLoading={pendingDelete}
            onClick={handleRemoveUser}
            color="red"
            disabled={pendingDelete}
          >
            {pendingDelete ? "Đang xóa nguời dùng" : "Xác nhận xóa"}
          </DsButton>,
          <DsButton
            key="cancel-del-prod"
            onClick={() => setIsOpen(false)}
            className="ds-ml-4"
            disabled={pendingDelete}
          >
            Thoát
          </DsButton>,
        ]}
      >
        Bạn có chắc chắn muốn xóa nguời dùng này không?
      </DsModal>
    </div>
  );
};

export default UserDelete;
