import { useMutation, useQueryClient } from "@tanstack/react-query";
import { DsButton, DsModal, message } from "mvi-ds-ui";
import React, { FC, useState } from "react";
import { deleteProduct } from "../../apis/product";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { IMPORT_DATA } from "../../constants/const";

type Props = {
  productCode?: string;
};

const ImportData: FC<Props> = ({ productCode }) => {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const [messageApi, contextHolder] = message.useMessage();
  const queryCache = useQueryClient();

  const { mutateAsync: deleteProdAsync, isPending: pendingImport } =
    useMutation({
      mutationFn: deleteProduct,
    });

  const handleRemoveProduct = async () => {
    if (productCode) {
      const rs = await deleteProdAsync(productCode);

      messageApi.open({
        type: rs ? "success" : "error",
        content: rs
          ? "Đã xóa sản phẩm"
          : "Đã xảy ra lỗi, vui lòng thử lại sau.",
        duration: 5,
        className: "right",
      });

      navigate(ROUTES.PRODUCTS);
      queryCache.invalidateQueries({ queryKey: ["product-list"] });
    }
  };

  return (
    <div>
      {contextHolder}
      <DsButton
        label={IMPORT_DATA}
        prefixIcon="icon-bx-import"
        color="red"
        type="outline"
        onClick={() => {
          setIsOpen(true);
        }}
      />
      <DsModal
        centered
        open={isOpen}
        onCancel={() => (pendingImport ? null : setIsOpen(false))}
        // footer={[
        //   <DsButton
        //     key="confirm-del-prod"
        //     isLoading={pendingImport}
        //     onClick={handleRemoveProduct}
        //     color="red"
        //     disabled={pendingImport}
        //   >
        //     {pendingImport ? "Đang nhập dữ liệu" : "Xác nhận nhập dữ liệu"}
        //   </DsButton>,
        //   <DsButton
        //     key="cancel-del-prod"
        //     onClick={() => setIsOpen(false)}
        //     className="ds-ml-4"
        //     disabled={pendingImport}
        //   >
        //     Thoát
        //   </DsButton>,
        // ]}
      >
        Tính năng này đang phát triển.
      </DsModal>
    </div>
  );
};

export default ImportData;
