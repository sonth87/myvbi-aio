import React, { FC } from "react";

type Props = {
  product: any;
};

const ImgThumnail: FC<Props> = ({ product }) => {
  return (
    <div className="flex justify-center">
      <img
        src={product}
        className="object-cover rounded-lg max-w-full max-h-40"
      />
    </div>
  );
};

export default ImgThumnail;
