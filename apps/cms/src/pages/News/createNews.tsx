import { useEffect, useState } from "react";
import { DsButton, DsTabs } from "mvi-ds-ui";
import LoadingComponent from "../../components/LoadingComponent";
import FormNews from "../../components/FormComponent/FormNewsComponent";
import {
  CREATE,
  CREATE_FORM,
  CREATE_NEWS,
  EDIT,
  EN,
  HELMET_NEWS,
  LANG_EN,
  LANG_VI,
  MANAGER_NEWS,
  STT_GROUP,
  SYS_GROUP,
  UPDATE,
  UPDATE_FORM,
  UPDATE_NEWS,
  VI,
  VIEW_FORM,
  VIEW_NEWS,
} from "../../constants/const";
import { Helmet } from "react-helmet";
import { ROUTES } from "../../constants/path";
import usePageInfo from "../../hooks/usePageInfo";
import BackBtn from "../../components/BackBtn";

import { useLocation, useNavigate, useSearchParams } from "react-router-dom";
import {
  extractOperation,
  getCurrentDateFormatted,
} from "../../constants/genaralFunction";
import { useFormik } from "formik";
import { useMutation, useQuery } from "@tanstack/react-query";
import { createNews, getNewsById, updateNews } from "../../apis/news";
import { useAuth } from "../../context/AuthContext";
import { newsSchema } from "../../constants/validation";
import DeleteNews from "./deleteNews";
import { PERMISSION } from "../../constants/user";

const CreateNews = () => {
  const navigate = useNavigate();
  const { setIsLoading, notify, checkPermission } = useAuth();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const location = useLocation();
  const typeForm = extractOperation(location.pathname);
  const [searchParams] = useSearchParams();
  const pId: string | null = searchParams.get("id");

  const [isOpenModalDel, setIsOpenModalDel] = useState(false);

  const handleCloseModalDelete = () => {
    setIsOpenModalDel(false);
  };

  const { mutateAsync } = useMutation({
    mutationFn: (data: any) => (pId ? updateNews(pId, data) : createNews(data)),
  });

  const { data, isLoading } = useQuery({
    queryKey: ["get-news-by-id", pId],
    queryFn: () => getNewsById(pId),
    enabled: !!pId,
    refetchOnWindowFocus: false,
  });

  const form = useFormik({
    initialValues: {
      vi: {
        tieu_de: {
          value: data?.vi?.tieu_de?.value || "",
          key_content: "",
        },
        slug: {
          value: data?.vi?.slug?.value || "",
          key_content: "",
        },
        noi_dung: {
          value: data?.vi?.noi_dung?.value || "",
          key_content: "",
        },
        tom_tat: {
          value: data?.vi?.tom_tat?.value || "",
          key_content: "",
        },
        he_thong: {
          value: data?.vi?.he_thong?.value || SYS_GROUP.WEB,
          key_content: "",
        },
        trang_thai: {
          value: data?.vi?.trang_thai?.value || STT_GROUP.ACTIVE,
          key_content: "",
        },
        mo_ta: {
          value: data?.vi?.mo_ta?.value || "",
          key_content: "",
        },
        tu_khoa: {
          value: data?.vi?.tu_khoa?.value || "",
          key_content: "",
        },
        mo_ta_tu_khoa: {
          value: data?.vi?.mo_ta_tu_khoa?.value || "",
          key_content: "",
        },
        ma_dm: {
          value: data?.vi?.ma_dm?.value || "",
          key_content: "",
        },
        anh: {
          value: data?.vi?.anh?.value || "",
          key_content: "",
        },
        anh_banner: {
          value: data?.vi?.anh_banner?.value || "",
          key_content: "",
        },
        link_banner: data?.vi?.link_banner || "",
        ngay_tao: {
          value: data
            ? data?.vi?.ngay_tao?.value
            : getCurrentDateFormatted("YYYYMMDDHHMMSS"),
          key_content: "",
        },
        nguoi_tao: {
          value: data?.vi?.nguoi_tao?.value || "",
          key_content: "",
        },
        thu_tu: {
          value: data?.vi?.thu_tu?.value || "",
          key_content: "",
        },
        tag: {
          value: data?.vi?.tag?.value || "",
          key_content: "",
        },
        ma_goc: "",
      },
      en: {
        tieu_de: {
          value: data?.en?.tieu_de?.value || "",
          key_content: "",
        },
        slug: {
          value: data?.en?.slug?.value || "",
          key_content: "",
        },
        noi_dung: {
          value: data?.en?.noi_dung?.value || "",
          key_content: "",
        },
        tom_tat: {
          value: data?.en?.tom_tat?.value || "",
          key_content: "",
        },
        he_thong: {
          value: data?.en?.he_thong?.value || SYS_GROUP.WEB,
          key_content: "",
        },
        trang_thai: {
          value: data?.en?.trang_thai?.value || STT_GROUP.ACTIVE,
          key_content: "",
        },
        mo_ta: {
          value: data?.en?.mo_ta?.value || "",
          key_content: "",
        },
        tu_khoa: {
          value: data?.en?.tu_khoa?.value || "",
          key_content: "",
        },
        mo_ta_tu_khoa: {
          value: data?.en?.mo_ta_tu_khoa?.value || "",
          key_content: "",
        },
        ma_dm: {
          value: data?.en?.ma_dm?.value || "",
          key_content: "",
        },
        anh: {
          value: data?.en?.anh?.value || "",
          key_content: "",
        },
        anh_banner: {
          value: data?.en?.anh_banner?.value || "",
          key_content: "",
        },
        link_banner: data?.en?.link_banner || "",
        ngay_tao: {
          value: data
            ? data?.vi?.ngay_tao?.value
            : getCurrentDateFormatted("YYYYMMDDHHMMSS"),
          key_content: "",
        },
        nguoi_tao: {
          value: data?.en?.nguoi_tao?.value || "",
          key_content: "",
        },
        thu_tu: {
          value: data?.en?.thu_tu?.value || "",
          key_content: "",
        },
        tag: {
          value: data?.en?.tag?.value || "",
          key_content: "",
        },
        ma_goc: "",
      },
    },
    enableReinitialize: !!pId,
    onSubmit: async (values: any) => {
      const rs = await mutateAsync(values);
      notify?.({ type: rs ? "success" : "error" });
      navigate(ROUTES.NEWS);
    },
    validationSchema: newsSchema,
  });

  const handleButtonClick = () => {
    // Modify the path as needed
    const newPath = location.pathname.replace("view", "update");
    navigate(newPath + location.search);
  };

  const renderDsButton = () => {
    if (typeForm === CREATE_FORM) {
      return (
        <div className="">
          <DsButton
            label={CREATE}
            buttonType="submit"
            //@ts-ignore
            onClick={form.handleSubmit}
          />
        </div>
      );
    } else if (typeForm == UPDATE_FORM) {
      return (
        <div className="flex">
          <div className="mr-4">
            <DsButton
              label={UPDATE}
              buttonType="submit"
              //@ts-ignore
              onClick={form.handleSubmit}
            />
          </div>
          {checkPermission(PERMISSION.NEWS_DELETE) && (
            <>
              <DsButton
                prefixIcon="icon-bx-trash"
                color="red"
                onClick={() => {
                  setIsOpenModalDel(true);
                }}
              />
              <DeleteNews
                isOpen={isOpenModalDel}
                handleClose={handleCloseModalDelete}
                pId={pId}
              />
            </>
          )}
        </div>
      );
    } else if (typeForm == VIEW_FORM) {
      return (
        <div className="flex">
          <div className="mr-4">
            {checkPermission(PERMISSION.NEWS_UPDATE) && (
              <DsButton
                label={EDIT}
                buttonType="submit"
                //@ts-ignore
                onClick={handleButtonClick}
              />
            )}
          </div>
          {checkPermission(PERMISSION.NEWS_DELETE) && (
            <>
              <DsButton
                prefixIcon="icon-bx-trash"
                color="red"
                onClick={() => {
                  setIsOpenModalDel(true);
                }}
              />
              <DeleteNews
                isOpen={isOpenModalDel}
                handleClose={handleCloseModalDelete}
                pId={pId}
              />
            </>
          )}
        </div>
      );
    }
  };

  useEffect(() => {
    setModuleInfo({
      module: {
        icon: "icon-bx-package",
        title: `${typeForm === CREATE_FORM ? CREATE_NEWS : typeForm === UPDATE_FORM ? UPDATE_NEWS : VIEW_NEWS}`,
      },
      path: [
        {
          label: `${MANAGER_NEWS}`,
          link: ROUTES.NEWS,
        },
        {
          label: `${typeForm === CREATE_FORM ? CREATE_NEWS : typeForm === UPDATE_FORM ? UPDATE_NEWS : VIEW_NEWS}`,
        },
      ],
    });

    return () => cleanModuleInfo();
  }, []);

  useEffect(() => {
    setIsLoading(isLoading);
  }, [isLoading]);
  return isLoading ? (
    <LoadingComponent />
  ) : (
    <div className="px-3">
      <Helmet>
        <title>
          {HELMET_NEWS} -{" "}
          {typeForm === CREATE_FORM
            ? CREATE_NEWS
            : typeForm === UPDATE_FORM
              ? UPDATE_NEWS
              : VIEW_NEWS}
        </title>
      </Helmet>

      <div className="flex justify-between">
        <div className="">
          <BackBtn />
        </div>
        {renderDsButton()}
      </div>

      <form onSubmit={form.handleSubmit}>
        <DsTabs
          items={[
            {
              children: (
                <FormNews
                  form={form}
                  lang={LANG_VI}
                  data={data}
                  typeForm={typeForm}
                />
              ),
              key: "1",
              label: VI,
              badge: !!form?.errors?.vi,
              badgeColorCustom: "red",
            },
            {
              children: (
                <FormNews
                  form={form}
                  lang={LANG_EN}
                  data={data}
                  typeForm={typeForm}
                />
              ),
              key: "2",
              label: EN,
              badge: !!form.errors?.en,
              badgeColorCustom: "red",
            },
          ]}
        />
      </form>
    </div>
  );
};

export default CreateNews;
