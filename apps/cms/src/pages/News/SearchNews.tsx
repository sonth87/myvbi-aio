import { useFormik } from "formik";
import { DsButton, DsDatePicker, DsInput, DsTreeSelect } from "mvi-ds-ui";
import React, { useState } from "react";
import { useSearchParams } from "react-router-dom";
import { convertToCategorySelectTree } from "../../constants/genaralFunction";
import { useQuery } from "@tanstack/react-query";
import { getListCategoriesByStatus } from "../../apis/category";
import { ROUTES } from "../../constants/path";
import { useAuth } from "../../context/AuthContext";

const SearchNews = () => {
  const { notify } = useAuth();
  const [value, setValue] = useState<string | null>(null);
  const [searchParams, setSearchParams] = useSearchParams();
  const searchData = {
    tieu_de: searchParams.get("tieu_de"),
    ma_dm: searchParams.get("ma_dm"),
    tu_ngay: searchParams.get("tu_ngay"),
    den_ngay: searchParams.get("den_ngay"),
  };
  const form = useFormik({
    initialValues: {
      tieu_de: searchData.tieu_de || "",
      ma_dm: searchData.ma_dm || "",
      tu_ngay: searchData.tu_ngay || "",
      den_ngay: searchData.den_ngay || "",
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      if (
        (values?.tu_ngay && !values?.den_ngay) ||
        (!values?.tu_ngay && values?.den_ngay)
      ) {
        notify?.({
          type: "error",
          content: "Chọn đủ ngày Từ ngày và Đến ngày",
        });
      } else {
        setSearchParams({
          tieu_de: values?.tieu_de?.trim() ?? "",
          ma_dm: values?.ma_dm?.trim() ?? "",
          tu_ngay: values?.tu_ngay?.trim() ?? "",
          den_ngay: values?.den_ngay?.trim() ?? "",
        });
      }
    },
  });

  const onDateChangeStartDate = (date: string, str: string) => {
    form.setFieldValue("tu_ngay", str);
  };
  const onDateChangeEndDate = (date: string, str: string) => {
    form.setFieldValue("den_ngay", str);
  };

  const handleSelectChange = (newValue: string | null) => {
    setValue(newValue);
    form.setFieldValue(`ma_dm`, newValue);
  };

  const { data: dataCategorySelectTree } = useQuery({
    queryKey: ["category-list-select-tree"],
    queryFn: () => getListCategoriesByStatus("active"),
    refetchOnWindowFocus: false,
  });
  const tProps = {
    treeData:
      (dataCategorySelectTree &&
        convertToCategorySelectTree(dataCategorySelectTree.data)) ||
      [],
    value,
    onChange: handleSelectChange,
    treeCheckable: false, //true cho phép chọn nhiều giá trị
    treeLine: true,
    showCheckedStrategy: "SHOW_PARENT",
    label: "Danh mục",
    style: {
      height: "54px",
    },
  };

  const handleResetForm = async () => {
    location.href = `${ROUTES.NEWS}`;
  };

  return (
    <div>
      <form onSubmit={form.handleSubmit}>
        <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:ds-grid-cols-5 gap-4">
          <DsInput
            label="Tên tiêu đề"
            name="tieu_de"
            size="large"
            type="text"
            value={form.values.tieu_de}
            onChange={(e) => form.setFieldValue("tieu_de", e)}
            // disabled={isLoading}
          />
          <DsTreeSelect {...tProps} size="large" treeDefaultExpandAll />
          <DsDatePicker
            label="Ngày tạo từ"
            size="large"
            name="tu_ngay"
            format="DD/MM/YYYY"
            // inputStyle="float-label"
            value={form.values?.tu_ngay}
            onChange={onDateChangeStartDate}
            picker="date"
          />

          <DsDatePicker
            label="đến ngày"
            size="large"
            name="den_ngay"
            format="DD/MM/YYYY"
            inputStyle="float-label"
            value={form.values?.den_ngay}
            onChange={onDateChangeEndDate}
            picker="date"
          />

          <div className="flex">
            <DsButton size="large" className="ds-w-full">
              Tìm kiếm
            </DsButton>
            <DsButton
              prefixIcon="icon-bx-revision"
              className="!bg-black100 ml-4"
              size="large"
              label="Đặt lại"
              color="black"
              type="light"
              buttonType="reset"
              onClick={handleResetForm}
            />
          </div>
        </div>
      </form>
    </div>
  );
};

export default SearchNews;
