import { useEffect, useMemo } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { DsButton, DsTable } from "mvi-ds-ui";
import { useState } from "react";
import { cols } from "./_colsNewsTable";
import { Helmet } from "react-helmet";
import { ROUTES } from "../../constants/path";
import usePageInfo from "../../hooks/usePageInfo";
import { CREATE_NEWS, HELMET_NEWS, MANAGER_NEWS } from "../../constants/const";
import { getListNews } from "../../apis/news";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import SearchNews from "./SearchNews";
import { useAuth } from "../../context/AuthContext";
import { PERMISSION } from "../../constants/user";
import DeleteNews from "./deleteNews";
import ImportData from "./ImportData";

const DashboardNews = () => {
  const { checkPermission } = useAuth();
  const queryCache = useQueryClient();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();

  const [page, setPage] = useState<number>(1);
  const [pageSize, setPageSize] = useState<number>(10);
  const [isOpenModalDel, setIsOpenModalDel] = useState(false);
  const [pId, setPId] = useState("");

  const handleCloseModalDelete = () => {
    setIsOpenModalDel(false);
    setPId("");
  };

  const handleDelete = (recordId: string) => {
    setIsOpenModalDel(true);
    setPId(recordId);
  };

  useEffect(() => {
    setModuleInfo({
      module: {
        icon: "icon-bx-package",
        title: `${MANAGER_NEWS}`,
      },
      path: [
        {
          label: `${MANAGER_NEWS}`,
        },
      ],
    });
    return () => cleanModuleInfo();
  }, []);

  // Lấy dữ liệu từ searchParams
  const searchData = {
    tieu_de: searchParams.get("tieu_de"),
    ma_dm: searchParams.get("ma_dm"),
    tu_ngay: searchParams.get("tu_ngay"),
    den_ngay: searchParams.get("den_ngay"),
  };

  const handleEdit = (_id: string, ma_goc: string, slug: string) => {
    if (checkPermission(PERMISSION.NEWS_UPDATE)) {
      navigate(`${ROUTES.NEWS_UPDATE}?id=${_id}&slug=${slug}&ma_goc=${ma_goc}`);
    } else {
      navigate("/404");
    }
  };
  const handleView = (_id: string, ma_goc: string, slug: string) => {
    navigate(`${ROUTES.NEWS_VIEW}?id=${_id}&slug=${slug}&ma_goc=${ma_goc}`);
  };

  const handlePaginationChange = (page: number, pageSize: number) => {
    setPage(page);
    setPageSize(pageSize);
  };

  const { data: dataTableNews, isLoading } = useQuery({
    queryKey: ["news-list", searchData, pageSize, page],
    queryFn: () => getListNews(pageSize, page, searchData),
    refetchOnWindowFocus: false,
  });

  // Tính toán giá trị phụ thuộc dựa trên giá trị ban đầu của page và pageSize
  const checkChange = useMemo(
    () => ({ initialPage: page, initialPageSize: pageSize }),
    []
  );

  useEffect(() => {
    // Kiểm tra xem có sự thay đổi so với giá trị ban đầu hay không
    if (
      page !== checkChange.initialPage ||
      pageSize !== checkChange.initialPageSize
    ) {
      queryCache.invalidateQueries({ queryKey: ["news-list"] });
    }
  }, [page, pageSize, checkChange]);

  return (
    <div>
      <Helmet>
        <title>{HELMET_NEWS}</title>
      </Helmet>

      <div className="bg-[#FFFFFF] rounded-lg">
        <SearchNews />
        <div className="flex mt-2">
          {checkPermission(PERMISSION.NEWS_CREATE) && (
            <>
              <DsButton
                label={CREATE_NEWS}
                onClick={() => {
                  navigate(`${ROUTES.NEWS_CREATE}`);
                }}
                className="mr-3"
              />
              {/* <ImportData /> */}
            </>
          )}
        </div>

        <div className="mt-4">
          <DsTable
            scroll={{ x: 1240 }}
            bordered
            dataSource={dataTableNews?.data || []}
            columns={cols(handleEdit, handleView, handleDelete)}
            pagination={{
              pageSize: pageSize,
              total: dataTableNews?.totalCount,
              onChange: handlePaginationChange,
            }}
            className="w-full min-h-[603px] !inline"
          />
          <DeleteNews
            isOpen={isOpenModalDel}
            handleClose={handleCloseModalDelete}
            pId={pId}
          />
        </div>
      </div>
    </div>
  );
};

export default DashboardNews;
