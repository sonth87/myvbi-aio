import { DsButton, DsIcon, DsImageView, DsTooltip } from "mvi-ds-ui";
import { NewsSchemaType } from "../../type/newsType";
import {
  TrangThaiHoatDong,
  convertDateTime,
} from "../../constants/genaralFunction";
import { PERMISSION } from "../../constants/user";
import { useAuth } from "../../context/AuthContext";

export const cols: any = (
  handleEdit: (_id: string, ma_goc: string, slug: string) => void,
  handleView: (_id: string, ma_goc: string, slug: string) => void,
  handleDelete: (_id: string) => void
) => [
  {
    dataIndex: "thao-tac",
    width: "120px",
    fixed: "left",
    align: "center",
    title: `Thao tác`,
    render: (_: any, record: NewsSchemaType) => {
      const { checkPermission } = useAuth();
      return (
        <>
          <div className="flex justify-center">
            {checkPermission(PERMISSION.NEWS_VIEW) && (
              <DsIcon
                onClick={() => {
                  handleView(record?._id, record?.ma_goc, record?.slug?.value);
                }}
                className="mx-1 text-[#0064D3]"
                name="icon-bxs-show"
              />
            )}

            {checkPermission(PERMISSION.NEWS_UPDATE) && (
              <DsIcon
                onClick={() => {
                  handleEdit(record?._id, record?.ma_goc, record?.slug?.value);
                }}
                className="mx-1 text-[#0064D3]"
                name="icon-bx-edit-alt"
              />
            )}

            {checkPermission(PERMISSION.NEWS_DELETE) && (
              <DsIcon
                className="mx-1 text-[#F4175A]"
                name="icon-bx-trash"
                onClick={() => handleDelete(record?._id)}
              />
            )}
          </div>
        </>
      );
    },
  },
  {
    dataIndex: "anh",
    width: "150px",
    title: "Ảnh Thumnail",
    render: (_: any, record: NewsSchemaType) => {
      const linkAnh = `${record?.anh?.value}`;
      return <DsImageView src={linkAnh} width={80} />;
    },
  },
  {
    dataIndex: "tieu_de",
    title: "Tiêu đề",
    width: "350px",
    render: (_: any, record: NewsSchemaType) => {
      return (
        <a
          onClick={() => {
            handleView(record?._id, record?.ma_goc, record?.slug?.value);
          }}
        >
          {record?.tieu_de?.value}
        </a>
      );
    },
  },
  {
    dataIndex: "ngay_tao",
    width: "150px",
    align: "center",
    title: "Ngày tạo",
    render: (_: any, record: NewsSchemaType) => {
      const dateTimeString = record?.ngay_tao?.value.toString();
      return <>{dateTimeString && convertDateTime(dateTimeString)}</>;
    },
  },
  {
    dataIndex: "slug",
    title: "Slug",
    width: "350px",
    render: (_: any, record: NewsSchemaType) => record?.slug?.value,
  },
  {
    dataIndex: "trang_thai",
    title: "Trạng thái",
    width: "180px",
    render: (_: any, record: NewsSchemaType) =>
      TrangThaiHoatDong(record?.trang_thai?.value),
  },
  {
    dataIndex: "nguoi_tao",
    title: "Tác giả",
    align: "center",
    width: "150px",
    render: (_: any, record: NewsSchemaType) => (
      <div className="text-sm">{record?.nguoi_tao?.value}</div>
    ),
  },
];
