import { useMutation, useQueryClient } from "@tanstack/react-query";
import { DsButton, DsModal, message } from "mvi-ds-ui";
import { FC } from "react";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { deleteNews } from "../../apis/news";

type Props = {
  isOpen: boolean;
  pId?: string | undefined | null;
  handleClose: (val: boolean) => void;
};

const DeleteNews: FC<Props> = ({ isOpen, pId, handleClose }) => {
  const navigate = useNavigate();
  const [messageApi, contextHolder] = message.useMessage();
  const queryCache = useQueryClient();

  const { mutateAsync: deleteNewsAsync, isPending: pendingDelete } =
    useMutation({
      mutationFn: deleteNews,
    });

  const handleRemoveProduct = async () => {
    if (pId) {
      const rs: any = await deleteNewsAsync(pId);
      messageApi.open({
        type: rs.status === 200 ? "success" : "error",
        content: rs ? rs.message : "Đã xảy ra lỗi, vui lòng thử lại sau.",
        duration: 5,
        className: "right",
      });
      handleClose(false);
      navigate(ROUTES.NEWS);
      queryCache.invalidateQueries({ queryKey: ["news-list"] });
    }
  };

  return (
    <div>
      {contextHolder}
      <DsModal
        centered
        open={isOpen}
        onCancel={() => handleClose(false)}
        footer={[
          <DsButton
            key="confirm-del-prod"
            isLoading={pendingDelete}
            onClick={handleRemoveProduct}
            color="red"
            disabled={pendingDelete}
          >
            {pendingDelete ? "Đang xóa bài viết" : "Xác nhận xóa"}
          </DsButton>,
          <DsButton
            key="cancel-del-prod"
            onClick={() => handleClose(false)}
            className="ds-ml-4"
            disabled={pendingDelete}
          >
            Thoát
          </DsButton>,
        ]}
      >
        Bạn có chắc chắn muốn xóa bài viết này không?
      </DsModal>
    </div>
  );
};

export default DeleteNews;
