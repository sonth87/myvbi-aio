import { useMutation, useQueryClient } from "@tanstack/react-query";
import { DsButton, DsModal, message } from "mvi-ds-ui";
import React, { FC, useState } from "react";
import { deleteProduct } from "../../apis/product";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/path";

type Props = {
  productId: string | null;
};

const ProductDelete: FC<Props> = ({ productId }) => {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const [messageApi, contextHolder] = message.useMessage();
  const queryCache = useQueryClient();

  const { mutateAsync: deleteProdAsync, isPending: pendingDelete } =
    useMutation({
      mutationFn: deleteProduct,
    });

  const handleRemoveProduct = async () => {
    if (productId) {
      const rs = await deleteProdAsync(productId);

      messageApi.open({
        type: rs ? "success" : "error",
        content: rs
          ? "Đã xóa sản phẩm"
          : "Đã xảy ra lỗi, vui lòng thử lại sau.",
        duration: 5,
        className: "right",
      });

      navigate(ROUTES.PRODUCTS);
      queryCache.invalidateQueries({ queryKey: ["product-list"] });
    }
  };

  return (
    <div>
      {contextHolder}
      <DsButton
        prefixIcon="icon-bx-trash"
        color="red"
        onClick={() => {
          setIsOpen(true);
        }}
      />
      <DsModal
        open={isOpen}
        onCancel={() => (pendingDelete ? null : setIsOpen(false))}
        footer={[
          <DsButton
            key="confirm-del-prod"
            isLoading={pendingDelete}
            onClick={handleRemoveProduct}
            color="red"
            disabled={pendingDelete}
          >
            {pendingDelete ? "Đang xóa sản phẩm" : "Xác nhận xóa"}
          </DsButton>,
          <DsButton
            key="cancel-del-prod"
            onClick={() => setIsOpen(false)}
            className="ds-ml-4"
            disabled={pendingDelete}
          >
            Thoát
          </DsButton>,
        ]}
      >
        Bạn có chắc chắn muốn xóa sản phẩm này không?
      </DsModal>
    </div>
  );
};

export default ProductDelete;
