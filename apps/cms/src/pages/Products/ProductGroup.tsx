import { useQuery } from "@tanstack/react-query";
import { DsTabs } from "mvi-ds-ui";
import React, { FC, useCallback } from "react";
import { getListProducts } from "../../apis/product";
import { PRD_TYPE, ProductType } from "../../constants/const";
import ProductListing from "./ProductListing";
import { ProductsType } from "@repo/types/products";

type Props = {
  handleItemClick?: (id?: string, product?: ProductsType) => void;
  isModalView?: boolean;
};

const ProductGroup: FC<Props> = ({ handleItemClick, isModalView }) => {
  const { data, isLoading } = useQuery({
    queryKey: ["product-list"],
    queryFn: getListProducts,
    refetchOnWindowFocus: false,
  });

  const getDataByGroup = useCallback(
    (type: string) =>
      data?.filter((row) => row?.type === type),
    [data]
  );

  return (
    <div className="w-full text-lg max-h-full overflow-auto">
      <DsTabs
        defaultActiveKey="1"
        items={Object.values(PRD_TYPE).map((itm) => ({
          children: (
            <ProductListing
              data={getDataByGroup(itm)}
              handleItemClick={handleItemClick}
              isModalView={isModalView}
            />
          ),
          key: itm,
          label: ProductType?.[itm],
        }))}
      />
    </div>
  );
};

export default ProductGroup;
