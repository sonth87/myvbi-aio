import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import React, { useCallback, useEffect } from "react";
import {
  createProduct,
  getProductById,
  updateProduct,
} from "../../apis/product";
import usePageInfo from "../../hooks/usePageInfo";
import { ROUTES } from "../../constants/path";
import BackBtn from "../../components/BackBtn";
import { useFormik } from "formik";
import { productSchema } from "../../constants/validation";
import { ProductsType } from "@repo/types/products";
import { DsButton, DsInput, DsSelect, DsSwitch, DsUpload } from "mvi-ds-ui";
import { useUpload } from "../../hooks/useUpload";
import {
  PRD_GROUP,
  PRD_TYPE,
  ProductGroup,
  ProductType,
} from "../../constants/const";
import { productCodeConvert } from "../../utils/slug";
import { useNavigate, useSearchParams } from "react-router-dom";
import ProductItem from "./ProductItem";
import ProductDelete from "./ProductDelete";
import { useAuth } from "../../context/AuthContext";
import { PERMISSION } from "../../constants/user";

const ProductForm = () => {
  const { notify, checkPermission } = useAuth();
  const navigate = useNavigate();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const { image, uploadFile } = useUpload();
  const queryCache = useQueryClient();
  const [searchParams] = useSearchParams();
  const pId = searchParams.get("id");
  const updateAble = checkPermission(PERMISSION.PRODUCT_UPDATE);

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (data: any) =>
      pId ? updateProduct(data) : createProduct(data),
  });

  useEffect(() => {
    setModuleInfo({
      module: { icon: "icon-bx-package", title: "Sản phẩm" },
      path: [{ label: "Products", link: ROUTES.PRODUCTS }, { label: "Update" }],
    });

    return () => cleanModuleInfo();
  }, []);

  const { data: pData, isLoading } = useQuery({
    queryKey: ["product", pId],
    queryFn: () => getProductById(pId || ""),
    refetchOnWindowFocus: false,
    enabled: !!pId,
    retry: 1,
  });

  const form = useFormik<ProductsType>({
    initialValues: {
      name: pData?.name || "",
      code: pData?.code || "",
      icon: pData?.icon || "",
      link: pData?.link || "",
      description: pData?.description || "",
      visible: pData?.visible || false,
      promotion: pData?.promotion || "",
      tags: pData?.tags || "",
      type: pData?.type || "",
      group: pData?.group || "",
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      console.log(form.values);
      const rs = await mutateAsync({
        code: form?.values.code,
        id: pId,
        form: values,
      });

      notify?.({ type: rs ? "success" : "error" });
      navigate(ROUTES.PRODUCTS);
      queryCache.invalidateQueries({ queryKey: ["product-list"] });
    },
    validationSchema: productSchema,
  });

  const handleUploadFile = async (file: any) => {
    const img = await uploadFile(file.file);
    if (img) form.setFieldValue("icon", img);
  };

  const getFieldErr = useCallback(
    (field: keyof ProductsType) => {
      const fErr = form.errors;
      return form.touched?.[field] && fErr?.[field] ? fErr?.[field] : null;
    },
    [form.errors, form.touched]
  );

  const isUpdate = pData?.id ? true : false;
  const loading = isPending || isLoading || !updateAble;

  if (pId && !loading && !pData) {
    return (
      <div>
        <div className="">
          <BackBtn />
        </div>
        <div className="w-full p-8 flex items-center justify-center text-xl">
          404 Không tìm thấy sản phẩm
        </div>
      </div>
    );
  }

  return (
    <div>
      <div className="flex justify-between">
        <BackBtn />

        {isUpdate &&
          pData?.code &&
          checkPermission(PERMISSION.PRODUCT_DELETE) && (
            <ProductDelete productId={pId} />
          )}
      </div>
      <form onSubmit={form.handleSubmit}>
        <div className="grid grid-cols-12 gap-4 w-full mt-4 mb-8">
          <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
            <div>Thông tin sản phẩm</div>
            <div>
              <DsInput
                label="Tên sản phẩm"
                placeholder="Tên loại bảo hiểm"
                required
                disabled={loading}
                name="name"
                onChangeEvent={form.handleChange}
                value={form.values?.name}
                state={getFieldErr("name") ? "error" : "normal"}
                message={getFieldErr("name")}
              />
            </div>
            <div>
              <DsInput
                label="Mã sản phẩm"
                required
                disabled={loading}
                name="code"
                onChange={(value) => {
                  const newCode = productCodeConvert(value?.toString() || "");
                  form.setFieldValue("code", newCode.toUpperCase());
                }}
                value={form.values?.code}
                state={getFieldErr("code") ? "error" : "normal"}
                message={getFieldErr("code")}
              />
            </div>
            <div>
              <DsInput
                label="Link sản phẩm"
                required
                disabled={loading}
                name="link"
                onChangeEvent={form.handleChange}
                value={form.values?.link}
                state={getFieldErr("link") ? "error" : "normal"}
                message={getFieldErr("link")}
              />
            </div>
            <div>
              <DsInput
                label="Mô tả sản phẩm"
                type="textarea"
                disabled={loading}
                name="description"
                onChangeEvent={form.handleChange}
                value={form.values?.description}
                state={getFieldErr("description") ? "error" : "normal"}
                message={getFieldErr("description")}
              />
            </div>
            <div>
              <DsSwitch
                disabled={loading}
                defaultChecked={false}
                checkedChildren="On"
                unCheckedChildren="Off"
                onChange={(checked: any) => {
                  form.setFieldValue("visible", checked);
                }}
                value={form.values?.visible}
                label="Trạng thái"
                bgColor="blue"
                size="default"
              />
            </div>
            <div>
              <DsInput
                label="Promotion tag"
                disabled={loading}
                name="promotion"
                onChangeEvent={form.handleChange}
                value={form.values?.promotion}
                state={getFieldErr("promotion") ? "error" : "normal"}
                message={getFieldErr("promotion")}
              />
            </div>
            {/* <div>
              <DsInput
                label="Tags"
                disabled={loading}
                name="tags"
                onChangeEvent={form.handleChange}
                value={form.values?.tags}
                state={getFieldErr("tags") ? "error" : "normal"}
                message={getFieldErr("tags")}
              />
            </div> */}
            <div>
              <DsSelect
                label="Group"
                disabled={loading}
                required
                name="group"
                onChange={(val) => {
                  form.setFieldValue("group", val);
                }}
                defaultValue={PRD_GROUP.K}
                value={form.values?.group}
                state={getFieldErr("group") ? "error" : "normal"}
                message={getFieldErr("group")}
                options={Object.entries(ProductGroup).map((p) => ({
                  label: p[1],
                  value: p[0],
                }))}
              />
            </div>
            <div>
              <DsSelect
                label="Tags"
                disabled={loading}
                // mode="multiple"
                required
                name="type"
                onChange={(val) => {
                  form.setFieldValue("type", val);
                }}
                defaultValue={PRD_TYPE.GD}
                value={form.values?.type}
                state={getFieldErr("type") ? "error" : "normal"}
                message={getFieldErr("type")}
                options={Object.entries(ProductType).map((p) => ({
                  label: p[1],
                  value: p[0],
                }))}
              />
            </div>
          </div>
          <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
            <div>Icon</div>
            <div className="text-center">
              <div className="relative text-center h-60 w-60 mx-auto p-4">
                <ProductItem product={form.values} />
              </div>
              {getFieldErr("icon") && (
                <div className="text-sm text-red-500 mt-1">
                  {getFieldErr("icon")}
                </div>
              )}

              <div className="mt-4">
                <DsUpload
                  type="button"
                  customRequest={handleUploadFile}
                  showUploadList={false}
                  label="Upload icon"
                  disabled={loading}
                  accept="image/png, image/jpeg"
                />
              </div>
            </div>
          </div>
        </div>

        <div className="sticky bottom-0 py-4 bg-white w-full border-t z-10">
          <DsButton
            label={isUpdate ? "Cập nhật" : "Tạo mới"}
            prefixIcon="icon-bxs-save"
            buttonType="submit"
            disabled={loading}
          />
        </div>
      </form>
    </div>
  );
};

export default ProductForm;
