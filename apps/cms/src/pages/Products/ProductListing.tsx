import React, { FC } from "react";
import { ProductGroup } from "../../constants/const";
import ProductItem from "./ProductItem";
import classNames from "classnames";
import { ProductsType } from "@repo/types/products";

type Props = {
  data?: ProductsType[];
  handleItemClick?: (id?: string, product?: ProductsType) => void;
  isModalView?: boolean;
};

const ProductListing: FC<Props> = ({ data, handleItemClick, isModalView }) => {
  return (
    <div className="w-full text-lg max-h-full overflow-auto mt-5">
      {Object.entries(ProductGroup).map((group) => {
        const prodInList = data?.filter((prod) => prod.group === group[0]);

        if (!prodInList?.length) return;

        return (
          <div key={"prod-group-" + group[0]} className="mb-10">
            <div>Sản phẩm bảo hiểm {group[1]}</div>
            <div
              className={classNames("grid grid-cols-2 gap-4 mt-2", {
                "sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-6 xl:grid-cols-7 2xl:grid-cols-9":
                  !isModalView,
                "md:grid-cols-3 lg:grid-cols-5 xl:grid-cols-6": isModalView,
              })}
            >
              {prodInList?.map((prod) => (
                <ProductItem
                  key={prod.id}
                  product={prod}
                  onClick={handleItemClick}
                />
              ))}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ProductListing;
