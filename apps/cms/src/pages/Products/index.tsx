import React, { useEffect } from "react";
import usePageInfo from "../../hooks/usePageInfo";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { DsButton } from "mvi-ds-ui";
import { PERMISSION } from "../../constants/user";
import { useAuth } from "../../context/AuthContext";
import ProductGroup from "./ProductGroup";

const Products = () => {
  const { checkPermission } = useAuth();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const navigate = useNavigate();

  useEffect(() => {
    setModuleInfo({
      module: { icon: "icon-bx-package", title: "Sản phẩm" },
      path: [{ label: "Products" }],
    });

    return () => cleanModuleInfo();
  }, []);

  const handleItemClick = (id?: string) => {
    if (id && checkPermission(PERMISSION.PRODUCT_VIEW))
      navigate(`${ROUTES.PRODUCTS_UPDATE}?id=${id}`);
  };

  const handleCreate = () => {
    navigate(ROUTES.PRODUCTS_CREATE);
  };

  return (
    <div>
      <div className="w-full text-right">
        {checkPermission(PERMISSION.PRODUCT_CREATE) && (
          <DsButton
            label="Tạo mới"
            prefixIcon="icon-bx-plus"
            onClick={handleCreate}
          />
        )}
      </div>

      <ProductGroup handleItemClick={handleItemClick} />
    </div>
  );
};

export default Products;
