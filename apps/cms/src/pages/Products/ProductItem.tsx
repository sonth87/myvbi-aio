import React, { FC } from "react";
import { ProductsType } from "@repo/types/products";
import classNames from "classnames";

type Props = {
  product?: ProductsType;
  onClick?: (id?: string, product?: ProductsType) => void;
};

const ProductItem: FC<Props> = ({ product, onClick }) => {
  return (
    <div
      className={classNames(
        "border rounded relative cursor-pointer hover:shadow-lg duration-300 h-full",
        {
          grayscale: !product?.visible,
        }
      )}
      onClick={() => onClick?.(product?.id, product)}
    >
      {!product?.visible && (
        <div className="absolute top-0 left-0 w-full h-full flex justify-center items-center z-10 bg-blackOpacity200 text-white font-bold">
          INACTIVE
        </div>
      )}
      <div className="w-full h-full left-0 top-0 p-3 flex flex-col gap-2">
        <div className="min-h-[40%] overflow-hidden flex items-center justify-center p-4">
          {product?.icon && (
            <img
              src={product?.icon}
              className="object-contain w-full h-full max-w-14 max-h-14"
            />
          )}
          {product?.promotion && (
            <div className="absolute top-0 right-0 px-2 py-1 bg-pink-500 text-white text-sm rounded-bl-lg max-w-full">
              {product?.promotion}
            </div>
          )}
        </div>
        <div className="text-center text-sm">{product?.name}</div>
      </div>
    </div>
  );
};

export default ProductItem;
