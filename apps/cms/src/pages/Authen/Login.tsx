import { DsButton } from "mvi-ds-ui";
import React, { useState } from "react";
import { ROUTES } from "../../constants/path";
import { useMutation } from "@tanstack/react-query";
import { signin } from "../../apis/user";
import { TOKEN_KEY } from "../../constants/const";
import Divform from "./comps/UForm";

const Login = () => {
  const { mutateAsync, isPending } = useMutation({
    mutationFn: () => signin(),
  });

  const signIn = async () => {
    const data = await mutateAsync();
    const login_url = data?.data?.login_url;
    if (login_url) window.location.href = login_url;

    // window.localStorage.setItem(TOKEN_KEY, "this-is-token");
    // window.location.href = "/";
  };

  return (
    <div>
      <section className="bg-gray-50 dark:bg-gray-900 relative overflow-hidden">
        <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
          <a
            href="#"
            className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
          >
            <img className="w-8 h-8 mr-2" src="/logo-vbi.png" alt="logo" />
            VBI
          </a>
          <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
              <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                Sign in to your account
              </h1>
              <div className="space-y-4 md:space-y-6">
                <DsButton
                  type="solid"
                  label="Sign in"
                  onClick={signIn}
                  className="!w-full"
                  disabled={isPending}
                  isLoading={isPending}
                />
              </div>
            </div>
          </div>
        </div>
        <Divform />
      </section>
    </div>
  );
};

export default Login;
