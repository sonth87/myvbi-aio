import { DsButton } from "mvi-ds-ui";
import React, { useState } from "react";
import { ROUTES } from "../../../constants/path";
import { useMutation } from "@tanstack/react-query";
import { signin } from "../../../apis/user";

const UForm = () => {
  const [login, setLogin] = useState({ u: "", p: "" });
  const [state, setState] = useState("");
  const { mutateAsync, isPending } = useMutation({
    mutationFn: () => signin(login),
  });

  const signIn = async () => {
    const data = await mutateAsync();
    if (data) {
      setState("");
      window.location.href = ROUTES.REDIRECT + "?c=" + data?.data?.c;
    } else {
      setState("Thông tin đăng nhập không chính xác. Vui lòng thử lại.");
    }
  };

  return (
    <div className="absolute bottom-0 left-0 -translate-x-full translate-y-full p-2 hover:translate-x-1 hover:translate-y-1 duration-500 delay-[2000ms] ml-4 mb-4">
      <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
            Sign in to your account
          </h1>
          <div className="space-y-4 md:space-y-6">
            {state && <div className="bg-red-200 p-4 rounded-lg">{state}</div>}
            <div>
              {/* <label
              htmlFor="email"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              Your email
            </label> */}
              <input
                type="text"
                name="name"
                id="name"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder=""
                onChange={(e) => setLogin({ ...login, u: e.target.value })}
                value={login.u}
              />
            </div>
            <div>
              {/* <label
              htmlFor="password"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              Password
            </label> */}
              <input
                type="password"
                name="password"
                id="password"
                placeholder="••••••••"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={(e) => setLogin({ ...login, p: e.target.value })}
                value={login.p}
              />
            </div>
            {/* <div className="flex items-center justify-between">
            <div className="flex items-start">
              <div className="flex items-center h-5">
                <input
                  id="remember"
                  aria-describedby="remember"
                  type="checkbox"
                  className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-primary-600 dark:ring-offset-gray-800"
                />
              </div>
              <div className="ml-3 text-sm">
                <label
                  htmlFor="remember"
                  className="text-gray-500 dark:text-gray-300"
                >
                  Remember me
                </label>
              </div>
            </div>
            <a
              href="#"
              className="text-sm font-medium text-primary-600 hover:underline dark:text-primary-500"
            >
              Forgot password?
            </a>
          </div> */}
            <DsButton
              type="solid"
              label="Sign in"
              onClick={signIn}
              className="!w-full"
            />
            {/* <p className="text-sm font-light text-gray-500 dark:text-gray-400">
            Don’t have an account yet?{" "}
            <a
              href="#"
              className="font-medium text-primary-600 hover:underline dark:text-primary-500"
            >
              Sign up
            </a>
          </p> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default UForm;
