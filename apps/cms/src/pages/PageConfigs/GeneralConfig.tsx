import { DsButton, DsTabs } from "mvi-ds-ui";
import React, { useEffect } from "react";
import ConfigSeo from "./general/ConfigSeo";
import CustomScript from "./general/CustomScript";
import CompanyInfo from "./general/CompanyInfo";
import usePageInfo from "../../hooks/usePageInfo";
import { useMutation, useQuery } from "@tanstack/react-query";
import {
  getGeneralConfig,
  updateGeneralConfig,
} from "../../apis/generalConfig";
import { useAuth } from "../../context/AuthContext";
import { useFormik } from "formik";
import { GeneralConfigType } from "@repo/types/general";
import { generalConfigSchema } from "../../constants/validation";
import { PERMISSION } from "../../constants/user";

const GeneralConfig = () => {
  const { setIsLoading, notify, checkPermission } = useAuth();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const { data, isLoading } = useQuery({
    queryKey: ["general-config"],
    queryFn: getGeneralConfig,
    refetchOnWindowFocus: false,
  });
  const { mutateAsync } = useMutation({
    mutationFn: updateGeneralConfig,
  });
  const updateAble = checkPermission(PERMISSION.GENERAL_CONFIG_UPDATE);

  const form = useFormik<GeneralConfigType>({
    initialValues: {
      meta: {
        title: data?.meta?.title || "",
        description: data?.meta?.description || "",
        tags: data?.meta?.tags || "",
        thumbnail: data?.meta?.thumbnail || "",
      },
      script: data?.script || [],
      company: {
        name: data?.company?.name || "",
        shortName: data?.company?.shortName || "",
        logo: data?.company?.logo || "",
        phone: data?.company?.phone || "",
        address: data?.company?.address || "",
        email: data?.company?.email || "",
        description: data?.company?.description || "",
      },
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      const rs = await mutateAsync(values);

      notify?.({ type: rs ? "success" : "error" });
    },
    validationSchema: generalConfigSchema,
  });

  useEffect(() => {
    setModuleInfo({
      module: { icon: "icon-bx-cog", title: "Cấu hình chung" },
      path: [{ label: "General Config" }],
    });

    return () => cleanModuleInfo();
  }, []);

  useEffect(() => {
    setIsLoading(isLoading);
  }, [isLoading]);

  return (
    <div>
      <form onSubmit={form.handleSubmit}>
        <div className="mb-8">
          <DsTabs
            items={[
              {
                children: (
                  <ConfigSeo
                    data={data?.meta}
                    isLoading={isLoading || !updateAble}
                    formik={form}
                  />
                ),
                key: "1",
                label: "SEO",
                icon: "icon-bx-at",
                badge: !!form.errors?.meta,
                badgeColorCustom: "red",
              },
              {
                children: (
                  <CustomScript
                    data={data?.script}
                    isLoading={isLoading || !updateAble}
                    formik={form}
                  />
                ),
                key: "2",
                label: "Custom script",
                icon: "icon-bxl-javascript",
                badge: !!form.errors?.script,
                badgeColorCustom: "red",
              },
              {
                children: (
                  <CompanyInfo
                    data={data?.company}
                    isLoading={isLoading || !updateAble}
                    formik={form}
                  />
                ),
                key: "3",
                label: "My Company",
                icon: "icon-bx-buildings",
                badge: !!form.errors?.company,
                badgeColorCustom: "red",
              },
            ]}
          />
        </div>

        <div className="sticky bottom-0 py-4 bg-white w-full border-t z-10">
          {updateAble && (
            <DsButton
              prefixIcon="icon-bxs-save"
              label="Cập nhật"
              buttonType="submit"
            />
          )}
        </div>
      </form>
    </div>
  );
};

export default GeneralConfig;
