import React from "react";
import ConfigElmByPage from "./PageConfigs";

const PageConfig = () => {
  return (
    <div>
      <ConfigElmByPage />
    </div>
  );
};

export default PageConfig;
