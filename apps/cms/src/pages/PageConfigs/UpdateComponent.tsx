import React, { FC, useCallback, useEffect, useMemo, useState } from "react";
import {
  COMPS,
  ComponentsMapping,
  componentList,
} from "../../constants/pagesConfig";
import BackBtn from "../../components/BackBtn";
import { PageCompType } from "@repo/types/page";
import { DsButton } from "mvi-ds-ui";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { updatePageConfig } from "../../apis/pageConfig";
import { ROUTES } from "../../constants/path";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";
import usePageInfo from "../../hooks/usePageInfo";
import { PERMISSION } from "../../constants/user";

export type CompType = keyof typeof COMPS;

type Props = {
  comp: CompType;
  data?: PageCompType;
};

const UpdateComponent: FC<Props> = ({ comp, data }) => {
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const { notify, checkPermission } = useAuth();
  const navigate = useNavigate();
  const queryCache = useQueryClient();
  const [formData, setFormData] = useState(data);
  const { mutateAsync, isPending } = useMutation({
    mutationFn: updatePageConfig,
  });
  const updateAble = checkPermission(PERMISSION.PAGE_CONFIG_UPDATE);

  const onFormChange = useCallback(
    (data: PageCompType) => {
      setFormData(data);
    },
    [data]
  );

  const updateForm = useMemo(
    () =>
      ComponentsMapping({
        type: comp,
        data,
        onFormChange,
        isLoading: isPending || !updateAble,
      }),
    [comp, data, onFormChange]
  );

  const handleSubmit = useCallback(async () => {
    if (formData) {
      const rs = await mutateAsync(formData);

      notify?.({ type: rs ? "success" : "error" });
      navigate(ROUTES.PAGE_CONFIG);
      queryCache.invalidateQueries({ queryKey: ["get-pages-comp", comp] });
      queryCache.invalidateQueries({ queryKey: ["pages-comp"] });
    }
  }, [formData, data]);

  useEffect(() => {
    const mdinfo = {
      module: { icon: "icon-bx-food-menu", title: "Quản trị nội dung website" },
      path: [],
    };
    const compName = componentList?.find((c) => c.key === comp)?.name;
    setModuleInfo({
      ...mdinfo,
      path: [
        { label: "Page Config", link: ROUTES.PAGE_CONFIG },
        { label: compName || comp },
      ],
    });

    return () => setModuleInfo({ ...mdinfo, path: [{ label: "Page Config" }] });
  }, [comp]);

  return (
    <div>
      <BackBtn link={ROUTES.PAGE_CONFIG} />
      <div className="mb-8">{updateForm}</div>

      {!!updateForm && updateAble && (
        <div className="sticky bottom-0 py-4 bg-white w-full border-t z-10">
          <DsButton
            prefixIcon="icon-bxs-save"
            label="Cập nhật"
            buttonType="submit"
            onClick={handleSubmit}
            disabled={isPending}
            isLoading={isPending}
          />
        </div>
      )}

      {!!!updateForm && (
        <div className="w-full p-8 flex items-center justify-center text-xl">
          404 Không tìm thấy cấu hình yêu cầu
        </div>
      )}
    </div>
  );
};

export default UpdateComponent;
