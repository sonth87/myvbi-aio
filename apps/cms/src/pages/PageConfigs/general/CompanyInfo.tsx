import { DsImageView, DsInput, DsUpload } from "mvi-ds-ui";
import React, { FC, useCallback, useState } from "react";
import { CompanyDataType } from "@repo/types/general";
import { useUpload } from "../../../hooks/useUpload";

type Props = {
  data?: CompanyDataType;
  isLoading?: boolean;
  formik?: any;
};

const CompanyInfo: FC<Props> = ({ data, isLoading, formik }) => {
  const { uploadFile } = useUpload();
  const [thumb, setThumb] = useState(formik.values?.company?.logo);

  const handleUploadFile = async (file: any) => {
    const img = await uploadFile(file.file);
    if (img) {
      setThumb(img);
      formik.setFieldValue("company.logo", img);
    }
  };

  const getFieldErr = useCallback(
    (field: string) => {
      const fErr = formik.errors.company;
      return formik.touched.company?.[field] && fErr?.[field]
        ? fErr?.[field]
        : null;
    },
    [formik.errors.company, formik.touched.company]
  );

  return (
    <div className="grid grid-cols-12 gap-8 w-full mt-4">
      <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
        <div className="col-span-12">Thông tin công ty</div>
        <div>
          <DsInput
            label="Tên công ty"
            required
            name="company.name"
            onChangeEvent={formik.handleChange}
            value={formik.values?.company?.name}
            state={getFieldErr("name") ? "error" : "normal"}
            message={getFieldErr("name")}
            disabled={isLoading}
          />
        </div>
        <div>
          <DsInput
            label="Tên viết tắt"
            name="company.shortName"
            onChangeEvent={formik.handleChange}
            value={formik.values?.company?.shortName}
            state={getFieldErr("shortName") ? "error" : "normal"}
            message={getFieldErr("shortName")}
            disabled={isLoading}
          />
        </div>
        <div>
          <DsInput
            label="Số điện thoại"
            required
            name="company.phone"
            onChangeEvent={formik.handleChange}
            value={formik.values?.company?.phone}
            state={getFieldErr("phone") ? "error" : "normal"}
            message={getFieldErr("phone")}
            disabled={isLoading}
          />
        </div>
        <div>
          <DsInput
            label="Email"
            required
            name="company.email"
            onChangeEvent={formik.handleChange}
            value={formik.values?.company?.email}
            state={getFieldErr("email") ? "error" : "normal"}
            message={getFieldErr("email")}
            disabled={isLoading}
          />
        </div>
        <div>
          <DsInput
            label="Địa chỉ"
            required
            name="company.address"
            onChangeEvent={formik.handleChange}
            value={formik.values?.company?.address}
            state={getFieldErr("address") ? "error" : "normal"}
            message={getFieldErr("address")}
            disabled={isLoading}
          />
        </div>
        <div>
          <DsInput
            label="Mô tả"
            type="textarea"
            name="company.description"
            onChangeEvent={formik.handleChange}
            value={formik.values?.company?.description}
            state={getFieldErr("description") ? "error" : "normal"}
            message={getFieldErr("description")}
            disabled={isLoading}
          />
        </div>
      </div>
      <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
        <div className="col-span-12">Logo</div>
        <DsImageView src={thumb} rootClassName="w-1/2" />
        <DsUpload
          type="button"
          customRequest={handleUploadFile}
          showUploadList={false}
          disabled={isLoading}
          accept="image/png, image/jpeg"
        />
      </div>
    </div>
  );
};

export default CompanyInfo;
