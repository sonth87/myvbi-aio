import React, { FC, useCallback, useState } from "react";
import { DsImageView, DsInput, DsUpload } from "mvi-ds-ui";
import { MetaDataType } from "@repo/types/general";
import { useUpload } from "../../../hooks/useUpload";

type Props = {
  data?: MetaDataType;
  isLoading?: boolean;
  formik?: any;
};

const ConfigSeo: FC<Props> = ({ data, isLoading, formik }) => {
  const { uploadFile } = useUpload();
  const [thumb, setThumb] = useState(formik.values?.meta?.thumbnail);

  const handleUploadFile = async (file: any) => {
    const img = await uploadFile(file.file);
    if (img) {
      setThumb(img);
      formik.setFieldValue("meta.thumbnail", img);
    }
  };

  const getFieldErr = useCallback(
    (field: string) => {
      const fErr = formik.errors.meta;

      return formik.touched.meta?.[field] && fErr?.[field]
        ? fErr?.[field]
        : null;
    },
    [formik.errors.meta, formik.touched.meta]
  );

  return (
    <div className="grid grid-cols-12 gap-8 w-full mt-4">
      <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
        <div>Meta data</div>
        <div>
          <DsInput
            label="Tiêu đề"
            placeholder="Ex: Bảo hiểm VietinBank (VBI) | Tiên phong công nghệ"
            required
            disabled={isLoading}
            name="meta.title"
            onChangeEvent={formik.handleChange}
            value={formik.values?.meta?.title}
            state={getFieldErr("title") ? "error" : "normal"}
            message={getFieldErr("title")}
          />
        </div>
        <div>
          <DsInput
            label="Mô tả"
            type="textarea"
            placeholder="Ex: Bảo hiểm VietinBank (VBI) là một trong những đơn vị tiên phong áp dụng công nghệ, cải tiến quy trình bồi thường với ứng dụng bảo hiểm số hàng đầu My VBI."
            required
            disabled={isLoading}
            name="meta.description"
            onChangeEvent={formik.handleChange}
            value={formik.values?.meta?.description}
            state={getFieldErr("description") ? "error" : "normal"}
            message={getFieldErr("description")}
          />
        </div>
        <div>
          <DsInput
            label="Tags"
            disabled={isLoading}
            name="meta.tags"
            onChangeEvent={formik.handleChange}
            value={formik.values?.meta?.tags}
            state={getFieldErr("tags") ? "error" : "normal"}
            message={getFieldErr("tags")}
          />
        </div>
      </div>
      <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
        <div>Thumbnail</div>
        <DsImageView src={thumb || formik.values?.meta?.thumbnail || ""} />
        <DsUpload
          type="button"
          customRequest={handleUploadFile}
          showUploadList={false}
          disabled={isLoading}
          accept="image/png, image/jpeg"
        />
      </div>
    </div>
  );
};

export default ConfigSeo;
