import { DsButton, DsIcon, DsInput } from "mvi-ds-ui";
import React, { FC, useState } from "react";

type Props = {
  data?: string[];
  isLoading?: boolean;
  formik?: any;
};

const CustomScript: FC<Props> = ({ data, isLoading, formik }) => {
  const [scrt, setScrt] = useState<string[]>(data || []);
  // formik.touched.script

  const addMoreScript = () => {
    const _scrt = [...scrt];
    _scrt.push("");
    setScrt(_scrt);
  };

  const removeScript = (index: number) => {
    let _scrt = [...scrt];
    _scrt.splice(index, 1);

    if (!_scrt.length) _scrt = [""];
    setScrt(_scrt);
    formik.setFieldValue("script", _scrt);
  };

  const onFieldChange = (index: number, txt: string | number | null) => {
    let _scrt = [...scrt];

    if (typeof _scrt?.[index] !== "undefined") {
      _scrt[index] = txt ? txt.toString() : "";
      setScrt(_scrt);

      formik.setFieldValue("script", _scrt?.filter((s) => s?.trim()) || [""]);
    }
  };

  return (
    <div>
      <div>Cấu hình các script về chatbot, tracking, ...</div>
      <div className="flex flex-col gap-4">
        {scrt &&
          scrt?.map((val, index) => (
            <div className="flex w-full gap-4" key={"custom-script" + index}>
              <div className="w-full">
                <DsInput
                  label="Script"
                  type="textarea"
                  name="script"
                  value={val}
                  disabled={isLoading}
                  onChange={(val) => onFieldChange(index, val)}
                  // state={getFieldErr("title") ? "error" : "normal"}
                  // message={getFieldErr("title")}
                />
              </div>
              <DsButton
                prefixIcon={"icon-bx-minus"}
                color="red"
                onClick={() => removeScript(index)}
                buttonType="button"
                disabled={isLoading}
              />
            </div>
          ))}
        {!scrt && (
          <DsInput
            label="Script"
            type="textarea"
            onChange={(val) => onFieldChange(0, val)}
            disabled={isLoading}
          />
        )}
      </div>
      <DsButton
        prefixIcon={"icon-bx-plus"}
        onClick={addMoreScript}
        buttonType="button"
        className="mt-4"
        disabled={isLoading}
      />
    </div>
  );
};

export default CustomScript;
