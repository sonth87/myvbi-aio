import Banner from "./Banner";
import TopProducts from "./Products/TopProducts";
import Partner from "./Partner";
import ProtectFamily from "./Products/ProtectFamily";
import ProtectCompany from "./Products/ProtectCompany";
import WhyVBI from "./WhyVBI";
import CustomerOpinion from "./CustomerOpinion";
import QnA from "./QnA";
import Invester from "./Invester";
import Structure from "./Structure";
import Award from "./Award";
import News from "./News";

export {
  Banner,
  TopProducts,
  Partner,
  ProtectCompany,
  ProtectFamily,
  WhyVBI,
  CustomerOpinion,
  QnA,
  Invester,
  Structure,
  Award,
  News,
};
