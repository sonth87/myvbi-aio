import { pages } from "@repo/types";
import React, { FC, useEffect, useRef, useState } from "react";
import { PageConfigProps } from "../../../type/page";
import { initFormData } from "../../../constants/pagesConfig";
import ConfirmRemove from "../../../components/ConfirmRemove";
import CommonInfo from "./CommonInfo";
import { DsButton, DsIcon, DsInput, DsSelect, DsTabs } from "mvi-ds-ui";
import { QnAContentType } from "@repo/types/page";
import usePageConfig from "../../../hooks/usePageConfig";

const QnA: FC<PageConfigProps> = ({ type, data, onFormChange, ...props }) => {
  const initData = initFormData(type, data);
  const [listContent, setListContent] = useState<QnAContentType[]>(
    (initData.content as QnAContentType[]) || []
  );
  const [form, setForm] = useState<pages.PageCompType>(initData);
  const [deleteItem, setDeleteItem] = useState<
    string | { group: number; index: number } | null
  >(null);
  const [activeGroup, setActiveGroup] = useState("0");

  const onUpdatePosition = (newList: any[]) => {
    try {
      let _listContent = [...listContent];

      if (_listContent?.[parseInt(activeGroup)]?.questions)
        _listContent[parseInt(activeGroup)].questions = newList;
    } catch (e) {
      // loi
    }
  };

  const { onDragOver, onDragStart, onDrop } = usePageConfig({
    listImg: listContent?.[parseInt(activeGroup)]?.questions || [],
    callback: onUpdatePosition,
  });

  useEffect(() => {
    setForm({ ...form, content: listContent as any });
  }, [listContent]);

  useEffect(() => {
    onFormChange?.(form);
  }, [form]);

  useEffect(() => {
    if (data) {
      setListContent(initData.content as QnAContentType[]);
      setForm(initData);
    }
  }, [data]);

  const removeItem = (item: string | { group: number; index: number }) => {
    let _listContent = [...listContent];
    if (typeof item === "string") {
      _listContent.splice(parseInt(item), 1);
      setListContent(_listContent);
    } else if (item?.group >= 0 && item?.index) {
      _listContent?.[item.group as any]?.questions?.splice(item.index, 1);
      setListContent(_listContent);
    }
  };

  const addMoreQuestion = (tabIdx: number) => {
    const _listContent = [...listContent];
    _listContent?.[tabIdx]?.questions.push({ question: "", answer: "" });
    setListContent(_listContent);
  };

  const addGroup = () => {
    const _listContent = [...listContent];
    _listContent.push({ groupName: "", questions: [] });

    setListContent(_listContent);
  };

  const onTabEdit = (key: any, action: "add" | "remove") => {
    if (action === "add") {
      addGroup();
    } else {
      setDeleteItem(key);
    }
  };

  const changeGroupName = (value: string, index: number) => {
    const _listContent = [...listContent];

    if (!_listContent?.[index])
      _listContent[index] = { groupName: value, questions: [] };
    else _listContent[index].groupName = value;

    setListContent(_listContent);
  };

  const editableTab = (idx: number) => (
    <Editable
      value={listContent?.[idx]?.groupName}
      onChange={(value: string) => changeGroupName(value, idx)}
    />
  );

  const handleChangeAttr = (
    tabIdx: number,
    qId: number,
    type: string,
    value: string
  ) => {
    const _listContent = [...listContent];
    const thisContent = _listContent?.[tabIdx]?.questions?.[qId];

    if (_listContent?.[tabIdx]?.questions?.[qId])
      _listContent[tabIdx].questions[qId] = { ...thisContent, [type]: value };

    setListContent(_listContent);
  };

  const tabContent = (tabIdx: number) => {
    const activeIdx = listContent[tabIdx];

    return (
      <div>
        <div className="w-full flex flex-col col-span-5 gap-2">
          {activeIdx?.questions?.map((q, qId) => (
            <div
              className="grid grid-cols-12 gap-4 w-full"
              key={`${tabIdx}-${qId}`}
              id={`item-${qId}`}
              draggable
              onDragOver={onDragOver}
              onDragStart={onDragStart}
              onDrop={onDrop}
            >
              <div className="col-span-5">
                <DsInput
                  label="Question"
                  value={q.question}
                  onChange={(value) =>
                    handleChangeAttr(
                      tabIdx,
                      qId,
                      "question",
                      value?.toString() || ""
                    )
                  }
                  disabled={props?.isLoading}
                />
              </div>
              <div className="col-span-6">
                <DsInput
                  label="Answer"
                  type="textarea"
                  value={q.answer}
                  onChange={(value) =>
                    handleChangeAttr(
                      tabIdx,
                      qId,
                      "answer",
                      value?.toString() || ""
                    )
                  }
                  disabled={props?.isLoading}
                />
              </div>
              <div>
                <DsButton
                  prefixIcon={"icon-bx-minus"}
                  color="red"
                  onClick={() =>
                    setDeleteItem({
                      group: tabIdx,
                      index: qId,
                    })
                  }
                  disabled={props?.isLoading}
                />
              </div>
            </div>
          ))}
        </div>

        <DsButton
          prefixIcon={"icon-bx-plus"}
          onClick={() => addMoreQuestion(tabIdx)}
          buttonType="button"
          className="mt-4"
        />
      </div>
    );
  };

  return (
    <div className="grid grid-cols-1 md:grid-cols-5 gap-8 relative">
      <ConfirmRemove
        open={deleteItem !== null}
        onOK={() => {
          deleteItem !== null && removeItem(deleteItem);
          setDeleteItem(null);
        }}
        onCancel={() => setDeleteItem(null)}
      />

      <div className="flex flex-col gap-4 col-span-2">
        <CommonInfo
          form={form}
          setForm={setForm}
          isLoading={props?.isLoading}
        />
      </div>

      <div className="w-full flex flex-col col-span-5 gap-2">
        <DsTabs
          defaultActiveKey="0"
          items={(form?.content as QnAContentType[])?.map((c, idx) => ({
            key: idx.toString(),
            label: editableTab(idx),
            children: tabContent(idx),
          }))}
          onEdit={onTabEdit}
          type="editable-card"
          onChange={(group: string) => setActiveGroup(group)}
        />
      </div>
    </div>
  );
};

const Editable: FC<{ value: string; onChange: (value: string) => void }> = ({
  value,
  onChange,
}) => {
  const [isEdit, setIsEdit] = useState(false);
  const [cval, setCVal] = useState(value);
  const ref = useRef<any>(null);

  useEffect(() => {
    setCVal(value);
  }, [value]);

  if (!isEdit) {
    return (
      <div className="flex">
        {cval}
        <div
          onClick={async () => {
            await setIsEdit(true);
            ref?.current?.focus?.();
          }}
          className="px-2 cursor-pointer"
        >
          <DsIcon name="icon-bx-edit-alt" className="!text-sm" />
        </div>
      </div>
    );
  }

  const onEditableChange = (value: string) => {
    setCVal(value);
  };

  const handleInput = (event: any) => {
    if (typeof event !== "string") event?.stopPropagation();
    if (event.key === "Enter" || event === "done") {
      setIsEdit(false);
      onChange?.(cval);
    }
  };

  return (
    <div className="relative">
      <input
        ref={ref}
        value={cval}
        onChange={(e) => onEditableChange(e.target.value)}
        className="bg-amber-100 outline-none pr-6"
        placeholder="Group name"
        onKeyDown={handleInput}
        onBlur={() => handleInput("done")}
      />
      <div
        onClick={() => handleInput("done")}
        className="absolute top-0 right-0"
      >
        <DsIcon name="icon-bx-check" className="text-sm" />
      </div>
    </div>
  );
};

export default QnA;
