import React from "react";
import { PageConfigProps } from "../../../../type/page";
import TopProducts from "./TopProducts";

const ProtectFamily: React.FC<PageConfigProps> = (props) => {
  return <TopProducts {...props} />;
};

export default ProtectFamily;
