import React from "react";
import { PageConfigProps } from "../../../../type/page";
import TopProducts from "./TopProducts";

const ProtectCompany: React.FC<PageConfigProps> = (props) => {
  return <TopProducts {...props} />;
};

export default ProtectCompany;
