import { pages } from "@repo/types";
import React, { FC, useEffect, useState } from "react";
import { PageConfigProps } from "../../../../type/page";
import { DsIcon, DsModal } from "mvi-ds-ui";
import { initFormData } from "../../../../constants/pagesConfig";
import CommonInfo from "../CommonInfo";
import ProductGroup from "../../../Products/ProductGroup";
import { ProductsType } from "@repo/types/products";
import ProductItem from "../../../Products/ProductItem";
import { getListProducts } from "../../../../apis/product";
import { useQuery } from "@tanstack/react-query";
import ConfirmRemove from "../../../../components/ConfirmRemove";

const TopProducts: FC<PageConfigProps> = ({
  type,
  data,
  onFormChange,
  ...props
}) => {
  const initData = initFormData(type, data);
  const [form, setForm] = useState<pages.PageCompType>(initData);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { data: allProducts, isLoading } = useQuery({
    queryKey: ["product-list"],
    queryFn: getListProducts,
    refetchOnWindowFocus: false,
  });
  const [deleteItem, setDeleteItem] = useState<{
    id: string;
    index: number;
  } | null>(null);

  useEffect(() => {
    onFormChange?.(form);
  }, [form]);

  useEffect(() => {
    if (data) setForm(data);
  }, [data]);

  const handleItemClick = (id?: string, product?: ProductsType) => {
    const _content: string[] = (form?.content as string[]) || [];
    if (id) _content.push(id);
    setForm({ ...form, content: _content as any });

    setIsModalOpen(false);
  };

  const handleConfirmRemove = (id: string, index: number) => {
    setDeleteItem({ id, index });
  };

  const handleRemoveProduct = (code: string, index: number) => {
    const _content: string[] = (form?.content as string[]) || [];
    _content.splice(index, 1);
    setForm({ ...form, content: _content as any });
  };

  return (
    <div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
        <ConfirmRemove
          open={deleteItem !== null}
          onOK={() => {
            deleteItem !== null &&
              handleRemoveProduct(deleteItem.id, deleteItem.index);
            setDeleteItem(null);
          }}
          onCancel={() => setDeleteItem(null)}
        />
        <div className="flex flex-col gap-4">
          <CommonInfo
            form={form}
            setForm={setForm}
            isLoading={props?.isLoading}
          />
        </div>
      </div>

      <div className="mt-4">
        <div>Danh sách sản phẩm</div>
        <div className="grid grid-cols-2 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-7 2xl:grid-cols-9 mt-4 gap-4">
          {form?.content?.map((code, idx) => (
            <div className="group relative" key={"p-selected-" + idx}>
              <ProductItem
                product={allProducts?.find((p) => p.id === code)}
              />

              <div
                onClick={() => handleConfirmRemove(code as string, idx)}
                className="group-hover:flex hidden absolute top-0 left-0 w-full h-full cursor-pointer bg-black bg-opacity-25 justify-center items-center text-white"
              >
                Remove
              </div>
            </div>
          ))}
          <div
            className="w-full pb-full relative border-dashed border border-ink200 rounded-lg cursor-pointer"
            onClick={() => setIsModalOpen(true)}
          >
            <DsIcon
              name="icon-bx-plus"
              className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2"
            />
          </div>
        </div>

        <DsModal
          open={isModalOpen}
          onCancel={() => setIsModalOpen(false)}
          width={"70%"}
        >
          <ProductGroup handleItemClick={handleItemClick} isModalView />
        </DsModal>
      </div>
    </div>
  );
};

export default TopProducts;
