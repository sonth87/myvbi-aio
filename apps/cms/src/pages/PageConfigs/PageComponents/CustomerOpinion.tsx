import { pages } from "@repo/types";
import React, { FC, useEffect, useState } from "react";
import { PageConfigProps } from "../../../type/page";
import CommonInfo from "./CommonInfo";
import { useUpload } from "../../../hooks/useUpload";
import { initFormData } from "../../../constants/pagesConfig";
import {
  DsAvatar,
  DsButton,
  DsInput,
  DsUpload,
} from "mvi-ds-ui";
import { OpinionContentType } from "@repo/types/page";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper/modules";
import ConfirmRemove from "../../../components/ConfirmRemove";
import usePageConfig from "../../../hooks/usePageConfig";

const CustomerOpinion: FC<PageConfigProps> = ({
  type,
  data,
  onFormChange,
  ...props
}) => {
  const { uploadFile, isLoading } = useUpload();
  const initData = initFormData(type, data);
  const [listContent, setListContent] = useState<any>(initData.content || []);
  const [form, setForm] = useState<pages.PageCompType>(initData);
  const [deleteItem, setDeleteItem] = useState<number | null>(null);
  const { onDragOver, onDragStart, onDrop } = usePageConfig({
    listImg: listContent,
    setListImg: setListContent,
  });

  const handleUploadFile = async (file: any, index: number) => {
    const img = await uploadFile(file);
    const _listContent = [...listContent];
    if (_listContent?.[index]) _listContent[index].avatar = img;

    if (img) setListContent(_listContent);
  };

  const addMorePeople = () => {
    const _listContent = [...listContent];
    _listContent.push({});
    setListContent(_listContent);
  };

  const handleConfirmRemove = (index: number) => {
    setDeleteItem(index);
  };

  const removeItem = (index: number) => {
    let _listContent = [...listContent];
    _listContent.splice(index, 1);

    if (!_listContent.length) _listContent = [];
    setListContent(_listContent);
  };

  useEffect(() => {
    setForm({ ...form, content: listContent });
  }, [listContent]);

  useEffect(() => {
    onFormChange?.(form);
  }, [form]);

  useEffect(() => {
    if (data) {
      setForm(data);
      setListContent(data?.content as any);
    }
  }, [data]);

  const handleChangeAttr = (
    idx: number,
    field: string,
    value: string | number | null
  ) => {
    const _listContent = [...listContent];
    if (_listContent?.[idx]) {
      _listContent[idx] = { ..._listContent[idx], [field]: value };

      setListContent([..._listContent]);
    }
  };

  return (
    <div className="grid grid-cols-1 md:grid-cols-5 gap-8">
      <ConfirmRemove
        open={deleteItem !== null}
        onOK={() => {
          deleteItem !== null && removeItem(deleteItem);
          setDeleteItem(null);
        }}
        onCancel={() => setDeleteItem(null)}
      />
      <div className="flex flex-col gap-4 col-span-2">
        <CommonInfo
          form={form}
          setForm={setForm}
          isLoading={props?.isLoading}
        />
      </div>

      <div className="w-full flex flex-col items-center col-span-3">
        <div className="mb-4">Preview</div>
        <div className="my-4 font-bold text-lg">{form?.title}</div>
        <div className="w-full">
          <Swiper
            pagination={{
              dynamicBullets: true,
            }}
            modules={[Pagination]}
            slidesPerView={2}
            spaceBetween={16}
          >
            {listContent?.map((content: OpinionContentType, index: number) => (
              <SwiperSlide key={index + content.name} className="!h-auto">
                <div className="w-full h-full flex flex-col justify-center items-center border rounded p-4 gap-4">
                  <div className="flex w-full gap-3 h-14">
                    <div>
                      <DsAvatar
                        size="X-Lagre"
                        src={content.avatar || "./imgs/icons/opi_icon.svg"}
                        type="Image"
                      />
                    </div>
                    <div className="flex-1 flex flex-col">
                      <div className="text-lg font-bold">{content?.name}</div>
                      <div>{content?.position}</div>
                    </div>
                  </div>

                  <div className="flex-1">{content?.text}</div>
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>

      <div className="w-full flex flex-col col-span-5 gap-2">
        {listContent?.map((content: OpinionContentType, index: number) => (
          <div
            className="grid grid-cols-12 gap-4 w-full"
            key={`item-${index}`}
            id={`item-${index}`}
            draggable
            onDragOver={onDragOver}
            onDragStart={onDragStart}
            onDrop={onDrop}
          >
            <div>
              {content.avatar && (
                <div className="rounded-full overflow-hidden">
                  <DsAvatar size="2X-Large" src={content.avatar} type="Image" />
                </div>
              )}
            </div>
            <div className="col-span-3">
              <DsInput
                label="Name"
                value={content?.name}
                onChange={(value) => handleChangeAttr(index, "name", value)}
                disabled={props?.isLoading}
              />
            </div>
            <div className="col-span-3">
              <DsInput
                label="Title"
                value={content?.position}
                onChange={(value) => handleChangeAttr(index, "position", value)}
                disabled={props?.isLoading}
              />
            </div>
            <div className="col-span-4">
              <DsInput
                label="Content"
                type="textarea"
                value={content?.text}
                onChange={(value) => handleChangeAttr(index, "text", value)}
                disabled={props?.isLoading}
              />
            </div>
            <div className="flex flex-col gap-4">
              <DsButton
                prefixIcon={"icon-bx-minus"}
                color="red"
                onClick={() => handleConfirmRemove(index)}
                disabled={props?.isLoading}
              />
              <DsUpload
                type="button"
                customRequest={(file: any) => handleUploadFile(file, index)}
                showUploadList={false}
                label=""
                disabled={isLoading || props?.isLoading}
                accept="image/png, image/jpeg"
              />
            </div>
          </div>
        ))}
        <DsButton
          prefixIcon={"icon-bx-plus"}
          onClick={addMorePeople}
          buttonType="button"
          className="mt-4"
        />
      </div>
    </div>
  );
};

export default CustomerOpinion;
