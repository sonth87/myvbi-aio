import { pages } from "@repo/types";
import React, { FC, useEffect, useState } from "react";
import { PageConfigProps } from "../../../type/page";
import { useUpload } from "../../../hooks/useUpload";
import { StructureGroup, initFormData } from "../../../constants/pagesConfig";
import { DsButton, DsIcon, DsModal, DsTabs } from "mvi-ds-ui";
import { StructureContentType } from "@repo/types/page";
import CommonInfo from "./CommonInfo";
import ConfirmRemove from "../../../components/ConfirmRemove";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../../constants/path";
import LeaderListing from "../../Leader/LeaderListing";
import LeaderItem from "../../Leader/LeaderItem";
import { useQuery } from "@tanstack/react-query";
import { getAllLeaders } from "../../../apis/leader";

const Structure: FC<PageConfigProps> = ({
  type,
  data,
  onFormChange,
  ...props
}) => {
  const initData = initFormData(type, data);
  const [listContent, setListContent] = useState<any>(initData.content || []);
  const [form, setForm] = useState<pages.PageCompType>(initData);
  const [deleteItem, setDeleteItem] = useState<{
    group: string;
    id: string;
  } | null>(null);
  const [activeGroup, setActiveGroup] = useState(StructureGroup?.[0].value);
  const navigator = useNavigate();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { data: dataLeader, isLoading: isLeaderLoading } = useQuery({
    queryKey: ["leader-list"],
    queryFn: getAllLeaders,
    refetchOnWindowFocus: false,
  });

  const handleConfirmRemove = (group: string, id: string) => {
    setDeleteItem({ group, id });
  };

  const removeItem = (group: string, id: string) => {
    let _listContent = [...listContent];
    const foundGroup = _listContent?.findIndex((c) => c.group === group);

    const idIndex = _listContent[foundGroup]?.ids?.findIndex(
      (pid: string) => pid === id
    );
    _listContent[foundGroup]?.ids?.splice(idIndex, 1);

    if (!_listContent.length) _listContent = [];
    setListContent(_listContent);
  };

  useEffect(() => {
    setForm({ ...form, content: listContent });
  }, [listContent]);

  useEffect(() => {
    onFormChange?.(form);
  }, [form]);

  useEffect(() => {
    if (data) {
      setForm(data);
      setListContent(data?.content as any);
    }
  }, [data]);

  const handleItemClick = (id?: string) => {
    const _listContent = [...listContent];

    let groupIdx = _listContent.findIndex((c) => c.group === activeGroup);

    if (groupIdx === undefined || groupIdx === -1) {
      _listContent.push({ group: activeGroup, ids: [] });
      groupIdx = _listContent.findIndex((c) => c.group === activeGroup);
    }

    if (groupIdx > -1) {
      _listContent?.[groupIdx]?.ids.push(id);
      setListContent(_listContent);

      setIsModalOpen(false);
    }
  };

  const tabContent = (group: string) => {
    const activeIdx = form?.content?.findIndex((c: any) => c?.group === group);

    return (
      <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 xl:grid-cols-6 2xl:grid-cols-7 mt-4 gap-4">
        {activeIdx !== undefined &&
          activeIdx > -1 &&
          (form?.content?.[activeIdx] as StructureContentType)?.ids?.map(
            (id, idx) => (
              <div className="group relative" key={"p-selected-" + idx}>
                <LeaderItem
                  data={dataLeader?.find((d) => d.id === id)}
                  className="h-full"
                />
                <div
                  onClick={() => handleConfirmRemove(group, id)}
                  className="group-hover:flex hidden absolute top-0 left-0 w-full h-full cursor-pointer bg-black bg-opacity-25 justify-center items-center text-white"
                >
                  Remove
                </div>
              </div>
            )
          )}
        <div
          className="w-full pb-full relative border-dashed border border-ink200 rounded-lg cursor-pointer"
          onClick={() => setIsModalOpen(true)}
        >
          <DsIcon
            name="icon-bx-plus"
            className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2"
          />
        </div>
      </div>
    );
  };

  return (
    <div className="grid grid-cols-1 md:grid-cols-5 gap-8 relative">
      <ConfirmRemove
        open={deleteItem !== null}
        onOK={() => {
          deleteItem !== null && removeItem(deleteItem.group, deleteItem.id);
          setDeleteItem(null);
        }}
        onCancel={() => setDeleteItem(null)}
      />
      <div className="absolute -top-14 right-0">
        <DsButton label="Manager" onClick={() => navigator(ROUTES.LEADERS)} />
      </div>
      <div className="flex flex-col gap-4 col-span-3 lg:col-span-2">
        <CommonInfo
          form={form}
          setForm={setForm}
          isLoading={props?.isLoading}
        />
      </div>

      <div className="col-span-5">
        <DsTabs
          defaultActiveKey={StructureGroup?.[0].value}
          items={StructureGroup.map((g) => ({
            children: tabContent(g.value),
            key: g.value,
            label: g.label,
          }))}
          onChange={(group: string) => setActiveGroup(group)}
        />
      </div>

      <DsModal
        open={isModalOpen}
        onCancel={() => setIsModalOpen(false)}
        width={"70%"}
      >
        <LeaderListing handleItemClick={handleItemClick} isModalView />
      </DsModal>
    </div>
  );
};

export default Structure;
