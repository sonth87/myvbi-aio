import { PageCompType } from "@repo/types/page";
import { DsInput, DsSwitch } from "mvi-ds-ui";
import React, { FC } from "react";

type Props = {
  form: PageCompType;
  setForm: any;
  isLoading?: boolean;
};

const CommonInfo: FC<Props> = ({ form, setForm, isLoading }) => {
  return (
    <div className="flex flex-col gap-4">
      <DsInput
        label="Title"
        value={form?.title}
        onChange={(value) => setForm({ ...form, title: value?.toString() })}
        disabled={isLoading}
      />
      <DsInput
        type="textarea"
        label="Description"
        value={form?.description}
        onChange={(value) =>
          setForm({ ...form, description: value?.toString() })
        }
        disabled={isLoading}
      />
      <DsSwitch
        checkedChildren="show"
        unCheckedChildren="hide"
        value={form?.visible}
        onChange={(value: any) => setForm({ ...form, visible: value })}
        bgColor="green"
        label="Visible"
      />
    </div>
  );
};

export default CommonInfo;
