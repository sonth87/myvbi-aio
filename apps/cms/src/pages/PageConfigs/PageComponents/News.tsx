import React, { FC, useEffect, useState } from "react";
import { PageConfigProps } from "../../../type/page";
import CommonInfo from "./CommonInfo";
import { initFormData } from "../../../constants/pagesConfig";
import { pages } from "@repo/types";
import { DsIcon, DsInput, DsModal, DsSelect, DsTreeSelect } from "mvi-ds-ui";
import { useQuery } from "@tanstack/react-query";
import { getListCategoriesByStatus } from "../../../apis/category";
import { convertToCategorySelectTree } from "../../../constants/genaralFunction";
import ConfirmRemove from "../../../components/ConfirmRemove";
import ListNews from "./News/ListNews";
import { NewsSchemaType } from "../../../type/newsType";
import { NewsContentType } from "@repo/types/page";
import { getListNewsByIds } from "../../../apis/news";
import classNames from "classnames";

const SELECT_TYPE = {
  auto: {
    value: "auto",
    label: "Auto",
  },
  manual: {
    value: "manual",
    label: "Manual",
  },
};

const News: FC<PageConfigProps> = ({ type, data, onFormChange, ...props }) => {
  const initData = initFormData(type, data);console.log("initData",initData, data)
  const [form, setForm] = useState<pages.PageCompType>(initData);
  const [listContentFull, setListContentFull] = useState<NewsSchemaType[]>([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [listContent, setListContent] = useState<string[]>(
    (initData.content?.[0] as NewsContentType)?.list || []
  );
  const [selectType, setSelectType] = useState(
    (initData?.content?.[0] as NewsContentType)?.type || SELECT_TYPE.auto.value
  );
  const initList = (initData.content?.[0] as NewsContentType)?.list;
  const [deleteItem, setDeleteItem] = useState<{
    code: string;
    index: number;
  } | null>(null);

  const { data: dataCategorySelectTree, isLoading: loadingCategory } = useQuery(
    {
      queryKey: ["category-list-select-tree"],
      queryFn: () => getListCategoriesByStatus("active"),
      refetchOnWindowFocus: false,
    }
  );

  console.log(dataCategorySelectTree);

  const { data: selectedNews, isLoading: loadingNews } = useQuery({
    queryKey: ["category-list", initList],
    queryFn: () => initList?.length && getListNewsByIds(initList),
    refetchOnWindowFocus: false,
    enabled: !!initList?.length,
  });

  useEffect(() => {
    handleFormChange("type", selectType);
  }, [selectType]);

  useEffect(() => {
    handleFormChange("list", listContent);
  }, [listContent]);

  useEffect(() => {
    onFormChange?.(form);
  }, [form]);

  useEffect(() => {
    setForm(initData);
  }, [data]);

  useEffect(() => {
    setListContent((initData.content?.[0] as NewsContentType)?.list || []);
    setSelectType(
      (initData?.content?.[0] as NewsContentType)?.type ||
        SELECT_TYPE.auto.value
    );
  }, [data]);

  useEffect(() => {
    setListContentFull(selectedNews?.vi || []);
  }, [selectedNews]);

  const handleConfirmRemove = (code: string, index: number) => {
    setDeleteItem({ code, index });
  };

  const handleRemoveItem = (code: string, index: number) => {
    const _content: string[] = (listContent as string[]) || [];
    _content.splice(index, 1);
    setListContent(_content);
  };

  const onSelectItem = (data: NewsSchemaType) => {
    setListContent([...listContent, data?._id]);

    const _list = [...listContentFull];
    _list.push(data);
    setListContentFull(_list);
    setIsModalOpen(false);
  };

  const handleFormChange = (field: string, value: any) => {
    const _content = form?.content || [];
    if (!_content?.[0]) {
      _content.push({ type: selectType, [field]: value } as any);
    } else _content[0] = { ...(_content[0] as any), [field]: value };

    setForm({ ...form, content: _content as any });
  };

  const getFormField = (field: string) => {
    return (form?.content?.[0] as any)?.[field] || "";
  };

  return (
    <div className="grid grid-cols-1 md:grid-cols-5 gap-8">
      <ConfirmRemove
        open={deleteItem !== null}
        onOK={() => {
          deleteItem !== null &&
            handleRemoveItem(deleteItem.code, deleteItem.index);
          setDeleteItem(null);
        }}
        onCancel={() => setDeleteItem(null)}
      />

      <div className="flex flex-col gap-4 col-span-2">
        <CommonInfo
          form={form}
          setForm={setForm}
          isLoading={props?.isLoading}
        />

        <div>
          <DsSelect
            label="Type"
            options={[SELECT_TYPE.auto, SELECT_TYPE.manual]}
            value={selectType}
            onChange={(val) => setSelectType(val)}
          />
        </div>

        {selectType === SELECT_TYPE.auto.value && (
          <>
            <div>
              <DsTreeSelect
                label="Chọn danh mục tin tức"
                treeData={
                  (dataCategorySelectTree &&
                    convertToCategorySelectTree(dataCategorySelectTree.data)) ||
                  []
                }
                disabled={loadingCategory}
                value={getFormField("category")}
                onChange={(val: any) => handleFormChange("category", val)}
              />
            </div>
            <div>
              <DsInput
                type="number"
                min={0}
                max={20}
                step={1}
                label="Số bài hiển thị"
                value={getFormField("count")}
                onChange={(val) => handleFormChange("count", val)}
              />
            </div>
          </>
        )}

        <div>
          <DsInput
            label="Link view more"
            value={getFormField("viewMore")}
            onChange={(val) => handleFormChange("viewMore", val)}
          />
        </div>
      </div>

      {selectType === SELECT_TYPE.manual.value && (
        <div
          className={classNames("mt-4 col-span-5", {
            "select-none": loadingNews,
          })}
        >
          <div>Danh sách tin</div>
          <div className="grid grid-cols-2 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-7 2xl:grid-cols-9 mt-4 gap-4">
            {(form?.content?.[0] as NewsContentType)?.list?.map((id, idx) => {
              const findingNemo = listContentFull?.find((c) => c._id === id);
              if (!findingNemo) return null;
              return (
                <div
                  className="group relative border rounded overflow-hidden"
                  key={"p-selected-" + idx}
                >
                  {findingNemo?.trang_thai.value === "inactive" ||
                  findingNemo?.deleted ? (
                    <div className="top-0 left-0 w-full h-full flex justify-center items-center z-10 bg-blackOpacity200 text-white font-bold">
                      INACTIVE
                    </div>
                  ) : (
                    <div className="flex flex-col h-full">
                      <div className="h-[60%] flex justify-center items-center flex-1">
                        <img
                          src={findingNemo.anh.value}
                          className="object-cover h-full w-full object-center"
                        />
                      </div>
                      <div className="p-2 text-sm overflow-hidden">
                        <div className="line-clamp-2">
                          {findingNemo?.tieu_de?.value}
                        </div>
                      </div>
                    </div>
                  )}

                  <div
                    onClick={() => handleConfirmRemove(id, idx)}
                    className="group-hover:flex hidden absolute top-0 left-0 w-full h-full cursor-pointer bg-black bg-opacity-25 justify-center items-center text-white"
                  >
                    Remove
                  </div>
                </div>
              );
            })}
            <div
              className="w-full pb-full relative border-dashed border border-ink200 rounded-lg cursor-pointer"
              onClick={() => setIsModalOpen(true)}
            >
              <DsIcon
                name="icon-bx-plus"
                className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2"
              />
            </div>
          </div>

          <DsModal
            open={isModalOpen}
            onCancel={() => setIsModalOpen(false)}
            width={"70%"}
          >
            <ListNews onSelectItem={onSelectItem} />
          </DsModal>
        </div>
      )}
    </div>
  );
};

export default News;
