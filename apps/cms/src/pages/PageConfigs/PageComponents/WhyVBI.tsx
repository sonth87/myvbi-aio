import { pages } from "@repo/types";
import React, { FC, useEffect, useState } from "react";
import { PageConfigProps } from "../../../type/page";
import CommonInfo from "./CommonInfo";
import { useUpload } from "../../../hooks/useUpload";
import { initFormData } from "../../../constants/pagesConfig";
import { DsButton, DsImageView, DsInput, DsUpload } from "mvi-ds-ui";
import { BannerContentType } from "@repo/types/page";
import ConfirmRemove from "../../../components/ConfirmRemove";
import usePageConfig from "../../../hooks/usePageConfig";

const WhyVBI: FC<PageConfigProps> = ({
  type,
  data,
  onFormChange,
  ...props
}) => {
  const { uploadFile, isLoading } = useUpload();
  const initData = initFormData(type, data);
  const [listImg, setListImg] = useState<any>(initData.content || []);
  const [form, setForm] = useState<pages.PageCompType>(initData);
  const [deleteItem, setDeleteItem] = useState<number | null>(null);
  const { onDragOver, onDragStart, onDrop } = usePageConfig({
    listImg,
    setListImg,
  });

  const handleUploadFile = async (file: any) => {
    const img = await uploadFile(file);
    const _listImg = [...listImg, { img }];

    if (img) setListImg(_listImg);
  };

  const handleConfirmRemove = (index: number) => {
    setDeleteItem(index);
  };

  const removeItem = (index: number) => {
    let _listImg = [...listImg];
    _listImg.splice(index, 1);

    if (!_listImg.length) _listImg = [];
    setListImg(_listImg);
  };

  useEffect(() => {
    setForm({ ...form, content: listImg });
  }, [listImg]);

  useEffect(() => {
    onFormChange?.(form);
  }, [form]);

  useEffect(() => {
    if (data) {
      setForm(data);
      setListImg(data?.content as any);
    }
  }, [data]);

  const handleChangeAttr = (
    idx: number,
    field: string,
    value: string | number | null
  ) => {
    const _listImg = [...listImg];
    if (_listImg?.[idx]) {
      _listImg[idx] = { ..._listImg[idx], [field]: value };

      setListImg([..._listImg]);
    }
  };

  return (
    <div className="grid grid-cols-1 md:grid-cols-5 gap-8">
      <ConfirmRemove
        open={deleteItem !== null}
        onOK={() => {
          deleteItem !== null && removeItem(deleteItem);
          setDeleteItem(null);
        }}
        onCancel={() => setDeleteItem(null)}
      />
      <div className="flex flex-col gap-4 col-span-2">
        <CommonInfo
          form={form}
          setForm={setForm}
          isLoading={props?.isLoading}
        />
      </div>

      <div className="w-full flex flex-col col-span-3 items-center">
        <div className="mb-4">Preview</div>
        <div className="my-4 font-bold text-lg">{form?.title}</div>
        <div className="w-full grid grid-cols-4 gap-4">
          {listImg?.map((imgInfo: BannerContentType) => (
            <div
              key={imgInfo.img}
              className="p-2 flex flex-col items-center gap-4 border rounded"
            >
              <img
                src={imgInfo.img}
                alt={imgInfo.alt}
                className="h-[60%] w-auto object-contain"
              />
              <div className="text-sm text-center">{imgInfo.title}</div>
            </div>
          ))}
        </div>
      </div>

      <div className="w-full flex flex-col col-span-5 gap-2">
        {listImg?.map((imgInfo: BannerContentType, index: number) => (
          <div
            className="grid grid-cols-6 gap-4 w-full"
            key={imgInfo.img}
            id={`item-${index}`}
            draggable
            onDragOver={onDragOver}
            onDragStart={onDragStart}
            onDrop={onDrop}
          >
            <div className="h-16">
              <DsImageView src={imgInfo.img} height={64} />
            </div>
            <div className="col-span-2">
              <DsInput
                label="Link"
                value={imgInfo?.link}
                onChange={(value) => handleChangeAttr(index, "link", value)}
                disabled={props?.isLoading}
              />
            </div>
            <div className="col-span-2">
              <DsInput
                label="Title"
                value={imgInfo?.title}
                onChange={(value) => handleChangeAttr(index, "title", value)}
                disabled={props?.isLoading}
              />
            </div>
            <DsButton
              prefixIcon={"icon-bx-minus"}
              color="red"
              onClick={() => handleConfirmRemove(index)}
              disabled={props?.isLoading}
            />
          </div>
        ))}

        <DsUpload
          type="button"
          customRequest={handleUploadFile}
          showUploadList={false}
          label="Upload image"
          disabled={isLoading || props?.isLoading}
          accept="image/png, image/jpeg"
        />
      </div>
    </div>
  );
};

export default WhyVBI;
