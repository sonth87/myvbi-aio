import { pages } from "@repo/types";
import React, { FC, useState } from "react";
import { PageConfigProps } from "../../../type/page";
import { useUpload } from "../../../hooks/useUpload";
import { initFormData } from "../../../constants/pagesConfig";

const Invester: FC<PageConfigProps> = ({
  type,
  data,
  onFormChange,
  ...props
}) => {
  const { uploadFile, isLoading } = useUpload();
  const initData = initFormData(type, data);
  const [listImg, setListImg] = useState<any[]>(initData.content || []);
  const [form, setForm] = useState<pages.PageCompType>(initData);

  return <div>Tính năng đang phát triển</div>;
};

export default Invester;
