import { useQuery } from "@tanstack/react-query";
import { DsImageView, DsInput, DsTable, useDebounce } from "mvi-ds-ui";
import React, { FC, useState } from "react";
import { getListNewsConfig } from "../../../../apis/news";
import { NewsSchemaType } from "../../../../type/newsType";

type Props = {
  onSelectItem?: (data: NewsSchemaType) => void;
};

const ListNews: FC<Props> = ({ onSelectItem }) => {
  const [page, setPage] = useState<number>(1);
  const [pageSize, setPageSize] = useState<number>(10);
  const [search, setSearch] = useState("");
  const searchDebounded = useDebounce(search, 1000);

  const { data: dataTableNews, isLoading } = useQuery({
    queryKey: ["news-list", { tieu_de: searchDebounded }, pageSize, page],
    queryFn: () =>
      getListNewsConfig(pageSize, page, { tieu_de: searchDebounded }),
    refetchOnWindowFocus: false,
  });

  const handlePaginationChange = (page: number, pageSize: number) => {
    setPage(page);
    setPageSize(pageSize);
  };

  const selectNews = (data: NewsSchemaType) => {
    onSelectItem?.(data);
  };

  return (
    <div>
      <div className="my-3">
        <DsInput
          label="Tìm kiếm"
          onChange={(val) => setSearch(val?.toString() || "")}
        />
      </div>
      <DsTable
        bordered
        dataSource={dataTableNews?.data || []}
        loading={isLoading}
        rowKey={"_id"}
        columns={[
          {
            dataIndex: "anh",
            title: "Thumnail",
            with: "200px",
            key: "anh",
            render: (item: any) => <DsImageView src={item?.value} width={80} />,
          },
          {
            dataIndex: "tieu_de",
            title: "Tiêu đề",
            key: "tieu_de",
            render: (item: any, row: NewsSchemaType) => (
              <div
                onClick={() => selectNews(row)}
                className="cursor-pointer hover:text-blue-600"
              >
                {item?.value}
              </div>
            ),
          },
        ]}
        pagination={{
          pageSize: pageSize,
          total: dataTableNews?.totalCount,
          onChange: handlePaginationChange,
        }}
        className="w-full min-h-[603px] !inline"
      />
    </div>
  );
};

export default ListNews;
