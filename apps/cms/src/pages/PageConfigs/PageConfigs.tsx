import { DsTable } from "mvi-ds-ui";
import React, { useEffect } from "react";
import {
  COMPS,
  columnsPageList,
  componentList,
} from "../../constants/pagesConfig";
import usePageInfo from "../../hooks/usePageInfo";
import { useSearchParams } from "react-router-dom";
import UpdateComponent, { CompType } from "./UpdateComponent";
import { useQuery } from "@tanstack/react-query";
import { getAllPageConfig, getPageConfigByKey } from "../../apis/pageConfig";
import Loading from "../../components/Loading";
import { PageCompType } from "@repo/types/page";

const ConfigElmByPage = () => {
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const [searchParams] = useSearchParams();
  const selectedComp =
    searchParams.get("comp") &&
    Object.values(COMPS).includes(searchParams.get("comp") || "")
      ? searchParams.get("comp")
      : null;

  const { data, isLoading: isGetCompLoading } = useQuery({
    queryKey: ["get-pages-comp", selectedComp],
    queryFn: () => selectedComp && getPageConfigByKey(selectedComp),
    enabled: !!selectedComp,
  });

  const { data: allConfig, isLoading: isGetAllLoading } = useQuery({
    queryKey: ["pages-comp"],
    queryFn: getAllPageConfig,
  });

  useEffect(() => {
    setModuleInfo({
      module: { icon: "icon-bx-food-menu", title: "Quản trị nội dung website" },
      path: [{ label: "Page Config" }],
    });

    return () => cleanModuleInfo();
  }, []);

  const fixDataSource = () => {
    return componentList?.map((comp) => {
      const findingNemo = allConfig?.find(
        (all: PageCompType) => all?.key === comp.key
      );
      return {
        ...comp,
        name: findingNemo?.title || comp.name,
        description: findingNemo?.description,
        visible: findingNemo?.visible,
      };
    });
  };

  return (
    <div>
      {(isGetCompLoading || isGetAllLoading) && <Loading />}
      {selectedComp && (
        <UpdateComponent comp={selectedComp as CompType} data={data} />
      )}
      {!selectedComp && (
        <DsTable
          dataSource={fixDataSource()}
          columns={columnsPageList}
          className="w-full"
        />
      )}
    </div>
  );
};

export default ConfigElmByPage;
