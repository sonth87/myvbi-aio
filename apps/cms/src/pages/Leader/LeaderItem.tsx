import { LeadersType } from "@repo/types/leaders";
import classNames from "classnames";
import React, { FC } from "react";

type Props = {
  handleItemClick?: (id: string) => void;
  data?: LeadersType;
  className?: string;
};

const LeaderItem: FC<Props> = ({ data, handleItemClick, className }) => {
  return (
    <div
      key={data?.id}
      className={classNames(
        "border rounded flex flex-col cursor-pointer hover:shadow-lg duration-300",
        className
      )}
      onClick={() => data?.id && handleItemClick?.(data?.id)}
    >
      <div className="pb-full relative">
        <img
          src={data?.avatar}
          className="w-full h-full object-cover object-top absolute top-0 left-0"
        />
      </div>
      <div className="flex flex-col justify-center gap-2 p-3">
        <div className="text-center font-bold">{data?.name}</div>
        <div className="text-center text-sm text-ink300">{data?.title}</div>
      </div>
    </div>
  );
};

export default LeaderItem;
