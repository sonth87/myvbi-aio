import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import React, { useCallback, useEffect } from "react";
import usePageInfo from "../../hooks/usePageInfo";
import { ROUTES } from "../../constants/path";
import BackBtn from "../../components/BackBtn";
import { useFormik } from "formik";
import { leaderSchema } from "../../constants/validation";
import { DsButton, DsImageView, DsInput, DsUpload } from "mvi-ds-ui";
import { useUpload } from "../../hooks/useUpload";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";
import { LeadersType } from "@repo/types/leaders";
import { createLeader, getLeaderById, updateLeader } from "../../apis/leader";
import LeaderDelete from "./LeaderDelete";
import { PERMISSION } from "../../constants/user";

const LeaderForm = () => {
  const { notify, checkPermission } = useAuth();
  const navigate = useNavigate();
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const { image, uploadFile } = useUpload();
  const queryCache = useQueryClient();
  const [searchParams] = useSearchParams();
  const pId = searchParams.get("id");

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (data: any) => (pId ? updateLeader(data) : createLeader(data)),
  });

  const { data: pData, isLoading } = useQuery({
    queryKey: ["leader", pId],
    queryFn: () => getLeaderById(pId || ""),
    refetchOnWindowFocus: false,
    enabled: !!pId,
    retry: 1,
  });

  const form = useFormik<LeadersType>({
    initialValues: {
      id: pData?.id || undefined,
      name: pData?.name || "",
      title: pData?.title || "",
      avatar: pData?.avatar || "",
      shortDescription: pData?.shortDescription || "",
      description: pData?.description || "",
    },
    enableReinitialize: true,
    onSubmit: async (values) => {
      const rs = await mutateAsync({ id: form?.values?.id, form: values });

      notify?.({ type: rs ? "success" : "error" });
      navigate(ROUTES.LEADERS);
      queryCache.invalidateQueries({ queryKey: ["leader", pId] });
      queryCache.invalidateQueries({ queryKey: ["leader-list"] });
    },
    validationSchema: leaderSchema,
  });

  useEffect(() => {
    setModuleInfo({
      module: { icon: "icon-bx-group", title: "Leader" },
      path: [
        { label: "Leader", link: ROUTES.LEADERS },
        { label: form?.values?.name },
      ],
    });

    return () => cleanModuleInfo();
  }, []);

  const handleUploadFile = async (file: any) => {
    const img = await uploadFile(file);
    if (img) form.setFieldValue("avatar", img);
  };

  const getFieldErr = useCallback(
    (field: keyof LeadersType) => {
      const fErr = form.errors;
      return form.touched?.[field] && fErr?.[field] ? fErr?.[field] : null;
    },
    [form.errors, form.touched]
  );

  const isUpdate = pData?.id ? true : false;
  const loading = isPending || isLoading;

  if (pId && !loading && !pData) {
    return (
      <div>
        <div className="">
          <BackBtn />
        </div>
        <div className="w-full p-8 flex items-center justify-center text-xl">
          404 Không tìm thấy bản ghi
        </div>
      </div>
    );
  }

  return (
    <div>
      <div className="flex justify-between">
        <BackBtn />
        {isUpdate &&
          pData &&
          checkPermission(PERMISSION.LEADER_DELETE) && (
            <LeaderDelete pId={pId} />
          )}
      </div>
      <form onSubmit={form.handleSubmit}>
        <div className="grid grid-cols-12 gap-4 w-full mt-4">
          <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
            <div>Thông tin</div>
            <div>
              <DsInput
                label="Họ và tên"
                required
                disabled={loading}
                name="name"
                onChangeEvent={form.handleChange}
                value={form.values?.name}
                state={getFieldErr("name") ? "error" : "normal"}
                message={getFieldErr("name")}
              />
            </div>
            <div>
              <DsInput
                label="Chức danh"
                required
                disabled={loading}
                name="title"
                onChangeEvent={form.handleChange}
                value={form.values?.title}
                state={getFieldErr("title") ? "error" : "normal"}
                message={getFieldErr("title")}
              />
            </div>
            <div>
              <DsInput
                label="Mô tả ngắn"
                type="textarea"
                disabled={loading}
                name="shortDescription"
                onChangeEvent={form.handleChange}
                value={form.values?.shortDescription}
                state={getFieldErr("shortDescription") ? "error" : "normal"}
                message={getFieldErr("shortDescription")}
              />
            </div>
            <div>
              <DsInput
                label="Mô tả"
                type="textarea"
                disabled={loading}
                name="description"
                onChangeEvent={form.handleChange}
                value={form.values?.description}
                state={getFieldErr("description") ? "error" : "normal"}
                message={getFieldErr("description")}
              />
            </div>
          </div>

          <div className="col-span-12 md:col-span-6 flex flex-col gap-4">
            <div>Ảnh đại diện</div>
            <div className="text-center">
              <div className="relative text-center mx-auto p-4">
                {form.values?.avatar && (
                  <DsImageView
                    src={form.values?.avatar}
                    rootClassName="max-h-full"
                    height={250}
                  />
                )}
              </div>
              {getFieldErr("avatar") && (
                <div className="text-sm text-red-500 mt-1">
                  {getFieldErr("avatar")}
                </div>
              )}

              <div className="mt-4">
                <DsUpload
                  type="button"
                  customRequest={handleUploadFile}
                  showUploadList={false}
                  label="Upload icon"
                  disabled={loading}
                  accept="image/png, image/jpeg"
                />
              </div>
            </div>
          </div>
        </div>

        <DsButton
          label={isUpdate ? "Cập nhật" : "Tạo mới"}
          prefixIcon="icon-bxs-save"
          buttonType="submit"
          className="mt-4"
        />
      </form>
    </div>
  );
};

export default LeaderForm;
