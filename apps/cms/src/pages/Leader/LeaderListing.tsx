import { useQuery } from "@tanstack/react-query";
import { FC } from "react";
import { getAllLeaders } from "../../apis/leader";
import LeaderItem from "./LeaderItem";
import classNames from "classnames";

type Props = {
  handleItemClick?: (id: string) => void;
  isModalView?: boolean;
};

const LeaderListing: FC<Props> = ({ handleItemClick, isModalView }) => {
  const { data } = useQuery({
    queryKey: ["leader-list"],
    queryFn: getAllLeaders,
    refetchOnWindowFocus: false,
  });

  return (
    <>
      <div
        className={classNames({
          "grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6 2xl:grid-cols-7 gap-4":
            !isModalView,
          "grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 2xl:grid-cols-6 gap-4":
            isModalView,
        })}
      >
        {data?.map((d, idx) => (
          <div className="group relative" key={"p-selected-" + idx}>
            <LeaderItem key={d.id} handleItemClick={handleItemClick} data={d} />
          </div>
        ))}
      </div>
    </>
  );
};

export default LeaderListing;
