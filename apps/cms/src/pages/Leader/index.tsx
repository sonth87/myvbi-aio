import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { DsButton } from "mvi-ds-ui";
import BackBtn from "../../components/BackBtn";
import LeaderListing from "./LeaderListing";
import usePageInfo from "../../hooks/usePageInfo";

const Leaders = () => {
  const { setModuleInfo, cleanModuleInfo } = usePageInfo();
  const navigate = useNavigate();

  const handleItemClick = (id?: string) => {
    if (id) navigate(`${ROUTES.LEADERS_UPDATE}?id=${id}`);
  };

  const handleCreate = () => {
    navigate(ROUTES.LEADERS_CREATE);
  };

  useEffect(() => {
    setModuleInfo({
      module: { icon: "icon-bx-group", title: "Leader" },
      path: [{ label: "Leader" }],
    });

    return () => cleanModuleInfo();
  }, []);

  return (
    <div>
      <div className="w-full text-right">
        <div className="flex justify-between">
          <BackBtn link="/config-page?comp=structure" />
          <DsButton
            label="Tạo mới"
            prefixIcon="icon-bx-plus"
            onClick={handleCreate}
          />
        </div>

        <LeaderListing handleItemClick={handleItemClick} />
      </div>
    </div>
  );
};

export default Leaders;
