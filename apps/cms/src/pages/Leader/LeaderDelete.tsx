import { useMutation, useQueryClient } from "@tanstack/react-query";
import { DsButton, DsModal, message } from "mvi-ds-ui";
import React, { FC, useState } from "react";
import { deleteProduct } from "../../apis/product";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../constants/path";
import { deleteLeader } from "../../apis/leader";

type Props = {
  pId: string | null;
};

const LeaderDelete: FC<Props> = ({ pId }) => {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const [messageApi, contextHolder] = message.useMessage();
  const queryCache = useQueryClient();

  const { mutateAsync, isPending: pendingDelete } = useMutation({
    mutationFn: (id: string) => deleteLeader(id),
  });

  const handleRemoveLeader = async () => {
    if (pId) {
      const rs = await mutateAsync(pId);

      messageApi.open({
        type: rs ? "success" : "error",
        content: rs ? "Đã xóa" : "Đã xảy ra lỗi, vui lòng thử lại sau.",
        duration: 5,
        className: "right",
      });
      navigate(ROUTES.LEADERS);
      queryCache.invalidateQueries({ queryKey: ["leader-list"] });
    }
  };

  return (
    <div>
      {contextHolder}
      <DsButton
        prefixIcon="icon-bx-trash"
        color="red"
        onClick={() => {
          setIsOpen(true);
        }}
      />
      <DsModal
        open={isOpen}
        onCancel={() => (pendingDelete ? null : setIsOpen(false))}
        footer={[
          <DsButton
            key="confirm-del-leader"
            isLoading={pendingDelete}
            onClick={handleRemoveLeader}
            color="red"
            disabled={pendingDelete}
          >
            {pendingDelete ? "Đang xóa" : "Xác nhận xóa"}
          </DsButton>,
          <DsButton
            key="cancel-del-leader"
            onClick={() => setIsOpen(false)}
            className="ds-ml-4"
            disabled={pendingDelete}
          >
            Thoát
          </DsButton>,
        ]}
      >
        Bạn có chắc chắn muốn xóa không?
      </DsModal>
    </div>
  );
};

export default LeaderDelete;
