import React, { useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { TOKEN_KEY } from "../constants/const";

const Redirect = () => {
  const [searchParams] = useSearchParams();
  const code = searchParams.get("c");

  useEffect(() => {
    if (code) window.localStorage.setItem(TOKEN_KEY, code);
    window.location.replace("/");
  }, [code]);

  return <></>;
};

export default Redirect;
