import { message } from "mvi-ds-ui";
import React, {
  FC,
  Dispatch,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";
import { NotifyType } from "../type/common";
import { getUserInfo } from "../apis/user";
import { getHa, setHa } from "../utils/getHa";
import { UserType } from "@repo/types/user";

const AuthContext = createContext<{
  isLoading: boolean;
  setIsLoading: Dispatch<any>;
  isAuth?: boolean;
  notify?: (n: NotifyType) => void;
  user: UserType | null;
  checkPermission: (action: string) => boolean;
}>({
  isLoading: false,
  setIsLoading: () => {},
  isAuth: false,
  notify: () => {},
  user: { email: "", name: "" },
  checkPermission: () => false,
});

const AuthenticationProvider: FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [messageApi, contextHolder] = message.useMessage();
  const [isLoading, setIsLoading] = useState(true);
  const [user, setUser] = useState<UserType | null>(null);
  const [isAuth, setIsAuth] = useState<boolean>();

  const getUser = async () => {
    const userInfo = await getUserInfo();
    // const userInfo = window.localStorage.getItem(TOKEN_KEY);
    setIsLoading(false);

    if (userInfo?.email) {
      // userInfo?.user
      setUser({ ...userInfo?.user, email: userInfo?.email });
      setIsAuth(true);
    } else {
      setUser(null);
      setIsAuth(false);
    }
  };

  useEffect(() => {
    setHa(getHa());
    setIsLoading(true);
    getUser();
  }, []);

  const notify = ({ type, content }: NotifyType) => {
    messageApi.open({
      type: type,
      content:
        content || type === "success"
          ? content || "Cập nhật thành công"
          : content || "Đã xảy ra lỗi, vui lòng thử lại sau.",
      duration: 3,
      className: "center translate-y-16",
    });
  };

  const checkPermission = (action: string) => {
    return user?.permission?.includes(action) || false;
  };

  return (
    <AuthContext.Provider
      value={{ isLoading, setIsLoading, isAuth, notify, user, checkPermission }}
    >
      {contextHolder}
      {children}
    </AuthContext.Provider>
  );
};

const useAuth = () => useContext(AuthContext);
export { AuthContext, useAuth };
export default AuthenticationProvider;
