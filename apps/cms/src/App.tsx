import React from "react";
import "./App.css";
import "./tailwind.css";
import "mvi-ds-ui/styles.css";
import "swiper/css";
import "swiper/css/pagination";
import { RecoilRoot } from "recoil";
import { BrowserRouter as Router } from "react-router-dom";
import DefaultLayOutPage from "./components/DefaultLayOutPage";
import AuthenticationProvider from "./context/AuthContext";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

// Create a client
const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <RecoilRoot>
        <Router>
          <AuthenticationProvider>
            <div className="App">
              <DefaultLayOutPage />
            </div>
          </AuthenticationProvider>
        </Router>
      </RecoilRoot>
    </QueryClientProvider>
  );
}

export default App;
