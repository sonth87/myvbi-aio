import React from "react";
import { ROUTES } from "../constants/path";
import { PrivateRouteProperties, PublicRouteProperties } from "../type/route";
import { PERMISSION } from "../constants/user";
import { useAuth } from "../context/AuthContext";

const LoginAsync = React.lazy(() => import("../pages/Authen/Login"));
const Err404Async = React.lazy(() => import("../pages/Err404Page"));
const LoginSuccessAsync = React.lazy(() => import("../pages/redirect"));

const publicRoutes: PublicRouteProperties[] = [
  {
    path: ROUTES.LOGIN,
    component: LoginAsync,
  },
  {
    path: ROUTES.LOGIN_SUCCESS,
    component: LoginSuccessAsync,
  },
];

const DashboardPage = React.lazy(() => import("../pages/Dashboard"));

const NewsPage = React.lazy(() => import("../pages/News"));
const NewsCreatePage = React.lazy(() => import("../pages/News/createNews"));

const CategoryPage = React.lazy(() => import("../pages/Categories"));
const CategoriesCreatePage = React.lazy(
  () => import("../pages/Categories/createCategories")
);

const PageConfig = React.lazy(() => import("../pages/PageConfigs"));
const GeneralConfig = React.lazy(
  () => import("../pages/PageConfigs/GeneralConfig")
);
const ProductConfig = React.lazy(() => import("../pages/Products"));
const ProductForm = React.lazy(() => import("../pages/Products/ProductForm"));
const LeaderConfig = React.lazy(() => import("../pages/Leader"));
const LeaderForm = React.lazy(() => import("../pages/Leader/LeaderForm"));

const UserList = React.lazy(() => import("../pages/User"));
const UserForm = React.lazy(() => import("../pages/User/UserForm"));
const privateRoutes: PrivateRouteProperties[] = [
  {
    path: ROUTES.DASHBOARD,
    component: DashboardPage,
  },
  {
    path: ROUTES.ERROR,
    component: Err404Async,
  },
  {
    path: ROUTES.NEWS,
    component: NewsPage,
  },
  {
    path: ROUTES.NEWS_CREATE,
    component: NewsCreatePage,
  },
  {
    path: ROUTES.NEWS_VIEW,
    component: NewsCreatePage,
  },
  {
    path: ROUTES.NEWS_UPDATE,
    component: NewsCreatePage,
  },
  {
    path: ROUTES.CATEGORY,
    component: CategoryPage,
  },
  {
    path: ROUTES.CATEGORY_VIEW,
    component: CategoriesCreatePage,
  },
  {
    path: ROUTES.CATEGORY_CREATE,
    component: CategoriesCreatePage,
  },
  {
    path: ROUTES.CATEGORY_UPDATE,
    component: CategoriesCreatePage,
  },
  {
    path: ROUTES.PAGE_CONFIG,
    component: PageConfig,
  },
  {
    path: ROUTES.GENERAL_CONFIG,
    component: GeneralConfig,
  },
  {
    path: ROUTES.PRODUCTS,
    component: ProductConfig,
  },
  {
    path: ROUTES.PRODUCTS_CREATE,
    component: ProductForm,
  },
  {
    path: ROUTES.PRODUCTS_UPDATE,
    component: ProductForm,
  },
  {
    path: ROUTES.LEADERS,
    component: LeaderConfig,
  },
  {
    path: ROUTES.LEADERS_CREATE,
    component: LeaderForm,
  },
  {
    path: ROUTES.LEADERS_UPDATE,
    component: LeaderForm,
  },
  {
    path: ROUTES.USERS,
    component: UserList,
  },
  {
    path: ROUTES.USERS_CREATE,
    component: UserForm,
  },
  {
    path: ROUTES.USERS_UPDATE,
    component: UserForm,
  },
];
export { publicRoutes, privateRoutes };
