import { FC, useCallback, useEffect, useState } from "react";
import { DsInput, DsTreeSelect, DsSelect, DsUpload } from "mvi-ds-ui";
import {
  convertToCategorySelectTree,
  convertToSlug,
  filterData,
} from "../../constants/genaralFunction";
import {
  STT_GROUP,
  StatusGroup,
  UPDATE_FORM,
  VIEW_FORM,
} from "../../constants/const";
import { useQuery } from "@tanstack/react-query";
import { getListCategoriesByStatus } from "../../apis/category";
import { useSearchParams } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";
import ImgThumnail from "../../pages/News/ImgThumnail";
import { useUpload } from "../../hooks/useUpload";

type Props = {
  lang: "vi" | "en" | "kr";
  form: any;
  typeForm: string | null;
  data: any;
};

const FormCategory: FC<Props> = ({ lang, form, typeForm, data }) => {
  const [value, setValue] = useState<string | null>();
  const [imgBanner, setImgBanner] = useState<string>("");
  const { uploadFile } = useUpload();
  const { isLoading } = useAuth();
  const [searchParams] = useSearchParams();
  const pId: any = searchParams.get("id");

  useEffect(() => {
    if (typeForm === UPDATE_FORM || typeForm === VIEW_FORM) {
      setValue(data?.[`${lang}`]?.ma_goc);
      setImgBanner(`${data?.[`${lang}`]?.banner_category?.value}`);
    } else {
      setValue(form.values?.[`${lang}`]?.ma_goc);
    }
  }, [data, lang]);

  const onChange = (newValue: string | null, label: any, extra: any) => {
    setValue(newValue && newValue.length > 0 ? newValue : null);
    form.setFieldValue(
      `${lang}.ma_goc`,
      newValue && newValue.length > 0 ? newValue : null
    );
  };

  const hanldeChangeSelectStatus = (e: string) => {
    form.setFieldValue(`${lang}.trang_thai.value`, e);
  };

  const handleChangeInput = (name: string, e: string) => {
    form.setFieldValue(`${lang}.${name}.value`, e);
    if (name === "tieu_de") {
      handleChangeSlug(e);
    }
  };

  const handleChangeSlug = (e: string) => {
    const newSlug = convertToSlug(e);
    form.setFieldValue(`${lang}.slug.value`, newSlug);
  };

  const handleUploadFileBanner = async (file: any) => {
    const img = await uploadFile(file.file);
    if (img) {
      setImgBanner(img);
      form.setFieldValue(`${lang}.banner_category.value`, img);
    }
  };

  const handleChangeInputLinkBanner = (name: string, e: any) => {
    form.setFieldValue(`${lang}.link_banner_category`, e);
  };

  const getFieldErr = useCallback(
    (field: string) => {
      const fErr = form.errors[`${lang}`];
      return form.touched[`${lang}`]?.[field].value && fErr?.[field]?.value
        ? fErr?.[field]?.value
        : null;
    },
    [form.errors[`${lang}`], form.touched[`${lang}`]]
  );

  const { data: dataCategory } = useQuery({
    queryKey: ["category-list-select-tree", lang],
    queryFn: () => getListCategoriesByStatus("active"),
    refetchOnWindowFocus: false,
    enabled: !!lang,
  });

  const DataNull = [
    {
      label: "Không",
      value: "",
      trang_thai: "active",
    },
  ];

  const dataCategorySelectTree =
    dataCategory &&
    filterData(
      convertToCategorySelectTree(dataCategory.data).concat(DataNull),
      pId
    );

  const tProps = {
    treeData: dataCategorySelectTree || [],
    value,
    onChange,
    treeCheckable: false,
    treeLine: true,
    showCheckedStrategy: "SHOW_PARENT",
    label: "Danh mục cha",
    style: {
      height: "54px",
    },
    required: true,
  };

  const loading = isLoading || typeForm === VIEW_FORM;

  return (
    <div className="grid grid-rows gap-4">
      <div className="grid grid-cols-2 gap-4">
        <DsInput
          required
          label="Tiêu đề"
          name={`${lang}.tieu_de`}
          onChange={(e) => handleChangeInput("tieu_de", e?.toString() || "")}
          value={form.values[`${lang}`]?.tieu_de?.value}
          state={getFieldErr("tieu_de") ? "error" : "normal"}
          message={getFieldErr("tieu_de")}
          placeholder="Ex: Thể thao"
          disabled={loading}
        />

        <DsInput
          label="Slug"
          name={`${lang}.slug`}
          value={form.values[`${lang}`].slug.value}
          onChange={(e) => handleChangeInput("slug", e?.toString() || "")}
          state={getFieldErr("slug") ? "error" : "normal"}
          message={getFieldErr("slug")}
          placeholder="The-thao"
          required
          disabled={loading}
        />

        <DsTreeSelect
          {...tProps}
          size="large"
          treeDefaultExpandAll
          disabled={loading}
        />

        {/* Trạng thái */}
        <DsSelect
          label="Trạng thái"
          name={`${lang}.trang_thai`}
          value={form.values[`${lang}`].trang_thai.value}
          defaultValue={STT_GROUP.ACTIVE}
          onChange={(e) => {
            hanldeChangeSelectStatus(e);
          }}
          size="large"
          required
          options={
            Object.entries(StatusGroup).map((p) => ({
              label: p[1],
              value: p[0],
            })) || []
          }
          disabled={loading}
        />
      </div>

      <DsInput
        type="textarea"
        required
        label="Mô tả"
        onChange={(e) => handleChangeInput("mo_ta", e?.toString() || "")}
        name={`${lang}.mo_ta`}
        value={form.values[`${lang}`].mo_ta.value}
        state={getFieldErr("mo_ta") ? "error" : "normal"}
        message={getFieldErr("mo_ta")}
        disabled={loading}
      />
      <div className="grid grid-cols-1 md:grid-cols-8 gap-4">
        <div className="col-span-3">
          <DsUpload
            type="drag-drop"
            accept="image/*"
            customRequest={handleUploadFileBanner}
            showUploadList={false}
            disabled={loading}
          />
        </div>
        <div className="col-span-5">
          {imgBanner && (
            <div className="ml-3">
              <ImgThumnail product={imgBanner} />
              <DsInput
                className="mt-3"
                label="Link Banner"
                name={`${lang}.link_banner_category`}
                value={form.values[`${lang}`]?.link_banner_category}
                onChange={(e) => {
                  handleChangeInputLinkBanner("link_banner_category", e);
                }}
                placeholder="Ex: https://myvbi.vn/..."
                disabled={loading}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default FormCategory;
