import { FC, useCallback, useEffect, useState } from "react";
import {
  DsDatePicker,
  DsEditor,
  DsInput,
  DsSelect,
  DsUpload,
  DsTreeSelect,
} from "mvi-ds-ui";
import {
  STT_GROUP,
  SYS_GROUP,
  StatusGroup,
  SystemGroup,
  UPDATE_FORM,
  VIEW_FORM,
} from "../../constants/const";
import {
  convertDateTime,
  convertToCategorySelectTree,
  convertToSlug,
  convertToTimestamp,
} from "../../constants/genaralFunction";
import { useUpload } from "../../hooks/useUpload";
import ImgThumnail from "../../pages/News/ImgThumnail";
import { useQuery } from "@tanstack/react-query";
import { getListCategoriesByStatus } from "../../apis/category";
import { useAuth } from "../../context/AuthContext";

type Props = {
  lang: "vi" | "en" | "kr";
  form: any;
  data: any;
  typeForm: string | null;
};

const FormNews: FC<Props> = ({ lang, form, data, typeForm }) => {
  const { uploadFile } = useUpload();
  const { isLoading, notify } = useAuth();
  const [dataContent, setDataContent] = useState<string>("");
  const [imgThumnail, setImgThumnail] = useState<string>("");
  const [imgBanner, setImgBanner] = useState<string>("");
  const [ngayTao, setNgayTao] = useState<string>(
    convertDateTime(form.values?.[lang].ngay_tao.value)
  );
  const [value, setValue] = useState<string | null>(null);

  useEffect(() => {
    if (
      (data && typeForm === UPDATE_FORM) ||
      (data && typeForm === VIEW_FORM)
    ) {
      setValue(data?.[`${lang}`]?.ma_dm?.value || null);
      setDataContent(data?.[`${lang}`]?.noi_dung?.value || "");
      setImgThumnail(`${data?.[`${lang}`]?.anh?.value}`);
      setImgBanner(`${data?.[`${lang}`]?.anh_banner?.value}`);
      setNgayTao(
        convertDateTime(data?.[`${lang}`]?.ngay_tao?.value.toString())
      );
    }
  }, [data]);

  const { data: dataCategorySelectTree } = useQuery({
    queryKey: ["category-list-select-tree", lang],
    queryFn: () => getListCategoriesByStatus("active"),
    refetchOnWindowFocus: false,
    enabled: !!lang,
  });

  const handleSelectChange = (newValue: string | null) => {
    setValue(newValue);
    form.setFieldValue(`${lang}.ma_dm.value`, newValue);
  };

  const hanldeChangeSelectSystem = (e: string) => {
    form.setFieldValue(`${lang}.he_thong.value`, e);
  };

  const hanldeChangeSelectStatus = (e: string) => {
    form.setFieldValue(`${lang}.trang_thai.value`, e);
  };

  const handleEditorChange = (event: any, editor: any) => {
    const data: string = editor.getData();
    setDataContent(data);
    form.setFieldValue(`${lang}.noi_dung.value`, data);
  };

  const handleUploadFile = async (file: any) => {
    // Validate file type
    const isJpgOrPng =
      file.file.type === "image/jpeg" || file.file.type === "image/png";

    if (!isJpgOrPng) {
      notify?.({
        type: "error",
        content: "Chỉ cho phép upload file ảnh định dạng PNG hoặc JPG",
      });
      return;
    }

    try {
      // Upload the file if the validation passes
      const img = await uploadFile(file.file);

      if (img) {
        // Set the uploaded image and form field values
        setImgThumnail(img);
        form.setFieldValue(`${lang}.anh.value`, img);
      }
    } catch (error) {
      notify?.({
        type: "error",
        content: "Upload ảnh không thành công",
      });
    }
  };

  const handleUploadFileBanner = async (file: any) => {
    const img = await uploadFile(file.file);
    if (img) {
      setImgBanner(img);
      form.setFieldValue(`${lang}.anh_banner.value`, img);
    }
  };

  const handleChangeInput = (name: string, e: any) => {
    if (name === "tieu_de") {
      handleChangeSlug(e);
    }
    form.setFieldValue(`${lang}.${name}.value`, e);
  };

  const handleChangeInputLinkBanner = (name: string, e: any) => {
    form.setFieldValue(`${lang}.link_banner`, e);
  };

  const handleChangeSlug = (e: string) => {
    const newSlug = convertToSlug(e);
    form.setFieldValue(`${lang}.slug.value`, newSlug);
  };

  const getFieldErr = useCallback(
    (field: string) => {
      const fErr = form.errors[`${lang}`];
      return form.touched[`${lang}`]?.[field].value && fErr?.[field]?.value
        ? fErr?.[field]?.value
        : null;
    },
    [form.errors[`${lang}`], form.touched[`${lang}`]]
  );

  const onDateChange = (date: any, str: string) => {
    form.setFieldValue(`${lang}.ngay_tao.value`, convertToTimestamp(`${str}`));
    setNgayTao(str);
  };

  const tProps = {
    treeData:
      (dataCategorySelectTree &&
        convertToCategorySelectTree(dataCategorySelectTree.data)) ||
      [],
    value,
    onChange: handleSelectChange,
    treeCheckable: false, //true cho phép chọn nhiều giá trị
    treeLine: true,
    showCheckedStrategy: "SHOW_PARENT",
    label: "Danh mục",
    style: {
      height: "54px",
    },
    required: true,
  };

  const loading = isLoading || typeForm === VIEW_FORM;

  return (
    <>
      <div className="grid grid-cols-4 gap-4 pb-4">
        <div className="col-span-12 md:col-span-3 flex flex-col gap-4">
          <div>
            <DsInput
              required
              label="Tiêu đề"
              name={`${lang}.tieu_de`}
              value={form.values[`${lang}`]?.tieu_de?.value}
              onChange={(e) => {
                handleChangeInput("tieu_de", e);
              }}
              state={getFieldErr("tieu_de") ? "error" : "normal"}
              message={getFieldErr("tieu_de")}
              placeholder="Ex: Thể thao"
              disabled={loading}
            />
          </div>
          <div>
            <DsInput
              label="Slug"
              name={`${lang}.slug`}
              value={form.values[`${lang}`]?.slug?.value}
              onChange={(e) => handleChangeSlug(e?.toString() || "")}
              state={getFieldErr("slug") ? "error" : "normal"}
              message={getFieldErr("slug")}
              placeholder="The-thao"
              required
              disabled={loading}
            />
          </div>

          <div className="grid grid-cols-2 gap-4">
            <DsTreeSelect
              {...tProps}
              allowClear
              treeDefaultExpandAll
              disabled={loading}
              state={getFieldErr("ma_dm") ? "error" : "normal"}
              message={getFieldErr("ma_dm")}
            />
            <div>
              <DsSelect
                label="Hệ thống"
                name={`${lang}.he_thong`}
                value={form.values[`${lang}`]?.he_thong?.value}
                defaultValue={SYS_GROUP.WEB}
                onChange={(e) => {
                  hanldeChangeSelectSystem(e);
                }}
                size="large"
                required
                options={
                  Object.entries(SystemGroup).map((p) => ({
                    label: p[1],
                    value: p[0],
                  })) || []
                }
                disabled={true}
              />
            </div>
          </div>
          <div>
            <DsInput
              size="large"
              type="textarea"
              required
              inputStyle="float-label"
              label="Mô tả"
              onChange={(e) => handleChangeInput("mo_ta", e?.toString() || "")}
              name={`${lang}.mo_ta`}
              value={form.values[`${lang}`]?.mo_ta?.value}
              state={getFieldErr("mo_ta") ? "error" : "normal"}
              message={getFieldErr("mo_ta")}
              disabled={loading}
            />
          </div>
          <div>
            <DsEditor
              disabled={loading}
              onChange={handleEditorChange}
              content={dataContent}
              onUploadImage={(file) => {
                return new Promise(async (resolve, reject) => {
                  const img = await uploadFile(file);
                  resolve({ default: img || "" });
                });
              }}
            />

            {form.errors[lang]?.noi_dung && (
              <p className="text-[#ff5959] mt-1">
                {form?.errors[lang]?.noi_dung.value}
              </p>
            )}
          </div>
          <div className="grid grid-cols-3 gap-4">
            {/* số thứ tự */}
            <DsInput
              placeholder=""
              label="Số thứ tự"
              name={`${lang}.thu_tu`}
              onChange={(e) => handleChangeInput("thu_tu", e?.toString() || "")}
              value={form.values[`${lang}`]?.thu_tu?.value}
              state={getFieldErr("thu_tu") ? "error" : "normal"}
              message={getFieldErr("thu_tu")}
              disabled={loading}
            />
            {/* Tag */}
            <DsInput
              label="Tag"
              name={`${lang}.tag`}
              onChange={(e) => handleChangeInput("tag", e?.toString() || "")}
              value={form.values[`${lang}`]?.tag?.value}
              state={getFieldErr("tag") ? "error" : "normal"}
              message={getFieldErr("tag")}
              placeholder=""
              disabled={loading}
            />
            {/* Ngày tạo */}
            <DsDatePicker
              allowClear
              format="HH:mm DD/MM/YYYY"
              label="Ngày tạo"
              showTime
              value={ngayTao}
              name={`${lang}.ngay_tao`}
              onChange={onDateChange}
              state={getFieldErr("ngay_tao") ? "error" : "normal"}
              message={getFieldErr("ngay_tao")}
              picker="date"
              disabled={loading}
            />
          </div>
        </div>
        <div className="col-span-12 md:col-span-1 flex flex-col gap-4">
          {/* Cài đặt */}
          <div>
            <div className=" bg-white rounded-lg">
              <div className="pb-4 border-b-[1px] border-[#DBE0E6]">
                <span className="font-bold text-base flex">Cài đặt</span>
              </div>
              <div className="mt-4">
                {/* Trạng thái */}
                <div>
                  <DsSelect
                    label="Trạng thái"
                    name={`${lang}.trang_thai`}
                    value={form?.values[`${lang}`]?.trang_thai?.value}
                    defaultValue={STT_GROUP.ACTIVE}
                    onChange={(e) => {
                      hanldeChangeSelectStatus(e);
                    }}
                    size="large"
                    required
                    options={
                      Object.entries(StatusGroup).map((p) => ({
                        label: p[1],
                        value: p[0],
                      })) || []
                    }
                    disabled={loading}
                  />
                </div>

                {/* Người tạo */}
                <div className="mt-4">
                  <DsInput
                    label="Tác giả"
                    name={`${lang}.nguoi_tao`}
                    onChange={(e) => handleChangeInput("nguoi_tao", e)}
                    value={form.values[`${lang}`]?.nguoi_tao?.value}
                    state={getFieldErr("nguoi_tao") ? "error" : "normal"}
                    message={getFieldErr("nguoi_tao")}
                    disabled={loading}
                  />
                </div>
              </div>
            </div>
          </div>

          {/* Ảnh thumnail */}
          <div>
            <div className="pb-2 border-b-[1px] border-[#DBE0E6]">
              <span className="flex font-bold text-base">Ảnh thumbnail</span>
            </div>
            <div className="mt-3">
              <DsUpload
                type="drag-drop"
                accept="image/*"
                customRequest={handleUploadFile}
                showUploadList={false}
                disabled={loading}
                className="border-rose-500"
              />
              {form?.errors[lang]?.anh && (
                <p className="text-[#ff5959] mt-1">
                  {form?.errors[lang]?.anh.value}
                </p>
              )}
              {imgThumnail && (
                <div className="mt-4">
                  <ImgThumnail product={imgThumnail} />
                </div>
              )}
            </div>
          </div>

          {/* Ảnh Banner */}
          {/* <div>
            <div className="pb-2 border-b-[1px] border-[#DBE0E6]">
              <span className="flex font-bold text-base">Ảnh banner</span>
            </div>
            <div className="mt-3">
              <DsUpload
                type="drag-drop"
                accept="image/*"
                customRequest={handleUploadFileBanner}
                showUploadList={false}
                disabled={loading}
              />
              {imgBanner && (
                <>
                  <div className="mt-4">
                    <ImgThumnail product={imgBanner} />
                  </div>
                  <DsInput
                    className="mt-3"
                    label="Link Banner"
                    name={`${lang}.link_banner`}
                    value={form.values[`${lang}`]?.link_banner}
                    onChange={(e) => {
                      handleChangeInputLinkBanner("link_banner", e);
                    }}
                    placeholder="Ex: https://myvbi.vn/..."
                    disabled={loading}
                  />
                </>
              )}
            </div>
          </div> */}
        </div>
      </div>
    </>
  );
};

export default FormNews;

{
  /*<DsInput
    inputStyle="float-label"
    label="Mô tả SEO"
    name={`${lang}.mo_ta_tu_khoa`}
    onChange={(e) =>
      handleChangeInput("mo_ta_tu_khoa", e?.toString() || "")
    }
    value={form.values[`${lang}`]?.mo_ta_tu_khoa?.value}
    state={getFieldErr("mo_ta_tu_khoa") ? "error" : "normal"}
    message={getFieldErr("mo_ta_tu_khoa")}
    placeholder=""
    size="large"
    type="text"
    disabled={loading}
  /> 

  <DsInput
    inputStyle="float-label"
    label="SEO"
    name={`${lang}.tu_khoa`}
    onChange={(e) =>
      handleChangeInput("tu_khoa", e?.toString() || "")
    }
    value={form.values[`${lang}`]?.tu_khoa?.value}
    state={getFieldErr("tu_khoa") ? "error" : "normal"}
    message={getFieldErr("tu_khoa")}
    placeholder=""
    size="large"
    type="text"
    disabled={loading}
  />*/
}
