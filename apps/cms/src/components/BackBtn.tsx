import { DsIcon } from "mvi-ds-ui";
import React, { FC } from "react";
import { useNavigate } from "react-router-dom";

type Props = {
  link?: string;
};

const BackBtn: FC<Props> = ({ link }) => {
  const navigate = useNavigate();

  const goBack = () => {
    if (link) navigate(link);
    else navigate(-1);
  };

  return (
    <div
      className="flex w-fit items-center text-blue-500 cursor-pointer gap-1 mb-8"
      onClick={goBack}
    >
      <DsIcon name="icon-bx-chevron-left" className="" />
      Quay lại
    </div>
  );
};

export default BackBtn;
