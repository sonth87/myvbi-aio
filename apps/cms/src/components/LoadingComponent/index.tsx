import React from "react";

const LoadingComponent = () => {
  return (
    <div className="background-loading">
      <img src="/images/loading.gif" alt="loading" className="icon-loading" />
    </div>
  );
};

export default LoadingComponent;
