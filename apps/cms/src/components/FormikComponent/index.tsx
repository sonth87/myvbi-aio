// import React, { FC } from "react";
// import classNames from "classnames";
// import { DsEditor, DsInput, DsSelect, DsUpload } from "mvi-ds-ui";
// import { DsEditorProps } from "mvi-ds-ui/lib/components/Editor/CkEditor";
// import { DsInputProps } from "mvi-ds-ui/lib/components/Form/Input/Input";
// import { SelectProps } from "mvi-ds-ui/lib/components/Form/Select/Select";
// import { DsUploadProps } from "mvi-ds-ui/lib/components/Uploads/Upload";
// import { useField } from "formik";

// type CustomInputProps = {
//   fieldtype?: "input" | "textarea";
//   defaultValue?: string;
// } & DsInputProps;

// type CustomSelectProps = {
//   fieldtype?: "select";
// } & SelectProps;

// type CustomUploadProps = {
//   fieldtype?: "upload";
// } & DsUploadProps;

// type CustomEditorProps = {
//   fieldtype?: "editor";
//   label?: string;
// } & DsEditorProps;

// type Props = {
//   formik: any;
//   name: string;
//   label?: string;
//   required?: boolean;
//   showSearch?: boolean;
//   size?: string;
//   onChange?: (e: any) => void;
// } & (
//   | CustomInputProps
//   | CustomSelectProps
//   | CustomUploadProps
//   | CustomEditorProps
// );

// const CustomFormikField: FC<Props> = ({
//   formik,
//   name,
//   label,
//   onChange,
//   required,
//   size,
//   showSearch,
//   ...props
// }) => {
//   const [field, meta] = useField({
//     name: name,
//   });

//   const handleSelectChange = (e: any) => {
//     if (e) formik?.setFieldValue(name, e);
//     else formik?.setFieldValue(name, null);
//   };

//   const handleEditorChange = (event: any, editor: any) => {
//     const data = editor.getData();
//     formik?.setFieldValue(name, data);
//   };

//   const handleFieldChanged = (e: any) => {
//     field.onChange(e);
//     onChange?.(e);
//   };

//   const renderFields = () => {
//     switch (props?.fieldtype) {
//       case "input": {
//         return (
//           <DsInput
//             {...props}
//             {...field}
//             label={label}
//             required={required}
//             name={name}
//             onChangeEvent={handleFieldChanged}
//             message={meta.touched && meta.error}
//             state={meta.touched && meta.error ? "error" : "normal"}
//           />
//         );
//       }

//       case "select": {
//         return (
//           <DsSelect
//             {...props}
//             {...field}
//             label={label}
//             required={required}
//             name={name}
//             showSearch={showSearch ? true : false}
//             onChange={(e) => {
//               handleSelectChange(e);
//               handleFieldChanged(e);
//             }}
//             message={meta.touched && meta.error}
//             state={meta.touched && meta.error ? "error" : "normal"}
//           />
//         );
//       }

//       case "upload": {
//         return (
//           <DsUpload
//             {...props}
//             {...field}
//             // onChangeEvent={handleFieldChanged}
//             // message={meta.touched && meta.error}
//             // state={meta.touched && meta.error ? "error" : "normal"}
//           />
//         );
//       }

//       case "editor": {
//         return (
//           <div
//             className={classNames(
//               "relative",
//               meta.error ? "duration-300 bg-white ck-error" : ""
//             )}
//           >
//             <DsEditor
//               {...props}
//               onChange={handleEditorChange}
//               content={field?.value}
//             />
//             {meta.error && (
//               <div className="text-sm absolute bottom-0 left-0 pt-1 translate-y-full truncate text-red500 max-w-full">
//                 {meta.error}
//               </div>
//             )}
//           </div>
//         );
//       }

//       case "textarea": {
//         return (
//           <DsInput
//             type="textarea"
//             {...props}
//             {...field}
//             label={label}
//             required={required}
//             name={name}
//             onChangeEvent={handleFieldChanged}
//           />
//         );
//       }
//     }
//   };

//   return <div>{renderFields()}</div>;
// };

// export default CustomFormikField;

import React from "react";

const Custom = () => {
  return <div>index</div>;
};

export default Custom;
