import React from "react";
import { DsIcon, DsLayout, DsLogo, MenuItems } from "mvi-ds-ui";
import { useNavigate } from "react-router-dom";
import { TOKEN_KEY } from "../../constants/const";
import { useAuth } from "../../context/AuthContext";
import { ADMIN, permissionRouterMapping } from "../../constants/user";

type AdminLayoutProps = {
  children?: any;
  hideTopMenu?: any;
  hideLeftMenu?: any;
  menu?: MenuItems[];
};
const AdminLayout = ({
  hideTopMenu,
  hideLeftMenu,
  menu,
  children,
}: AdminLayoutProps) => {
  const { user } = useAuth();
  const navigate = useNavigate();

  const mappingMenu = menu?.filter((m) => {
    const per = m.path ? permissionRouterMapping?.[m.path] : null;
    if (
      (per && user?.permission?.includes(per)) ||
      m.path === "/" ||
      user?.permission?.includes(ADMIN)
    )
      return m;
  });

  return (
    <div className="flex flex-col h-screen overflow-hidden">
      <DsLayout
        topMenu={{
          logo: <DsLogo />,
          onLogoClick: () => navigate("/"),
          fixedMenu: true,
        }}
        menu={{
          menuItems: mappingMenu,
          onItemClick: (item) => {
            navigate(item?.path || "/");
          },
        }}
        user={{
          menu: [
            {
              label: (
                <div
                  className="flex items-center py-1"
                  onClick={() => {
                    window.localStorage.removeItem(TOKEN_KEY);
                    window.location.assign("/signout");
                  }}
                >
                  <DsIcon name="icon-bx-log-out-circle" className="mr-2" />
                  <span>Đăng xuất</span>
                </div>
              ),
              key: "1",
            },
          ],
          name: user?.name ? user?.email : "",
          userName: user?.name || user?.email || "User",
          avatar: user?.avatar,
        }}
      >
        <div className="p-4">{children}</div>
      </DsLayout>
    </div>
  );
};

export default AdminLayout;
