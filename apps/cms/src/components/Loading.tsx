import { DsIcon } from "mvi-ds-ui";
import React from "react";

const Loading = () => {
  return (
    <div className="absolute top-0 left-0 w-full h-full bg-transparent flex justify-center items-center z-50">
      <div className="absolute top-0 left-0 w-full h-full bg-black opacity-50" />
      <div className="flex justify-center items-center relative text-2xl text-white font-bold">
        <DsIcon
          name="icon-bx-loader-alt"
          className="mr-4 animate-spin !text-3xl"
        />
        Loading...
      </div>
    </div>
  );
};

export default Loading;
