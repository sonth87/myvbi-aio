import axiosInstance from "./axios";
import { RequestFormType } from "../type/leader";
import { LeadersType } from "@repo/types/leaders";

export const getAllLeaders = async () => {
  const result = await axiosInstance.get<LeadersType[]>("leaders");

  return result?.data;
};

export const getLeaderById = async (id: string) => {
  const result = await axiosInstance.get<LeadersType>(`leaders/${id}`);

  return result?.data;
};

export const createLeader = async ({ form }: RequestFormType) => {
  const result = await axiosInstance.post(`/leaders/create`, form);

  return result;
};

export const updateLeader = async ({ id, form }: RequestFormType) => {
  const result = await axiosInstance.put(`/leaders/${id}`, form);

  return result;
};

export const deleteLeader = async (id: string) => {
  const result = await axiosInstance.delete(`/leaders/${id}`);

  return result;
};
