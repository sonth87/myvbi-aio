import { ProductsType } from "@repo/types/products";
import axiosInstance from "./axios";
import { RequestFormType } from "../type/product";

export const getListProducts = async () => {
  const result = await axiosInstance.get<ProductsType[]>("products");

  return result?.data;
};

export const getProductById = async (id: string) => {
  const result = await axiosInstance.get<ProductsType>(`products/${id}`);

  return result?.data;
};

export const createProduct = async ({ form }: RequestFormType) => {
  const result = await axiosInstance.post(`/products/create`, form);

  return result;
};

export const updateProduct = async ({ code, id, form }: RequestFormType) => {
  const result = await axiosInstance.put(`/products/${code}/${id}`, form);

  return result;
};

export const deleteProduct = async (id: string) => {
  const result = await axiosInstance.delete(`/products/${id}`);

  return result;
};
