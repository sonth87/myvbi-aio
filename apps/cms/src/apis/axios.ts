import axios, { HttpStatusCode } from "axios";
import { TOKEN_KEY } from "../constants/const";

const baseUrl = process.env.REACT_APP_BE_BASEURL + "api/";
const token = window.localStorage.getItem(TOKEN_KEY);

const axiosInstance = axios.create({
  baseURL: baseUrl,
  timeout: 30000,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
    Authorization: token ? `Bearer ${token}` : undefined,
  },
});

axiosInstance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    if (response.status === HttpStatusCode.Ok) return response?.data;
    else return response?.data; // response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axiosInstance;
