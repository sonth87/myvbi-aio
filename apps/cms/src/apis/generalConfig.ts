import axiosInstance from "./axios";
import { GeneralConfigType } from "@repo/types/general";

export const getGeneralConfig = async () => {
  const result = await axiosInstance.get<GeneralConfigType>("general-config");

  return result?.data;
};

export const updateGeneralConfig = async (data: GeneralConfigType) => {
  const result = await axiosInstance.post<GeneralConfigType>(
    `general-config/update`,
    data
  );

  return result?.data;
};
