import { FileUpload } from "../type/common";
import axiosInstance from "./axios";

export const upload = async (file: File) => {
  const formData = new FormData();
  formData.append("files", file);

  const result = await axiosInstance.post<FileUpload[]>("upload", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
  return result?.data;
};
