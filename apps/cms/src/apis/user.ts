import axios from "axios";
import { ENV, HA_TOKEN } from "../constants/const";
import axiosInstance from "./axios";
import { UserType } from "@repo/types/user";
import { RequestFormType } from "../type/user";
import { CriptWithHA } from "../utils/getHa";

export const signin = async (form?: { u: string; p: string }) => {
  let data;
  if (form) {
    data = await axios.post(ENV.be + "auth/signin", {
      user: CriptWithHA(form?.u),
      password: CriptWithHA(form?.p),
    });
  } else data = await axios.get(ENV.be + "auth/signin");

  return data?.data;
};

export const signout = async () => {
  const data = await axios.get(ENV.be + "auth/signout");

  return data?.data;
};

export const getUserInfo = async () => {
  const response = await axiosInstance.get("user/profile");
  return response?.data;
};

export const getUserById = async (id: string) => {
  const response = await axiosInstance.get("users/" + id);
  return response?.data;
};

export const getListUsers = async () => {
  const result = await axiosInstance.get<UserType[]>("users");

  return result?.data;
};

export const createUser = async ({ form }: RequestFormType) => {
  const result = await axiosInstance.post(`/users/create`, form);

  return result;
};

export const updateUser = async ({ id, form }: RequestFormType) => {
  const result = await axiosInstance.put(`/users/${id}`, form);

  return result;
};

export const deleteUser = async (id: string) => {
  const result = await axiosInstance.delete(`/users/${id}`);

  return result;
};
