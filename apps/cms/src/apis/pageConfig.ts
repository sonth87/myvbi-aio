import { PageCompType } from "@repo/types/page";
import axiosInstance from "./axios";

export const getAllPageConfig = async () => {
  const data = await axiosInstance.get("/component");

  return data?.data;
};

export const getPageConfigByKey = async (key: string) => {
  const data = await axiosInstance.get("/component/" + key);

  return data?.data;
};

export const updatePageConfig = async (form: PageCompType) => {
  const data = await axiosInstance.put("/component/" + form.key, form);

  return data?.data;
};
