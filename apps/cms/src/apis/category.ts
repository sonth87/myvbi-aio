import { message } from "mvi-ds-ui";
import { RequestFormType } from "../type/product";
import axiosInstance from "./axios";
import { convertDate } from "../constants/genaralFunction";

export const getCategoryById = async (param: string) => {
  const result = await axiosInstance.get(`category/${param}`);
  return result.data;
};

export const getListCategories = async (
  limit: number,
  page: number,
  searchData: any
) => {
  const result = await axiosInstance.get("category/getAllCategories", {
    params: {
      limit: limit,
      page: page,
      tieu_de: searchData.tieu_de || null,
      tu_ngay: searchData.tu_ngay ? convertDate(searchData.tu_ngay) : null,
      den_ngay: searchData.tu_ngay ? convertDate(searchData.den_ngay) : null,
    },
  });

  return result?.data;
};

export const getListCategoriesByStatus = async (
  status?: string,
  limit?: number,
  page?: number
) => {
  const result = await axiosInstance.get("category/getAllCategoriesByStatus", {
    params: {
      limit: limit,
      page: page,
      status: status,
    },
  });

  return result?.data;
};

export const createCategory = async ({ form }: RequestFormType) => {
  const result = await axiosInstance.post<any>("category", form);

  return result?.data;
};

export const updateCategory = async ({ code, form }: RequestFormType) => {
  const result = await axiosInstance.put(`category/${code}`, form);

  return result?.data;
};

export const deleteCategory = async (id: string) => {
  try {
    const result = await axiosInstance.delete(`/category/${id}`);
    return result;
  } catch (error: any) {
    if (error.response.status === 404) {
      return {
        status: error.response.data.status,
        message: error.response.data.message,
      };
    }
    throw error;
  }
};
