import { convertDate } from "../constants/genaralFunction";
import axiosInstance from "./axios";

export const getListNews = async (
  limit: number,
  page: number,
  searchData: any
) => {
  const result = await axiosInstance.get("news/getAllNews", {
    params: {
      limit: limit,
      page: page,
      tieu_de: searchData.tieu_de || null,
      ma_dm: searchData.ma_dm || null,
      tu_ngay: searchData.tu_ngay ? convertDate(searchData.tu_ngay) : null,
      den_ngay: searchData.tu_ngay ? convertDate(searchData.den_ngay) : null,
    },
  });

  return result?.data;
};

export const getListNewsConfig = async (
  limit: number,
  page: number,
  searchData: any
) => {
  const result = await axiosInstance.get("news/getAllNewsConfig", {
    params: {
      limit: limit,
      page: page,
      tieu_de: searchData.tieu_de || null,
      ma_dm: searchData.ma_dm || null,
      tu_ngay: searchData.tu_ngay ? convertDate(searchData.tu_ngay) : null,
      den_ngay: searchData.tu_ngay ? convertDate(searchData.den_ngay) : null,
    },
  });

  return result?.data;
};

export const getListNewsByIds = async (ids: string[]) => {
  const result = await axiosInstance.post("news/getAllNewsByIds", { ids });

  return result?.data;
};

export const getNewsById = async (param: string | null) => {
  const result = await axiosInstance.get(`news/${param}`);
  return result.data;
};

export const createNews = async (data: any) => {
  const result = await axiosInstance.post<any>(`news`, data);

  return result?.data;
};

export const updateNews = async (pId: string, data: any) => {
  const result = await axiosInstance.put<any>(`news/${pId}`, data);

  return result?.data;
};

export const deleteNews = async (pId: string) => {
  const result = await axiosInstance.delete<any>(`news/${pId}`);
  return result;
};
