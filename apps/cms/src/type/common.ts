export type FileUpload = {
  file_upload: string;
  original_name: string;
  mimetype: string;
  file_size: number;
};

export type NotifyType = { type: "success" | "error"; content?: string };

export type UserType = {
  username: string;
  email: string;
  name: string;
};
