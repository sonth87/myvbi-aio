export type FilterQueryType = {
  title?: string;
  id?: string;
  category?: string;
  status?: string;
};
export type NewsSchemaType = {
  _id: string;
  ma_goc: string;
  deleted: boolean;
  tieu_de: {
    value: string;
    key_content: string;
  };
  slug: {
    value: string;
    key_content: string;
  };
  noi_dung: {
    value: string;
    key_content: string;
  };
  tom_tat: {
    value: string;
    key_content: string;
  };
  mo_ta_tu_khoa: {
    value: string;
    key_content: string;
  };
  he_thong: {
    value: string;
    key_content: string;
  };
  anh_banner: {
    value: string;
    key_content: string;
  };
  link_banner: string;
  trang_thai: {
    value: string;
    key_content: string;
  };
  mo_ta: {
    value: string;
    key_content: string;
  };
  tu_khoa: {
    value: string;
    key_content: string;
  };
  ma_dm: {
    value: string;
    key_content: string;
  };
  anh: {
    value: string;
    key_content: string;
  };
  ngay_tao: {
    value: string;
    key_content: string;
  };
  nguoi_tao: {
    value: string;
    key_content: string;
  };
  thu_tu: {
    value: number;
    key_content: "";
  };
  tag: {
    value: string;
    key_content: "";
  };
};
export type CategoryType = {
  _id?: string | any;
  tieu_de: {
    value: string;
    key_content: string;
  };
  slug: {
    value: string;
    key_content: string;
  };
  ma_goc: {
    value: string | string[];
    key_content: string;
  };
  mo_ta: {
    value: string;
    key_content: string;
  };
  trang_thai: {
    value: string;
    key_content: string;
  };
  banner_category: {
    value: string;
    key_content: string;
  };
  link_banner_category: "";
  children?: CategoryType[];
};

export type newsQueryType = {
  vi: NewsSchemaType;
  // en: NewsSchemaType;
};

export type CategoryQueryType = {
  vi: CategoryType;
  en: CategoryType;
};
