import React from "react";

export type PrivateRouteProperties = {
  path: string;
  component: React.LazyExoticComponent<(props?: any) => JSX.Element>;
  params?: any;
  layout?: (props: any) => JSX.Element;
};

export type PublicRouteProperties = {
  path: string;
  component?: React.LazyExoticComponent<(props?: any) => JSX.Element>;
  params?: any;
  layout?: (props: any) => JSX.Element;
};
