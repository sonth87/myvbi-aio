import { ProductsType } from "@repo/types/products";

export type RequestFormType = {
  code?: string;
  id?: string;
  form: ProductsType;
};
