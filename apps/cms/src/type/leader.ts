import { LeadersType } from "@repo/types/leaders";

export type RequestFormType = {
  code?: string;
  id?: string;
  form: LeadersType;
};
