import { PageCompType } from "@repo/types/page";
import { COMPS } from "../constants/pagesConfig";

export type PageConfigProps = {
  type: keyof typeof COMPS;
  data?: PageCompType;
  onFormChange?: (data: PageCompType) => void;
  isLoading?: boolean;
};
