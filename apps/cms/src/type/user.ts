import { UserType } from "@repo/types/user";

export type RequestFormType = {
  code?: string;
  id?: string;
  form: UserType;
};
