import { LANG_VI } from "./const";

//hàm ktra object có giá trị hay không
export const checkObjectValues = (formData: any) => {
  return Object.keys(formData).reduce((result: any, key) => {
    result[key] = Object.values(formData[key]).some((value) => value);
    return result;
  }, {});
};
// Hàm loại bỏ tiền tố trong các key của object
export const removePrefix = (data: any, prefix: string) => {
  const result: any = {};
  Object.keys(data).forEach((key) => {
    const newKey = key.replace(`${prefix}.`, "");
    result[newKey] = data[key];
  });
  return result;
};

export const validateForm = (form: any, tab: string) => {
  console.log(`-------------------Start Validate form ${tab} --------------`);
  return form
    .validateFields()
    .then((values: any) => {
      console.log("Form values:", removePrefix(values, tab));
      console.log(`-------------------End Validate form ${tab}--------------`);
      return removePrefix(values, tab); // Trả về giá trị khi validate thành công
    })
    .catch((error: any) => {
      console.error("Validation failed:", error);
    });
};
