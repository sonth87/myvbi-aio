import classNames from "classnames";

export function convertToSlug(text: string) {
  const normalizedText = text
    .toLowerCase() // Chuyển tất cả thành chữ thường
    .normalize("NFD") // Chuẩn hóa Unicode
    .replace(/[\u0300-\u036f]/g, "") // Loại bỏ dấu diacritics
    .replace(/đ/g, "d") // Chuyển đổi chữ "đ" thành "d"
    .replace(/[^a-zA-Z0-9\s]/g, "") // Loại bỏ ký tự không phải chữ cái, số, hoặc dấu cách
    .replace(/\s+/g, "-") // Thay thế dấu cách bằng dấu gạch ngang
    .replace(/-+/g, "-") // Loại bỏ các dấu gạch ngang liên tiếp
    .replace(/(^-+|-+$)/, ""); // Loại bỏ dấu gạch ngang ở đầu và cuối chuỗi
  return normalizedText;
}

export const convertDateTime = (dateTimeString: string) => {
  // Lấy các thành phần từ chuỗi
  const year = dateTimeString?.slice(0, 4);
  const month = dateTimeString?.slice(4, 6);
  const day = dateTimeString?.slice(6, 8);
  const hour = dateTimeString?.slice(8, 10);
  const minute = dateTimeString?.slice(10, 12);
  // Kết hợp các thành phần lại với nhau
  const formattedDateTime = `${hour}:${minute} ${day}/${month}/${year}`;
  return formattedDateTime;
};

// convert DD/MM/YYYY to YYYY-MM-DD
export const convertDate = (date: string) => {
  const parts = date.split("/"); // Tách chuỗi theo dấu "/"
  const formattedDate = `${parts[2]}-${parts[1]}-${parts[0]}`;
  return formattedDate;
};

export const convertToTimestamp = (dateTimeString: string) => {
  const [time, date] = dateTimeString.split(" ");
  const [hours, minutes] = time.split(":");
  const [day, month, year] = date.split("/");
  const isoDate = `${year}${month.padStart(2, "0")}${day.padStart(2, "0")}${hours.padStart(2, "0")}${minutes.padStart(2, "0")}00`; //yyyymmddhhmmss
  return isoDate;
};

//chuyển ngày tháng hiện tại
export function getCurrentDateFormatted(format?: string, firstDate?: boolean) {
  const currentDate = new Date();

  firstDate ? currentDate.setDate(1) : null;

  // Get the year, month, and day
  const year = currentDate.getFullYear();
  const month = (currentDate.getMonth() + 1).toString().padStart(2, "0"); // Months are zero-based
  const day = currentDate.getDate().toString().padStart(2, "0");
  const hours = currentDate.getHours().toString().padStart(2, "0");
  const minutes = currentDate.getMinutes().toString().padStart(2, "0");
  const seconds = currentDate.getSeconds().toString().padStart(2, "0");

  switch (format) {
    case "YYYYMMDD":
      return `${year}${month}${day}`;
    case "DD/MM/YYYY":
      return `${day}/${month}/${year}`;
    case "HHMMDDMMYYYY":
      return `${hours}:${minutes} ${day}/${month}/${year}`;
    case "YYYYMMDDHHMMSS":
      return `${year}${month}${day}${hours}${minutes}${seconds}`;
    default:
      break;
  }
}

export const TrangThaiHoatDong = (status: string) => {
  let bgColor = "bg-purple100";
  let textColor = "text-ink800";
  let text = "";
  switch (status) {
    case "inactive":
      bgColor = "bg-[#FFEFEF]";
      textColor = "text-[#FF6262]";
      text = "Không hoạt động";
      break;
    case "active":
      bgColor = "bg-[#EBFAF2]";
      textColor = "text-[#39CC7E]";
      text = "Đang hoạt động";
      break;

    default:
      break;
  }
  return (
    <div className={classNames("w-fit	px-2 py-1 rounded-full", bgColor)}>
      <p className={classNames("leading-5", textColor)}>{text}</p>
    </div>
  );
};

export const extractOperation = (url: string) => {
  // Tạo biểu thức chính quy để tìm chuỗi "create" hoặc "update" trong URL
  const regex = /\/(create|update|view)\b/;

  // Sử dụng phương thức exec của biểu thức chính quy để tìm kết quả trong URL
  const match = regex.exec(url);

  // Nếu có kết quả, trả về phần tử đầu tiên từ mảng match
  if (match) {
    return match[1]; // Trả về "create" hoặc "update"
  } else {
    return null; // Nếu không tìm thấy, trả về null
  }
};

export const convertToCategoryTree = (dataTable: any) => {
  const categoryMap: any = {};
  const result: any = [];

  // First pass: Group items by ma_node
  dataTable.forEach((item: any) => {
    categoryMap[item._id] = item;
    categoryMap[item._id].children = [];
  });

  // Second pass: Attach children to their parent items
  dataTable.forEach((item: any) => {
    if (item.ma_goc && categoryMap[item.ma_goc]) {
      categoryMap[item.ma_goc].children.push(categoryMap[item._id]);
    } else {
      result.push(categoryMap[item._id]);
    }
  });

  // Remove `children` property if it is an empty array
  const removeEmptyChildren = (category: any) => {
    if (category.children.length === 0) {
      delete category.children;
    } else {
      category.children.forEach(removeEmptyChildren);
    }
  };

  result.forEach(removeEmptyChildren);

  return result;
};

export const convertToCategorySelectTree = (dataTable: any) => {
  const categoryMap: any = {};
  const result: any = [];

  // First pass: Group items by ma_node
  dataTable.forEach((item: any) => {
    categoryMap[item._id] = item;
    categoryMap[item._id].children = [];
  });

  // Second pass: Attach children to their parent items
  dataTable.forEach((item: any) => {
    if (item.ma_goc && categoryMap[item.ma_goc]) {
      categoryMap[item.ma_goc].children.push(categoryMap[item._id]);
    } else {
      result.push(categoryMap[item._id]);
    }
  });

  // Convert to the desired format
  const convertToFormat = (data: any) => {
    return data.map((item: any) => ({
      label: item?.tieu_de?.value,
      value: item?._id,
      trang_thai: item?.trang_thai?.value,
      ...(item.children.length > 0 && {
        children: convertToFormat(item.children),
      }),
    }));
  };

  return convertToFormat(result);
};

export const filterData = (data: any, id: string) => {
  // Lặp qua mỗi phần tử trong mảng data
  return (
    data &&
    data.filter((item: any) => {
      // Nếu phần tử có giá trị value khác với id
      if (item.value !== id) {
        // Kiểm tra xem phần tử có thuộc tính children không
        if (item.children) {
          // Loại bỏ bất kỳ phần tử con nào có giá trị value trùng với id
          item.children = item.children.filter(
            (child: any) => child.value !== id
          );
        }
        return true; // Giữ lại phần tử nếu không có giá trị value trùng với id
      }
      return false; // Loại bỏ phần tử nếu có giá trị value trùng với id
    })
  );
};
