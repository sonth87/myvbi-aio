import { Link } from "react-router-dom";
import { ROUTES } from "./path";
import {
  Banner,
  TopProducts,
  Partner,
  ProtectCompany,
  ProtectFamily,
  WhyVBI,
  CustomerOpinion,
  QnA,
  Invester,
  Structure,
  Award,
  News,
} from "../pages/PageConfigs/PageComponents";
import { PageCompType } from "@repo/types/page";
import { PageConfigProps } from "../type/page";

export const PAGES = {
  home: "home",
  invester: "invester",
  structure: "structure",
  award: "award",
  cosoyte: "cosoyte",
};

export const pagesList = {
  [`${PAGES.home}`]: "Trang chủ",
  [`${PAGES.invester}`]: "Nhà đầu tư",
  [`${PAGES.structure}`]: "Cơ cấu quản trị",
  [`${PAGES.award}`]: "Giải thưởng",
  [`${PAGES.cosoyte}`]: "Cơ sở y tế",
};

export const COMPS = {
  banner: "banner",
  top_products: "top_products",
  partner: "partner",
  protect_family: "protect_family",
  protect_company: "protect_company",
  why_vbi: "why_vbi",
  customer_opinion: "customer_opinion",
  qna: "qna",
  invester: "invester",
  structure: "structure",
  award: "award",
  news1: "news1",
  news2: "news2",
  cosoyte_news: "cosoyte_news",
};

export const ComponentsMapping = (props: PageConfigProps) => {
  switch (props.type) {
    case "banner":
      return <Banner {...props} />;
    case "top_products":
      return <TopProducts {...props} />;
    case "partner":
      return <Partner {...props} />;
    case "protect_family":
      return <ProtectFamily {...props} />;
    case "protect_company":
      return <ProtectCompany {...props} />;
    case "why_vbi":
      return <WhyVBI {...props} />;
    case "customer_opinion":
      return <CustomerOpinion {...props} />;
    case "qna":
      return <QnA {...props} />;
    case "invester":
      return <Invester {...props} />;
    case "structure":
      return <Structure {...props} />;
    case "award":
      return <Award {...props} />;
    case "news1":
      return <News {...props} />;
    case "news2":
      return <News {...props} />;
    case "cosoyte_news":
      return <News {...props} />;
    default:
      return null;
  }
};

export const componentList = [
  { key: COMPS.banner, name: "Top Banner", pages: [PAGES.home] },
  {
    key: COMPS.top_products,
    name: "Sản phẩm bảo hiểm được khách hàng quan tâm tại VBI",
    pages: [PAGES.home],
  },
  // { key: COMPS.partner, name: "Banner", pages: [PAGES.home] },
  {
    key: COMPS.protect_family,
    name: "Bảo vệ bạn và gia đình",
    pages: [PAGES.home],
  },
  {
    key: COMPS.protect_company,
    name: "Bảo vệ doanh nghiệp của bạn",
    pages: [PAGES.home],
  },
  {
    key: COMPS.news1,
    name: "Section Tin tức 1",
    pages: [PAGES.home],
  },
  {
    key: COMPS.news2,
    name: "Section Tin tức 2",
    pages: [PAGES.home],
  },
  {
    key: COMPS.why_vbi,
    name: "Vì sao chọn VBI?",
    pages: [PAGES.home],
  },
  {
    key: COMPS.customer_opinion,
    name: "Khách hàng nói về chúng tôi",
    pages: [PAGES.home],
  },
  {
    key: COMPS.qna,
    name: "Câu hỏi thường gặp",
    pages: [PAGES.home],
  },
  // {
  //   key: COMPS.invester,
  //   name: "Nhà đầu tư",
  //   pages: [PAGES.invester],
  // },
  {
    key: COMPS.structure,
    name: "Cơ cấu quản trị",
    pages: [PAGES.structure],
  },
  {
    key: COMPS.award,
    name: "Giải thưởng",
    pages: [PAGES.award],
  },
  {
    key: COMPS.cosoyte_news,
    name: "Cơ sở y tế VBI từ chối chi trả quyền lợi bảo hiểm",
    pages: [PAGES.cosoyte],
  },
];

export const columnsPageList = [
  // { title: "Key", dataIndex: "key" },
  {
    title: "Page",
    dataIndex: "pages",
    render(val: string[]) {
      return val.map((v) => pagesList?.[v]).join(", ");
    },
  },
  { title: "Tên component", dataIndex: "name" },
  { title: "Mô tả", dataIndex: "description" },
  {
    title: "Trạng thái",
    dataIndex: "visible",
    render: (val: any) => {
      return val ? (
        <div className="bg-green-400 text-white text-xs w-fit rounded py-1 px-2">
          show
        </div>
      ) : (
        <div className="bg-red-400 text-white text-xs w-fit rounded py-1 px-2">
          hide
        </div>
      );
    },
  },
  {
    title: "action",
    key: "action",
    render: (val: any, rowData: any) => {
      return <Link to={ROUTES.PAGE_CONFIG + "?comp=" + rowData.key}>edit</Link>;
    },
  },
];

export const StructureGroup = [
  { value: "hoi-dong-quan-tri", label: "Hội đồng quản trị" },
  { value: "ban-dieu-hanh", label: "Ban điều hành" },
  { value: "ban-kiem-soat", label: "Ban kiểm soát" },
];

export const initFormData = (key: string, data?: PageCompType) => {
  return {
    key: data?.key || key,
    title: data?.title || "",
    description: data?.description || "",
    updatedAt: data?.updatedAt || "",
    content: data?.content || null,
    visible: data?.visible || false,
  } as PageCompType;
};
