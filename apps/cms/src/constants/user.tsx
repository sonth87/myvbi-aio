import { Link } from "react-router-dom";
import { ROUTES } from "./path";
import { DsAvatar, DsBadge } from "mvi-ds-ui";

export const columnsUserList = ({ editable }: { editable: boolean }) => [
  {
    title: "Avatar",
    dataIndex: "avatar",
    render: (val: string) => {
      return val ? <DsAvatar type="Image" src={val} /> : null;
    },
  },
  {
    title: "Name",
    dataIndex: "name",
  },
  { title: "Email", dataIndex: "email" },
  {
    title: "Department",
    key: "department",
    dataIndex: "department",
  },
  {
    title: "Title",
    key: "title",
    dataIndex: "title",
  },
  {
    title: "Status",
    key: "status",
    dataIndex: "status",
    render: (val: boolean) => {
      return val ? (
        <DsBadge
          bgcolor="green"
          className="!min-w-3 !min-h-3 !max-h-3 !max-w-3"
        />
      ) : (
        <DsBadge
          bgcolor="red"
          className="!min-w-3 !min-h-3 !max-h-3 !max-w-3"
        />
      );
    },
  },
  {
    title: "Action",
    key: "action",
    render: (val: any, rowData: any) => {
      return (
        editable && (
          <Link to={ROUTES.USERS_UPDATE + "?id=" + rowData.id}>edit</Link>
        )
      );
    },
  },
];

export const ADMIN = "ADMINISTRATOR";

export const PERMISSION = {
  // NEWS
  NEWS_LIST: "NEWS_LIST",
  NEWS_CREATE: "NEWS_CREATE",
  NEWS_VIEW: "NEWS_VIEW",
  NEWS_UPDATE: "NEWS_UPDATE",
  NEWS_DELETE: "NEWS_DELETE",
  //CATEGORY
  CATEGORY_LIST: "CATEGORY_LIST",
  CATEGORY_CREATE: "CATEGORY_CREATE",
  CATEGORY_VIEW: "CATEGORY_VIEW",
  CATEGORY_UPDATE: "CATEGORY_UPDATE",
  CATEGORY_DELETE: "CATEGORY_DELETE",
  // COMMON CONFIG
  GENERAL_CONFIG: "GENERAL_CONFIG",
  GENERAL_CONFIG_UPDATE: "GENERAL_CONFIG_UPDATE",
  // LEADER
  LEADER_LIST: "LEADER_LIST",
  LEADER_VIEW: "LEADER_VIEW",
  LEADER_CREATE: "LEADER_CREATE",
  LEADER_UPDATE: "LEADER_UPDATE",
  LEADER_DELETE: "LEADER_DELETE",
  // PAGE CONFIG
  PAGE_CONFIG_LIST: "PAGE_CONFIG_LIST",
  PAGE_CONFIG_VIEW: "PAGE_CONFIG_VIEW",
  PAGE_CONFIG_UPDATE: "PAGE_CONFIG_UPDATE",
  // PRODUCT
  PRODUCT_LIST: "PRODUCT_LIST",
  PRODUCT_VIEW: "PRODUCT_VIEW",
  PRODUCT_CREATE: "PRODUCT_CREATE",
  PRODUCT_UPDATE: "PRODUCT_UPDATE",
  PRODUCT_DELETE: "PRODUCT_DELETE",
  // UPLOAD
  UPLOAD: "UPLOAD",
  // USER
  USER_LIST: "USER_LIST",
  USER_VIEW: "USER_VIEW",
  USER_CREATE: "USER_CREATE",
  USER_UPDATE: "USER_UPDATE",
  USER_DELETE: "USER_DELETE",
};

export enum MODULE {
  news = "news",
  category = "category",
  user = "user",
  config = "config",
  page_config = "page_config",
  product = "product",
  leader = "leader",
}

export const permissionGroup = {
  [`${MODULE.news}`]: {
    view: [PERMISSION.NEWS_LIST, PERMISSION.NEWS_VIEW],
    update: [PERMISSION.NEWS_UPDATE, PERMISSION.NEWS_CREATE],
    delete: [PERMISSION.NEWS_DELETE],
  },
  [`${MODULE.category}`]: {
    view: [PERMISSION.CATEGORY_LIST, PERMISSION.CATEGORY_VIEW],
    update: [PERMISSION.CATEGORY_UPDATE, PERMISSION.CATEGORY_CREATE],
    delete: [PERMISSION.CATEGORY_DELETE],
  },
  [`${MODULE.user}`]: {
    view: [PERMISSION.USER_LIST, PERMISSION.USER_VIEW],
    update: [PERMISSION.USER_CREATE, PERMISSION.USER_UPDATE],
    delete: [PERMISSION.USER_DELETE],
  },
  [`${MODULE.config}`]: {
    view: [PERMISSION.GENERAL_CONFIG],
    update: [PERMISSION.GENERAL_CONFIG_UPDATE],
  },
  [`${MODULE.page_config}`]: {
    view: [PERMISSION.PAGE_CONFIG_LIST, PERMISSION.PAGE_CONFIG_VIEW],
    update: [PERMISSION.PAGE_CONFIG_UPDATE],
  },
  [`${MODULE.product}`]: {
    view: [PERMISSION.PRODUCT_LIST, PERMISSION.PRODUCT_VIEW],
    update: [PERMISSION.PRODUCT_CREATE, PERMISSION.PRODUCT_UPDATE],
    delete: [PERMISSION.PRODUCT_DELETE],
  },
  [`${MODULE.leader}`]: {
    view: [PERMISSION.LEADER_LIST, PERMISSION.LEADER_VIEW],
    update: [PERMISSION.LEADER_CREATE, PERMISSION.LEADER_UPDATE],
    delete: [PERMISSION.LEADER_DELETE],
  },
};

export const permissionLabelMapping = {
  [`${MODULE.news}`]: "Tin tức",
  [`${MODULE.category}`]: "Danh mục",
  [`${MODULE.user}`]: "Người dùng",
  [`${MODULE.config}`]: "Cấu hình chung",
  [`${MODULE.page_config}`]: "Cấu hình Page",
  [`${MODULE.product}`]: "Sản phẩm",
  [`${MODULE.leader}`]: "Quản trị",
};

export const permissionRouterMapping = {
  [`${ROUTES.USERS}`]: PERMISSION.USER_LIST,
  [`${ROUTES.NEWS}`]: PERMISSION.NEWS_LIST,
  [`${ROUTES.CATEGORY}`]: PERMISSION.CATEGORY_LIST,
  [`${ROUTES.GENERAL_CONFIG}`]: PERMISSION.GENERAL_CONFIG,
  [`${ROUTES.PAGE_CONFIG}`]: PERMISSION.PAGE_CONFIG_LIST,
  [`${ROUTES.PRODUCTS}`]: PERMISSION.PRODUCT_LIST,
  [`${ROUTES.LEADERS}`]: PERMISSION.LEADER_LIST,
};
