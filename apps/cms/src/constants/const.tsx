export const LANG_VI = "vi";
export const LANG_EN = "en";
export const LANG_KR = "kr";

export const VI = "Tiếng Việt";
export const EN = "Tiếng Anh";
export const KR = "Tiếng Hàn";

export const CREATE_FORM = "create";
export const UPDATE_FORM = "update";
export const VIEW_FORM = "view";
export const CREATE = "Tạo mới";
export const UPDATE = "Cập nhật";
export const EDIT = "Chỉnh sửa";
export const CONCEAL = "Ẩn";
export const DELETE = "Xoá";
export const CONFIRM = "Xác nhận";
export const EXIT = "Thoát";
export const CONFIRM_RETURN = "Quay lại";
export const IMPORT_DATA = "Import dữ liệu";

export const TITLE = "Tiêu đề";
export const DATE_FROM = "Từ ngày";
export const DATE_TO = "Đến ngày";
export const ACTION = "Thao tác";

export const HELMET_CATEGORY = "Tin tức - Danh mục";
export const CATEGORY = "Danh mục";
export const CATEGORY_NEW = "Danh mục mới";
export const CREATE_CATEGORY = "Tạo danh mục mới";
export const UPDATE_CATEGORY = "Cập nhật danh mục";
export const VIEW_CATEGORY = "Hiển thị danh mục";
export const MANAGER_CATEGORY = "Quản lý danh mục";

export const HELMET_NEWS = "Tin tức - Bài viết";
export const NEWS = "Bài viết mới";
export const CREATE_NEWS = "Tạo bài viết mới";
export const UPDATE_NEWS = "Cập nhật bài viết";
export const VIEW_NEWS = "Hiển thị bài viết";
export const MANAGER_NEWS = "Quản lý bài viết";
export const LANG_VN = "vi";
export const LANG_ENG = "en";
export const LANG_KOREA = "kr";

export const PRD_GROUP = {
  SK: "suc-khoe",
  XE: "xe-co-gioi",
  DL: "du-lich",
  TS: "tai-san",
  KT: "ky-thuat",
  HH: "hang-hoa-tau-thuyen",
  NL: "nang-luong",
  NH: "ngan-hang",
  K: "khac",
};

export const ProductGroup = {
  [`${PRD_GROUP.SK}`]: "Sức khỏe",
  [`${PRD_GROUP.XE}`]: "Xe cơ giới",
  [`${PRD_GROUP.DL}`]: "Du lịch",
  [`${PRD_GROUP.TS}`]: "Tài sản",
  [`${PRD_GROUP.KT}`]: "Kỹ thuật",
  [`${PRD_GROUP.HH}`]: "Hàng hóa và tàu thuyền",
  [`${PRD_GROUP.NL}`]: "Năng lượng",
  [`${PRD_GROUP.NH}`]: "Ngân hàng",
  [`${PRD_GROUP.K}`]: "Khác",
};

export enum PRD_TYPE {
  GD = "gia-dinh",
  DN = "doanh-nghiep",
  NH = "ngan-hang",
}

export const ProductType = {
  [`${PRD_TYPE.GD}`]: "Cho bạn và gia đình",
  [`${PRD_TYPE.DN}`]: "Doanh nghiệp",
  [`${PRD_TYPE.NH}`]: "Ngân hàng",
};

export enum SYS_GROUP {
  WEB = "website",
  APP = "app",
}

export const SystemGroup = {
  [`${SYS_GROUP.WEB}`]: "Website",
  [`${SYS_GROUP.APP}`]: "App",
};

export enum STT_GROUP {
  ACTIVE = "active",
  INACTIVE = "inactive",
}

export const StatusGroup = {
  [`${STT_GROUP.ACTIVE}`]: "Hoạt động",
  [`${STT_GROUP.INACTIVE}`]: "Không hoạt động",
};

export const ENV = {
  file: process.env.REACT_APP_FILE_SERVER || "",
  be: process.env.REACT_APP_BE_BASEURL || "",
};

export const TOKEN_KEY = "code";
export const HA_TOKEN = "hacode";
export const KEY = "a375936a876de769923c507ce05f9e23";
