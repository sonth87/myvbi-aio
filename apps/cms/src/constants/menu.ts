import { MenuItems } from "mvi-ds-ui";
import { ROUTES } from "./path";

export const MainMenu: MenuItems[] = [
  {
    label: "Dashboard",
    icon: "icon-bxs-dashboard",
    path: ROUTES.DASHBOARD,
    group: "1",
  },
  {
    label: "User",
    icon: "icon-bx-user-circle",
    path: ROUTES.USERS,
    group: "2",
  },
  {
    label: "Quản lý nội dung",
    icon: "icon-bx-news",
    path: ROUTES.NEWS,
    childs: [
      { label: "Quản lý bài viết", path: ROUTES.NEWS },
      { label: "Quản lý danh mục", path: ROUTES.CATEGORY },
    ],
    group: "3",
  },
  {
    label: "Cấu hình chung",
    icon: "icon-bx-cog",
    path: ROUTES.GENERAL_CONFIG,
    group: "4",
  },
  {
    label: "Page Config",
    icon: "icon-bx-category",
    path: ROUTES.PAGE_CONFIG,
    group: "4",
  },
  {
    label: "Products",
    icon: "icon-bx-package",
    path: ROUTES.PRODUCTS,
    group: "4",
  },
];
