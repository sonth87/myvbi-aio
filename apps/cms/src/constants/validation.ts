import { array, boolean, number, object, string } from "yup";

export const generalConfigSchema = object({
  meta: object().shape({
    title: string().required("Title không được để trống"),
    description: string().required("Description không được để trống"),
    tags: string(),
    thumbnail: string().required("Thumbnail không được để trống"),
  }),
  script: array().of(string()),
  company: object().shape({
    name: string().required("Tên công ty không được để trống"),
    shortName: string(),
    phone: string().required("Số điện thoại không được để trống"),
    email: string().required("Email không được để trống"),
    address: string().required("Địa chỉ không được để trống"),
    description: string(),
  }),
});

export const productSchema = object({
  name: string().required("Tên sản phẩm không được để trống"),
  code: string().required("Mã sản phẩm không được để trống"),
  icon: string().required("Icon không được để trống"),
  link: string().required("Link sản phẩm không được để trống"),
  description: string(),
  visible: boolean(),
  promotion: string(),
  tags: string(),
  group: string().required("Nhóm sản phẩm không được để trống"),
  type: string().required("Loại sản phẩm không được để trống"),
});

export const categorySchema = object({
  vi: object({
    tieu_de: object({
      value: string().required("Tiêu đề không được để trống"),
      key_content: string(),
    }),
    slug: object({
      value: string().required("Slug không được để trống"),
      key_content: string(),
    }),
    mo_ta: object({
      value: string().required("Mô tả không được để trống"),
      key_content: string(),
    }),
    trang_thai: object({
      value: string().required("Trạng thái không được để trống"),
      key_content: string(),
    }),
  }),
  // en: object({
  //   tieu_de: object({
  //     value: string().required("Tiêu đề không được để trống"),
  //     key_content: string(),
  //   }),
  //   slug: object({
  //     value: string().required("Slug không được để trống"),
  //     key_content: string(),
  //   }),
  //   mo_ta: object({
  //     value: string().required("Mô tả không được để trống"),
  //     key_content: string(),
  //   }),
  //   trang_thai: object({
  //     value: number().required("Trạng thái không được để trống"),
  //     key_content: string(),
  //   }),
  // }),
});

export const newsSchema = object({
  vi: object({
    tieu_de: object().shape({
      value: string().required("Tiêu đề không được để trống"),
      key_content: string(),
    }),
    slug: object().shape({
      value: string().required("Slug không được để trống"),
      key_content: string(),
    }),
    noi_dung: object().shape({
      value: string().required("Nội dung không được để trống"),
      key_content: string(),
    }),
    tom_tat: object().shape({
      value: string(),
      key_content: string(),
    }),
    he_thong: object().shape({
      value: string().required("Hệ thống không được để trống"),
      key_content: string(),
    }),
    trang_thai: object().shape({
      value: string().required("Trạng thái không được để trống"),
      key_content: string(),
    }),
    mo_ta: object().shape({
      value: string().required("Mô tả không được để trống"),
      key_content: string(),
    }),
    tu_khoa: object().shape({
      value: string(),
      key_content: string(),
    }),
    mo_ta_tu_khoa: object().shape({
      value: string(),
      key_content: string(),
    }),
    ma_dm: object().shape({
      value: string().required("Danh mục không được để trống"),
      key_content: string(),
    }),
    anh: object().shape({
      value: string().required("Ảnh không được để trống"),
      key_content: string(),
    }),
    ngay_tao: object().shape({
      value: string().required("Ngày tạo không được để trống"),
      key_content: string(),
    }),
    // thu_tu: object().shape({
    //   value: number().required("Thứ tự không được để trống"),
    //   key_content: string(),
    // }),
    tag: object().shape({
      value: string(),
      key_content: string(),
    }),
  }),
  // en: object({
  //   tieu_de: object({
  //     value: string().required("Tiêu đề không được để trống"),
  //     key_content: string(),
  //   }),
  //   slug: object({
  //     value: string().required("Slug không được để trống"),
  //     key_content: string(),
  //   }),
  //   mo_ta: object({
  //     value: string().required("Mô tả không được để trống"),
  //     key_content: string(),
  //   }),
  //   trang_thai: object({
  //     value: number().required("Trạng thái không được để trống"),
  //     key_content: string(),
  //   }),
  // }),
});

export const leaderSchema = object({
  name: string().required("Tên không được để trống"),
  title: string().required("Chức danh không được để trống"),
  avatar: string().required("Ảnh đại diện không được để trống"),
  shortDescription: string(),
  description: string(),
});

export const userSchema = object({
  name: string().required("Tên không được để trống"),
  email: string().required("Email không được để trống"),
  avatar: string(),
  address: string(),
  department: string(),
  phone: string(),
  title: string(),
  permission: array(),
});
