import { HA_TOKEN, KEY } from "../constants/const";
const CryptoJS = require("crypto-js");

export const getHa = () => {
  return window.location.origin;
};

export const setHa = (ha: string) => {
  const key = CryptoJS.enc.Utf8.parse(ha + KEY);
  if (ha) window.sessionStorage.setItem(HA_TOKEN, key.toString());
};

export const CriptWithHA = (txt: string) => {
  const ha = window.sessionStorage.getItem(HA_TOKEN);
  if (ha) {
    return CryptoJS.AES.encrypt(txt, ha).toString();
  }
  return;
};
