import { useMutation } from "@tanstack/react-query";
import { upload } from "../apis/upload";
import { ENV } from "../constants/const";
import { useState } from "react";

const useUpload = () => {
  const [image, setImage] = useState<string | null>();
  const { mutateAsync, isPending, isError, isSuccess } = useMutation({
    mutationFn: upload,
  });

  const handleUploadFile = async (file: any) => {
    const result = await mutateAsync(file?.file || file);
    const img = result ? ENV.file + result?.[0].file_upload : null;
    setImage(img);
    return img;
  };

  return {
    uploadFile: handleUploadFile,
    image,
    isLoading: isPending,
    isError,
    isSuccess,
  };
};

export { useUpload };
