import { useLayout } from "mvi-ds-ui";
import React, { useCallback, useState } from "react";

function usePageInfo() {
  const { setModuleTitle, setBreadCrumb } = useLayout();
  const [isLoading, setIsLoading] = useState(false);

  const setModuleInfo = useCallback(
    (options: {
      module?: { icon: string; title: string };
      path?: { label: string; link?: string }[];
    }) => {
      setModuleTitle(options?.module || null);
      setBreadCrumb(
        options?.path
          ? {
              homeIcon: true,
              homeUrl: "/",
              paths: options?.path,
            }
          : null
      );
    },
    []
  );

  const cleanModuleInfo = useCallback(() => {
    setModuleTitle(null);
    setBreadCrumb(null);
  }, []);

  return { setModuleInfo, cleanModuleInfo, isLoading, setIsLoading };
}

export default usePageInfo;
