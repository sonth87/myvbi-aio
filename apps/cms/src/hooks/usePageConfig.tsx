import React, { Dispatch, useCallback, useState } from "react";

const usePageConfig = ({
  listImg,
  setListImg,
  callback,
}: {
  listImg: any[];
  setListImg?: Dispatch<any[]>;
  callback?: (newList: any[]) => void;
}) => {
  const [dragging, setDragging] = useState<number | null>(null);

  const handleReOrder = useCallback(
    (newIdx: number) => {
      if (newIdx >= 0 && dragging !== null) {
        let _listImg = [...listImg];

        _listImg
          .splice(newIdx, 0, _listImg.splice(dragging, 1)[0])
          .forEach((item, index) => {
            item.order = index;
          });
        setListImg?.(_listImg);
        callback?.(_listImg);
      }
    },
    [listImg, dragging]
  );

  const onDragOver = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
  };

  const onDragStart = (e: React.DragEvent<HTMLDivElement>) => {
    const index = parseInt(e.currentTarget.id.replace("item-", ""));
    setDragging(index);
  };

  const onDrop = (e: React.DragEvent<HTMLDivElement>) => {
    handleReOrder(parseInt(e.currentTarget.id.replace("item-", "")));
    setDragging(null);
  };

  return { dragging, setDragging, onDragOver, onDragStart, onDrop };
};

export default usePageConfig;
