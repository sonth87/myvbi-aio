const dotenv = require("dotenv");
const express = require("express");
const path = require("path");
const app = express();
dotenv.config({ path: path.join(__dirname, "./.env") });

const port = process.env.FE_PORT;

// const PUBLIC_URL = process.env.PUBLIC_URL || `http://localhost:${PORT}`;
app.listen(port);

app.use(express.static(path.join(__dirname + "/build")));

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "/build", "index.html"));
});

console.log("server started : localhost:" + port);
