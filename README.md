# MyVBI-AIO



## Getting started

### Installation & Run
From root level:

with npm:
```
npm install
npm run dev
```

with yarn:
```
yarn
yarn dev
```

### Application
#### 1. Website
Nextjs 14, React canary ( beta version )

#### 2. CMS
React canary ( beta version ), design system, tailwind

#### 3. Backend
Express 4