import * as general from "./general";
import * as products from "./products";
import * as pages from "./page";
import * as leaders from "./leaders";
import * as user from "./user";

export { general, products, pages, leaders, user };
