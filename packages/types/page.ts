export type CompType = "banner";

export type PageCompType = {
  title?: string;
  description?: string;
  visible?: boolean;
  updatedAt?: string;
} & (
  | BannerType<BannerContentType[] | null>
  | ProductType<string[] | null>
  | WhyVBIType<BannerContentType[] | null>
  | CustomerOpinionType<OpinionContentType[] | null>
  | QnAType<QnAContentType[] | null>
  | InvesterType<InvesterContentType[] | null>
  | StructureType<StructureContentType[] | null>
  | AwardType<AwardContentType[] | null>
  | NewsType<NewsContentType[] | null>
);

export type BannerContentType = {
  img: string;
  link?: string;
  alt?: string;
  title?: string;
};

export type OpinionContentType = {
  name: string;
  avatar?: string;
  position?: string;
  text?: string;
};

export type QnAContentType = {
  questions: { question: string; answer: string }[];
  groupName: string;
};

export type InvesterContentType = {
  year: number;
  file: string;
  name: string;
  group: string;
  date?: string;
};

export type StructureContentType = {
  group: string;
  ids: string[];
};

export type AwardContentType = {
  img: string;
  title: string;
  description?: string;
  src?: string;
};

export type NewsContentType = {
  type: string;
  category?: string;
  list?: string[];
  count?: number;
  viewMore?: string;
};

export type BannerType<T> = {
  key: "banner" | "partner";
  content?: T;
};

export type ProductType<T> = {
  key: "top_products" | "protect_family" | "protect_company";
  content?: T;
};

export type WhyVBIType<T> = {
  key: "why_vbi";
  content?: T;
};

export type CustomerOpinionType<T> = {
  key: "customer_opinion";
  content?: T;
};

export type QnAType<T> = {
  key: "qna";
  content?: T;
};

export type InvesterType<T> = {
  key: "invester";
  content?: T;
};

export type StructureType<T> = {
  key: "structure";
  content?: T;
};

export type AwardType<T> = {
  key: "award";
  content?: T;
};

export type NewsType<T> = {
  key: "news";
  content?: T;
};
