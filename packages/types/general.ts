export type MetaDataType = {
  title: string;
  description: string;
  tags: string;
  thumbnail: string;
};

export type CompanyDataType = {
  name: string;
  shortName?: string;
  phone: string;
  email?: string;
  address?: string;
  description?: string;
  logo: string;
};

export type GeneralConfigType = {
  meta: MetaDataType;
  script?: string[];
  company?: CompanyDataType;
};
