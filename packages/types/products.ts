export type ProductsType = {
  id?: string;
  name: string;
  code: string;
  icon: string;
  link: string;
  description?: string;
  visible?: boolean;
  promotion?: string;
  tags?: string;
  type?: string;
  group?: string;
};
