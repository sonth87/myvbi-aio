export type LeadersType = {
  id?: string;
  name: string;
  title: string;
  avatar: string;
  shortDescription: string;
  description?: string;
};
