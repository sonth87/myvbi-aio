export type UserType = {
  id?: string;
  email: string;
  name: string;
  phone?: string;
  address?: string;
  title?: string;
  department?: string;
  avatar?: string;
  status?: boolean;
  permission?: string[];
  deleted?: boolean;
  createdAt?: any;
  updatedAt?: any;
};
export type UserTypeAC = {
  email: string;
  hash: string;
  loai: string;
  name: string;
  phong: string;
  tai_khoan: string;
};

export type LoginType = {
  mcr: boolean;
  form: boolean;
};
