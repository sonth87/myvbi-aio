export const PERMISSION = {
  // NEWS
  NEWS_LIST: "NEWS_LIST",
  NEWS_CREATE: "NEWS_CREATE",
  NEWS_VIEW: "NEWS_VIEW",
  NEWS_UPDATE: "NEWS_UPDATE",
  NEWS_DELETE: "NEWS_DELETE",
  //CATEGORY
  CATEGORY_LIST: "CATEGORY_LIST",
  CATEGORY_CREATE: "CATEGORY_CREATE",
  CATEGORY_VIEW: "CATEGORY_VIEW",
  CATEGORY_UPDATE: "CATEGORY_UPDATE",
  CATEGORY_DELETE: "CATEGORY_DELETE",
  // COMMON CONFIG
  GENERAL_CONFIG: "GENERAL_CONFIG",
  GENERAL_CONFIG_UPDATE: "GENERAL_CONFIG_UPDATE",
  // LEADER
  LEADER_LIST: "LEADER_LIST",
  LEADER_VIEW: "LEADER_VIEW",
  LEADER_CREATE: "LEADER_CREATE",
  LEADER_UPDATE: "LEADER_UPDATE",
  LEADER_DELETE: "LEADER_DELETE",
  // PAGE CONFIG
  PAGE_CONFIG_LIST: "PAGE_CONFIG_LIST",
  PAGE_CONFIG_VIEW: "PAGE_CONFIG_VIEW",
  PAGE_CONFIG_UPDATE: "PAGE_CONFIG_UPDATE",
  // PRODUCT
  PRODUCT_LIST: "PRODUCT_LIST",
  PRODUCT_VIEW: "PRODUCT_VIEW",
  PRODUCT_CREATE: "PRODUCT_CREATE",
  PRODUCT_UPDATE: "PRODUCT_UPDATE",
  PRODUCT_DELETE: "PRODUCT_DELETE",
  // UPLOAD
  UPLOAD: "UPLOAD",
  // USER
  USER_LIST: "USER_LIST",
  USER_VIEW: "USER_VIEW",
  USER_CREATE: "USER_CREATE",
  USER_UPDATE: "USER_UPDATE",
  USER_DELETE: "USER_DELETE",
};

// all permission 4 loged in user
export const exceptPermission = [PERMISSION.UPLOAD, PERMISSION.USER_VIEW];

export const exceptUser = [
  "sonth1@myvbi.vn",
  "tuvm@myvbi.vn",
  "ngudq@myvbi.vn",
];

export const permissionGroup = {
  news: {
    view: [PERMISSION.NEWS_LIST],
  },
};
