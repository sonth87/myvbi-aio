module.exports = {
  apps: [
    {
      namespace: "cms-project",
      name: "cms-be",
      script: "export PORT=2424 && npm run start",
      cwd: "./apps/backend",
      env: {
        NODE_ENV: "development",
        BE_PORT: 2424,
        DB_CMS_URI:
          "mongodb://CMS:wkawQgh17us@172.23.4.29:27017/CMS?authMechanism=DEFAULT&authSource=CMS",
        OPENAPI: "https://apinodets.evbi.vn/",
        OPENAPI_KEY: "xIUNF6WjpHdbyWu3Uek10OCb8uhIn0wV",

        REACT_APP_BE_BASEURL:"https://dev-api-cms.evbi.vn/",
        REACT_APP_FE_BASEURL: "https://dev-cms.evbi.vn/",
        REACT_APP_FILE_SERVER:"https://apinodets.evbi.vn/f/",
      },
      env_production: {
        NODE_ENV: "production",
        BE_PORT: 2424,
        DB_CMS_URI:
          "mongodb://CMS:wkawQgh17us@172.23.4.29:27017/CMS?authMechanism=DEFAULT&authSource=CMS",
        OPENAPI: "https://apinodets.evbi.vn/",
        OPENAPI_KEY: "xIUNF6WjpHdbyWu3Uek10OCb8uhIn0wV",

        REACT_APP_BE_BASEURL:"https://dev-api-cms.evbi.vn/",
        REACT_APP_FE_BASEURL: "https://dev-cms.evbi.vn/",
        REACT_APP_FILE_SERVER:"https://apinodets.evbi.vn/f/",
      },
    },
    {
      namespace: "cms-project",
      name: "cms-fe",
      script: "export PORT=2425 && npm run start",
      cwd: "./apps/cms",
      env: {
        PM2_SERVE_PATH: './apps/cms',
        PM2_SERVE_PORT: 2425,
        PM2_SERVE_SPA: 'true',
        // PM2_SERVE_HOMEPAGE: './index.html',

        NODE_ENV: "production",
        FE_PORT: 2425,
        BE_PORT: 2424,
        REACT_APP_BE_BASEURL:"https://dev-api-cms.evbi.vn/",
        REACT_APP_FE_BASEURL: "https://dev-cms.evbi.vn/",
        REACT_APP_FILE_SERVER: "https://apinodets.evbi.vn/f/",
      },
      env_production: {
        PM2_SERVE_PATH: './apps/cms/build',
        PM2_SERVE_PORT: 2425,
        PM2_SERVE_SPA: 'true',
        PM2_SERVE_HOMEPAGE: './apps/cms/build/index.html',

        NODE_ENV: "production",
        FE_PORT: 2425,
        REACT_APP_BE_BASEURL:"https://dev-api-cms.evbi.vn/",
        REACT_APP_FE_BASEURL: "https://dev-cms.evbi.vn/",
        REACT_APP_FILE_SERVER: "https://apinodets.evbi.vn/f/",
      },
    },
    {
      namespace: "ac-project",
      name: "anchi-fe",
      script: "export PORT=2526 && npm run start",
      cwd: "./apps/QuanLyAnChi",
      env: {
        PM2_SERVE_PATH: './apps/QuanLyAnChi',
        PM2_SERVE_PORT: 2526,
        PM2_SERVE_SPA: 'true',
        // PM2_SERVE_HOMEPAGE: './index.html',
        NODE_ENV: "development",
        FE_PORT: 2526,
        BE_PORT: 2527,
        REACT_APP_BE_AN_CHI_BASEURL:"http://10.0.2.2:2527/",
        REACT_APP_FE_BASEURL: "https://dev-qlac.evbi.vn/",
        REACT_APP_FILE_SERVER: "https://apinodets.evbi.vn/f/",
        REACT_APP_URL_IMG: "https://m.evbi.vn"
      },
    },
    {
      namespace: "ac-project",
      name: "anchi-be",
      script: "export PORT=2527 && npm run start",
      cwd: "./apps/backend_AC",
      env: {
        NODE_ENV: "development",
        BE_PORT: 2527,
        OPENAPI: "https://apinodets.evbi.vn/",
        OPENAPI_KEY: "xIUNF6WjpHdbyWu3Uek10OCb8uhIn0wV",
        OPENAPI_KEY_AC: "aaoe4r7YnJ4bDw0H0m43NB6g75JcTpP0",
        REACT_APP_BE_AN_CHI_BASEURL:"http://10.0.2.2:2527/",
        REACT_APP_FE_BASEURL: "https://dev-qlac.evbi.vn/",
        REACT_APP_FILE_SERVER:"https://apinodets.evbi.vn/f/",
        REACT_APP_URL_IMG: "https://m.evbi.vn"
      }
    }
  ],
};
